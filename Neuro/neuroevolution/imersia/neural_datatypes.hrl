% ----------------------------------------------------------------------------------------------------
% Copyright Imersia Ltd 2018
% Author: Dr. Roy C. Davies
% Description: Data types for use in the database and API
% Reference: The Handbook of Neuroevolution Through Erlang
% ----------------------------------------------------------------------------------------------------

-type uuid() :: binary().

-record(sensor, {
    id = null :: uuid(),
    cx_id = null :: uuid(),
    name = null :: binary(),
    value = 0 :: integer(),
    fanout_ids  = [] :: list(#uuid{})
}).

-record(actuator, {
    id = null :: uuid(),
    cx_id = null :: uuid(),
    name = null :: binary(),
    value = 0 :: integer(),
    fanin_ids  = [] :: list(#uuid{})
}).

-record(neuron, {
    id = null :: uuid(),
    cx_id = null :: uuid(),
    af,
    input_idps = [] :: list(#uuid{}),
    output_ids = [] :: list(#uuid{})
}).

-record(cortex, {
    id = null :: uuid(),
    sensor_ids = [] :: list(#uuid{}),
    actuator_ids = [] :: list(#uuid{}),
    nids = [] :: list(#uuid{})
}).
