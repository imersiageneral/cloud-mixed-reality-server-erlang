% ----------------------------------------------------------------------------------------------------
% Copyright Imersia Ltd 2018
% Author: Dr. Roy C. Davies
% Description: Data types for use in the database and API
% Reference: The Handbook of Neuroevolution Through Erlang
% ----------------------------------------------------------------------------------------------------

-module (nerual_constructor).

-include("neural_datatypes.hrl").



% ----------------------------------------------------------------------------------------------------
% Generate a unique ID
% ----------------------------------------------------------------------------------------------------
generate_id() ->
    {UUID_binary, _} = uuid:get_v1(uuid:new(self())),
    list_to_binary(uuid:uuid_to_string(UUID_binary)).
% ----------------------------------------------------------------------------------------------------



% ----------------------------------------------------------------------------------------------------
% The construct_genotype function accepts the name of the file to which we’ll save the geno-
% type, sensor name, actuator name, and the hidden layer density parameters. We have to generate
% unique Ids for every sensor and actuator. The sensor and actuator names are used as input to the
% create_Sensor and create_Actuator functions, which in turn generate the actual Sensor and Ac-
% tuator representing tuples. We create unique Ids for sensors and actuators so that when in the
% future a NN uses 2 or more sensors or actuators of the same type, we will be able to differenti-
% ate between them using their Ids. After the Sensor and Actuator tuples are generated, we extract
% the NN’s input and output vector lengths from the sensor and actuator used by the system. The
% Input_VL is then used to specify how many weights the neurons in the input layer will need,
% and the Output_VL specifies how many neurons are in the output layer of the NN. After ap-
% pending the HiddenLayerDensites to the now known number of neurons in the last layer to gen-
% erate the full LayerDensities list, we use the create_NeuroLayers function to generate the Neu-
% ron representing tuples. We then update the Sensor and Actuator records with proper fanin and
% fanout ids from the freshly created Neuron tuples, compose the Cortex, and write the genotype
% to file.
% ----------------------------------------------------------------------------------------------------

construct_genotype(SensorName, ActuatorName, HiddenLayerDensities)->
    construct_genotype(ffnn, SensorName, ActuatorName, HiddenLayerDensities).

construct_genotype(FileName, SensorName, ActuatorName, HiddenLayerDensities)->
    Sensor = create_sensor(SensorName),
    Actuator = create_actuator(ActuatorName),

    OutputValue = Actuator#actuator.value,

    LayerDensities = lists:append(HiddenLayerDensities, [OutputValue]),

    CortexID = {cortex, generate_id()},
    Neurons = create_neurolayers(CortexID, Sensor, Actuator, LayerDensities),

    [InputLayer | _] = Neurons,
    [OutputLayer | _] = lists:reverse(Neurons),

    FirstlayerNeuronIDs = [N#neuron.id || N <- InputLayer],
    LastlayerNeuronIDs = [N#neuron.id || N <- OutputLayer],
    NeuronIDs = [N#neuron.id || N <- lists:flatten(Neurons)],

    GenotypeSensor = Sensor#sensor{cx_id = CortexID, fanout_ids = FirstlayerNeuronIDs},
    GenotypeActuator = Actuator#actuator{cx_id = CortexID, fanin_ids = LastlayerNeuronIDs},
    Cortex = create_cortex(CortexID, [Sensor#sensor.id], [Actuator#actuator.id], NeuronIDs),
    Genotype = lists:flatten([Cortex, GenotypeSensor, GenotypeActuator|Neurons]),

    {ok, File} = file:open(FileName, write),
    lists:foreach(fun(X) -> io:format(File, “~p.~n”,[X]) end, Genotype),
    file:close(File).
% ----------------------------------------------------------------------------------------------------



% ----------------------------------------------------------------------------------------------------
% Every sensor and actuator uses some kind of function associated with it, a function that either
% polls the environment for sensory signals (in the case of a sensor) or acts upon the environment
% (in the case of an actuator). It is the function that we need to define and program before it is
% used, and the name of the function is the same as the name of the sensor or actuator itself. For
% example, the create_Sensor/1 has specified only the rng sensor, because that is the only sensor
% function we’ve finished developing. The rng function has its own vl specification, which will
% determine the number of weights that a neuron will need to allocate if it is to accept this sen-
% sor’s output vector. The same principles apply to the create_Actuator function. Both, cre-
% ate_Sensor and create_Actuator function, given the name of the sensor or actuator, will return a
% record with all the specifications of that element, each with its own unique Id.
% ----------------------------------------------------------------------------------------------------
create_sensor(SensorName) ->
    case SensorName of
        rng -> #sensor{id = {sensor, generate_id()}, name = rng, value = 2};
        _ -> exit(“System does not yet support a sensor by the name:~p.”, [SensorName])
    end.

create_actuator(ActuatorName) ->
    case ActuatorName of
        pts -> #actuator{id = {actuator, generate_id()}, name = pts, value = 1};
        _ -> exit(“System does not yet support an actuator by the name:~p.”, [ActuatorName])
    end.
% ----------------------------------------------------------------------------------------------------



% ----------------------------------------------------------------------------------------------------
% ----------------------------------------------------------------------------------------------------
create_neurolayers(CortexID, Sensor, Actuator, LayerDensities) ->
    InputIDPs = [{Sensor#sensor.id, Sensor#sensor.value}],
    TotalLayers = length(LayerDensities),
    [FL_Neurons | MoreLayerDensities] = LayerDensities,
    NeuronIDs = [{neuron, {1, NeuronID}} || NeuronID <- generate_ids(FL_Neurons , [])],
    create_NeuroLayers(CortexID, Actuator#actuator.id, 1, TotalLayers, InputIDPs, NeuronIDs, MoreLayerDensities, []).
% ----------------------------------------------------------------------------------------------------
