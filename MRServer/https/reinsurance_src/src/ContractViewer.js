// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draws a Geobot card given the details
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Segment, Header, Icon, Button, Card } from 'semantic-ui-react'
import Contract from './Contract.js';
import Login from "./Login.js";
import cookie from 'react-cookies'

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class ContractViewer extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			loggingin: false,
			ready: false,
			error: false
		};

		this.details = {
			channel: {},
			geobot: {},
			automations: []
		}
	}
	// ----------------------------------------------------------------------------------------------------


	// ----------------------------------------------------------------------------------------------------
    // What to start with when the page loads
    // ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
    {
		if (this.props.geobotid)
		{
			// Get the Geobot
			this.getGeobot();

			// Try logging in if allowed to
			let loadfromcookie = (cookie.load('rememberme') === "checked");
			if (loadfromcookie)
			{
				let useremail = cookie.load('useremail');
				let password = cookie.load('password');
				this.props.imersiasdk.LOGIN(useremail, password)
				.then ((resp) => {
					this.setLoggedIn(true);
				});
			}
		}
    }
    componentWillUnmount = () =>
    {
    }
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	getGeobot = () =>
	{
		this.props.imersiasdk.GET("geobots", {geobotid: this.props.geobotid})
		.then((resp) => {
			this.details.geobot = resp.json;
			return (this.props.imersiasdk.GET("geobots/automations", {geobotid: this.props.geobotid}));
		})
		.then ((resp) => {
			this.details.automations = resp.json;
			return (this.props.imersiasdk.GET("channels", {channelid:this.details.geobot.channelid}));
		})
		.then ((resp) => {
			this.details.channel = resp.json;
			this.setState({ ready: true, error: false });
		})
		.catch ((err) => {
			this.details = {
				channel: {},
				geobot: {},
				automations: []
			};
			this.setState({ ready: false, error: true});
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	doLogin = () =>
	{
		this.setState({loggingin:true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
    // Set the result of logging in
    // ----------------------------------------------------------------------------------------------------
    setLoggedIn = (loggedIn) =>
    {
		if (loggedIn)
		{
			this.setState({loggingin:false});
			this.getGeobot();
		}
    }
    // ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		if (this.state.loggingin)
		{
			return (
				<Login
					imersiasdk={this.props.imersiasdk}
					loginMessage="Please log in."
					setLoggedIn={this.setLoggedIn}
					loginIfRemembered={true}
				/>
			)
		}
		else if (this.state.error)
		{
			return (
				<Segment placeholder>
					<Header icon>
						<Icon name='close' />
						Currently unavailable - may be hidden or out of range.
						<Button basic fluid onClick={this.doLogin}>Login</Button>
					</Header>
				</Segment>
			)
		}
		else if (this.state.ready)
		{
			return (
				<Card.Group centered itemsPerRow={3} stackable doubling>
					<Contract
						imersiasdk={this.props.imersiasdk}
						currentchannel={this.details.channel}
						geobotdetails={this.details.geobot}
						automations={this.details.automations}
						doLogin={this.doLogin}
					/>
				</Card.Group>
			)
		}
		else
		{
			return (
				<Segment placeholder>
					<Header icon>
						<Icon name='sync' />
						Loading ...
					</Header>
				</Segment>
			);
		}
	}
};
// --------------------------------------------------------------------------------------------------------

export default ContractViewer;
