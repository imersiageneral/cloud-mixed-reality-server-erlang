// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draws a Geobot card given the details
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { } from 'semantic-ui-react';

import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import geohash from 'geohash';

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class HeatMap extends Component {
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	constructor(props) {
		super (props);

		this.mapContainer = React.createRef();
		this.mapcanvas = null;
		this.isCursorOverPoint = false;
		this.isDragging = false;
		this.grabbedMarker = null;

		this.heatmapjson =
        {
            type: 'FeatureCollection',
            features: []
        };

		mapboxgl.accessToken = this.props.imersiasdk.maptoken;

		this.state = {
			analytics: {},
			showinggeobot: false,
			geobotidtoshow: "",
			eventstoshow: [],
			loading: true
		};
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		if (this.props.entityid)
		{
			let headers = this.props.entityid;
			if (this.props.startdate) headers.startdate = this.props.startdate;
			if (this.props.enddate) headers.enddate = this.props.enddate;

			// Get Analytics from geobot
			if (this.props.imersiasdk.isadmin)
			{
				this.props.imersiasdk.GET("admin", {"command":"analytics", "parameters":JSON.stringify(this.props.entityid)})
				.then ((resp) => {
					this.setState({analytics: resp.json, loading:false});
					if (this.props.imersiasdk.maptoken) this.drawMap();
				})
				.catch ((err) => {
					// Do nothing
				});
			}
			else {
				this.props.imersiasdk.GET("analytics", headers)
				.then ((resp) => {
					this.setState({analytics: resp.json, loading:false});
					if (this.props.imersiasdk.maptoken) this.drawMap();
				})
				.catch ((err) => {
					// Do nothing
				});
			}
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentWillUnmount = () =>
	{
		if (this.map) this.map.remove();
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Extract the unique event names from the analytics data
	// ----------------------------------------------------------------------------------------------------
	eventNames = () =>
	{
		let eventnames = [];
		if (this.state.analytics != null)
		{
			// Collate unique events
			this.state.analytics.forEach( (entry) => {
				if (eventnames.indexOf(entry.event) === -1)
				{
					eventnames.push(entry.event);
				}
			});
		}
		return (eventnames);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	drawMap = () => {
		this.map = new mapboxgl.Map({
		   container: this.mapContainer.current,
		   style: 'mapbox://styles/mapbox/outdoors-v10',
		   trackResize: true,
		   center: this.props.imersiasdk.mapstore.center,
		   zoom: this.props.imersiasdk.mapstore.zoom
		});

		let colours = ["#6f0028", "rgba(159,54,53,0.8)", "rgba(201,100,62,0.8)", "rgba(229,144,73,0.8)", "rgba(241,183,84,0.8)", "rgba(231,211,99,0.8)", "rgba(214,222,216,0.8)"];

		this.setState({eventstoshow: this.eventNames ()});

		this.mapcanvas = this.map.getCanvasContainer();

		this.waitUntilMapReady( () =>
		{
			this.setMapFeatures();

			this.map.addSource('analytics', {
				"type": "geojson",
				"data": this.heatmapjson
			});

			this.map.addLayer(
			{
				"id":"analytics-heat",
				"type": "heatmap",
				"source": "analytics",
				"paint": {

					// Color ramp for heatmap.  Domain is 0 (low) to 1 (high).
					// Begin color ramp at 0-stop with a 0-transparancy color
					// to create a blur-like effect.
					"heatmap-color": [
						"interpolate",
						["linear"],
						["heatmap-density"],
						0, "rgba(255,255,255,0)",
						0.1, colours[6],
						0.2, colours[5],
						0.3, colours[4],
						0.4, colours[4],
						0.6, colours[3],
						0.8, colours[3],
						1, colours[2]
					]
				}
			});

			this.zoomToExtents ();

			this.map.on('zoomend', () => {
				this.props.imersiasdk.mapstore = {
					center: this.map.getCenter(),
					zoom: this.map.getZoom()
				}
			})
		});
	}
	/* ,
	// Increase the heatmap weight based on frequency and property magnitude
	"heatmap-weight": [
		"interpolate",
		["linear"],
		["get", "mag"],
		0, 0,
		6, 1
	],
	// Increase the heatmap color weight weight by zoom level
	// heatmap-intensity is a multiplier on top of heatmap-weight
	"heatmap-intensity": [
		"interpolate",
		["linear"],
		["zoom"],
		0, 1,
		9, 3
	],
	// Adjust the heatmap radius by zoom level
	"heatmap-radius": [
		"interpolate",
		["linear"],
		["zoom"],
		0, 2,
		5, 15
	]*/
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	mouseDownOnMap = (e) =>
	{
		if (this.isCursorOverPoint)
		{
			// Mouse events
			this.map.on('mousemove', this.onMove);
			this.map.once('mouseup', this.onUp);
		}
	}
	// ----------------------------------------------------------------------------------------------------
	onMove = (e) =>
	{
		this.isDragging = true;

		let coords = e.lngLat;

		// Set a UI indicator for dragging.
		this.mapcanvas.style.cursor = 'grabbing';

		this.markerjson.features[this.grabbedMarker.properties.index].geometry.coordinates = [coords.lng, coords.lat];
		let markersource = this.map.getSource('markers');
		if (markersource) markersource.setData(this.markerjson);
	}
	// ----------------------------------------------------------------------------------------------------
	onUp = (e) =>
	{
		if (!this.grabbedMarker) return;

		if (this.isDragging)
		{
			let coords = e.lngLat;
			let thisGeobotID = this.grabbedMarker.properties.geobotid;

			let newlocation = {
				latitude: coords.lat,
				longitude: coords.lng,
				altitude: this.grabbedMarker.properties.details.location.altitude
			};

			this.props.imersiasdk.PUT("geobots", {geobotid:thisGeobotID}, {location:newlocation})
			.then ((resp) => {

			})
			.catch ((err) => {

			});
		}
		else
		{
			this.doSomethingOnClick (this.grabbedMarker.properties.geobotid);
		}

		this.mapcanvas.style.cursor = '';
		this.isDragging = false;
		this.isCursorOverPoint = true;
		this.map.off('mousemove', this.onMove);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	doSomethingOnClick = (geobotid) => {
		// check geobot class
		let geobot_class = this.markers[geobotid].class;

		let headers = {
			geobotid: geobotid,
			key: geobot_class
		};

		// check the metadata from the geobot class
		this.props.imersiasdk.GET("metadata", headers)
		.then ((resp) => {
			if (resp.json.value.hasOwnProperty("touch"))
			{
				if (resp.json.value.touch === "touch")
				{
					// Send touch event
				}
				else
				{
					// Open webapp
					this.setState({showinggeobot: true, geobotidtoshow: geobotid});
				}
			}
		})
		.catch ((err) => {
			// Metadata doesn't exist, so just open webapp
			this.setState({showinggeobot: true, geobotidtoshow: geobotid});
		});

		// if metadata class data has a touch, send it, otherwise do something else
		// let win = window.open("https://" + window.location.host + "/" + geobotid, "", "");
		// win.focus();
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Convert geobots list to geojson
	// ----------------------------------------------------------------------------------------------------
	setMapFeatures = () => {
		this.heatmapjson.features = [];
		this.state.analytics.forEach((analyticsitem) => {
			if (this.state.eventstoshow.indexOf(analyticsitem.event) !== -1)
			{
				let pointGeoJSON = this.convertPointToGeoJson(analyticsitem);
				for (let i=0; i < analyticsitem.tally; i++)
				{
					this.heatmapjson.features.push(pointGeoJSON);
				}
			}
		});
	}

	convertPointToGeoJson = (analyticsitem) => {
		let coords = geohash.GeoHash.decodeGeoHash(analyticsitem.geohash)
		return (
			{
				type: "Feature",
				geometry: {
					type: "Point",
					coordinates: [coords.longitude[1], coords.latitude[1]]
				},
				properties: {
					event: analyticsitem.event,
					tally: analyticsitem.tally
				}
			}
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	zoomToExtents = () => {
		if (this.heatmapjson.features.length > 0)
		{
			let bounds = new mapboxgl.LngLatBounds();

				this.heatmapjson.features.forEach((feature) => {
			    bounds.extend(feature.geometry.coordinates);
			});

			this.map.fitBounds(bounds, {
				padding: 100
			});
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// A callback that waits until the map is loaded and ready in order to begin adding things to it
	// ----------------------------------------------------------------------------------------------------
	waitUntilMapReady = (callback) =>
	{
		this.checkMapReady(10, callback);
	}
	checkMapReady = (counter, callback) =>
	{
		if (counter > 0)
		{
			if (this.map.isStyleLoaded())
			{
				callback(true);
			}
			else
			{
				setTimeout(() => {this.checkMapReady(counter-1, callback)}, 1000);
			}
		}
		else
		{
			callback(false);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	render() {
		if (this.props.imersiasdk.maptoken)
		{
			if (this.state.loading)
			{
				return (<>Loading... </>);
			}
			else
			{
				if (this.state.analytics && this.state.analytics.length > 0)
				{
					let height = (this.props.height) ? this.props.height + "vh" : "70vh";
					return (
						<div style={{ height:height, width:"100%"}} ref={this.mapContainer}></div>
					)
				}
				else
				{
					return (<>There are no analytics entries... </>);
				}
			}
		}
		else
		{
			return (<> There is no mapbox token ID set in the mrserver.toml file </>);
		}
	}
	// ----------------------------------------------------------------------------------------------------
}

export default HeatMap;
