// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draws a Geobot card given the details
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Card, Modal, Icon, Menu, Button, Grid } from 'semantic-ui-react'
import PatternEditor from './PatternEditor.js';
import Geobot from './patterns/Geobot.json';
import Contract from './patterns/Contract.json';
import Analytics from './Analytics.js';
import HeatMap from './HeatMap.js';
import ImageImersia from './ImageImersia.js';
import { DatesRangeInput } from 'semantic-ui-calendar-react';

import noimage from './images/noimage.png';

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class ContractCard extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			geobotdetails: JSON.parse(JSON.stringify(this.props.geobotdetails)),
			editedgeobotdetails: {},
			editing: false,
			deleting: false,
			analyticsexplorer_update:false,
			analyticsexplorer:false,
			makingbid:false,
			makingoffer:false,
			showheatmaps: false,
			error: false,
			error_message: "",
			datesRange: ''
		};
	}
	// ----------------------------------------------------------------------------------------------------


	// ----------------------------------------------------------------------------------------------------
	// Register and deregister the event listeners
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () => {
		document.addEventListener(this.props.geobotdetails.geobotid, this.processGeobotUpdate);
	}
	// ----------------------------------------------------------------------------------------------------
	componentWillUnmount = () => {
		document.removeEventListener(this.props.geobotdetails.geobotid, this.processGeobotUpdate);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Process the messages coming from the websocket
	// ----------------------------------------------------------------------------------------------------
	processGeobotUpdate = (message) =>
	{
		if (message.detail.wotcha.hasOwnProperty("geobot_set"))
		{
			this.setNewValues(message.detail.wotcha.geobot_set);
		}
	}

	setNewValues = (details) =>
	{
		let newDetails = this.state.geobotdetails;
		Object.keys(details).forEach((key) => {
			newDetails[key] = details[key];
		});
		this.setState({geobotdetails: newDetails});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Pass back up to the geobot list which geobot was clicked on
	// ----------------------------------------------------------------------------------------------------
	openGeobot = () =>
	{
		this.props.openGeobot(this.state.geobotdetails);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Extract the default settings from a pattern
	// ----------------------------------------------------------------------------------------------------
	getDefaults = (pattern) =>
	{
		let returnvalue = {};
		pattern.forEach((element) => {
			if (element.type === "object")
			{
				returnvalue[element.name] = this.getDefaults(pattern.object);
			}
			else {
				returnvalue[element.name] = (element.default ? element.default : "");
			}
		});
		return returnvalue;
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Edit main details of the Geobot
	// ----------------------------------------------------------------------------------------------------
	editModal = () =>
	{
		return (
			<Modal open={this.state.editing} centered={false} >
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Edit</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content scrolling>
					<PatternEditor
						imersiasdk={this.props.imersiasdk}
						pattern={Geobot["com.imersia.geobot"]}
						class={"com.imersia.geobot"}
						details={this.state.geobotdetails}
						changeDetails={this.changeDetails}
						uploadFiles = {this.uploadFiles}
					/>
		  		</Modal.Content>
				<Modal.Actions>
					<Button basic negative onClick={this.askDeleteGeobot}>
						<Icon name='cancel' /> Delete
					</Button>
					<Button basic color="blue" onClick={this.cancelEdit}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic positive onClick={this.saveEdit}>
						Save <Icon name='right chevron' />
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------
	changeDetails = (newdetails) =>
	{
		this.setState({editedgeobotdetails:JSON.parse(JSON.stringify(newdetails))});
	}
	// ----------------------------------------------------------------------------------------------------
	cancelEdit = () => {
		this.setState({editing:false});
	}
	// ----------------------------------------------------------------------------------------------------
	saveEdit = () => {
		if (this.state.editedgeobotdetails !== {})
		{
			this.props.imersiasdk.PUT("geobots", {geobotid:this.state.editedgeobotdetails.geobotid, contextids: JSON.stringify(this.state.geobotdetails.contextids)}, this.state.editedgeobotdetails)
			.then ((resp) => {
				this.saveMetadata(this.state.editedgeobotdetails.metadata);
				this.setState({geobotdetails:this.state.editedgeobotdetails, editing:false});
			})
			.catch ((resp) => {
				this.setState({error:true, error_message:"An error occurred saving the Geobot."});
			});
		}
	}
	// ----------------------------------------------------------------------------------------------------
	askDeleteGeobot = () => {
		this.setState({deleting:true});
	}
	// ----------------------------------------------------------------------------------------------------
	uploadFiles = (files) => {
		return (
			this.props.imersiasdk.POSTFILES({"geobotid":this.state.geobotdetails.geobotid, contextids: JSON.stringify(this.state.geobotdetails.contextids)}, files)
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	saveMetadata = (newmetadata) =>
	{
		newmetadata.forEach((metadata) => {
			if (metadata.deleted)
			{
				this.props.imersiasdk.DELETE("metadata", {geobotid:this.state.geobotdetails.geobotid, contextids: JSON.stringify(this.state.geobotdetails.contextids), metadataid:metadata.metadataid});
			}
			else if (metadata.new)
			{
				this.props.imersiasdk.POST("metadata", {geobotid:this.state.geobotdetails.geobotid, contextids: JSON.stringify(this.state.geobotdetails.contextids)}, metadata);
			}
			else
			{
				this.props.imersiasdk.PUT("metadata", {geobotid:this.state.geobotdetails.geobotid, contextids: JSON.stringify(this.state.geobotdetails.contextids), metadataid:metadata.metadataid}, metadata);
			}
		})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Delete the Geobot
	// ----------------------------------------------------------------------------------------------------
	askDeleteModal = () =>
	{
		return (
			<Modal size="tiny" open={this.state.deleting} centered={true}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Delete Geobot</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content>
					Delete Geobot? - there is no UNDO.
				</Modal.Content>
				<Modal.Actions>
					<Button basic color="blue" positive onClick={this.cancelDelete}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic negative onClick={this.deleteGeobot}>
						<Icon name='cancel' /> DELETE
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------
	deleteGeobot = () => {
        this.props.imersiasdk.DELETE("geobots", {geobotid:this.state.geobotdetails.geobotid})
        .then ((resp) => {
			this.props.closeGeobot();
        })
        .catch((err) => {
			this.setState({deleting:false});
        });
	}
	// ----------------------------------------------------------------------------------------------------
	cancelDelete = () => {
		this.setState({deleting:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Modal dialog for exploring the analytics for this Geobot
	// ----------------------------------------------------------------------------------------------------
	analyticsModal = () =>
	{
		if (this.state.analyticsexplorer_update)
		{
			this.setState({analyticsexplorer_update:false});
			return (<></>);
		}
		else {
			return (
				<Modal size="large" open={this.state.analyticsexplorer} centered={false} >
					<Modal.Header>
						Analytics
						<Button.Group floated='right'>
							<Button compact basic style={{padding:"0px"}}>
								<DatesRangeInput
									name="datesRange"
									floated="right"
									placeholder="Start date - End date"
									value={this.state.datesRange}
									iconPosition="left"
									onChange={this.handleDateChange}
								/>
							</Button>
							{ this.state.showheatmaps
							?<>
								<Button icon value={false} basic color="blue" onClick={this.setanalyticsmode }>
									<Icon name='line graph' />
								</Button>
								<Button icon value={true} color="blue" onClick={this.setanalyticsmode }>
									<Icon name='world' />
								</Button>
							</>:<>
								<Button icon value={false} color="blue" onClick={this.setanalyticsmode }>
									<Icon name='line graph' />
								</Button>
								<Button icon value={true} basic color="blue" onClick={this.setanalyticsmode }>
									<Icon name='world' />
								</Button>
							</>
							}
						</Button.Group>
					</Modal.Header>
					<Modal.Content style={{overflow:"hidden"}}>
						{ this.state.showheatmaps ? (
							<HeatMap
								imersiasdk={this.props.imersiasdk}
								entityid={{geobotid:this.state.geobotdetails.geobotid}}
								startdate={this.extractStartdate(this.state.datesRange)}
								enddate={this.extractEnddate(this.state.datesRange)}
							/>
						) : (
							<Analytics
								imersiasdk={this.props.imersiasdk}
								entityid={{geobotid:this.state.geobotdetails.geobotid}}
								startdate={this.extractStartdate(this.state.datesRange)}
								enddate={this.extractEnddate(this.state.datesRange)}
							/>
						)}
					</Modal.Content>
					<Modal.Actions>
						<Button basic positive onClick={this.closeAnalyticsModal}>
							<Icon name='check' /> Done
						</Button>
					</Modal.Actions>
				</Modal>
			)
		}
	}
	// ----------------------------------------------------------------------------------------------------
	closeAnalyticsModal = () =>
	{
		this.setState({analyticsexplorer:false});
	}
	setanalyticsmode = (e, data) =>
	{
		this.setState({showheatmaps:data.value});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// General Error modal dialog
	// ----------------------------------------------------------------------------------------------------
	errorModal = () =>
	{
		return (
			<Modal size="tiny" open={this.state.error} centered={true}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Error</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content>
					{this.state.error_message}
				</Modal.Content>
				<Modal.Actions>
					<Button basic positive onClick={this.errorOK}>
						OK
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------
	errorOK = () => {
		this.setState({error:false})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleDateChange = (event, {name, value}) => {
		if (this.state.hasOwnProperty(name)) {
			this.setState({ [name]: value });
			if ((value.length >= 16) || (value.length === 0)) this.setState({analyticsexplorer_update: true});
		}
    }
	extractStartdate = (datesRange) => {
		let elements = datesRange.split('-');
		if (elements.length >= 3)
		{
			return (elements[2].trim() + elements[1].trim() + elements[0].trim() + "");
		}
		else {
			return ""
		}
	}
	extractEnddate = (datesRange) => {
		let elements = datesRange.split('-');
		if (elements.length >= 6)
		{
			return (elements[5].trim() + elements[4].trim() + elements[3].trim() + "");
		}
		else {
			return ""
		}
	}
	// ----------------------------------------------------------------------------------------------------


	// ----------------------------------------------------------------------------------------------------
	// dialog box for mkaing a bid
	// ----------------------------------------------------------------------------------------------------
	bidModal = () =>
	{
		return (
			<Modal size="medium" open={this.state.makingbid} centered={true}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Bid : {this.state.geobotdetails.name}</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content>
					<PatternEditor
						imersiasdk={this.props.imersiasdk}
						pattern={Contract["bid"]}
						class={"bid"}
						details={{}}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button basic positive onClick={this.bidOK}>
						Make bid
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------
	bidOK = () => {
		this.setState({makingbid:false})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// dialog box for mkaing an offer
	// ----------------------------------------------------------------------------------------------------
	offerModal = () =>
	{
		return (
			<Modal size="medium" open={this.state.makingoffer} centered={true}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Offer : {this.state.geobotdetails.name}</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content>
					<PatternEditor
						imersiasdk={this.props.imersiasdk}
						pattern={Contract["offer"]}
						class={"offer"}
						details={{}}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button basic positive onClick={this.offerOK}>
						Make Offer
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------
	offerOK = () => {
		this.setState({makingoffer:false})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Handle the selection of the menu option
	// ----------------------------------------------------------------------------------------------------
	handleEditClick = () =>
	{
		this.setState({editing:true});
	}
	// ----------------------------------------------------------------------------------------------------
	handleAnalyticsClick = () =>
	{
		this.setState({analyticsexplorer:true});
	}
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleBidClick = () =>
	{
		this.setState({makingbid:true});
	}
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleOfferClick = () =>
	{
		this.setState({makingoffer:true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Choose which modal to open depending on the menu item selected
	// ----------------------------------------------------------------------------------------------------
	handleMenuChoice = (e, data) =>
	{
		switch (data.value)
		{
			case "edit":
			this.handleEditClick ();
			break;
			case "analytics":
			this.handleAnalyticsClick ();
			break;
			case "bid":
			this.handleBidClick ();
			break;
			case "offer":
			this.handleOfferClick ();
			break;
			default:
			break;
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		var imageurl = this.state.geobotdetails.imageurl;

		return (
            <Card>
				<Button
					basic compact
					onClick={this.openGeobot}>
					<ImageImersia src={imageurl} default={noimage} imersiasdk={this.props.imersiasdk} fluid/>
				</Button>
                <Card.Content textAlign="center">
                    <Card.Header>{this.state.geobotdetails.name}</Card.Header>
					<Card.Meta>{this.state.geobotdetails.class}</Card.Meta>
					<Card.Description>{this.state.geobotdetails.description}</Card.Description>
                </Card.Content>
				<Card.Content extra textAlign="center">
					<Button fluid basic onClick={this.handleMenuChoice} value="edit" >
						<Icon name='edit' /> Edit
					</Button>
				</Card.Content>
				<Card.Content extra textAlign="center">
					<Grid columns={2} divided>
						<Grid.Row>
							<Grid.Column>
								<Button fluid basic onClick={this.handleMenuChoice} value="bid" >
									<Icon name='pencil' /> Bid
								</Button>
							</Grid.Column>
							<Grid.Column>
								<Button fluid basic onClick={this.handleMenuChoice} value="offer" >
									<Icon name='pencil' /> Offer
								</Button>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Card.Content>
				{this.state.editing ?			this.editModal() 		: <></>}
				{this.state.deleting ?			this.askDeleteModal()	: <></>}
				{this.state.error ?				this.errorModal()		: <></>}
				{this.state.analyticsexplorer ?	this.analyticsModal()	: <></>}
				{this.state.makingbid ?			this.bidModal()			: <></>}
				{this.state.makingoffer ?		this.offerModal()		: <></>}
            </Card>
		)
	}
};
// --------------------------------------------------------------------------------------------------------

export default ContractCard;
