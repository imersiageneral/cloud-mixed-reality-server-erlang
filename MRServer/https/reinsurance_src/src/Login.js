// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// The Login dialog box.  Returns either an empty sessionid if there was an error,or the sessionid after
// logging in successfully.
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import Register from './Register.js';
import { Segment, Form, Checkbox, Divider, Grid, Button, Modal, Image, Header } from 'semantic-ui-react';
import cookie from 'react-cookies'

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class Login extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		let loadfromcookie = cookie.load('rememberme') === "checked";

		this.state = {
			open : true,
			useremail : (loadfromcookie)?cookie.load('useremail'):"",
			password : (loadfromcookie)?cookie.load('password'):"",
			rememberme: (loadfromcookie)?cookie.load('rememberme'):"",
			loginMessage : this.props.loginMessage,
			registering: false
		};

		this.styles = {
			actionStyle : { paddingLeft: '1.5em', paddingRight: '1.5em' }
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		let loadfromcookie = cookie.load('rememberme') === "checked";

		if ((this.props.loginIfRemembered) && (loadfromcookie))
		{
			this.doLogin();
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Perform a login and either close the dialog or show a message, depending on the outcome.
	// ----------------------------------------------------------------------------------------------------
	doLogin = () => {
		if (this.state.rememberme === "checked")
		{
			cookie.save("useremail", this.state.useremail, {maxAge:31536000}); // 1 year
			cookie.save("password", this.state.password, {maxAge:31536000});
			cookie.save("rememberme", this.state.rememberme, {maxAge:31536000});
		}
		else
		{
			cookie.remove("useremail");
			cookie.remove("password");
			cookie.remove("rememberme");
		}

		this.props.imersiasdk.LOGIN(this.state.useremail, this.state.password)
		.then ((resp) => {
			this.props.setLoggedIn(true);
		}).catch ((err) => {
			this.setState({loginMessage:"Error logging in"});
			this.props.setLoggedIn(false);
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	doRegister = () => {
		this.setState({registering:true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	doneRegistering = () => {
		this.setState({registering:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Set the local variables when the input fields are changed, ready for when the user presses Login
	// ----------------------------------------------------------------------------------------------------
	handleChange = (e, { name, value, checked }) => {
		if (name === "rememberme")
		{
			this.setState({rememberme:(checked)?"checked":"notchecked"});
			if (!checked)
			{
				this.setState({useremail:"", password:""});
				cookie.remove("useremail");
				cookie.remove("password");
				cookie.remove("rememberme");
			}
		}
		else
		{
			this.setState({ [name]: value })
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Check for the enter key
	// ----------------------------------------------------------------------------------------------------
	handleKeyPress = (e) => { if (e.charCode === 13) { this.doLogin(e); } }
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		if (this.state.registering)
		{
			return (
				<Register message="Please register" doneRegistering={this.doneRegistering} imersiasdk={this.props.imersiasdk}/>				
			)
		}
		else
		{
			return (
				<Modal open={this.state.open} size='tiny' >
					<Modal.Content>
						<Image fluid size='medium' src='/images/logo.png' centered />
						<Header as='h4' textAlign='center'>
							<span>{this.state.loginMessage}</span>
						</Header>
						<Divider />
						<Grid columns='one'>
							<Grid.Row>
								<Grid.Column>
									<Form>
										<Form.Field>
											<Form.Input placeholder="Email Address" name="useremail" type="text" value={this.state.useremail} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
										</Form.Field>
										<Form.Field>
											<Form.Input placeholder="Password" name="password" type="password" value={this.state.password} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
										</Form.Field>
										<Form.Field>
											<Checkbox label='Remember me' name="rememberme" defaultChecked={this.state.rememberme === "checked"} onChange={this.handleChange}/>
										</Form.Field>
										<Segment secondary mini textAlign="justify">
											We use browser cookies to remember your settings for this site.  If you don't wish us to do that, do NOT click 'remember me'.
										</Segment>
									</Form>
								</Grid.Column>
							</Grid.Row>
						</Grid>
					</Modal.Content>
					<Modal.Actions style={this.styles.actionStyle}>
						<Grid columns='two'>
							<Grid.Row>
								<Grid.Column>
									<Button fluid basic color='blue' onClick={this.doRegister}>Register</Button>
								</Grid.Column>
								<Grid.Column>
									<Button fluid basic color='green' onClick={this.doLogin}>Log in</Button>
								</Grid.Column>
							</Grid.Row>
						</Grid>
					</Modal.Actions>
				</Modal>
			)
		}
	}
};
// --------------------------------------------------------------------------------------------------------

export default Login;
