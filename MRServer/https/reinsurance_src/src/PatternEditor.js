// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// An editor for JSON settings
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Form, Table, Item, Button, Modal, Menu, Icon } from 'semantic-ui-react';
import Classes from './patterns/Classes.json';
import ImageExtensions from './patterns/ImageExtensions.json';
import Dropzone from 'react-dropzone'
import ImageImersia from './ImageImersia.js';

import noimage from './images/noimage.png';

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class PatternEditor extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// props:
	// 		pattern			pattern to build the editor from
	//		details			initial values to fill the editor with (pointer)
	// 		changeDetails	function to send changes to
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.styles = {
			dropzone: {
				display:'flex',
				'justify-content': 'center',
				'align-items': 'center',
				height:"100px",
				'border-style': 'dashed',
				'border-color': 'grey',
				'border-width': "1px",
				'border-radius': "5px"
			}
		}

		this.state = {
			ready: false,
			details: {}
		};
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		this.addNewMetadata();
		// Set the state details as a link to the details pointed to in the props
		this.setState({ready:true, details: this.props.details});

		// Make sure the calling section has something to save later.
		// this.props.changeDetails(this.props.details);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Go through the items in the pattern, and make sure the metadata referenced by any metadata-edit element
	// is actually present.
	// ----------------------------------------------------------------------------------------------------
	addNewMetadata = () =>
	{
		this.props.pattern.forEach((element) => {
			if (element.type === "metadata-edit")
			{
				this.addMetadataIfNew (element.name);
			}
		});
	}
	addMetadataIfNew = (name) =>
	{
		if (this.props.details.metadata)
		{
			let foundit = false;

			this.props.details.metadata.forEach((metadata) =>
			{
				if (metadata.key === name)
				{
					foundit = true;
				}
			});

			if (!foundit)
			{
				this.props.details.metadata.push ({
					key:name, value:{}, metadataid:"", new:true
				});
			}
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	convertPatternToHTML = () =>
	{
		let returnvalue = [];

		if (this.props.pattern.length === 0)
		{
			returnvalue.push(
				<Table.Row>
					<Table.Cell>Nothing to edit</Table.Cell>
				</Table.Row>
			)
		}
		else
		{
			this.props.pattern.forEach((element) => {
				if (!element.hide)
				{
					let value = this.getValue(element, this.state.details);

					switch (element.type)
					{
						case "text" :
							returnvalue.push ( this.textHTML(element, value) ); break;
						case "textarea":
							returnvalue.push ( this.textareaHTML(element, value) ); break;
						case "number" :
							returnvalue.push ( this.numberHTML(element, value) ); break;
						case "dropdown" :
							returnvalue.push ( this.dropdownHTML(element, value) ); break;
						case "checkbox" :
							returnvalue.push ( this.checkboxHTML(element, value) ); break;
						case "filename" :
							returnvalue.push ( this.filenameHTML(element, value) ); break;
						case "metadata-edit" :
							returnvalue.push ( this.metadataeditHTML(element, value) ); break;
						case "dropdown-edit" :
							returnvalue.push ( this.dropdowneditHTML(element, value) ); break;
						case "object" :
							returnvalue.push ( this.objectHTML(element, value) ); break;
						default:
						break;
					}
				}
			})
		}

		return returnvalue;
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	getValue = (element, details) =>
	{
		let value = "";

		if (details && details.hasOwnProperty(element.name))
		{
			value = details[element.name];
		}
		else
		{
			if (element.hasOwnProperty("default"))
			{
				value = element.default;
			}
			else
			{
				switch (element.type)
				{
					case "number":
						value = 0;
						break;
					case "object":
					case "metadata-edit":
						value = {};
						break;
					case "checkbox":
						value = false;
						break;
					default:
						value = "";
						break;
				}
			}
		}

		return value;
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	textHTML = (element, value) =>
	{
		return (
			<Table.Row>
				<Table.Cell width={4}>{element.text}</Table.Cell>
				<Table.Cell>
					<Form.Input type={element.type} name={element.name} value={value} placeholder={element.label} readOnly={element.locked} onChange={this.handleChange}/>
				</Table.Cell>
			</Table.Row>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	textareaHTML = (element, value) =>
	{
		return (
			<Table.Row>
				<Table.Cell width={4}>{element.text}</Table.Cell>
				<Table.Cell>
					<Form.TextArea type={element.type} name={element.name} value={value} placeholder={element.label} readOnly={element.locked} onChange={this.handleChange}/>
				</Table.Cell>
			</Table.Row>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	numberHTML = (element, value) =>
	{
		return (
			<Table.Row>
				<Table.Cell width={4}>{element.text}</Table.Cell>
				<Table.Cell>
					<Form.Input type={element.type} name={element.name} value={value} placeholder={element.label} readOnly={element.locked} onChange={this.handleChange}/>
				</Table.Cell>
			</Table.Row>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	dropdownHTML = (element, value) =>
	{
		return (
			<Table.Row>
				<Table.Cell width={4}>{element.text}</Table.Cell>
				<Table.Cell>
					<Form.Dropdown type={element.type} name={element.name} fluid value={value} selection options={element.class} placeholder={element.label} readOnly={element.locked} onChange={this.handleChange}/>
				</Table.Cell>
			</Table.Row>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	checkboxHTML = (element, value) =>
	{
		return (
			<Table.Row>
				<Table.Cell width={4}>{element.text}</Table.Cell>
				<Table.Cell>
					<Form.Checkbox type={element.type} name={element.name} checked={value} readOnly={element.locked} onChange={this.handleChange} label={element.label}/>
				</Table.Cell>
			</Table.Row>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	filenameHTML = (element, value) =>
	{
		if (this.props.uploadFiles)
		{
			return (
				<Table.Row>
					<Table.Cell width={4}>{element.text}</Table.Cell>
					<Table.Cell>
						<Item.Group divided>
							<Item>
								{this.imageOrIcon (this.state.details[element.name])}
								<Item.Content>
									{(element.locked)?(<div></div>):(
									<Dropzone name={element.name} onDrop={this.onDrop}>
									 {({ getRootProps, getInputProps }) => (
										<div style={this.styles.dropzone} {...getRootProps({ refKey: 'innerRef' })}>
										  <input {...getInputProps()} />
										  <p>{element.label} </p>
										</div>
									  )}
									  </Dropzone>)
									}
								</Item.Content>
							</Item>
						</Item.Group>
						<Form.Input type={element.type} name={element.name} value={value} readOnly={element.locked} onChange={this.handleChange}/>
					</Table.Cell>
				</Table.Row>
			)
		}
		else {
			return (<></>);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	metadataeditHTML = (element, value) =>
	{
		return(
			<Table.Row>
				<Table.Cell width={4}>{element.text}</Table.Cell>
				<Table.Cell>
					<PatternEditor
						imersiasdk={this.props.imersiasdk}
						pattern={Classes[this.props.class][element.name]}
						details={this.metadataValue(element.name)}
						class={this.props.class}
						type={element.type}
						name={element.name}
						value={element.value}
						changeDetails={this.handleChange}
						uploadFiles={this.props.uploadFiles}
					/>
				</Table.Cell>
			</Table.Row>
		)
	}

	metadataValue = (key) =>
	{
		this.addMetadataIfNew (key);

		let returnvalue = null;
		this.state.details.metadata.forEach((metadata) => {
			if (metadata.key === key)
			{
				returnvalue = metadata.value;
			}
		});
		return returnvalue;
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	dropdowneditHTML = (element, value) =>
	{
		let options = [];
		Object.keys(Classes[this.props.class]).forEach((key) => {
			options.push({text:key, value:key});
		});

		return (
			<Table.Row>
				<Table.Cell width={4}>{element.text}</Table.Cell>
				<Table.Cell>
					<Form.Dropdown name={element.name} fluid value={value} selection options={options} placeholder={element.label} onChange={this.handleChange}/>
					<PatternEditor
						imersiasdk={this.props.imersiasdk}
						pattern={Classes[this.props.class][value]}
						details={this.metadataValue(value)}
						class={this.props.class}
						type={element.type}
						name={element.name}
						value={element.value}
						changeDetails={this.handleChange}
						uploadFiles={this.props.uploadFiles}
					/>
				</Table.Cell>
			</Table.Row>
		);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	objectHTML = (element, value) =>
	{
		return(
			<Table.Row>
				<Table.Cell width={4}>{element.text}</Table.Cell>
				<Table.Cell>
					<PatternEditor
						imersiasdk={this.props.imersiasdk}
						pattern={element.object}
						details={this.state.details[element.name]}
						class={this.props.class}
						type={element.type}
						name={element.name}
						value={value}
						changeDetails={this.handleChange}
						uploadFiles={this.props.uploadFiles}
					/>
				</Table.Cell>
			</Table.Row>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	imageOrIcon = (filename) =>
	{
		if (!filename)
		{
			return (
				<></>
			)
		}
		else
		{
			let extension = filename.split('.').pop();
			if (ImageExtensions.indexOf(extension) > -1)
			{
				return (
					<ImageImersia item size={"small"} src={filename} default={noimage} imersiasdk={this.props.imersiasdk} />
				)
			}
			else {
				return (
					<></>
				)
			}
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Set the local variables when the input fields are changed
	// ----------------------------------------------------------------------------------------------------
	handleChange = (e, {name, type, value, checked}) => {
		let newDetails = this.state.details;
		if (checked === undefined)
		{
			if (type === "number")
			{
				let newValue = Number(value);
				if (isNaN(newValue))
				{
					newDetails[name] = 0;
				}
				else {
					newDetails[name] = newValue;
				}
			}
			else {
				newDetails[name] = value;
			}
		}
		else
		{
			newDetails[name] = checked;
		}
		this.setState({ details: newDetails });
		this.props.changeDetails(newDetails, this.props.name);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Upload files and set the parameter
	// TODO: Put this outside
	// ----------------------------------------------------------------------------------------------------
	onDrop = (acceptedFiles, rejectedFiles, e) => {
		// Do something with files
		this.props.uploadFiles(acceptedFiles)
		.then ((resp) => {
			let newDetails = this.state.details;
			newDetails[e.target.name] = resp.json.fileurls[0];
			this.setState({ details: newDetails });
			this.props.changeDetails(newDetails, this.props.name);
		})
		.catch ((err) => {
			// Do nothing
		});
    }
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		if (!this.state.ready)
		{
			return (<div></div>);
		}
		else {
			return (
				<Form>
					<Table celled fluid definition>
						<Table.Body>
							{this.convertPatternToHTML()}
						</Table.Body>
					</Table>
				</Form>
			);
		}
	}
};
// --------------------------------------------------------------------------------------------------------

export default PatternEditor;
