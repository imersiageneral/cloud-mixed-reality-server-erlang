import React, { Component } from 'react';
import { Image, Item } from 'semantic-ui-react'

class ImageImersia extends Component
{
	render ()
	{
		let imageurl = this.props.src ? this.props.src : "";
		if (imageurl !== "")
		{
			if (imageurl.indexOf("?") === -1)
			{
				imageurl = imageurl + "?"
			}
			else
			{
				imageurl = imageurl + "&"
			}
			imageurl = imageurl + "sessionid=" + this.props.imersiasdk.sessionid + "&random=" + Math.random();
		}

		if (this.props.item)
		{
			return (
				<Item.Image
					src={(imageurl === "") ? this.props.default : imageurl}
					wrapped={this.props.wrapped}
					fluid={this.props.fluid}
					size={this.props.size}
					centered={this.props.centered}
				/>
			)
		}
		else {
			return (
				<Image
					src={(imageurl === "") ? this.props.default : imageurl}
					wrapped={this.props.wrapped}
					fluid={this.props.fluid}
					size={this.props.size}
					centered={this.props.centered}
				/>
			)
		}
	}
}

export default ImageImersia;
