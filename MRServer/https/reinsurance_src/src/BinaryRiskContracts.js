// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// A simulation of Binary Risk Contracts
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Icon, Header, Button } from 'semantic-ui-react'

import ContractClasses from './ContractClasses.js';
import Contracts from './Contracts.js';
import Automations from './patterns/Automations.json'
import ContractClassTitle from './ContractClassTitle.js';
import TradingViewer2 from './TradingViewer2.js';
import ContractTitle from './ContractTitle.js';
import Map from './Map.js';


// --------------------------------------------------------------------------------------------------------
// The main class
// --------------------------------------------------------------------------------------------------------
class BinaryRiskContracts extends Component {
    // ----------------------------------------------------------------------------------------------------
    // Set up the object
    // props expected are sessionid and userid
    // ----------------------------------------------------------------------------------------------------
    constructor (props)
    {
        super (props);

        this.container = React.createRef();

        this.state = {
            currentstate: "none",
            showingmap: false,
            maplocation: "xbpbpbpbp",
            map_style: {
                    position:"absolute",
                    top:"120px",
                    bottom:"11px",
                    left:"11px",
                    right:"11px"
                }
        }

        this.data = {
            currentchannel: {},
            currentgeobot: {},
            currentgeobot_automations: [],
            currentgeobot_metadata: [],
            currentgeobot_files: [],
            channels: [],
            geobots: []
        };
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // What to start with when the page loads
    // ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
    {
        document.addEventListener("channel_new", this.processWotchaEvent);
        this.loadChannels();
    }
    componentWillUnmount = () =>
    {
        document.removeEventListener("channel_new", this.processWotchaEvent);
        this.closeChannels ();
    }
	// ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    closeChannels = () =>
    {
        this.data.channels.forEach((channel) => {
            // this.props.imersiasdk.CLOSE (channel.channelid);
            document.removeEventListener(channel.channelid, this.processWotchaEvent);
        });
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Load each channel's details and set up the wotcha events
    // ----------------------------------------------------------------------------------------------------
    loadChannels = () =>
    {
        this.props.imersiasdk.GEOJSON()
        .then ((resp) => {
            this.data.channels = [];
            let channelcounter = resp.json.channels.length;
            resp.json.channels.forEach((channel) => {
                if (channel.class === "com.telluriq.contracts")
                {
                    this.props.imersiasdk.GET("context", {channelid: channel.channelid})
                    .then ((resp) => {
                        channel.contextids = this.extractContextIDs(resp.json);

                        return (this.props.imersiasdk.GET("metadata", {channelid: channel.channelid}))
                    })
                    .then ((resp) => {
                        channel.metadata = resp.json;

                        this.data.channels.push(channel);
                        // Make sure our Companion is wotching these channels
                        this.props.imersiasdk.OPEN (channel.channelid);
                        // Listen for the relevant channel events
                        document.addEventListener(channel.channelid, this.processWotchaEvent);

                        if (--channelcounter <= 0) this.setState({currentstate:"reload_channels"});
                    })
                    .catch((err) => {
                        this.data.channels.push(channel);
                        // Make sure our Companion is wotching these channels
                        this.props.imersiasdk.OPEN (channel.channelid);
                        // Listen for the relevant channel events
                        document.addEventListener(channel.channelid, this.processWotchaEvent);

                        if (--channelcounter <= 0) this.setState({currentstate:"reload_channels"});
                    })
                }
                else {
                    if (--channelcounter <= 0) this.setState({currentstate:"reload_channels"});
                }
            });
        })
        .catch((err) => {
            // Do nothing
        });
    }

    extractContextIDs = (rawContextIDs) =>
    {
        let returnData = [];
        rawContextIDs.forEach((rawContextID) => {
            returnData.push(rawContextID.contextid);
        });
        return (returnData);
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    loadGeobots = () =>
    {
        this.props.imersiasdk.GEOJSON(this.data.currentchannel.name)
        .then ((resp) => {
            this.data.geobots = this.processGeobots(resp.json.features);
            return (this.setGeobotsContexts (this.data.geobots, this.data.currentchannel.contextids));
        })
        .then (() => {
            this.setState({currentstate: "reload_geobots"});
        })
        .catch((err) => {
            // Do nothing
        });
    }
    processGeobots = (geobots) =>
    {
        let processedGeobots = [];
        geobots.forEach ((geobot) => {
            let theGeobot = geobot.properties;
            processedGeobots.push(theGeobot);
        });
        return processedGeobots;
    }
    setGeobotsContexts = (geobots, channelcontextids) =>
    {
        return new Promise ( (resolve, reject) =>
		{
            let numGeobots = geobots.length;

            if (numGeobots === 0)
            {
                resolve();
            }
            else {
                geobots.forEach ((geobot) => {
                    this.props.imersiasdk.GET("context", {geobotid: geobot.geobotid})
                    .then((resp) => {
                        geobot.contextids = channelcontextids.concat(this.extractContextIDs(resp.json));
                        if (--numGeobots <= 0)
                        {
                            resolve();
                        }
                    })
                    .catch((err) => {
                        reject();
                    });
                });
            }
        });
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Note when showing a list of channels or geobots when a new one is created, or one is deleted
    // and go back a step to the channel when the geobot currently being viewed is deleted.
    // ----------------------------------------------------------------------------------------------------
    processWotchaEvent = (message) =>
    {
        switch (this.state.currentstate)
        {
            case "channels":
                if (message.detail.wotcha.hasOwnProperty("channel_delete"))
                {
                    this.closeChannels ();
                    this.loadChannels ();
                }
                else if (message.detail.wotcha.hasOwnProperty("channel_new"))
                {
                    this.closeChannels ();
                    this.loadChannels ();
                }
            break;
            case "geobots":
            case "geobot":
                if (message.detail.wotcha.hasOwnProperty("channel_delete"))
                {
                    // If the channel we are currently viewing has just been deleted
                    if (message.detail.wotcha.channel_delete.channelid === this.data.currentchannel.channelid)
                    {
                        this.closeChannels ();
                        this.loadChannels ();
                    }
                }
                else if (!this.state.showingmap)
                {
                    if (message.detail.wotcha.hasOwnProperty("geobot_delete"))
                    {
                        this.loadGeobots ();
                    }
                    else if (message.detail.wotcha.hasOwnProperty("geobot_new"))
                    {
                        this.loadGeobots ();
                    }
                    else if (message.detail.wotcha.hasOwnProperty("geobot_set"))
                    {
                        if (this.data.currentgeobot.geobotid === message.detail.wotcha.geobot_set.geobotid)
                        {
                            this.data.currentgeobot = message.detail.wotcha.geobot_set;
                        }
                    }
                }
            break;
            default:
            break;
        }
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // where to go back to with the back button
    // ----------------------------------------------------------------------------------------------------
    goback = () =>
    {
        switch (this.state.currentstate)
        {
            case "geobots":
                this.closeChannels ();
                this.loadChannels ();
            break;
            case "geobot":
                this.loadGeobots ();
            break;
            default:
            break;
        }
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Open a list of geobots from a list of channels
    // ----------------------------------------------------------------------------------------------------
    openChannel = (chosenchannel) =>
    {
        this.props.imersiasdk.GEOJSON(chosenchannel.name)
        .then ((resp) => {
            this.data.currentchannel = chosenchannel;
            this.data.geobots = this.processGeobots(resp.json.features);
            return (this.setGeobotsContexts (this.data.geobots, this.data.currentchannel.contextids));
        })
        .then (() => {
            document.addEventListener(this.data.currentchannel.channelid, this.processWotchaEvent);
            this.setState({currentstate: "geobots"});
        })
        .catch((err) => {
            // Do nothing
        });
    }
    closeChannel = () =>
    {
        this.closeChannels ();
        this.loadChannels ();
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Show a set of Geobots on a Channel
    // ----------------------------------------------------------------------------------------------------
    channelsContent = () =>
    {
        return (
            <>
                <Header style={{height:"50px"}}>
                    <span><Icon link large name='home' /></span>
                    <Button.Group floated='right'>
                        <Button basic color="green" onClick={this.newChannel} >
                            <Icon name='magic'/> new contract class
                        </Button>
                    </Button.Group>
                </Header>
                <ContractClasses
                    imersiasdk={this.props.imersiasdk}
                    channels={this.data.channels}
                    openChannel={this.openChannel}
                    closeChannel={this.closeChannel}
                />
            </>
        )
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Open a Geobot when clicked on in the GeobotsContent set
    // ----------------------------------------------------------------------------------------------------
    openGeobot = (chosengeobot) =>
    {
        this.props.imersiasdk.GET("geobots/automations", {geobotid:chosengeobot.geobotid})
        .then ((resp) => {
            this.data.currentgeobot = chosengeobot;
            this.data.currentgeobot_automations = resp.json;
            this.setState({currentstate: "geobot"});
        })
        .catch ((err) => {
            // Do nothing
        });
    }
    closeGeobot = () =>
    {
        this.loadGeobots ();
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    newChannel = () =>
    {
        let channelName = "Contract Class_" + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);

        this.props.imersiasdk.POST("channels", {}, {name:channelName, class:"com.telluriq.contracts"})
        .then ((resp) => {
            return this.props.imersiasdk.POST("context", {channelid: resp.json.channelid}, {attributes:["read", "add", "remove"]});
        })
        .then ((resp) => {
            // this.closeChannels ();
            // this.loadChannels();
        })
        .catch((err) => {
            // Do nothing
        });
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    newGeobot = () =>
    {
        let geobotName = "Contract_" + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        let headers = {
            channelid: this.data.currentchannel.channelid,
            contextids: this.data.currentchannel.contextids
        }
        if (this.state.showingmap)
        {
            headers.location = this.state.maplocation;
        }

        // Create a new Geobot
        this.props.imersiasdk.POST("geobots", headers, {name:geobotName, class:"com.telluriq.brc"})
        .then ((resp) => {
            let geobotheaders = {
                geobotid: resp.json.geobotid,
                contextids: this.data.currentchannel.contextids
            }
            // Set the context for reading and editing
            return this.props.imersiasdk.POST("context", geobotheaders, {attributes:["read", "edit"]}, geobotheaders);
        })
        .then ((resp) => {
            let geobotheaders = resp.passthrough;
            geobotheaders.contextids.push(resp.json.contextid);
            // Set the Automations on the Geobot
            return (this.loadAutomations(0, geobotheaders));
        })
        .then((resp) => {
            // Do nothing
            console.log(resp);
        })
        .catch((err) => {
            // Do nothing
            console.log(err);
        });
    }

    loadAutomations = (index, geobotheaders) =>
    {
        return new Promise ( (resolve, reject) =>
        {
            if (index < Automations.length)
            {
                let thisAutomation = Automations[index];
                this.props.imersiasdk.POST("geobots/automations", geobotheaders, thisAutomation.automation)
                .then((resp) => {
                    return(this.loadAutomations(index + 1, geobotheaders));
                })
                .catch((err) => {
                    reject("error");
                })
            }
            else {
                resolve("saved");
            }
        });
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    toggleMap = (e) =>
    {
        // Reload geobots if we're going back from map view in case there were some changes
        if (this.state.showingmap)
        {
            this.loadGeobots();
        }
        else {
            // Set up the map dimensions
            let rect = this.container.current.parentNode.getBoundingClientRect();
            this.setState({map_style: {
                    position:"absolute",
                    top:"120px",
                    right:"11px",
                    left:rect.left,
                    bottom:"11px"
                }
            });
        }
        // Change the state
        this.setState({showingmap:!this.state.showingmap});
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Store the map center from the Map object
    // ----------------------------------------------------------------------------------------------------
    mapcenter = (newmaplocation) =>
    {
        this.setState({maplocation:newmaplocation});
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Show a set of Geobots either in the list or on the map
    // ----------------------------------------------------------------------------------------------------
    geobotsOrMap = () =>
    {
        if (this.state.showingmap)
        {
            return (
                <div style={this.state.map_style}>
                    <Map
                        imersiasdk={this.props.imersiasdk}
                        channelid={this.data.currentchannel.channelid}
                        geobots={this.data.geobots}
                        mapcenter={this.mapcenter}
                    />
                </div>
            )
        }
        else
        {
            return (
                <Contracts
                    imersiasdk={this.props.imersiasdk}
                    geobots={this.data.geobots}
                    currentchannel={this.data.currentchannel}
                    openGeobot={this.openGeobot}
                    closeGeobot={this.closeGeobot}
                />
            )
        }
    }

    geobotsContent = () =>
    {
        return (
            <>
                <Header style={{height:"50px"}}>
                    <span><Icon link large name='arrow left' onClick={this.goback} /></span>
                    <ContractClassTitle
                        imersiasdk={this.props.imersiasdk}
                        channelid={this.data.currentchannel.channelid}
                    />
                    <Button.Group floated='right'>
                    {
                        this.state.showingmap
                        ?<>
                            <Button value={false} basic color="blue" onClick={this.toggleMap }>
                                <Icon name='picture' /> list
                            </Button>
                            <Button value={true} color="blue" onClick={this.toggleMap }>
                                <Icon name='map' /> map
                            </Button>
                        </>:<>
                            <Button value={false} color="blue" onClick={this.toggleMap }>
                                <Icon name='picture' /> list
                            </Button>
                            <Button value={true} basic color="blue" onClick={this.toggleMap }>
                                <Icon name='map' /> map
                            </Button>
                        </>
                    }
                    <Button basic color="green" floated="right" onClick={this.newGeobot} >
                        <Icon name='magic'/> new contract
                    </Button>
                    </Button.Group>
                </Header>
                {this.geobotsOrMap()}
            </>
        )
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Show a single Geobot
    // ----------------------------------------------------------------------------------------------------
    geobotContent = () =>
    {
        return (
            <>
                <Header style={{height:"50px"}}>
                    <span><Icon link large name='arrow left' onClick={this.goback} /></span>
                    <ContractClassTitle
                        imersiasdk={this.props.imersiasdk}
                        channelid={this.data.currentchannel.channelid}
                    />
                    <span> | </span>
                    <ContractTitle
                        imersiasdk={this.props.imersiasdk}
                        geobotid={this.data.currentgeobot.geobotid}
                    />
                </Header>
                <TradingViewer2
                    imersiasdk={this.props.imersiasdk}
                    geobotid={this.data.currentgeobot.geobotid}
                    height="80"
                />
            </>
        )
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Makes sure the content tab is rendering the correct output depending on the current state
    // ----------------------------------------------------------------------------------------------------
    showCorrectContent = () =>
    {
        let returnvalue = "";
        switch (this.state.currentstate)
        {
            case "channels":
                returnvalue = (this.channelsContent());
            break;
            case "reload_channels":
                returnvalue = (<div></div>);
                this.setState({currentstate: "channels"});
            break;
            case "geobots":
                returnvalue = (this.geobotsContent());
            break;
            case "reload_geobots":
                returnvalue = (<div></div>);
                this.setState({currentstate: "geobots"});
            break;
            case "geobot":
                returnvalue = (this.geobotContent());
            break;
            default:
            break;
        }
        return returnvalue;
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // The main render loop
    // ----------------------------------------------------------------------------------------------------
    render ()
    {
        return (
            <div ref={this.container}>{this.showCorrectContent()}</div>
        );
    }
};
// --------------------------------------------------------------------------------------------------------


export default BinaryRiskContracts;
