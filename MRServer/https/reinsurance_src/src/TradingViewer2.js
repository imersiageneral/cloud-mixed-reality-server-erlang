// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draw a Contract's Simulated Trading
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { } from 'semantic-ui-react'
import { HorizontalBar } from 'react-chartjs-2'

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class TradingViewer2 extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// props:
	//		entityid
	//		startdate
	//		enddate
	//		events
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);
		let numPoints = 0; //Math.round(Math.random()*30) + 30;

		this.state = {
			loading: true,
			rawData: [],
			labels: [],
			trading: -1,
			numPoints: numPoints,
			divPoint: Math.round(numPoints / 2)
		};

		this.timerid1 = null;
		this.timerid2 = null;
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		this.setState({rawData: this.createData(this.state.numPoints)});
		this.setState({labels: this.createLabels(this.state.numPoints), loading:false});

		this.props.imersiasdk.OPEN(this.props.geobotid);
		document.addEventListener(this.props.geobotid, this.processGeobotUpdate);

		// Start the trading simulation
		//this.doTrading ();

		this.getValues ();
	}

	componentWillUnmount = () =>
	{
		// if (this.timerid1 !== null) clearTimeout(this.timerid1);
		// if (this.timerid2 !== null) clearTimeout(this.timerid2);

		this.props.imersiasdk.CLOSE(this.props.geobotid);
		document.removeEventListener(this.props.geobotid, this.processGeobotUpdate);

	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Process the messages coming from the websocket
	// ----------------------------------------------------------------------------------------------------
	processGeobotUpdate = (message) =>
	{
		if (message.detail.wotcha.hasOwnProperty("trigger"))
		{
			if ((message.detail.wotcha.trigger.geobotid === this.props.geobotid) && (message.detail.wotcha.trigger.event === "update"))
			{
				let rawData = this.state.rawData;
				let divPoint = message.detail.wotcha.trigger.parameters.divpoint;
				let trading = message.detail.wotcha.trigger.parameters.trading;
				let numPoints = message.detail.wotcha.trigger.parameters.numpoints;
				rawData[trading] = message.detail.wotcha.trigger.parameters.newvalue;

				this.setState({rawData: rawData, trading: trading, numPoints: numPoints, divPoint: divPoint, labels: this.createLabels(this.state.numPoints)});
			}
		}
	}
	// ----------------------------------------------------------------------------------------------------




	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	getValues = () =>
	{
		let numPoints = 0;
		let rawData = [];
		let trading = 0;
		let divPoint = 0;

		this.props.imersiasdk.GET("metadata", {"geobotid": this.props.geobotid, "key": "numpoints"})
		.then((resp) => {
			numPoints = resp.json.value;
			return (this.props.imersiasdk.GET("metadata", {"geobotid": this.props.geobotid, "key": "data"}))
		})
		.then((resp) => {
			rawData = resp.json.value;
			return (this.props.imersiasdk.GET("metadata", {"geobotid": this.props.geobotid, "key": "divpoint"}))
		})
		.then((resp) => {
			divPoint = resp.json.value;
			return (this.props.imersiasdk.GET("metadata", {"geobotid": this.props.geobotid, "key": "trading"}))
		})
		.then ((resp) => {
			trading = resp.json.value;

			this.setState({rawData: rawData, numPoints:numPoints, trading: trading, divPoint: divPoint, labels: this.createLabels(numPoints)});
		})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Simulate trading
	// ----------------------------------------------------------------------------------------------------
	doTrading = () =>
	{
		let timeInterval = Math.random() * 600 + 100;
		let itemToChange = this.state.divPoint + ((Math.random() > 0.6) ? -1 : 0);
		this.setState({trading: itemToChange, labels: this.createLabels(this.state.numPoints)});

		this.timerid1 = setTimeout(() => {
			let theData = this.state.rawData;
			theData[itemToChange] = this.createPoint();
			if (itemToChange === this.state.divPoint)
			{
				this.setState({rawData: theData, divPoint: this.state.divPoint + 1, labels: this.createLabels(this.state.numPoints)});
			}
			else if (itemToChange === this.state.divPoint - 1)
			{
				this.setState({rawData: theData, divPoint: this.state.divPoint - 1, labels: this.createLabels(this.state.numPoints)})
			}
			else
			{
				this.setState({rawData: theData, labels: this.createLabels(this.state.numPoints)});
			}

			if (this.state.divPoint < (this.state.numPoints / 3))
			{
				this.shiftDown();
			}
			else if (this.state.divPoint > (2 * this.state.numPoints / 3))
			{
				this.shiftUp();
			}
		}, timeInterval);

		this.timerid2 = setTimeout(this.doTrading, timeInterval);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	createData = (length) =>
	{
		let data = [];
		for (let i = 0; i<length; i++)
		{
			data.push(this.createPoint());
		}
		return data;
	}
	// ----------------------------------------------------------------------------------------------------
	getBids = (theData) =>
	{
		let returnData = [];

		theData.forEach((entry, i) => {
			if ((i >= this.state.divPoint) && (i !== this.state.trading))
			{
				returnData.push(-entry);
			}
			else
			{
				returnData.push(0);
			}
		});

		return returnData;
	}
	// ----------------------------------------------------------------------------------------------------
	getOffers = (theData) =>
	{
		let returnData = [];

		theData.forEach((entry, i) => {
			if ((i < this.state.divPoint) && (i !== this.state.trading))
			{
				returnData.push(entry);
			}
			else {
				returnData.push(0);
			}
		});

		return returnData;
	}
	// ----------------------------------------------------------------------------------------------------
	getTrading = (theData) =>
	{
		let returnData = [];

		theData.forEach((entry, i) => {
			if (i === this.state.trading)
			{
				if (i < this.state.divPoint)
				{
					returnData.push(entry);
				}
				else {
					returnData.push(-entry);
				}
			}
			else {
				returnData.push(0);
			}
		});

		return returnData;
	}
	// ----------------------------------------------------------------------------------------------------
	createLabels = (length) =>
	{
		let labels = [];
		for (let i = 0; i<length; i++)
		{
			if (i >= this.state.rawData.length)
			{
				labels.push("");
			}
			else
			{
				if (i < this.state.divPoint)
				{
					labels.push(this.state.rawData[i] + "");
				}
				else {
					labels.push(this.state.rawData[i] + "");
				}
			}
		}
		return labels;
	}
	// ----------------------------------------------------------------------------------------------------
	shiftUp = () =>
	{
		// let newData = [];
		// let difference = this.state.divPoint - Math.round(this.state.numPoints / 2);
		//
		// // Move them all up to put the division in the middle again
		// for (let i = difference; i < this.state.numPoints; i++)
		// {
		// 	newData.push(this.state.rawData[i]);
		// }
		// // Fill up the bottom rows
		// for (let i = 0; i < difference; i++)
		// {
		// 	newData.push(this.createPoint());
		// }
		//
		// this.setState({rawData: newData, divPoint: Math.round(this.state.numPoints / 2), labels: this.createLabels(this.state.numPoints)})
	}
	// ----------------------------------------------------------------------------------------------------
	shiftDown = () =>
	{
		// let newData = [];
		// let difference = Math.round(this.state.numPoints / 2) - this.state.divPoint;
		//
		// // Fill up the top rows
		// for (let i = 0; i < difference; i++)
		// {
		// 	newData.push(this.createPoint());
		// }
		// // Move them all down to put the division in the middle again
		// for (let i = 0; i < this.state.numPoints - difference; i++)
		// {
		// 	newData.push(this.state.rawData[i]);
		// }
		//
		// this.setState({rawData: newData, divPoint: Math.round(this.state.numPoints / 2), labels: this.createLabels(this.state.numPoints)})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		// let colours = ["#CFF09E", "#A8DBA8", "#79BD9A", "#3B8686", "#0B486B"];

		const data = {
			labels: this.state.labels,
			datasets: [
				{
					label: 'bids',
					backgroundColor: "#79BD9A",
					borderColor: "#79BD9A",
					borderWidth: 1,
					hoverBackgroundColor: "#79BD9A",
					hoverBorderColor: "#79BD9A",
					data: this.getBids(this.state.rawData)
				},
				{
					label: 'trading',
					backgroundColor: "#0B486B",
					borderColor: "#0B486B",
					borderWidth: 1,
					hoverBackgroundColor: "#0B486B",
					hoverBorderColor: "#0B486B",
					data: this.getTrading(this.state.rawData)
				},
				{
					label: 'offers',
					backgroundColor: "#CFF09E",
					borderColor: "#CFF09E",
					borderWidth: 1,
					hoverBackgroundColor: "#CFF09E",
					hoverBorderColor: "#CFF09E",
					data: this.getOffers(this.state.rawData)
				}
			]
		};

		const options = {
			responsive: true,
			maintainAspectRatio: false,
		    scales: {
				yAxes: [{
					// display: false,
					stacked: true
				}]
			},
			animation: {
				duration: 50
			},
			legend: {
				// display: false
			}
		}

		if (this.state.loading)
		{
			return (<>Loading... </>);
		}
		else {
			let height = (this.props.height) ? this.props.height + "vh" : "90vh";
			return (
				<div style={{ height:height, width:"100%"}}>
					<HorizontalBar data={data} options={options}/>
				</div>
			)
		}
	}
};
// --------------------------------------------------------------------------------------------------------

export default TradingViewer2;
