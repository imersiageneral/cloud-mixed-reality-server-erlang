// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// A holder for the dashboard elements
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Grid, Icon, Segment, Statistic } from 'semantic-ui-react';

class DashboardFrame extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			isadmin: false,
			loading: true,
			adminstats: {
				userids:[],
				channelids:[],
				geobotids:[]
			},
			userstats: {}
		};
	}
	// ----------------------------------------------------------------------------------------------------


	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () => {
		this.props.imersiasdk.GET("admin", {"command":"stats", "parameters":{}})
		.then ((resp) => {
			this.setState({adminstats: resp.json, isadmin:true, loading:false});
		})
		.catch ((err) => {
			this.props.imersiasdk.GEOJSON()
			.then ((resp) => {
				let channels = [];
				resp.json.channels.forEach((channel) => {
					if (channel.class === "com.telluriq.contracts")
					{
						channels.push(channel);
					}
				});
				this.setState({userstats: {channels:channels}});
				return (channels);
			})
			.then ((channels) => {
				let counter=0;
				channels.forEach((channel) => {
					this.props.imersiasdk.GET("geobots", {channelid:channel.channelid})
					.then ((resp) => {
						channel.geobots = resp.json;
						counter++;
						if (counter >= channels.length)
						{
							this.setState({isadmin: false, loading:false});
						}
					})
				})
			});
		});
	}
	componentWillUnmount = () => {
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	countusergeobots = () => {
		let counter = 0;
		this.state.userstats.channels.forEach ((channel) => {
			counter += channel.geobots.length;
		});
		return counter;
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		if (this.state.loading)
		{
			return (<>Loading ...</>);
		}
		else {
			if (this.state.isadmin)
			{
				return (
					<Grid stackable columns={3}>
						<Grid.Column stretched>
							<Segment textAlign="center" style={{height:"30vh"}} padded stretched>
								<Statistic color="grey" size="large" style={{position: "absolute", transform: "translate(-50%, -50%)", top:"50%"}}>
									<Statistic.Value>
										{this.state.adminstats.userids.length}
									</Statistic.Value>
									<Statistic.Label color="teal"><Icon color="teal" name='user' /> User{this.state.adminstats.userids.length===1?"":"s"}</Statistic.Label>
								</Statistic>
							</Segment>
						</Grid.Column>
						<Grid.Column stretched>
							<Segment textAlign="center" style={{height:"30vh"}} padded stretched>
								<Statistic color="grey" size="large"style={{position: "absolute", transform: "translate(-50%, -50%)", top:"50%"}}>
									<Statistic.Value>
										{this.state.adminstats.channelids.length}
									</Statistic.Value>
									<Statistic.Label color="teal"><Icon color="teal" name='list' /> Channel{this.state.adminstats.channelids.length===1?"":"s"}</Statistic.Label>
								</Statistic>
							</Segment>
						</Grid.Column>
						<Grid.Column stretched>
							<Segment textAlign="center" style={{height:"30vh"}} padded stretched>
								<Statistic color="grey" size="large"style={{position: "absolute", transform: "translate(-50%, -50%)", top:"50%"}}>
									<Statistic.Value>
										{this.state.adminstats.geobotids.length}
									</Statistic.Value>
									<Statistic.Label color="teal"><Icon color="teal" name='map marker alternate' /> Geobot{this.state.adminstats.geobotids.length===1?"":"s"}</Statistic.Label>
								</Statistic>
							</Segment>
						</Grid.Column>
						<Grid.Column stretched>
							<Segment textAlign="center" style={{height:"30vh"}} padded stretched>
							</Segment>
						</Grid.Column>
						<Grid.Column stretched>
							<Segment textAlign="center" style={{height:"30vh"}} padded stretched>
							</Segment>
						</Grid.Column>
						<Grid.Column stretched>
							<Segment textAlign="center" style={{height:"30vh"}} padded stretched>
							</Segment>
						</Grid.Column>
					</Grid>
				)
			}
			else {
				return (
					<Grid stackable columns={3}>
						<Grid.Column stretched>
							<Segment textAlign="center" style={{height:"30vh"}} padded stretched>
								<Statistic color="grey" size="large"style={{position: "absolute", transform: "translate(-50%, -50%)", top:"50%"}}>
									<Statistic.Value>
										{this.state.userstats.channels.length}
									</Statistic.Value>
									<Statistic.Label color="teal"><Icon color="teal" name='folder' /> Class{this.state.userstats.channels.length===1?"":"es"}</Statistic.Label>
								</Statistic>
							</Segment>
						</Grid.Column>
						<Grid.Column stretched>
							<Segment textAlign="center" style={{height:"30vh"}} padded stretched>
								<Statistic color="grey" size="large"style={{position: "absolute", transform: "translate(-50%, -50%)", top:"50%"}}>
									<Statistic.Value>
										{this.countusergeobots()}
									</Statistic.Value>
									<Statistic.Label color="teal"><Icon color="teal" name='certificate' /> BRC{this.countusergeobots()===1?"":"s"}</Statistic.Label>
								</Statistic>
							</Segment>
						</Grid.Column>
						<Grid.Column stretched>
							<Segment textAlign="center" style={{height:"30vh"}} padded stretched>
							</Segment>
						</Grid.Column>
						<Grid.Column stretched>
							<Segment textAlign="center" style={{height:"30vh"}} padded stretched>
							</Segment>
						</Grid.Column>
						<Grid.Column stretched>
							<Segment textAlign="center" style={{height:"30vh"}} padded stretched>
							</Segment>
						</Grid.Column>
						<Grid.Column stretched>
							<Segment textAlign="center" style={{height:"30vh"}} padded stretched>
							</Segment>
						</Grid.Column>
					</Grid>
				);
			}
		}
	}
};
// --------------------------------------------------------------------------------------------------------

export default DashboardFrame;
