// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draws a contract class card given the details
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Modal, Card, Icon, Menu, Button } from 'semantic-ui-react';
import PatternEditor from './PatternEditor.js';
import Channel from './patterns/Channel.json';
import ImageImersia from './ImageImersia.js';

import folder from './images/folder.png';

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class ContractClassCard extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			channeldetails: JSON.parse(JSON.stringify(this.props.channeldetails)),
			editedchanneldetails: {},
			editing: false,
			deleting: false,
			error: false,
			error_message: ""
		};
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () => {
		document.addEventListener(this.props.channeldetails.channelid, this.processWotchaEvent);
    }
    componentWillUnmount = () => {
		document.removeEventListener(this.props.channeldetails.channelid, this.processWotchaEvent);
    }
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	processWotchaEvent = (message) =>
	{
		if (message.detail.wotcha.hasOwnProperty("channel_set"))
		{
			this.setNewValues(message.detail.wotcha.channel_set);
		}
		else if (message.detail.wotcha.hasOwnProperty("channel_rename"))
		{
			this.setNewValues(message.detail.wotcha.channel_rename);
		}
	}

	setNewValues = (details) =>
	{
		let newDetails = this.state.channeldetails;
		Object.keys(details).forEach((key) => {
			newDetails[key] = details[key];
		});
		this.setState({channeldetails: newDetails});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Pass back up to the channel list which channel was clicked on
	// ----------------------------------------------------------------------------------------------------
	openChannel = () =>
	{
		this.props.openChannel(this.state.channeldetails);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleEditClick = () =>
	{
		this.setState({editing:true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	saveMetadata = (newmetadata) =>
	{
		newmetadata.forEach((metadata) => {
			if (metadata.deleted)
			{
				this.props.imersiasdk.DELETE("metadata", {channelid:this.state.channeldetails.channelid, contextids: JSON.stringify(this.state.channeldetails.contextids), metadataid:metadata.metadataid});
			}
			else if (metadata.new)
			{
				this.props.imersiasdk.POST("metadata", {channelid:this.state.channeldetails.channelid, contextids: JSON.stringify(this.state.channeldetails.contextids)}, metadata);
			}
			else
			{
				this.props.imersiasdk.PUT("metadata", {channelid:this.state.channeldetails.channelid, contextids: JSON.stringify(this.state.channeldetails.contextids), metadataid:metadata.metadataid}, metadata);
			}
		})
	}
	// ----------------------------------------------------------------------------------------------------




	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	changeDetails = (newdetails) =>
	{
		this.setState({editedchanneldetails:JSON.parse(JSON.stringify(newdetails))});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	cancelEdit = () => {
		this.setState({editing:false});
	}

	saveEdit = () => {
		if (this.state.editedchanneldetails !== {})
		{
			this.props.imersiasdk.PUT("channels", {channelid:this.state.editedchanneldetails.channelid, contextids: JSON.stringify(this.state.channeldetails.contextids)}, this.state.editedchanneldetails)
			.then ((resp) => {
				this.saveMetadata(this.state.editedchanneldetails.metadata);
				this.setState({channeldetails:this.state.editedchanneldetails, editing:false});
			})
			.catch ((resp) => {
				if (resp.err === "name")
				{
					this.setState({error:true, error_message:"A Channel with that name already exists on this Domain."});
				}
				else {
					this.setState({error:true, error_message:"An error occurred saving the Channel."});
				}
			});
		}
	}
	askDeleteChannel = () => {
		this.setState({deleting:true});
	}
	deleteChannel = () => {
		this.props.imersiasdk.DELETE("channels", {channelid:this.state.channeldetails.channelid})
		.then ((resp) => {
			this.props.closeChannel();
			this.setState({editing:false, deleting:false});
		})
		.catch((err) => {
			this.setState({deleting:false});
		});
	}
	cancelDelete = () => {
		this.setState({deleting:false});
	}
	errorOK = () => {
		this.setState({error:false})
	}
	uploadFiles = (files) => {
		return (
			this.props.imersiasdk.POSTFILES({"channelid":this.state.channeldetails.channelid}, files)
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	editModal = () =>
	{
		return (
			<Modal open={this.state.editing} centered={false}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Edit</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content scrolling>
					<PatternEditor
						imersiasdk={this.props.imersiasdk}
						pattern={Channel["com.imersia.channel"]}
						class={"com.imersia.channel"}
						details={this.state.channeldetails}
						changeDetails={this.changeDetails}
						uploadFiles={this.uploadFiles}
					/>
		  		</Modal.Content>
				<Modal.Actions>
					<Button basic negative onClick={this.askDeleteChannel}>
						<Icon name='delete' /> Delete
					</Button>
					<Button basic color="blue" onClick={this.cancelEdit}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic positive onClick={this.saveEdit}>
						Save <Icon name='right chevron' />
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	askDeleteModal = () =>
	{
		return (
			<Modal size="tiny" open={this.state.deleting} centered={true}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Delete Channel</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content>
					Delete Channel and all its Geobots? - there is no UNDO.
				</Modal.Content>
				<Modal.Actions>
					<Button basic positive onClick={this.cancelDelete}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic negative onClick={this.deleteChannel}>
						<Icon name='cancel' /> DELETE
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	errorModal = () =>
	{
		return (
			<Modal size="tiny" open={this.state.error} centered={true}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Error</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content>
					{this.state.error_message}
				</Modal.Content>
				<Modal.Actions>
					<Button basic positive onClick={this.errorOK}>
						OK
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	mainBit = () =>
	{
		const imageurl = this.state.channeldetails.imageurl;

		return (
			<>
				<Button
					basic compact
					onClick={this.openChannel}>
					<ImageImersia src={imageurl} default={folder} imersiasdk={this.props.imersiasdk} fluid/>
				</Button>
			</>
		)
	}
	handleMenuChoice = (e, data) =>
	{
		switch (data.value)
		{
			case "edit":
			this.handleEditClick ();
			break;
			default:
			break;
		}
	}
	handleLinkClick = (e, data) =>
	{
		let metadatavalue = this.metadataValue("com.telluriq.contracts");
		let rawurl = metadatavalue[data.value];
		let url = ((rawurl.indexOf("http") === -1) ? "https://" : "") + rawurl;

		window.open(url, "_blank");
	}

	metadataValue = (key) =>
	{
		let returnvalue = null;
		this.state.channeldetails.metadata.forEach((metadata) => {
			if (metadata.key === key)
			{
				returnvalue = metadata.value;
			}
		});
		return returnvalue;
	}

	hasNonBlankValue = (key, subkey = null) =>
	{
		let returnvalue = false;
		this.state.channeldetails.metadata.forEach((metadata) => {
			if (metadata.key === key)
			{
				if (subkey && (typeof metadata.value === 'object') && metadata.value.hasOwnProperty(subkey))
				{
					if (metadata.value[subkey] !== "")
					{
						returnvalue = true;
					}
				}
				else {
					if (metadata.value !== "")
					{
						returnvalue = true;
					}
				}
			}
		});
		return returnvalue;
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		return (
			<Card style={{minHeight: "350px"}}>
				{this.mainBit()}
				<Card.Content textAlign="center">
					<Card.Header>{this.state.channeldetails.name}</Card.Header>
					<Card.Meta>{this.state.channeldetails.class}</Card.Meta>
					<Card.Description>{this.state.channeldetails.description}</Card.Description>
				</Card.Content>
				{
					this.state.channeldetails.ownerid === this.props.imersiasdk.userid ?
					(
						<Card.Content extra textAlign="center">
							<Button fluid basic onClick={this.handleMenuChoice} value="edit" >
								<Icon name='edit' /> Edit
							</Button>
						</Card.Content>
					):(
						<></>
					)
				}
				{
					this.hasNonBlankValue("com.telluriq.contracts", "livefeed") ?
					(
						<Card.Content extra textAlign="center">
							<Button fluid basic onClick={this.handleLinkClick} value="livefeed" >
								<Icon name='link' /> Live Feed
							</Button>
						</Card.Content>
					):(
						<></>
					)
				}

				{this.state.editing ?			this.editModal() 		: <></>}
				{this.state.error ?				this.errorModal()		: <></>}
				{this.state.deleting ?			this.askDeleteModal()	: <></>}
			</Card>
		)
	}
};
// <a rel="noopener noreferrer" target="_blank" href={"https://" + window.location.host + "?id=" + this.state.geobotdetails.geobotid}>direct link</a>

// --------------------------------------------------------------------------------------------------------

export default ContractClassCard;
