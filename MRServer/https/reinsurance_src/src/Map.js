// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draws a Geobot card given the details
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Modal, Button, Icon } from 'semantic-ui-react';

import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import brc_icon from './images/brc_icon.png';
import geohash from 'geohash';


// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class Map extends Component {
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	constructor(props) {
		super (props);

		this.mapContainer = React.createRef();
		this.mapcanvas = null;
		this.isCursorOverPoint = false;
		this.isDragging = false;
		this.grabbedMarker = null;

		this.markerjson =
        {
            type: 'FeatureCollection',
            features: [ ]
        };

		this.markers = {};

		mapboxgl.accessToken = this.props.imersiasdk.maptoken;

		this.state = {
			showinggeobot: false,
			geobotidtoshow: ""
		};

	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		document.addEventListener(this.props.channelid, this.processWotchaEvent);
		if (this.props.imersiasdk.maptoken) this.drawMap();
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentWillUnmount = () =>
	{
		document.removeEventListener(this.props.channelid, this.processWotchaEvent);
		if (this.map) this.map.remove();
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	geobotModal = () =>
	{
		return (
			<Modal size="tiny" style={{height:"80vh"}} open={this.state.showinggeobot} centered={false} >
				<Modal.Header>
					{"https://" + window.location.host + "?id=" + this.state.geobotidtoshow}
				</Modal.Header>
				<Modal.Content style={{height:"75%"}}>
					<iframe title="content" aria-hidden="true" frameBorder="0" padding="0px" width="100%" height="100%" src={"https://" + window.location.host + "?id=" + this.state.geobotidtoshow}></iframe>
				</Modal.Content>
				<Modal.Actions>
					<Button basic positive onClick={this.closeGeobotModal}>
						<Icon name='check' /> Done
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	closeGeobotModal = () =>
	{
		this.setState({showinggeobot:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Adjust the map points appropriately when geobots come and go, or change
	// ----------------------------------------------------------------------------------------------------
	processWotchaEvent = (message) =>
    {
		if (message.detail.wotcha.hasOwnProperty("geobot_new"))
		{
			this.markers[message.detail.wotcha.geobot_new.geobotid] = message.detail.wotcha.geobot_new;
			this.resetMapFeatures();

			// let newpoint = this.convertPointToGeoJson(message.detail.wotcha.geobot_new);
			// this.markerjson.features.push(newpoint);
		}
		else if (message.detail.wotcha.hasOwnProperty("geobot_delete"))
		{
			delete this.markers[message.detail.wotcha.geobot_delete.geobotid];
			this.resetMapFeatures();
		}
		else if (message.detail.wotcha.hasOwnProperty("geobot_set"))
		{
			this.markers[message.detail.wotcha.geobot_set.geobotid] = message.detail.wotcha.geobot_set;
			this.resetMapFeatures();
		}

		// Update points on the map
		let markersource = this.map.getSource('markers');
		if (markersource) markersource.setData(this.markerjson);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	drawMap = () => {
		this.map = new mapboxgl.Map({
			container: this.mapContainer.current,
			style: 'mapbox://styles/mapbox/outdoors-v10',
			trackResize: true,
			center: this.props.imersiasdk.mapstore.center,
			zoom: this.props.imersiasdk.mapstore.zoom
		});

		this.mapcanvas = this.map.getCanvasContainer();

		this.waitUntilMapReady( () => {
			this.setMapFeatures(this.props.geobots);

			this.map.addSource('markers', {
				"type": "geojson",
				"data": this.markerjson
			});

			this.map.loadImage(brc_icon, (error, image) => {
				if (error) throw error;
				if (!this.map.hasImage('imersiastar')) this.map.addImage('imersiastar', image);
				this.map.addLayer(
					{
						"id": "markerslayer",
						"type": "symbol",
						"source": "markers",
						"layout":
						{
							"icon-image": "imersiastar",
							"icon-size" : 0.15,
							"text-field": "{title}",
							"icon-allow-overlap":true,
							"text-allow-overlap":true,
							"text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
							"text-size": 14,
							"text-offset": [0, 0.8],
							"text-anchor": "top",
							"text-optional": true
						}
					}
				);

				this.zoomToGeobots();

				this.map.addControl(new mapboxgl.GeolocateControl({
					positionOptions: {
						enableHighAccuracy: true
					},
					trackUserLocation: true
				}));

				// When the cursor enters a feature in the point layer, prepare for dragging.
				this.map.on('mouseenter', 'markerslayer', () => {
					this.mapcanvas.style.cursor = 'move';
					this.isCursorOverPoint = true;
					this.map.dragPan.disable();
				});

				this.map.on('mouseleave', 'markerslayer', () => {
					this.mapcanvas.style.cursor = '';
					this.isCursorOverPoint = false;
					this.map.dragPan.enable();
				});

				this.map.on('mousemove', (e) => {
					if (this.isDragging) return;

					if (this.map.getLayer('markerslayer') !== undefined)
					{
						let features = this.map.queryRenderedFeatures(e.point, { layers: ['markerslayer'] });

						// Change cursor style as a UI indicator
						// and set a flag to enable other mouse events.
						if (features.length) {
							this.mapcanvas.style.cursor = 'pointer';
							this.isCursorOverPoint = true;
							this.grabbedMarker = features[0];
							// For some reason, the contents of the feature has become a string
							this.grabbedMarker.properties.details = JSON.parse(this.grabbedMarker.properties.details);
							this.grabbedMarker.properties.coordinates = JSON.parse(this.grabbedMarker.properties.coordinates);
							this.map.dragPan.disable();
						} else {
							this.mapcanvas.style.cursor = '';
							this.isCursorOverPoint = false;
							this.grabbedMarker = undefined;
							this.map.dragPan.enable();
						}
					}
				});

				this.map.on('mousedown', 'markerslayer', this.mouseDownOnMap);
				this.map.on('moveend', () => {
					let lnglat = this.map.getCenter();
					this.props.mapcenter(geohash.GeoHash.encodeGeoHash(lnglat.lat, lnglat.lng));
				})

				this.map.on('zoomend', () => {
					this.props.imersiasdk.mapstore = {
						center: this.map.getCenter(),
						zoom: this.map.getZoom()
					}
				})
			});
		})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	mouseDownOnMap = (e) =>
	{
		if (this.isCursorOverPoint)
		{
			// Mouse events
			this.map.on('mousemove', this.onMove);
			this.map.once('mouseup', this.onUp);
		}
	}
	// ----------------------------------------------------------------------------------------------------
	onMove = (e) =>
	{
		this.isDragging = true;

		let coords = e.lngLat;

		// Set a UI indicator for dragging.
		this.mapcanvas.style.cursor = 'grabbing';

		this.markerjson.features[this.grabbedMarker.properties.index].geometry.coordinates = [coords.lng, coords.lat];
		let markersource = this.map.getSource('markers');
		if (markersource) markersource.setData(this.markerjson);
	}
	// ----------------------------------------------------------------------------------------------------
	onUp = (e) =>
	{
		if (!this.grabbedMarker) return;

		if (this.isDragging)
		{
			let coords = e.lngLat;
			let thisGeobotID = this.grabbedMarker.properties.geobotid;

			let newlocation = {
				latitude: coords.lat,
				longitude: coords.lng,
				altitude: this.grabbedMarker.properties.details.location.altitude
			};

			this.props.imersiasdk.PUT("geobots", {geobotid:thisGeobotID}, {location:newlocation})
			.then ((resp) => {

			})
			.catch ((err) => {

			});
		}
		else
		{
			this.doSomethingOnClick (this.grabbedMarker.properties.geobotid);
		}

		this.mapcanvas.style.cursor = '';
		this.isDragging = false;
		this.isCursorOverPoint = true;
		this.map.off('mousemove', this.onMove);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	doSomethingOnClick = (geobotid) => {
		// check geobot class
		let geobot_class = this.markers[geobotid].class;

		let headers = {
			geobotid: geobotid,
			key: geobot_class
		};

		// check the metadata from the geobot class
		this.props.imersiasdk.GET("metadata", headers)
		.then ((resp) => {
			if (resp.json.value.hasOwnProperty("touch"))
			{
				if (resp.json.value.touch === "touch")
				{
					// Send touch event
				}
				else
				{
					// Open webapp
					this.setState({showinggeobot: true, geobotidtoshow: geobotid});
				}
			}
		})
		.catch ((err) => {
			// Metadata doesn't exist, so just open webapp
			this.setState({showinggeobot: true, geobotidtoshow: geobotid});
		});

		// if metadata class data has a touch, send it, otherwise do something else
		// let win = window.open("https://" + window.location.host + "/" + geobotid, "", "");
		// win.focus();
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Convert geobots list to geojson
	// ----------------------------------------------------------------------------------------------------
	resetMapFeatures = () => {
		this.markerjson.features = [];
		Object.keys(this.markers).forEach((geobotid) => {
			let pointGeoJSON = this.convertPointToGeoJson(this.markers[geobotid]);
			this.markerjson.features.push(pointGeoJSON);
		})
	}

	setMapFeatures = (points) => {
		this.markerjson.features = [];
		points.forEach ((point) => {
			this.markers[point.geobotid] = point;
			let pointGeoJSON = this.convertPointToGeoJson(point);
			this.markerjson.features.push(pointGeoJSON);
		});
	}

	convertPointToGeoJson = (pointdetails) => {
		return (
			{
				type: "Feature",
				geometry: {
					type: "Point",
					coordinates: [pointdetails.location.longitude, pointdetails.location.latitude]
				},
				properties: {
					title: pointdetails.name,
					geobotid: pointdetails.geobotid,
					channelid: pointdetails.channelid,
					coordinates: [pointdetails.location.longitude, pointdetails.location.latitude],
					index: this.markerjson.features.length,
					details: pointdetails
				}
			}
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	zoomToGeobots = () => {
		if (this.markerjson.features.length > 0)
		{
			let bounds = new mapboxgl.LngLatBounds();

				this.markerjson.features.forEach((feature) => {
			    bounds.extend(feature.geometry.coordinates);
			});

			this.map.fitBounds(bounds, {
				padding: 100
			});
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// A callback that waits until the map is loaded and ready in order to begin adding things to it
	// ----------------------------------------------------------------------------------------------------
	waitUntilMapReady = (callback) =>
	{
		this.checkMapReady(10, callback);
	}
	checkMapReady = (counter, callback) =>
	{
		if (counter > 0)
		{
			if (this.map.isStyleLoaded())
			{
				callback(true);
			}
			else
			{
				setTimeout(() => {this.checkMapReady(counter-1, callback)}, 1000);
			}
		}
		else
		{
			callback(false);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	render() {
		if (this.props.imersiasdk.maptoken)
		{
			return (
				<>
					<div style={{ height:"100%", width:"100%"}} ref={this.mapContainer}></div>
					{this.geobotModal ()}
				</>
			)
		}
		else
		{
			return (
				<>
					There is no mapbox token ID set in the mrserver.toml file
				</>
			)
		}
	}
	// ----------------------------------------------------------------------------------------------------
}

export default Map;
