// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draws a user's card given the details
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Card, Modal, Button, Icon, Dropdown, Menu } from 'semantic-ui-react'
import PatternEditor from './PatternEditor.js';
import UserTokens from './UserTokens.js';
import ImageImersia from './ImageImersia.js';
import User from './patterns/User.json';

import nobody from './images/nobody.png';

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class UserCard extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			userdetails: JSON.parse(JSON.stringify(this.props.userdetails)),
			editeduserdetails: {},
			editing: false,
			filemanager: false,
			error: false,
			error_message: ""
		};
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Register and deregister the event listeners
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () => {
		document.addEventListener(this.props.imersiasdk.userid, this.processUserUpdate);
	}
	// ----------------------------------------------------------------------------------------------------
	componentWillUnmount = () => {
		document.removeEventListener(this.props.imersiasdk.userid, this.processUserUpdate);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	processUserUpdate = (message) =>
	{
		if (message.detail.wotcha.hasOwnProperty("user_set"))
		{
			this.setNewValues(message.detail.wotcha.user_set);
		}
	}

	setNewValues = (details) =>
	{
		let newDetails = this.state.userdetails;
		Object.keys(details).forEach((key) => {
			newDetails[key] = details[key];
		});
		this.setState({userdetails: newDetails});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidUpdate = () => {
		if (this.state.userdetails === null)
		{
			this.setState({userdetails: JSON.parse(JSON.stringify(this.props.userdetails))});
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	errorModal = () =>
	{
		return (
			<Modal size="tiny" open={this.state.error} centered={true}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Error</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content>
					{this.state.error_message}
				</Modal.Content>
				<Modal.Actions>
					<Button basic positive onClick={this.errorOK}>
						OK
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	errorOK = () => {
		this.setState({error:false})
	}
// ----------------------------------------------------------------------------------------------------




	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	cancelEdit = () => {
		this.setState({editing:false});
	}

	saveEdit = () => {
		this.props.imersiasdk.PUT("user", {userid:this.state.editeduserdetails.userid}, this.state.editeduserdetails.details)
		.then ((resp) => {
			this.saveMetadata(this.state.editeduserdetails.metadata);
			this.setState({userdetails:JSON.parse(JSON.stringify(this.state.editeduserdetails)), editing:false});
			// this.setState({editing:false});
		})
		.catch ((resp) => {
			this.setState({error:true, error_message:"An error occurred saving the User details."});
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	saveMetadata = (newmetadata) =>
	{
		newmetadata.forEach((metadata) => {
			if (metadata.deleted)
			{
				this.props.imersiasdk.DELETE("metadata", {userid:this.state.userdetails.userid, metadataid:metadata.metadataid});
			}
			else if (metadata.new)
			{
				this.props.imersiasdk.POST("metadata", {userid:this.state.userdetails.userid}, metadata);
			}
			else
			{
				this.props.imersiasdk.PUT("metadata", {userid:this.state.userdetails.userid, metadataid:metadata.metadataid}, metadata);
			}
		})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	changeDetails = (newdetails) =>
	{
		this.setState({editeduserdetails:JSON.parse(JSON.stringify(newdetails))});
	}
	// ----------------------------------------------------------------------------------------------------
	uploadFiles = (files) => {
		return (
			this.props.imersiasdk.POSTFILES({"userid":this.state.userdetails.userid}, files)
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	editModal = () =>
	{
		return (
			<Modal open={this.state.editing} centered={false}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Edit</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content scrolling>
					<PatternEditor
						imersiasdk={this.props.imersiasdk}
						pattern={User["com.imersia.user"]}
						class={"com.imersia.user"}
						details={this.state.userdetails}
						changeDetails={this.changeDetails}
						uploadFiles={this.uploadFiles}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button basic color="blue" onClick={this.cancelEdit}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic positive onClick={this.saveEdit}>
						Save <Icon name='right chevron' />
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleEditClick = () =>
	{
		this.setState({editing:true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleMenuChoice = (e, data) =>
	{
		switch (data.value)
		{
			case "edit":
			this.handleEditClick ();
			break;
			default:
			break;
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		if (this.state.userdetails)
		{
			var imageurl = this.state.userdetails.details.imageurl;
			const trigger = (
				<Button style={{left:"0px", "background-color": "white", "color": "grey", opacity: 0.8}} compact circular icon="ellipsis vertical"/>
			)
			const options = [
				{ value: 'edit', text: 'Edit Details', icon: 'edit' }
			]

			return (
				<>
					<Card fluid>
						<ImageImersia wrapped src={imageurl} default={nobody} imersiasdk={this.props.imersiasdk}/>
						{this.state.userdetails.userid === this.props.imersiasdk.userid ?
						(
							<div style={{ position: "absolute", top: "15px", left: "20px"}}>
								<Dropdown trigger={trigger} options={options} pointing='top left' icon={null} onChange={this.handleMenuChoice}/>
							</div>
						):(
							<></>
						)}
		                <Card.Content textAlign="center">
		                    <Card.Header>{this.state.userdetails.details.firstname} {this.state.userdetails.details.surname}</Card.Header>
							<Card.Description>{this.state.userdetails.details.nickname}</Card.Description>
		                </Card.Content>
						<Card.Content extra textAlign="center">
							<Icon name='money' />
							<UserTokens imersiasdk={this.props.imersiasdk} /> tokens
						</Card.Content>
		            </Card>

					{this.state.editing 		? this.editModal() 		: <></>}
					{this.state.error 			? this.errorModal() 	: <></>}
					{this.state.filemanager 	? this.filesModal() 	: <></>}
				</>
			);
		}
		else {
			return (<div></div>);
		}
	}
};
// --------------------------------------------------------------------------------------------------------

export default UserCard;
