// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// The main screen after logging in
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';

import { Sidebar, Icon, Segment, Menu, Image, Button } from 'semantic-ui-react'

import UserCard from './UserCard.js';
import LicenseViewer from './LicenseViewer.js';
import {isMobile} from 'react-device-detect';

// --------------------------------------------------------------------------------------------------------
// The main class
// --------------------------------------------------------------------------------------------------------
class Main extends Component {
    // ----------------------------------------------------------------------------------------------------
    // Set up the object
    // ----------------------------------------------------------------------------------------------------
    constructor (props)
    {
        super (props);

        this.state = {
            currenttab: "licenses",
            menuvisible: !isMobile,
            userdetails: null,
            licensetypes: []
        };

        this.websocket = null;

        this.styles = {
            contentStyleOpen : {"padding-left": "270px", "padding-top":"10px", "padding-right":"10px", "padding-bottom":"10px"},
            contentStyleClosed : {"padding-left": "10px", "padding-top":"10px", "padding-right":"10px", "padding-bottom":"10px"}
        }
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // What to start with when the page loads
    // ----------------------------------------------------------------------------------------------------
    componentDidMount = () =>
    {
        this.props.imersiasdk.GET("user", {useremail: this.props.imersiasdk.useremail})
        .then ((resp) => {
            this.setState({ userdetails : resp.json });

            this.getDetailsFromINI()
            .then((resp2) => {
                this.setState({licensetypes: resp2});
                console.log(this.state.licensetypes);
            })
        })
        .catch((err) => {
            // Do nothing
        });
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Licenses content
    // ----------------------------------------------------------------------------------------------------
    licenseContent = () =>
    {
        return (
            <div>
                <Menu borderless>
                    <Menu.Item>
                        <Icon link name='bars' onClick={this.toggleSidebar} />
                    </Menu.Item>
                    <Menu.Item>
                        <h3>Licenses</h3>
                    </Menu.Item>
                </Menu>
                {this.allLicenseTypes()}
            </div>
        )
    }

    allLicenseTypes = () =>
    {
        let html = [];
        this.state.licensetypes.forEach((licensetype) => {
            html.push (
                    <LicenseViewer
                    name = {licensetype.name}
                    title = {licensetype.title}
                    channelid = {licensetype.channelid}
                    contextid = {licensetype.contextid}
                    terms = {licensetype.terms}
                    imersiasdk={this.props.imersiasdk}
                />
            )
        })

        return (html)
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Load the details about licenses from the mrserver.toml file
    // ----------------------------------------------------------------------------------------------------
    getDetailsFromINI = (licensecounter = 0) =>
    {
        return new Promise ((resolve, reject) =>
        {
            if (licensecounter < 9)
            {
                this.props.imersiasdk.GET("vars", {"category":"licensor", "key":"name" + (licensecounter === 0 ? "" : licensecounter)})
                .then((resp) => {
                    return (this.props.imersiasdk.GET("vars",
                        {"category":"licensor", "key":"channelid" + (licensecounter === 0 ? "" : licensecounter)},
                        {"name":resp.json.value})
                    );
                })
                .then((resp) => {
                    return (this.props.imersiasdk.GET("vars",
                        {"category":"licensor", "key":"title" + (licensecounter === 0 ? "" : licensecounter)},
                        {"channelid":resp.json.value, "name":resp.passthrough.name})
                    );
                })
                .then((resp) => {
                    return (this.props.imersiasdk.GET("vars",
                        {"category":"licensor", "key":"contextid" + (licensecounter === 0 ? "" : licensecounter)},
                        {"title":resp.json.value, "channelid":resp.passthrough.channelid, "name":resp.passthrough.name})
                    );
                })
                .then ((resp) => {
                    return (this.props.imersiasdk.GET("vars",
                        {"category":"licensor", "key":"terms" + (licensecounter === 0 ? "" : licensecounter)},
                        {"contextid":resp.json.value, "title":resp.passthrough.title, "channelid":resp.passthrough.channelid, "name":resp.passthrough.name})
                    );
                })
                .then ((resp) => {
                    let thislicense = {"terms":resp.json.value, "contextid":resp.passthrough.contextid, "title":resp.passthrough.title, "channelid":resp.passthrough.channelid, "name":resp.passthrough.name};
                    this.getDetailsFromINI(licensecounter + 1)
                    .then((resp2) => {
                        resp2.push(thislicense);
                        resolve (resp2);
                    })
                })
                .catch((err) => {
                    resolve([]);
                })
            }
            else
            {
                resolve([]);
            }
        });
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    // userContentViewer = () =>
    // {
    //     return (
    //         <div>
    //             <Menu borderless>
    //                 <Menu.Item>
    //                     <Icon link name='bars' onClick={this.toggleSidebar} />
    //                 </Menu.Item>
    //                 <Menu.Item>
    //                     <h3>Content</h3>
    //                 </Menu.Item>
    //             </Menu>
    //             <UserContentViewer
    //                 imersiasdk={this.props.imersiasdk}
    //                 doLogin={this.props.doLogin}
    //             />
    //         </div>
    //     )
    // }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Update the contents when the menu item / tab is selected
    // ----------------------------------------------------------------------------------------------------
    handleChange = (e, {name}) =>
    {
        var newstate = name;
        switch (name)
        {
            case "licenses":
            break;
            case "content":
                newstate = "content_reload";
            break;
            default:
            break;
        }
        this.setState({currenttab: newstate});
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Makes sure the content tab is rendering the correct output depending on then current state
    // ----------------------------------------------------------------------------------------------------
    showCorrectContent = () =>
    {
        var returnvalue = "";
        switch (this.state.currenttab)
        {
            case "licenses":
                returnvalue = (this.licenseContent());
            break;
            default:
                returnvalue = (this.licenseContent());
            break;
        }
        return returnvalue;
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Toggle the sidebar menu
    // ----------------------------------------------------------------------------------------------------
    toggleSidebar = () =>
    {
        this.setState({menuvisible: !this.state.menuvisible});
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Hide the sidebar in mobile mode, when clicked outside the sidebar
    // ----------------------------------------------------------------------------------------------------
    hideSidebar = (e) =>
    {
        if (isMobile && this.state.menuvisible) this.setState({menuvisible: false});
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // The main render loop
    // ----------------------------------------------------------------------------------------------------
    render ()
    {
        return (
            <Sidebar.Pushable as={Segment} currenttab={this.state.currenttab} style={{height:'100vh'}}>
                <Sidebar as={Menu} animation='overlay' vertical visible={this.state.menuvisible}>
                    <Menu.Item>
                        <Image fluid src='/images/logo.png' centered />
                    </Menu.Item>
                    <Menu.Item name="user">
                        <UserCard imersiasdk={this.props.imersiasdk} userdetails={this.state.userdetails} />
                    </Menu.Item>
                    <Menu.Item>
                        <Segment><Button basic fluid onClick={this.props.logout}>log out</Button></Segment>
                    </Menu.Item>
                </Sidebar>

                <Sidebar.Pusher dimmed={isMobile && this.state.menuvisible} onClick={this.hideSidebar}>
                    <div style={((this.state.menuvisible && !isMobile) ? this.styles.contentStyleOpen : this.styles.contentStyleClosed)}>
                        {this.showCorrectContent()}
                    </div>
                </Sidebar.Pusher>
            </Sidebar.Pushable>
        );
    }
};
// --------------------------------------------------------------------------------------------------------

export default Main;
