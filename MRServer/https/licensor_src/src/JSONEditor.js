// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// An editor for JSON settings
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Form, Table, Item, Button, Modal, Menu, Icon } from 'semantic-ui-react';
import Classes from './patterns/Classes.json';
import ImageExtensions from './patterns/ImageExtensions.json';
import Dropzone from 'react-dropzone'

import noimage from './images/noimage.png';

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class JSONEditor extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// props:
	// 		imersiasdk		an ImersiaSDK object
	// 		pattern			pattern to build the editor from
	//		name			name of element that is being changed (optional)
	//		details			initial values to fill the editor with
	//		parameters		parameters for the file saving command, eg {"channelid":GUID}
	// 		changeDetails	function to send changes to
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.styles = {
			dropzone: {
				display:'flex',
				'justify-content': 'center',
				'align-items': 'center',
				height:"100px",
				'border-style': 'dashed',
				'border-color': 'grey',
				'border-width': "1px",
				'border-radius': "5px"
			}
		}

		this.state = {
			editingmetadata: false,
			metadatakey:"",
			metadatapattern:{},
			metadatadetails:{
				metadataid:"",
				key:"",
				value:""
			},
			ready: false,
			details: {}
		};
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		if (!this.props.details)
		{
			let newdetails = this.getDefaults(this.props.pattern);
			this.setState({ready:true, details:newdetails});
			this.props.changeDetails(newdetails);
		}
		else
		{
			this.setState({ready:true, details:JSON.parse(JSON.stringify(this.props.details))});
			this.props.changeDetails(this.props.details);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	getDefaults = (pattern) =>
	{
		let returnvalue = {};
		pattern.forEach((element) => {
			if (element.type === "object")
			{
				returnvalue[element.name] = this.getDefaults(pattern.object);
			}
			else {
				returnvalue[element.name] = (element.default ? element.default : "");
			}
		});
		return returnvalue;
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// A set of functions for handling editing metadata inside another object's data
	// ----------------------------------------------------------------------------------------------------
	handleMetadataEditClick = (e, details) =>
	{
		this.setState({metadatakey:details.metadatakey, metadatapattern:details.pattern});

		// Load metadata value
		let parameters = this.props.parameters;
		parameters.key = details.metadatakey;
		this.props.imersiasdk.GET("metadata", parameters )
		.then ((resp) => {
			// Metadata exists and we've loaded it
			this.setState({editingmetadata:true, metadatadetails:resp.json});
		})
		.catch ((err) => {
			// Metadata doesn't yet exist (or some other error)
			let metadatadetails = {
				key: details.metadatakey,
				value: this.getDefaults(details.pattern),
				metadataid: ""
			}
			this.setState({editingmetadata:true, metadatadetails:metadatadetails});
		});
	}
	cancelMetadataEdit = () =>
	{
		this.setState({editingmetadata:false});
	}
	saveMetadata = () =>
	{
		// Convert any text that looks like a number into a number.
		let metadatadetails = this.state.metadatadetails;
		Object.keys(metadatadetails.value).forEach((key) => {
			// let newValue = Number(metadatadetails.value[key]);
			// metadatadetails.value[key] = (isNaN(newValue)) ? metadatadetails.value[key] : newValue;
			metadatadetails.value[key] = this.parseJSON(metadatadetails.value[key]);
		});
		this.setState({metadatadetails:metadatadetails});

		this.props.imersiasdk.POST("metadata", this.props.parameters, this.state.metadatadetails)
		.then ((resp) => {
			this.setState({editingmetadata:false});
		})
		.catch ((err) => {
			this.setState({editingmetadata:false});
		});
	}
	parseJSON = (value) =>
	{
		let converted = null;
		try {
			converted = JSON.parse(value);
		}
		catch (err)
		{
			converted = value;
		}
		return (converted);
	}
	changeMetadataDetails = (details) => {
		if (details)
		{
			let newDetails = this.state.metadatadetails;
			newDetails.value = details;
			this.setState({ metadatadetails: newDetails });
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	editMetadata = () =>
	{
		return (
			<Modal open={this.state.editingmetadata} centered={false}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>{this.state.metadatadetails.key}</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content scrolling>
					<JSONEditor
						imersiasdk={this.props.imersiasdk}
						pattern={this.state.metadatapattern}
						details={this.state.metadatadetails.value}
						parameters={this.props.parameters}
						changeDetails={this.changeMetadataDetails}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button basic color="blue" onClick={this.cancelMetadataEdit}>
						<Icon name='left chevron' /> Cancel
					</Button>

					<Button basic positive onClick={this.saveMetadata}>
						Save <Icon name='right chevron' />
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	convertPatternToHTML = (pattern, details) =>
	{
		let returnvalue = [];

		if (pattern.length === 0)
		{
			returnvalue.push(
				<Table.Row>
					<Table.Cell>Nothing to edit</Table.Cell>
				</Table.Row>
			)
		}
		else
		{
			pattern.forEach((element) => {
				switch (element.type)
				{
					case "text" :
						returnvalue.push (
							<Table.Row>
								<Table.Cell width={4}>{element.text}</Table.Cell>
								<Table.Cell>
									<Form.Input name={element.name} value={details[element.name]} type='text' placeholder={element.label} readOnly={element.locked} onChange={this.handleChange}/>
								</Table.Cell>
							</Table.Row>
						);
						break;
					case "textarea":
						returnvalue.push (
							<Table.Row>
								<Table.Cell width={4}>{element.text}</Table.Cell>
								<Table.Cell>
									<Form.TextArea name={element.name} value={details[element.name]} placeholder={element.label} readOnly={element.locked} onChange={this.handleChange}/>
								</Table.Cell>
							</Table.Row>
						);
						break;
					case "number" :
						returnvalue.push (
							<Table.Row>
								<Table.Cell width={4}>{element.text}</Table.Cell>
								<Table.Cell>
									<Form.Input name={element.name} value={details[element.name]} type='text' placeholder={element.label} readOnly={element.locked} onChange={this.handleChange}/>
								</Table.Cell>
							</Table.Row>
						);
						break;
					case "dropdown" :
						returnvalue.push (
							<Table.Row>
								<Table.Cell width={4}>{element.text}</Table.Cell>
								<Table.Cell>
									<Form.Dropdown name={element.name} fluid value={details[element.name]} selection options={element.parameters} placeholder={element.label} readOnly={element.locked} onChange={this.handleChange}/>
								</Table.Cell>
							</Table.Row>
						);
						break;
					case "dropdown-edit" :
						let options = [];
						Object.keys(Classes[element.parameters]).forEach((key) => {
							options.push({text:key, value:key});
						});
						returnvalue.push (
							<Table.Row>
								<Table.Cell>{element.text}</Table.Cell>
								<Table.Cell>
									<Form.Dropdown name={element.name} fluid value={this.state.details[element.name]} selection options={options} placeholder={element.label} onChange={this.handleChange}/>
									<Button basic fluid pattern={Classes[element.parameters][this.state.details[element.name]]} metadatakey={this.state.details[element.name]} onClick={this.handleMetadataEditClick}>edit {this.state.details[element.name]} details</Button>
								</Table.Cell>
							</Table.Row>
						);
						break;
					case "checkbox" :
						returnvalue.push (
							<Table.Row>
								<Table.Cell width={4}>{element.text}</Table.Cell>
								<Table.Cell>
									<Form.Checkbox name={element.name} checked={details[element.name]} readOnly={element.locked} onChange={this.handleChange} label={element.label}/>
								</Table.Cell>
							</Table.Row>
						);
						break;
					case "filename" :
						returnvalue.push (
							<Table.Row>
								<Table.Cell width={4}>{element.text}</Table.Cell>
								<Table.Cell>
									<Item.Group divided>
										<Item>
											{this.imageOrIcon (details[element.name])}
											<Item.Content>
												{(element.locked)?(<div></div>):(
												<Dropzone name={element.name} onDrop={this.onDrop}>
												 {({ getRootProps, getInputProps }) => (
													<div style={this.styles.dropzone} {...getRootProps({ refKey: 'innerRef' })}>
													  <input {...getInputProps()} />
													  <p>{element.label} </p>
													</div>
												  )}
												  </Dropzone>)
											  	}
											</Item.Content>
										</Item>
									</Item.Group>
									<Form.Input name={element.name} value={details[element.name]} type='text' readOnly={element.locked} onChange={this.handleChange}/>
								</Table.Cell>
							</Table.Row>
						);
						break;
					case "object" :
						returnvalue.push (
							<Table.Row>
								<Table.Cell width={4}>{element.text}</Table.Cell>
								<Table.Cell>
									<JSONEditor
										imersiasdk={this.props.imersiasdk}
										name={element.name}
										pattern={element.object}
										details={details[element.name]}
										parameters={this.props.parameters}
										changeDetails={this.changeObject}
									/>
								</Table.Cell>
							</Table.Row>
						);
						break;
					default:
					break;
				}
			})
		}

		return returnvalue;
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	imageOrIcon = (filename) =>
	{
		if (filename === undefined)
		{
			return (
				<></>
			)
		}
		else
		{
			let extension = filename.split('.').pop();
			if (ImageExtensions.indexOf(extension) > -1)
			{
				return (
					<Item.Image size='small' src={((filename==="")?noimage:filename + "?sessionid=" + this.props.imersiasdk.sessionid + "&random=" + Math.random())} />
				)
			}
			else {
				return (
					<></>
				)
			}
		}
	}
	// ----------------------------------------------------------------------------------------------------




	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	changeObject = (value, name) =>
	{
		let newValue = Number(value);
		let newDetails = this.state.details;
		newDetails[name] = (isNaN(newValue)) ? value : newValue;
		this.setState({ details: newDetails });
		this.props.changeDetails(newDetails, this.props.name);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleEditClick = (e) =>
	{
		this.setState({editing:true});
	}
	// ----------------------------------------------------------------------------------------------------


	// ----------------------------------------------------------------------------------------------------
	// Set the local variables when the input fields are changed
	// ----------------------------------------------------------------------------------------------------
	handleChange = (e, { name, value, checked}) => {
		let newDetails = this.state.details;
		if (value)
		{
			newDetails[name] = value;
		}
		else
		{
			newDetails[name] = checked;
		}
		this.setState({ details: newDetails });
		this.props.changeDetails(newDetails, this.props.name);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Upload files and set the parameter
	// ----------------------------------------------------------------------------------------------------
	onDrop = (acceptedFiles, rejectedFiles, e) => {
		// Do something with files
		this.props.imersiasdk.POSTFILES(this.props.parameters, acceptedFiles)
		.then ((resp) => {
			let newDetails = this.state.details;
			newDetails[e.target.name] = resp.json.fileurls[0];
			this.setState({ details: newDetails });
			this.props.changeDetails(newDetails, this.props.name);
		})
		.catch ((err) => {
			// Do nothing
		});
    }
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		if (!this.state.ready)
		{
			return (<div></div>);
		}
		else {
			return (
				<Form>
					<Table celled fluid definition>
						<Table.Body>
							{this.convertPatternToHTML(this.props.pattern, this.state.details)}
						</Table.Body>
					</Table>
					{this.editMetadata()}
				</Form>
			);
		}
	}
};
// --------------------------------------------------------------------------------------------------------

export default JSONEditor;
