// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// The Register dialog box.
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Form, Divider, Grid, Button, Modal, Image, Header, Message } from 'semantic-ui-react';

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class Register extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			open : true,
			useremail : "",
			password : "",
			password2 : "",
			surname : "",
			firstname : "",
			nickname : "",
			message : this.props.message,
			passwordMessage : '',
			passwordMessageColor : 'white',
			passwordMatchMessage : '',
			passwordMatchMessageColor : 'white',
			passwordquality : 0
		};

		this.styles = {
			actionStyle : { paddingLeft: '1.5em', paddingRight: '1.5em' },
			messageStyle : {height: '38px', width: '100%', textAlign: 'center', fontSize: 'small'}
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Perform a login and either close the dialog or show a message, depending on the outcome.
	// ----------------------------------------------------------------------------------------------------
	doRegister = () => {
		if (this.state.password === this.state.password2)
		{
			if (this.state.passwordQuality >= 0.5)
			{
				let body = {
					useremail: this.state.useremail,
					password: this.state.password,
					details: {
						firstname : this.state.firstname,
						surname : this.state.surname,
						nickname : this.state.nickname
					}
				};

				this.props.imersiasdk.POST("user", {}, body)
				.then ((resp) => {
					this.props.doneRegistering();
				}).catch ((err) => {
					this.setState({message:"Error registering"});
				});
			}
			else {
				this.setState({message:"Password is not complex enough"});
			}
		}
		else {
			this.setState({message:"Passwords don't match"});
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	cancel = () => {
		this.props.doneRegistering();
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Set the local variables when the input fields are changed, ready for when the user presses Login
	// ----------------------------------------------------------------------------------------------------
	handleChange = (e, { name, value }) => {
		this.setState({ [name]: value });

		switch (name)
		{
			case "password" :
				this.checkPassword(value, this.state.password2);
				break;
			case "password2" :
				this.checkPassword(this.state.password, value);
				break;
			default:
				break;
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	checkPassword = (pwd, pwd2) => {
		let strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
		let mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
		let enoughRegex = new RegExp("(?=.{6,}).*", "g");

		if (pwd.length === 0) {
			this.setState({ passwordMessage : '', passwordMessageColor: 'white', passwordQuality: 0});
		} else if (false === enoughRegex.test(pwd)) {
			this.setState({ passwordMessage : 'too short', passwordMessageColor: 'red', passwordQuality: 0.1})
		} else if (strongRegex.test(pwd)) {
			this.setState({ passwordMessage : 'strong', passwordMessageColor: 'green', passwordQuality: 1})
		} else if (mediumRegex.test(pwd)) {
			this.setState({ passwordMessage : 'medium', passwordMessageColor: 'orange', passwordQuality: 0.5})
		} else {
			this.setState({ passwordMessage : 'weak', passwordMessageColor: 'red', passwordQuality: 0.1})
		}

		if ((pwd.length > 0) && (pwd2.length > 0))
		{
			if (pwd === pwd2)
			{
				this.setState({ passwordMatchMessage : 'matches', passwordMatchMessageColor: 'green'})
			}
			else {
				this.setState({ passwordMatchMessage : 'no match', passwordMatchMessageColor: 'red', passwordQuality: 0})
			}
		}
		else {
			if (pwd.length === 0)
			{
				this.setState({ passwordMatchMessage : '', passwordMessageColor: 'white', passwordMatchMessageColor: 'white'})
			}
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Check for the enter key
	// ----------------------------------------------------------------------------------------------------
	handleKeyPress = (e) => {
		if (e.charCode === 13) { this.doRegister(e); };
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		return (
			<Modal open={this.state.open} size='tiny' >
				<Modal.Content>
					<Image fluid size='medium' src='/images/logo.png' centered />
					<Header as='h4' textAlign='center'>
						<span>{this.state.message}</span>
					</Header>
			        <Divider />
			        <Grid columns='one'>
						<Grid.Row>
							<Grid.Column>
					            <Form>
					                <Form.Field>
					                    <Form.Input placeholder="Email Address" name="useremail" type="text" value={this.state.useremail} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
					                </Form.Field>
					                <Form.Field>
										<Grid relaxed columns='two'>
											<Grid.Row>
												<Grid.Column width={11}>
													<Form.Input placeholder="Password" name="password" type="password" value={this.state.password} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
												</Grid.Column>
												<Grid.Column width={5}>
													<Message color={this.state.passwordMessageColor} style={this.styles.messageStyle}>{this.state.passwordMessage}</Message>
												</Grid.Column>
											</Grid.Row>
										</Grid>
									</Form.Field>
									<Form.Field>
										<Grid relaxed columns='two'>
											<Grid.Row>
												<Grid.Column width={11}>
													<Form.Input placeholder="Password check" name="password2" type="password" value={this.state.password2} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
												</Grid.Column>
												<Grid.Column width={5}>
													<Message color={this.state.passwordMatchMessageColor} style={this.styles.messageStyle}>{this.state.passwordMatchMessage}</Message>
												</Grid.Column>
											</Grid.Row>
										</Grid>
					                </Form.Field>
									<Form.Field>
										<Form.Input placeholder="First name" name="firstname" type="text" value={this.state.firstname} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
									</Form.Field>
									<Form.Field>
										<Form.Input placeholder="Surname" name="surname" type="text" value={this.state.surname} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
									</Form.Field>
									<Form.Field>
										<Form.Input placeholder="Nickname" name="nickname" type="text" value={this.state.nickname} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
									</Form.Field>
					            </Form>
							</Grid.Column>
						</Grid.Row>
			        </Grid>
				</Modal.Content>
				<Modal.Actions style={this.styles.actionStyle}>
			        <Grid columns='two'>
			            <Grid.Row>
							<Grid.Column>
								<Button fluid basic color='red' onClick={this.cancel}>Cancel</Button>
							</Grid.Column>
			                <Grid.Column>
			                    <Button fluid basic color='green' onClick={this.doRegister}>Register</Button>
			                </Grid.Column>
			            </Grid.Row>
					</Grid>
				</Modal.Actions>
			</Modal>
		)
	}
};
// --------------------------------------------------------------------------------------------------------

export default Register;
