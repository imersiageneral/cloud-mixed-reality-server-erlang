// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Table, Button, Icon, Header, Modal, Grid, Form, Segment, Message} from 'semantic-ui-react'
import ImersiaSDK from './ImersiaSDK';
import ImersiaLicense from './ImersiaLicense';


// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class LicenseViewer extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			licensesGeobotID: "",			// The ID of the Geobot that this user's licenses are on
			editinglicense: false,			// True if the new license modal is visible
			deletinglicense: false,			// True if the delete license modal is visible
			viewinglicense: false,			// True if viewing the license code
			index: 0,						// Index of license currently being edited
			editinglicenseIDs: false,		// True if the license IDs modal is visible
			licenses: [],					// List of licenses owned by this user
			licensesGeobotContextID: "", 	// Context ID for the Geobot that the user's licenses are on
			errorMessage: ""				// Something went wrong
		};
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The component has loaded, now we need to find the licenses (ie metadata fields on geobots) for this user.
	// First, we have to get the channelid from the mrserver.toml file and the contextID for the channel.
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		return new Promise ((resolve, reject) =>
		{
			this.testLicenseGeobotExists(this.props.imersiasdk.userid, this.props.channelid, this.props.contextid, this.props.name)
			.then((resp) => {
				console.log("User's license geobot exists.");
				this.setState({"licensesGeobotID": resp.geobotid});
				this.setState({"licensesGeobotContextID": resp.contextid});
				resolve({geobotid: resp.geobotid, contextid:resp.contextid});
			})
			.catch((resp) => {
				console.log("User's license geobot does not exist - creating one now.");
				// Create License Geobot and Set Metadata field in Companion metadata
				this.createLicenseGeobot(this.props.channelid, this.props.contextid, this.props.imersiasdk.useremail, this.props.imersiasdk.userid, this.props.name)
				.then((resp) => {
					this.setState({"licensesGeobotID": resp.geobotid});
					this.setState({"licensesGeobotContextID": resp.contextid});
					resolve({geobotid: resp.geobotid, contextid:resp.contextid});
				})
				.catch((resp) => {
					reject ();
				})
			})
		})
		.then((resp) => {
			return (this.loadLicensesFromGeobot(resp.geobotid, [this.props.contextid, resp.contextid]));
		})
		.then((resp) => {
			console.log("Licenses loaded.");
		})
		.catch((resp) => {
			console.log(resp);
			this.setState({errorMessage: "Error loading lienses"});
		})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Anything to do before closing the component
	// ----------------------------------------------------------------------------------------------------
	componentWillUnmount = () =>
	{
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Load the details about licenses from the mrserver.toml file
	// ----------------------------------------------------------------------------------------------------
	getDetailsFromINI = () =>
	{
		return new Promise ((resolve, reject) =>
		{
			let channelid = "";
			let contextid = "";
			let terms = "";
			let name = "";

			this.props.imersiasdk.GET("vars", {"category":"licensor", "key":"channelid"})
			.then((resp) => {
				channelid = resp.json.value;
				return (this.props.imersiasdk.GET("vars", {"category":"licensor", "key":"contextid"}));
			})
			.then ((resp) => {
				contextid = resp.json.value;
				return (this.props.imersiasdk.GET("vars", {"category":"licensor", "key":"terms"}));
			})
			.then ((resp) => {
				terms = resp.json.value;
				return (this.props.imersiasdk.GET("vars", {"category":"licensor", "key":"name"}));
			})
			.then ((resp) => {
				name = resp.json.value;
				resolve ( {"result":ImersiaSDK.OK, "channelid": channelid, "contextid": contextid, "name":name, "terms": terms} );
			})
			.catch ((err) => {
				reject ( {"result":ImersiaSDK.ERROR, "err":"Error loading details from mrserver.toml file"} );
			});
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Tests that the license geobot exists in the licenses channel.
	// ----------------------------------------------------------------------------------------------------
	testLicenseGeobotExists = (userid, channelid, channelcontextid, licensename) =>
	{
		return new Promise ((resolve, reject) =>
		{
			let geobotid = "";
			let contextid = "";

			// Get Metadata on Companion that has the key the with the license type name.
			this.props.imersiasdk.GET("metadata", {"userid": userid, "key": licensename})
			.then((resp) => {
				geobotid = resp.json.value[0];
				contextid = resp.json.value[1];

				let contextids = [channelcontextid, contextid];

				// Test that this Geobot actually exists
				return (this.props.imersiasdk.GET("geobots", {geobotid: geobotid, contextids: JSON.stringify(contextids)}));
			})
			.then((resp) => {
				if (resp.json.channelid === channelid)
				{
					resolve ( {"result":ImersiaSDK.OK, "geobotid": geobotid, "contextid": contextid} );
				}
				else {
					reject ( {"result":ImersiaSDK.ERROR, "err":"Error Finding License Geobot"} );
				}
			})
			.catch((err) => {
				reject ( {"result":ImersiaSDK.ERROR, "err":"Error Finding License Geobot"} );
			})
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Creates a license Geobot for a given user with correct context, and sets their metadata to point to it
	// ----------------------------------------------------------------------------------------------------
	createLicenseGeobot = (channelid, channelcontextid, useremail, userid, licensename) =>
	{
		return new Promise ((resolve, reject) =>
		{
			let geobotid = "";
			let contextid = "";

			// Create the Geobot with the name as the userEmail
			this.props.imersiasdk.POST("geobots", {"channelid": channelid, "contextids": channelcontextid}, {"name":useremail, "hidden":true})
			.then((resp) => {
				geobotid = resp.json.geobotid;
				// Set the Context so that the Geobot can be modified
				return (this.props.imersiasdk.POST("context", {"geobotid": geobotid, "contextids": channelcontextid}, {"attributes": ["read", "edit"]}));
			})
			.then((resp) => {
				contextid = resp.json.contextid;
				// Set a Metadata on the Companion to the new GeobotID and ContextID
				return (this.props.imersiasdk.POST("metadata", {"userid": userid}, {"key": licensename, "value": [geobotid, contextid]}));
			})
			.then((resp) => {
				// Return the resulting GeobotID and ContextID
				resolve ( {"result": ImersiaSDK.OK, "geobotid": geobotid, "contextid": contextid} );
			})
			.catch((err) => {
				reject ( {"result": ImersiaSDK.ERROR, "err":"Error creating License Geobot"} );
			})
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Load the metadata on the user's license Geobot and process them into individual licenses
	// TODO: Check License Geobot On Correct Channel
	// ----------------------------------------------------------------------------------------------------
	loadLicensesFromGeobot = (geobotid, contextids) =>
	{
		this.setState({licenses: []});

		return new Promise ((resolve, reject) =>
		{
			this.props.imersiasdk.GET("metadata", {"geobotid":geobotid, "contextids": JSON.stringify(contextids)})
			.then((resp) => {
				let metadataList = resp.json;
				metadataList.forEach((metadataEntry, i) => {
					this.addLicenseToTable(metadataEntry);
				});
				resolve ( {result:ImersiaSDK.OK} );
			})
			.catch((err) => {
				reject ( {result:ImersiaSDK.ERROR, err:"Error loading licenses"} );
			});
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Add a license to the table of licenses for this user
	// ----------------------------------------------------------------------------------------------------
	addLicenseToTable = (metadataEntry) =>
	{
		let allLicenses = this.state.licenses;
		let license = {
			url: metadataEntry.key,
			appids: metadataEntry.value,
			type: this.props.name,
			metadataid: metadataEntry.metadataid,
			new: false,
			deleted: false
		};
		allLicenses.push(license);
		this.setState({licenses: allLicenses});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Open the new license modal dialog box
	// ----------------------------------------------------------------------------------------------------
	newLicense = (e) =>
	{
		let allLicenses = this.state.licenses;
		let newlicense = {
			url: "",
			appids: [],
			type: this.props.name,
			metadataid: "",
			new: true,
			deleted: false
		};
		allLicenses.push(newlicense);
		this.setState({index: allLicenses.length-1, licenses: allLicenses, editinglicense: true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Save the licenses for this user by converting them back to metadata
	// ----------------------------------------------------------------------------------------------------
	saveLicenses = () =>
	{
		let geobotid = this.state.licensesGeobotID;
		let contextids = [this.props.contextid, this.state.licensesGeobotContextID];

		this.state.licenses.forEach ((item, i) => {
			if (item.deleted)
			{
				// Delete metadata field as given by the metadataid
				if (!item.new)
				{
					this.props.imersiasdk.DELETE("metadata", {"geobotid": geobotid, "metadataid": item.metadataid, "contextids": JSON.stringify(contextids)});
				}
			}
			else if (item.new)
			{
				return (this.props.imersiasdk.POST("metadata", {"geobotid": geobotid, "contextids": JSON.stringify(contextids)}, {"key": item.url, "value": item.appids}));
			}
			else
			{
				return (this.props.imersiasdk.PUT("metadata", {"geobotid": geobotid, "metadataid": item.metadataid, "contextids": JSON.stringify(contextids)}, {"key": item.url, "value": item.appids}));
			}
		});

		this.setState({editinglicense: false});

		this.loadLicensesFromGeobot(geobotid, contextids);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Create a license row based on the data entered in the modal dialog box
	// ----------------------------------------------------------------------------------------------------
	editLicense = (e, data) =>
	{
		this.setState({index: data.index, editinglicense: true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	deleteLicense = (e, data) =>
	{
		let allLicenses = this.state.licenses;
		allLicenses[this.state.index].deleted = true;
		this.setState({licenses: allLicenses, deletinglicense:false});

		this.saveLicenses();
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Set the local variables when the input fields are changed, ready for saving to the automations object
	// ----------------------------------------------------------------------------------------------------
	handleChange = (e, data) => {
		let allLicenses = this.state.licenses;
		let index = this.state.index;

		if (data.name === "appids")
		{
			allLicenses[index][data.name] = this.parseJSON(data.value);
		}
		else
		{
			allLicenses[index][data.name] = data.value;
		}
		this.setState({licenses: allLicenses});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	licenseList = () =>
	{
		let licensehtml = [];
		this.state.licenses.forEach ((item, i) => {
			if (!item.deleted)
			{
				licensehtml.push (
					<Table.Row>
						<Table.Cell textAlign='center' width={1}>
							<Button index={i} icon color="red" basic onClick={this.openDeleteModal}><Icon name="delete"/></Button>
						</Table.Cell>
						<Table.Cell width={6}>
							<Segment basic size="big">{item.url}</Segment>
						</Table.Cell>
						<Table.Cell width={7}>
							<Segment basic size="big">{this.stringifyJSON(item.appids)}</Segment>
						</Table.Cell>
						<Table.Cell textAlign='center' width={1}>
							<Button index={i} icon color="green" basic onClick={this.editLicense}><Icon name="pencil"/></Button>
						</Table.Cell>
						<Table.Cell textAlign='center' width={1}>
							<Button index={i} icon color="blue" basic onClick={this.openViewModal}><Icon name="certificate"/></Button>
						</Table.Cell>
					</Table.Row>
				)
			}
		});
		return (licensehtml);
	}
	parseJSON = (value) =>
	{
		let converted = null;
		try {
			converted = JSON.parse(value);
		}
		catch (err)
		{
			converted = value;
		}
		return (converted);
	}
	stringifyJSON = (value) =>
	{
		if (typeof value === 'string' || value instanceof String)
			{ return value }
		else
		{
			console.log(JSON.stringify(value));
			return (JSON.stringify(value))
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	editLicenseModal = () => {
		let index = this.state.index;
		let licenseterms = this.props.terms.split("|");

		if (this.state.editinglicense)
		{
			return (
				<Modal open={this.state.editinglicense} centered={false}>
					<Modal.Header>
						{this.props.title} License
					</Modal.Header>
					<Modal.Content scrolling>
						<Message warning size="medium">
							Copyright 2019, Imersia Group Ltd (NZ), All rights reserved.
						</Message>
						<Message style={{wordWrap: "break-word"}} size="medium">
							<Message.Header>License Terms and Conditions</Message.Header>
							<Message.List items={licenseterms} />
						</Message>
						<Message negative size="medium">
							By downloading and/or using this software, you agree to the license terms and conditions stated above.
						</Message>
						<Grid columns='one'>
							<Grid.Row>
								<Grid.Column>
						            <Form>
						                <Form.Field>
											<label>Server URL - this has to be the same as the url parameter in mrserver.ini</label>
						                    <Form.Input placeholder="Server URL" name="url" type="text" value={this.state.licenses[index].url} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
						                </Form.Field>
						                <Form.Field>
											<label>JSON Array of App IDs (string); one of these must be used in every API call as the developerid.</label>
											<Form.Input placeholder="JSON Array of App IDs (strings)" name="appids" type="text" value={this.stringifyJSON(this.state.licenses[index].appids)} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
											<Segment basic>Example: ["com.imersia.portal", "com.imersia.webapp"]. Note that the standard admin and http portals included with the MRServer use "com.imersia.portal" and "com.imersia.webapp" respectively, so if using these default portals, be sure to include them in the App IDs above.  App IDs can be any string of characters, but using a revserse DNS format can be convenient.</Segment>
										</Form.Field>
						            </Form>
								</Grid.Column>
							</Grid.Row>
				        </Grid>
					</Modal.Content>
					<Modal.Actions>
						<Button basic positive onClick={this.saveLicenses}>
							Done
						</Button>
					</Modal.Actions>
				</Modal>
			);
		}
		else {
			return (<></>);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	viewLicenseModal = () => {
		let index = this.state.index;

		if (this.state.viewinglicense)
		{
			return (
				<Modal open={this.state.viewinglicense} centered={false}>
					<Modal.Header>
						License Code for {this.state.licenses[index].url}
					</Modal.Header>
					<Modal.Content scrolling>
						<Segment basic size="big">Copy and paste this into your mrserver.toml file.</Segment>
						<Segment style={{wordWrap: "break-word", fontFamily: "monospace, monospace"}}>
							[license]
							code="
							<ImersiaLicense
								imersiasdk={this.props.imersiasdk}
								url={this.state.licenses[index].url}
								type={this.state.licenses[index].type}
								geobotid={this.state.licensesGeobotID}
								contextids={JSON.stringify([this.props.contextid, this.state.licensesGeobotContextID])} />
							"
						</Segment>
					</Modal.Content>
					<Modal.Actions>
						<Button basic positive onClick={this.closeViewModal}>
							Done
						</Button>
					</Modal.Actions>
				</Modal>
			);
		}
		else {
			return (<></>);
		}
	}
	openViewModal = (e, data) =>
	{
		this.setState({index: data.index, viewinglicense: true});
	}
	closeViewModal = (e, data) =>
	{
		this.setState({viewinglicense: false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	deleteLicenseModal = () => {
		if (this.state.deletinglicense)
		{
			return (
				<Modal open={this.state.deletinglicense} centered={false}>
					<Modal.Header>
						Delete License
					</Modal.Header>
					<Modal.Content scrolling>
						<Segment basic size="big">Delete selected license?  Once it is deleted, it is gone forever...</Segment>
					</Modal.Content>
					<Modal.Actions>
						<Button basic color="green" onClick={this.closeDeleteModal}>
							no
						</Button>
						<Button basic color="red" onClick={this.deleteLicense}>
							yes
						</Button>
					</Modal.Actions>
				</Modal>
			);
		}
		else {
			return (<></>);
		}
	}
	openDeleteModal = (e, data) =>
	{
		this.setState({index: data.index, deletinglicense: true});
	}
	closeDeleteModal = (e, data) =>
	{
		this.setState({deletinglicense: false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		return (
			<>
				<Table celled unstackable>
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell colSpan='6'>{this.props.title}</Table.HeaderCell>
						</Table.Row>
						<Table.Row>
							<Table.HeaderCell textAlign='center' width={1}>
								<Button basic color="blue" onClick={this.newLicense}>new</Button>
							</Table.HeaderCell>
							<Table.HeaderCell width={6}>MRServer URL</Table.HeaderCell>
							<Table.HeaderCell width={7}>App IDs</Table.HeaderCell>
							<Table.HeaderCell width={1}>Edit</Table.HeaderCell>
							<Table.HeaderCell width={1}>License</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						{this.licenseList ()}
					</Table.Body>
				</Table>
				{this.editLicenseModal ()}
				{this.deleteLicenseModal ()}
				{this.viewLicenseModal ()}
			</>
		);
	}
};
// --------------------------------------------------------------------------------------------------------

export default LicenseViewer;
