// --------------------------------------------------------------------------------------------------------
// A dynamically updating channel title.
// --------------------------------------------------------------------------------------------------------
import React, { Component } from 'react';
import { } from 'semantic-ui-react'

class ImersiaLicense extends Component {
    constructor (props)
    {
        super (props);
        this.state = {
            licensecode : ""
        }
    }


    componentDidMount = () => {
        let contextids = this.props.contextids
        let geobotid = this.props.geobotid;
        let type = this.props.type;
        let url = this.props.url;

		this.props.imersiasdk.GET("vars", {"category":"licensor", "key":"generate", "url":url, "geobotid": geobotid, "type": type, "contextids": contextids})
		.then((resp) => {
			this.setState({licensecode:resp.json.value});
		})
        .catch((err) => {
            this.setState({licensecode:""});
        });
    }

    render ()
    {
        return (<>{this.state.licensecode}</>);
    }
}
// --------------------------------------------------------------------------------------------------------


export default ImersiaLicense;
