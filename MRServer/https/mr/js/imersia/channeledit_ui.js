// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Manages UI panel to edit a Channel
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


IMERSIA.ChannelEdit_UI = function ()
{
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Public Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Private Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    var panelside = "right";
    var panelname = "channeleditsidebar";
    var currentlySelectedMetadata = "";
    var dirty = false;

    // Pointer to self
    var self = this;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Initialise the menu
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Initialise = function ()
    {
        $('.'+panelname).sidebar({dimPage:false, closable: false, transition: "overlay", mobileTransition: "overlay"});

        // Save Channel
        $("body").on("click", "#savechannelbutton", function (e)
        {
            SaveChannel();
        });

        // Add new Metadata
        $("body").on("click", "#addchannelmetadatabutton", function (e) {
            APP.MetadataEdit_UI.NewMetadata("channel");
        });

        // Edit Metadata
        $("body").on("click", ".editchannelmetadata", function (e)
        {
            currentlySelectedMetadata = e.currentTarget.parentNode.parentNode.id;
            APP.MetadataEdit_UI.EditMetadata(currentlySelectedMetadata, "channel");
        });

        // Delete Metadata
        $("body").on("click", ".deletechannelmetadata", function(e) {
            currentlySelectedMetadata = e.currentTarget.parentNode.parentNode.id;
            APP.MetadataEdit_UI.DeleteMetadataDialog(currentlySelectedMetadata, "channel");
        });

        // Delete File
        $("body").on("click", ".deletechannelfile", function(e) {
            var FileToDelete = e.currentTarget.name;
            APP.SDK.DeleteFromAPI(APP.companion.sessionid, "files", {channelid:APP.currentlySelectedChannel, filename:FileToDelete},
                function(result)
                {
                    UpdateFileTable();
                    APP.Companion_UI.AddInfoMessage("File " + FileToDelete + " deleted.");
                },
                function(error)
                {
                    APP.Companion_UI.AddErrorMessage("File " + FileToDelete + " NOT deleted.");
                }
            );
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Open the menu (and close any others on the same side)
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Open = function ()
    {
        // Close any LHS panels
        $(".sidebar."+panelside).not("."+panelname).sidebar("hide");

        // Open this one.
        $("."+panelname).sidebar("show");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Close the menu
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Close = function ()
    {
        $("."+panelname).sidebar("hide");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Close all menus on the same side except this one
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CloseAllBut = function ()
    {
        $(".sidebar."+panelside).not("."+panelname).sidebar('setting', 'transition', 'overlay').sidebar("hide");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Save the results of editing the channel details
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SaveChannel = function (callback)
    {
        // Save Channel
        var thisChannel = APP.companion.channels[APP.currentlySelectedChannel];

        // Get the metadata object for this channel
        var Channelmetadata = thisChannel.metadata;

        var MetadataUpdates = {};
        // Create a set of metadata from the main UI and the variables that can then be saved out in one go to the Channel

        // Go through each element of the Variables UI and gather the Metadata
        $(".channel_variable_record").each(function (index, DOMelement) {
            var metadataid = DOMelement.id;

            // Get the key
            var key = $('#' + metadataid + "_key").text();

            // Get the value, trying to parse it as JSON
            var value = null;
            try {
                value = JSON.parse($('#' + metadataid + "_value").text());
            } catch (e) {
                value = $('#' + metadataid + "_value").text();
            }

            // Check what is to be done with each metadata variable
            if ($('#' + metadataid).hasClass("deleted"))
            {
                MetadataUpdates[key] = {action:"delete", metadataid:metadataid};
            }
            else
            {
                if ($('#' + metadataid).hasClass("new"))
                {
                    MetadataUpdates[key] = {action:"new", key:key, value:value};
                }
                else
                {
                    MetadataUpdates[key] = {action:"update", metadataid:metadataid, key:key, value:value};
                }
            }
        });

        // Go through all the Metadata and make the appropriate changes
        Object.keys(MetadataUpdates).forEach(function(key, index)
        {
            var MetadataDetails = MetadataUpdates[key];

            switch (MetadataDetails.action)
            {
                case "delete":
                    Channelmetadata.Delete(APP.companion.sessionid, MetadataDetails.metadataid);
                    break;
                case "update":
                    Channelmetadata.Set(APP.companion.sessionid, MetadataDetails.metadataid, MetadataDetails.key, MetadataDetails.value);
                    break;
                case "new":
                    Channelmetadata.Set(APP.companion.sessionid, undefined, MetadataDetails.key, MetadataDetails.value);
                break;
            }
        });

        // Set up the Details
        var Channeldetails = {
            channelid:$('#com_imersia_channel_channelid').val(),
            name:$('#com_imersia_channel_name').val(),
            description:$('#com_imersia_channel_description').val(),
            class:$('#com_imersia_channel_class').val(),
            hidden:$('#com_imersia_channel_hidden').is(':checked'),
            imageurl:$('#com_imersia_channel_imageurl').val()
        };

        // Save it to the API
        thisChannel.Save(APP.companion.sessionid, Channeldetails, function (result) {
        }, function (error) {
            if (error.error == "name")
            {
                APP.Companion_UI.AddErrorMessage("Error renaming channel - the new name is already being used.");
            }
            else {
                APP.Companion_UI.AddErrorMessage("Error saving channel");
            }
        });

        // Close this menu
        APP.ChannelEdit_UI.Close();
        APP.ChannelList_UI.SelectChannel();
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Set the details of the UI
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetDetails = function(details) {
        var imageurl = APP.Classes.InterpretContentURL(details.channelid, details.imageurl, "size=medium");

        details.id = details.channelid;
        details.image = imageurl;

        $('#channeleditmain').html(
            APP.Classes.ConvertClassToHTML("com.imersia.channel", details)
        );

        CreateTableFromMetadata(details.metadata);

        CreateTableFromFiles(details.fileurls);
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreateTableFromMetadata = function(metadata)
    {
        var html = "";

        html += '<div style="overflow-x:scroll"><table style="width:650px" class="ui compact table">';
        html += '<thead> <tr> <th></th> <th></th> <th style="width:300px">Key</th> <th style="width:300px">Value</th> </tr> </thead>';
        html += '<tbody id="channelvariablestable">';
        html += '</tbody>'
        html += '<tfoot class="full-width"> <tr> <th colspan="4"> \
                    <div class="ui basic button green" id="addchannelmetadatabutton">Add</div> \
                    </th> </tr> </tfoot>';
        html += '</table></div>';

        $('#channeleditvariables').html(html);

        Object.keys(metadata.metadata).forEach(function (metadataid)
        {
            APP.MetadataEdit_UI.AddRowToMetadataTable(metadataid, "channel", metadata.metadata[metadataid].key, metadata.metadata[metadataid].value);
        });

        return html;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Upload an image to the Channel and change the imageurl
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var UpdateImage = function (details, thefile, callback)
    {
        var channelid = details.channelid;

        APP.SDK.PostFileToAPI(APP.companion.sessionid, 'files', details, thefile, function (result) {
            if (result.hasOwnProperty("fileurls"))
            {
                var fileurl = result.fileurls[0];
                APP.SDK.PutToAPI(APP.companion.sessionid, "channels", {channelid: channelid}, {imageurl:fileurl}, function (result) {
                    APP.companion.channels[APP.currentlySelectedChannel].imageurl = fileurl;
                    callback (fileurl);
                },
                function (error) {
                    callback ("");
                });
            };
        }, function (error) {
            callback("")
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var UpdateFileTable = function ()
    {
        APP.SDK.GetFromAPI(APP.companion.sessionid, 'files', {channelid : APP.currentlySelectedChannel}, function(files)
        {
            var CurrentChannel = APP.companion.channels[APP.currentlySelectedChannel];
            CurrentChannel.fileurls = files.fileurls;

            CreateTableFromFiles(CurrentChannel.fileurls);
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreateTableFromFiles = function(fileurls)
    {
        var html = "";

        html += '<div style="overflow-x:scroll"><table class="ui compact table">';
        html += '<thead> <tr> <th></th> <th></th> <th></th> </tr> </thead>';
        html += '<tbody id="channelfilesstable">';
        html += '</tbody>'
        html += '<tfoot class="full-width"> <tr> <th colspan="4"> \
                    <div class="ui basic button green fileloader" id="com_imersia_channel_file">Load File</div> \
                    </th> </tr> </tfoot>';
        html += '</table></div>';

        $('#channeleditfiles').html(html);

        if (fileurls != undefined)
        {
            for (let fileurl of fileurls) {
                AddRowToFilesTable(fileurl);
            }
        };

        return html;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Add a row to the files table
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var AddRowToFilesTable = function(fileurl)
    {
        var filename = fileurl.split("/")[3];
        var html = "";

        html += '<tr class="file_record">';

        html += '<td>'
        html += '<button name="' + filename + '" class="ui mini basic icon button red deletechannelfile"><i class="delete icon"></i></button>'
        html += '</td>';
        html += '<td>' + filename + '</td>';
        html += "</tr>";

        $("#channelfilesstable").append(html);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Clear the menu
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ClearOutput = function()
    {
        $('#channeleditmain').html("");
        $('#channeleditexperience').html("");
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    this.Initialise = Initialise;
    this.Open = Open;
    this.Close = Close;
    this.CloseAllBut = CloseAllBut;
    this.SetDetails = SetDetails;
    this.ClearOutput = ClearOutput;
    this.UpdateImage = UpdateImage;
    this.UpdateFileTable = UpdateFileTable;
}
