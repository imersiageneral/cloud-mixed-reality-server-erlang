// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Description: Companion datastructure - a writethrough cache.
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Constructor
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion = function ()
{
    this.userid = "";
    this.sessionid = "";
    this.useremail = "";
    this.details = {
        firstname : "",
        surname : "",
        nickname : ""
    };

    this.fileurls = [];
    this.metadata = {};

    this.channels = {};

    this.websocket = null;
    this.callbacks = {};
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion.prototype.SetLogin = function(sessionid, useremail)
{
    var self = this;
    self.sessionid = sessionid;
    self.useremail = useremail;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// (re)load the companion details from the API, which in turn loads the Channels and Geoboots
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion.prototype.Load = function (callback, errorcallback)
{
    var self = this;

    if(!(("WebSocket" in window) || ("MozWebSocket" in window))){
        APP.Companion_UI.AddErrorMessage('Cannot connection to your Companion because websockets are not supported.');
    }
    else
    {
        // Load the Companion details from the API
        APP.SDK.GetFromAPI(self.sessionid, 'user', {useremail: self.useremail}, function (userdata)
        {
            self.userid = userdata.userid;
            self.details = userdata.details;

            APP.SDK.GetFromAPI(self.sessionid, "metadata", {key: "com.imersia.realitybrowser", userid: userdata.userid}, function(usermetadata)
            {
                self["com.imersia.realitybrowser"] = usermetadata.value;
            },function (e)
            {
                self["com.imersia.realitybrowser"] = {};
            });

            // Connect to the Companion via Secure Websocket to monitor changes and messages
            var ws = window.WebSocket || window.MozWebSocket;
            self.websocket = new ws("wss://" + window.location.host + "/wotcha/" + self.userid);
            self.websocket.onopen = function(evt) {
                // All going well, load the channels
                self.LoadChannels(function () {
                    if (callback) callback();
                }, function (error) {
                    if (errorcallback) errorcallback(error);
                });
                APP.Companion_UI.ClearOutput();
                APP.Companion_UI.AddInfoMessage('connected to your companion');
            };
            self.websocket.onclose = function(evt) {
                self.WotchaClose(evt);
            };
            self.websocket.onmessage = function(evt) { self.Wotcha.call(self, evt) };
            self.websocket.onerror = function(evt) {
                self.WotchaError(evt)
            };
        },
        function(error)
        {
            self.userid = "";
            if (errorcallback) errorcallback(error);
        });
    }
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion.prototype.Disconnect = function()
{
    var self = this;

    if (self.websocket) self.websocket.close();
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Wotcha / WebSocket functions
// Messages come via the websocket connection to the Companion on the MR Server.  Each Channel or Geobot change results in a message - these then need to be interpreted and dealt with.
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion.prototype.Wotcha = function (evt)
{
    var self = this;

    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var UpdateChannel = function (message)
    {
        var channelid = message.channel_set.channelid;
        self.channels[channelid].Wotcha.call(self.channels[channelid], message);
        var imageurl = APP.Classes.InterpretContentURL(message.channel_set.channelid, message.channel_set.imageurl, "size=thumb&shape=round");
        APP.Companion_UI.AddCompanionMessage ("changed", imageurl, message.channel_set.name);
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var RenameChannel = function (message)
    {
        var channelid = message.channel_rename.channelid;
        self.channels[channelid].Wotcha.call(self.channels[channelid], message);
        var imageurl = APP.Classes.InterpretContentURL(message.channel_rename.channelid, message.channel_rename.imageurl, "size=thumb&shape=round");
        APP.Companion_UI.AddCompanionMessage ("renamed", imageurl, message.channel_rename.name);
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var UpdateGeobot = function (message)
    {
        var channelid = message.geobot_set.channelid;
        var geobotid = message.geobot_set.geobotid;
        self.channels[channelid].geobots[geobotid].Wotcha.call(self.channels[channelid].geobots[geobotid], message);
        var imageurl = APP.Classes.InterpretContentURL(message.geobot_set.geobotid, message.geobot_set.imageurl, "size=thumb&shape=round");
        APP.Companion_UI.AddCompanionMessage ("changed", imageurl, message.geobot_set.name, geobotid);
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var AddNewGeobot = function (message)
    {
        var details = message.geobot_new;
        var channelid = message.geobot_new.channelid;
        var geobotid = message.geobot_new.geobotid;
        var newGeobot = new IMERSIA.Geobot(details);
        newGeobot.metadata = new IMERSIA.Metadata(geobotid);
        newGeobot.metadata.Load(APP.companion.sessionid, function (result) {
            self.channels[channelid].geobots[geobotid] = newGeobot;

            self.channels[channelid].Wotcha.call(self.channels[channelid], message);
            var imageurl = APP.Classes.InterpretContentURL(message.geobot_new.geobotid, message.geobot_new.imageurl, "size=thumb&shape=round");
            APP.Companion_UI.AddCompanionMessage ("created", imageurl, message.geobot_new.name);
        });
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var DeleteGeobot = function (message)
    {
        var geobotid = message.geobot_delete.geobotid;
        var GeobottoDelete = APP.companion.FindGeobot(geobotid);
        var channelid = GeobottoDelete.channelid;
        if (GeobottoDelete != null) GeobottoDelete.Wotcha.call(GeobottoDelete, message);

        var imageurl = APP.Classes.InterpretContentURL(geobotid, GeobottoDelete.imageurl, "size=thumb&shape=round");
        APP.Companion_UI.AddCompanionMessage ("deleted", imageurl, GeobottoDelete.name);
        if (APP.currentlySelectedGeobot == message.geobot_delete.geobotid) APP.currentlySelectedGeobot = "";

        delete self.channels[channelid].geobots[geobotid].metadata;
        delete self.channels[channelid].geobots[geobotid];
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var AddNewChannel = function (message)
    {
        // Create the new channel in the datastructure
        var details = message.channel_new;
        var newChannel = new IMERSIA.Channel(details);
        self.channels[details.channelid] = newChannel;

        // Callback anything that is watching the companion that there is a new channel
        Object.keys(self.callbacks).forEach (function (ID) {
            self.callbacks[ID] ( message );
        });

        // Wotch the channel for any changes
        self.websocket.send(JSON.stringify({ command : 'wotcha', sessionid : self.sessionid, parameters : { id : details.channelid } }));

        // Add it to the UIs
        APP.ChannelList_UI.AddChannel(details);
        APP.ExperienceList_UI.AddExperience(details);
        var imageurl = APP.Classes.InterpretContentURL(message.channel_new.channelid, message.channel_new.imageurl, "size=thumb&shape=round");
        APP.Companion_UI.AddCompanionMessage ("created", imageurl, message.channel_new.name);
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var DeleteChannel = function (message)
    {
        var channelid = message.channel_delete.channelid;
        var channelname = self.channels[channelid].name;
        // Send delete message to all wotching Geobots on the channel
        Object.keys(self.channels[channelid].geobots).forEach(function(geobotid)
        {
            var geobot = self.channels[channelid].geobots[geobotid];
            var geobotmessage = {geobot_delete : geobot.geobotid};
            geobot.Wotcha.call(geobot, geobotmessage);
        });
        // Send delete message to all wotching the channel
        self.channels[channelid].Wotcha.call(self.channels[channelid], message);

        APP.ChannelList_UI.RemoveChannel(channelid);
        APP.ExperienceList_UI.RemoveExperience(channelid);
        var imageurl = APP.Classes.InterpretContentURL(channelid, self.channels[channelid].imageurl, "size=thumb&shape=round");
        APP.Companion_UI.AddCompanionMessage ("deleted", imageurl, self.channels[channelid].name);

        // Delete it from the datastructure (which also deletes the Geobots)
        delete self.channels[channelid];
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ProcessGeobotMetadata = function (message)
    {
        var geobotid = message.geobot_metadata.geobotid;
        var GeobotToNotify = APP.companion.FindGeobot(geobotid);
        if (GeobotToNotify != null) GeobotToNotify.Wotcha.call(GeobotToNotify, message);
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ProcessChannelMetadata = function (message)
    {
        var channelid = message.channel_metadata.channelid;
        var ChannelToNotify = APP.companion.channels[channelid];
        if (ChannelToNotify != null) ChannelToNotify.Wotcha.call(ChannelToNotify, message);
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ProcessUserMetadata = function (message)
    {
        Object.keys(self.callbacks).forEach (function (ID) {
            self.callbacks[ID] ( message );
        });
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var DeleteChannelMetadata = function (message)
    {
        var channelid = message.channel_metadata_delete.channelid;
        var ChannelToNotify = APP.companion.channels[channelid];
        if (ChannelToNotify != null) ChannelToNotify.Wotcha.call(ChannelToNotify, message);
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var DeleteGeobotMetadata = function (message)
    {
        var geobotid = message.geobot_metadata_delete.geobotid;
        var GeobotToNotify = APP.companion.FindGeobot(geobotid);
        if (GeobotToNotify != null) GeobotToNotify.Wotcha.call(GeobotToNotify, message);
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ProcessEventChange = function (message)
    {
        var newstate = message.geobot_state_change.newstate;

        if (message.geobot_state_change.hasOwnProperty("geobotid"))
        {
            var senderGeobot = APP.companion.FindGeobot(message.geobot_state_change.geobotid);
            if (senderGeobot != null)
            {
                var senderAutomation = senderGeobot.automations.Get(message.geobot_state_change.automationid);
                if (senderAutomation)
                {
                    senderGeobot.Wotcha.call(senderGeobot, message);
                    var imageurl = APP.Classes.InterpretContentURL(senderGeobot.geobotid, senderGeobot.imageurl, "size=thumb&shape=round");
                    APP.Companion_UI.AddCompanionMessage(senderAutomation.name + " changed state to " + newstate, imageurl, senderGeobot.name, message.geobot_state_change.geobotid);
                }
            }
        }
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ProcessStatusUpdate = function (message)
    {
        if (message.geobot_status.hasOwnProperty("geobotid"))
        {
            var geobotid = message.geobot_status.geobotid;
            var GeobotToNotify = APP.companion.FindGeobot(geobotid);
            if (GeobotToNotify != null) GeobotToNotify.Wotcha.call(GeobotToNotify, message);
        }
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ProcessTrigger = function (message)
    {
        if (message.trigger.hasOwnProperty("geobotid"))
        {
            var geobotid = message.trigger.geobotid;
            var GeobotToNotify = APP.companion.FindGeobot(geobotid);
            if (GeobotToNotify != null) GeobotToNotify.Wotcha.call(GeobotToNotify, message);
        }
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ProcessAutomations = function (message)
    {
        if (message.geobot_automations.hasOwnProperty("geobotid"))
        {
            var geobotid = message.geobot_automations.geobotid;
            var GeobotToNotify = APP.companion.FindGeobot(geobotid);
            if (GeobotToNotify != null) GeobotToNotify.Wotcha.call(GeobotToNotify, message);
        }
    }
    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    console.debug("websocket : " + evt.data);

    if (evt.data == 'ping')
    {
        self.websocket.send('pong');
    }
    else
    {
        var message = JSON.parse(evt.data);
        if (message.hasOwnProperty("wotcha"))
        {
            if (message.wotcha.hasOwnProperty("geobot_set")) { UpdateGeobot (message.wotcha);  }
            else if (message.wotcha.hasOwnProperty("geobot_delete")) { DeleteGeobot (message.wotcha);  }
            else if (message.wotcha.hasOwnProperty("geobot_new")) { AddNewGeobot (message.wotcha);  }
            else if (message.wotcha.hasOwnProperty("channel_new")) { AddNewChannel (message.wotcha);  }
            else if (message.wotcha.hasOwnProperty("channel_set")) { UpdateChannel (message.wotcha); }
            else if (message.wotcha.hasOwnProperty("channel_rename")) { RenameChannel (message.wotcha); }
            else if (message.wotcha.hasOwnProperty("channel_delete")) { DeleteChannel (message.wotcha);  }
            else if (message.wotcha.hasOwnProperty("geobot_metadata")) { ProcessGeobotMetadata (message.wotcha); }
            else if (message.wotcha.hasOwnProperty("channel_metadata")) { ProcessChannelMetadata (message.wotcha); }
            else if (message.wotcha.hasOwnProperty("geobot_metadata_delete")) { DeleteGeobotMetadata (message.wotcha); }
            else if (message.wotcha.hasOwnProperty("channel_metadata_delete")) { DeleteChannelMetadata (message.wotcha); }
            else if (message.wotcha.hasOwnProperty("user_metadata")) { ProcessUserMetadata (message.wotcha); }
            else if (message.wotcha.hasOwnProperty("geobot_state_change")) { ProcessEventChange(message.wotcha) }
            else if (message.wotcha.hasOwnProperty("geobot_status")) { ProcessStatusUpdate(message.wotcha) }
            else if (message.wotcha.hasOwnProperty("geobot_automations")) { ProcessAutomations(message.wotcha) }
            else if (message.wotcha.hasOwnProperty("trigger")) {ProcessTrigger(message.wotcha) }
        }
        else if (message.hasOwnProperty("message"))
        {
            if (message.message.hasOwnProperty("geobotid"))
            {
                var senderGeobot = APP.companion.FindGeobot(message.message.geobotid);
                var imageurl = APP.Classes.InterpretContentURL(senderGeobot.geobotid, senderGeobot.imageurl, "size=thumb&shape=round");
                if (senderGeobot != null) APP.Companion_UI.AddCompanionMessage("says <i>" + message.message.message + "</i>", imageurl, senderGeobot.name, message.message.geobotid);
            }
        }
    };
};
IMERSIA.Companion.prototype.WotchaClose = function (evt)
{
    APP.Companion_UI.AddErrorMessage('disconnected from your companion');
    // APP.playing = false;

    APP.Logout();
};
IMERSIA.Companion.prototype.WotchaError = function (evt)
{
    APP.Companion_UI.AddErrorMessage("There was a problem obtaining a direct connection.");
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Change the companion details
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion.prototype.Save = function (details, callback, errorcallback)
{
    var self = this;

    // Save the Companion details to the API
    APP.SDK.PutToAPI(self.sessionid, 'user', {}, {firstname : details.firstname, surname : details.surname, nickname: details.nickname}, function (result)
    {
        self.details.firstname = details.firstname;
        self.details.surname = details.surname;
        self.details.nickname = details.nickname;
        if (callback) callback ();
    },
    function(error)
    {
        if (errorcallback) errorcallback(error);
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Load the user's channels and Geobots
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion.prototype.LoadChannels = function (callback, errorcallback)
{
    var self = this;

    // Clear any previously loaded Channels for garbage collection to dispose of
    self.channels = {};

    // Get the list of channel for this user
    APP.SDK.GetFromAPI(self.sessionid, 'channels', {}, function (channels)
    {
        // Create each channel (and thus in turn each Geobot in that channel)
        var channelsCount = channels.length;
        channels.forEach(function (channel) {
            // Create the channel data structure
            var newChannel = new IMERSIA.Channel(channel);

            // Load the Geobots on the channel
            newChannel.LoadGeobots(self.sessionid, function () {

                // Load the Metadata and Files on the channel (and ignore any errors)
                newChannel.LoadFilesAndMetadata(self.sessionid, function () {
                    if ((--channelsCount == 0) && callback) callback(); //LoadChannelsStep2(self.sessionid, channel.channelid, self.channels, newChannel, callback);
                }, function () {
                    if ((--channelsCount == 0) && callback) callback(); //LoadChannelsStep2(self.sessionid, channel.channelid, self.channels, newChannel, callback);
                });

            });
            self.channels[channel.channelid] = newChannel;

            // Add this to the channels inspector
            APP.ChannelList_UI.AddChannel(newChannel);
            APP.ExperienceList_UI.AddExperience(newChannel);

            // Wotch the channel for any changes
            self.websocket.send(JSON.stringify({ command : 'wotcha', sessionid : self.sessionid, parameters : { id : channel.channelid } }));
        });
    },
    function(error)
    {
        if (errorcallback) errorcallback(error);
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var LoadChannelsStep2 = function (sessionid, channelid, channels, newChannel, callback)
{
    channels[channelid] = newChannel;

    // Add this to the channels inspector
    APP.ChannelList_UI.AddChannel(newChannel);
    APP.ExperienceList_UI.AddExperience(newChannel);

    // Wotch the channel for any changes
    self.websocket.send(JSON.stringify({ command : 'wotcha', sessionid : sessionid, parameters : { id : channelid } }));

    // if (callback) callback();
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion.prototype.FindGeobot = function (geobotidin)
{
    var self = this;
    var thisGeobot = null;
    Object.keys(self.channels).forEach(function (channelid) {
        var channel = self.channels[channelid];
        Object.keys(channel.geobots).forEach(function(geobotid) {
            if (geobotid == geobotidin) thisGeobot = channel.geobots[geobotid];
        });
    });
    return thisGeobot;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Create a new channel
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion.prototype.NewChannel = function(details, callback, errorcallback)
{
    APP.SDK.PostToAPI(self.sessionid, "channels", {}, details, function (result) {
        if (callback) callback ();
    }, function (error) {
        if (errorcallback) errorcallback(error);
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion.prototype.DeleteChannel = function ()
{
    var self = this;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion.prototype.Register = function (ID, callback)
{
    this.callbacks[ID] = callback;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion.prototype.Deregister = function (ID)
{
    delete this.callbacks[ID];
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion.prototype.SendEvent = function (ID, message, parameters)
{
    var self = this;
    parameters = parameters || {};

    if (message != "")
    {
        if (self.websocket)
        {
            if (self.websocket.readyState == self.websocket.OPEN)
            {
                var senderGeobot = APP.companion.FindGeobot(ID);
                if (senderGeobot != null)
                {
                    APP.Companion_UI.AddUserMessage(" <i>" + message + "</i> to " + senderGeobot.name);
                    var txtjson = {
                        command : 'event',
                        sessionid : self.sessionid,
                        parameters : {
                            id : ID,
                            event : message,
                            parameters : parameters
                        }
                    };
                    self.websocket.send(JSON.stringify(txtjson));
                }
            }
            else
            {
                APP.Companion_UI.AddErrorMessage("not connected");
            }
        }
        else
        {
            APP.Companion_UI.AddErrorMessage("not connected");
        }
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Companion.prototype.SendCommand = function (ID, command, parameters)
{
    var self = this;

    parameters = parameters || {};
    parameters.id = ID;

    if (command != "")
    {
        if (self.websocket)
        {
            if (self.websocket.readyState == self.websocket.OPEN)
            {
                var senderGeobot = APP.companion.FindGeobot(ID);
                if (senderGeobot != null)
                {
                    var txtjson = {
                        command : command,
                        sessionid : self.sessionid,
                        parameters : parameters
                    };
                    self.websocket.send(JSON.stringify(txtjson));
                }
            }
            else
            {
                APP.Companion_UI.AddErrorMessage("not connected");
            }
        }
        else
        {
            APP.Companion_UI.AddErrorMessage("not connected");
        }
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
