// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Manages UI panel for a Geobot
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


IMERSIA.Instruct_UI = function ()
{
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Public Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Private Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    var CurrentAutomations = [];
    var CurrentAutomationIndex = 0;
    var CurrentTransitionIndex = 0;
    var CurrentActionIndex = 0;
    var CurrentCommands = [];

    var jsoneditor = null;
    var jsoneditoroptions = {
        onChange: function() {
            if (CurrentAutomations.length > 0)
            {
                var UpdatedAutomation = jsoneditor.get();
                UpdatedAutomation.automationid = CurrentAutomations[CurrentAutomationIndex].automationid;
                UpdatedAutomation.name = CurrentAutomations[CurrentAutomationIndex].name;

                CurrentAutomations[CurrentAutomationIndex] = UpdatedAutomation;

                var AutomationsContent = CreateAutomationTable(UpdatedAutomation);
                $('#instructautomationtable > tbody').html(AutomationsContent);
            }
            else {
                $('#instructautomationtable > tbody').html("");
            }
        }
    };

    // Pointer to self
    var self = this;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Setup all the interactions etc
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Initialise = function ()
    {
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Main Instruct Box
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $("body").on("click", "#instructguided", function (e) {
            jsoneditor.set(CurrentAutomations[CurrentAutomationIndex]);
            $("#instructautomationtable").show();
            $("#instructautomationjsondiv").hide();
            $("#instructguided").removeClass("basic");
            $("#instructexpert").addClass("basic");
        });

        $("body").on("click", "#instructexpert", function (e) {
            jsoneditor.set(CurrentAutomations[CurrentAutomationIndex]);
            $("#instructautomationtable").hide();
            $("#instructautomationjsondiv").show();
            $("#instructguided").addClass("basic");
            $("#instructexpert").removeClass("basic");
        });

        $("body").on("click", "#instructcancel", function (e) {
            $('#instructbox').modal({allowMultiple: false}).modal("hide");
        });

        $("body").on("click", "#instructhelp", function (e) {
            // $('#instructbox').modal({allowMultiple: false}).modal("hide");
            $('#instructhelpbox').modal({allowMultiple: false}).modal("show");
        });

        $("body").on("click", "#instructhelpdone", function (e) {
            // $('#instructhelpbox').modal({allowMultiple: false}).modal("hide");
            $('#instructbox').modal({allowMultiple: false}).modal("show");
        });

        $("body").on("click", "#instructok", function (e) {
            var counter = CurrentAutomations.length;
            $.each(CurrentAutomations, function (Index, Automation)
            {
                if (Automation.automationid == "new")
                {
                    delete(Automation.automationid);
                    var Headers = {geobotid : APP.currentlySelectedGeobot};
                    APP.SDK.PostToAPI(APP.companion.sessionid, 'geobots/automations', Headers, Automation, function (result)
                    {
                        if (--counter == 0)
                        {
                            $('#instructbox').modal({allowMultiple: false}).modal("hide");
                        }
                    },
                    function(error)
                    {
                        $('#instructbox').modal({allowMultiple: false}).modal("hide");
                    });
                }
                else
                {
                    var Headers = {geobotid : APP.currentlySelectedGeobot, automationid : Automation.automationid};
                    APP.SDK.PutToAPI(APP.companion.sessionid, 'geobots/automations', Headers, Automation, function (result)
                    {
                        if (--counter == 0)
                        {
                            $('#instructbox').modal({allowMultiple: false}).modal("hide");
                        }
                    },
                    function(error)
                    {
                        $('#instructbox').modal({allowMultiple: false}).modal("hide");
                    });
                }
            });
        });
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Add and Edit Transition
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $("body").on("click", "#instructnewtransition", function (e) {
            var NewTransition = {
                state:"",
                event:"",
                newstate:"",
                actions:[]
            };
            CurrentAutomations[CurrentAutomationIndex].transitions.push(NewTransition);
            $('#instructautomationtable > tbody').html(CreateAutomationTable(CurrentAutomations[CurrentAutomationIndex]));
        });

        $("body").on("click", "#transitioneditcancel", function (e) {
            $('#instructbox').modal({allowMultiple: false}).modal("show");
        });

        $("body").on("click", "#transitioneditok", function (e) {
            var theevent = $('#transitioneditevent').val();

            CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex].state = $('#transitioneditstate').val();
            CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex].event = theevent;
            CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex].newstate = $('#transitioneditnewstate').val();

            if($("#transitioneditvisible").is(':checked'))
            {
                if (! CurrentAutomations[CurrentAutomationIndex].hasOwnProperty("commands"))
                {
                    CurrentAutomations[CurrentAutomationIndex].commands = [];
                }
                var i = CurrentAutomations[CurrentAutomationIndex].commands.length;
                var alreadyexists = false;
                while (i--) {
                    if (CurrentAutomations[CurrentAutomationIndex].commands[i] == theevent)
                    {
                        alreadyexists = true;
                    }
                }
                if (!alreadyexists)
                {
                    CurrentAutomations[CurrentAutomationIndex].commands.push(theevent);
                }
            }
            else
            {
                if (CurrentAutomations[CurrentAutomationIndex].hasOwnProperty("commands"))
                {
                    var i = CurrentAutomations[CurrentAutomationIndex].commands.length;
                    var position = -1;
                    while (i--) {
                        if (CurrentAutomations[CurrentAutomationIndex].commands[i] == theevent)
                        {
                            position = i;
                        }
                    }
                    if (position != -1)
                    {
                        CurrentAutomations[CurrentAutomationIndex].commands.splice(i,1);
                    }
                }
            }

            $('#instructautomationtable > tbody').html(CreateAutomationTable(CurrentAutomations[CurrentAutomationIndex]));

            $('#instructbox').modal({allowMultiple: false}).modal("show");
        });
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Row (Transition) Buttons
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $("body").on("click", ".instructrowdelete", function (e) {
            var index = Number(this.dataset.index)
            CurrentAutomations[CurrentAutomationIndex].transitions.splice(index, 1);
            $('#instructautomationtable > tbody').html(CreateAutomationTable(CurrentAutomations[CurrentAutomationIndex]));
        });

        $("body").on("click", ".instructrowedit", function (e) {
            CurrentTransitionIndex = Number(this.dataset.index);

            var eventiscommand = false;
            if (CurrentAutomations[CurrentAutomationIndex].hasOwnProperty("commands"))
            {
                var j = CurrentAutomations[CurrentAutomationIndex].commands.length;
                while (j--) {
                    if (CurrentAutomations[CurrentAutomationIndex].commands[j] == CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex].event)
                    {
                        eventiscommand = true;
                    }
                }
            }

            $('#transitioneditstate').val(CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex].state);
            $('#transitioneditevent').val(CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex].event);
            $('#transitioneditnewstate').val(CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex].newstate);
            $('#transitioneditvisible').prop('checked', eventiscommand);

            $('#transitioneditbox').modal({allowMultiple: false}).modal("show");
        });

        $("body").on("click", ".instructrowprogram", function (e) {
            CurrentTransitionIndex = Number(this.dataset.index);
            $('#actionstable > tbody').html(CreateActionsTable(CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex]));
            $('#actionsbox').modal({allowMultiple: false}).modal("show");
        });
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Change Automation being viewed
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $("body").on("click", ".instructautomationbutton", function (e) {
            CurrentAutomationIndex = Number(this.dataset.index)
            $("#instructautomationbuttons").html(ExtractAutomationNames(CurrentAutomations, CurrentAutomationIndex));
            $('#instructautomationtable > tbody').html(CreateAutomationTable(CurrentAutomations[CurrentAutomationIndex]));

            var jsoneditordiv = document.getElementById("instructautomationjson");
            if (jsoneditor == null)
            {
                jsoneditor = new JSONEditor(jsoneditordiv, jsoneditoroptions);
            }
            jsoneditor.set(CurrentAutomations[CurrentAutomationIndex]);
        });
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Create new Automation
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $("body").on("click", "#instructnewautomation", function (e) {
            $('#automationnamebox').modal({allowMultiple: false}).modal("show");
        });

        $("body").on("click", "#automationnameok", function (e) {
            var NewName = $("#automationnamename").val();
            if (alreadyexists(NewName, CurrentAutomations))
            {
                APP.Companion_UI.AddErrorMessage("An Automation with that name already exists.");
            }
            else
            {
                var NewAutomation = {
                    automationid:"new",
                    name:NewName,
                    commands:[],
                    transitions:[{state:"init", event:"start", newstate:"idle", actions: []}]
                };
                CurrentAutomations.push(NewAutomation);
                CurrentAutomationIndex = CurrentAutomations.length - 1;
                $("#instructautomationbuttons").html(ExtractAutomationNames(CurrentAutomations, CurrentAutomationIndex));
                $('#instructautomationtable > tbody').html(CreateAutomationTable(CurrentAutomations[CurrentAutomationIndex]));
            }

            $('#instructbox').modal({allowMultiple: false}).modal("show");
        });

        $("body").on("click", "#automationnamecancel", function (e) {
            $('#instructbox').modal({allowMultiple: false}).modal("show");
        });

        var alreadyexists = function(NewName, CurrentAutomations)
        {
            var i = CurrentAutomations.length;
            while (i--) {
                if (CurrentAutomations[i].name == NewName) {
                    return true;
                }
            }
            return false;
        }
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Delete Automation
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $("body").on("click", "#instructdeleteautomation", function (e) {
            CurrentAutomations.splice(CurrentAutomationIndex, 1);
            CurrentAutomationIndex = (CurrentAutomations.length > 0) ? CurrentAutomationIndex-1 : -1;
            $("#instructautomationbuttons").html(ExtractAutomationNames(CurrentAutomations, CurrentAutomationIndex));
            $('#instructautomationtable > tbody').html(CreateAutomationTable(CurrentAutomations[CurrentAutomationIndex]));
        });
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Actions programming
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $("body").on("click", "#actionsnewaction", function (e) {
            CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex].actions.push(ActionParameters("choose"));
            $('#actionstable > tbody').html(CreateActionsTable(CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex]));
        });

        $("body").on("click", "#actionsok", function (e) {
            $(".actionparameters").each(function(parameter) {
                var value = null;
                try {
                    value = JSON.parse(this.value);
                } catch (e) {
                    value = this.value;
                };
                CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex].actions[this.dataset.index].parameters[this.dataset.key] = value;
            });
            $('#instructbox').modal({allowMultiple: false}).modal("show");
        });
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Row (Action) Buttons
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $("body").on("click", ".actionrowdelete", function (e) {
            var index = Number(this.dataset.index);
            CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex].actions.splice(index, 1);
            $('#actionstable > tbody').html(CreateActionsTable(CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex]));
        });

        $("body").on("click", ".actionrowedit", function (e) {
            CurrentActionIndex = Number(this.dataset.index);
            CreateActionSelectionTable();
            $('#selectactionbox').modal({allowMultiple: false}).modal("show");
        });

        $("body").on("click", ".selectactionbutton", function (e) {
            var actionname = this.dataset.action;
            CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex].actions[CurrentActionIndex] = ActionParameters(actionname);
            $('#actionstable > tbody').html(CreateActionsTable(CurrentAutomations[CurrentAutomationIndex].transitions[CurrentTransitionIndex]));
            $('#actionsbox').modal({allowMultiple: false}).modal("show");
        });

        $("body").on("click", "#selectactioncancel", function (e) {
            $('#actionsbox').modal({allowMultiple: false}).modal("show");
        });
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ExtractAutomationNames = function (Automations, active)
    {
        var Result = "";
        if (Automations != undefined)
        {
            var AutomationIndex = 0;
            $.each(Automations, function (AutomationID, Automation) {
                if (AutomationID == active) {
                    Result += '<button data-index="' + AutomationIndex + '" class="ui button active instructautomationbutton">' + Automation.name + '</button>';
                } else {
                    Result += '<button data-index="' + AutomationIndex + '" class="ui button instructautomationbutton">' + Automation.name + '</button>';
                };
                AutomationIndex ++;
            });
        }
        return Result;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreateAutomationTable = function (Automation)
    {
        var Result = "";
        if (Automation != undefined)
        {
            var commands = Automation.commands;
            if (commands == undefined) commands = [];
            var newcommands = [];

            $.each(Automation.transitions, function (i, Transition) {
                var eventiscommand = false;
                var j = commands.length;
                while (j--) {
                    if (commands[j] == Transition.event)
                    {
                        eventiscommand = true;
                        newcommands.push(Transition.event);
                    }
                }

                Result +=
                '<tr>' +
                    '<td style="text-align:center">' +
                        '<button data-index="' + i + '" class="ui basic icon button green instructrowedit"><i class="edit icon"></i></button>' +
                        '<button data-index="' + i + '" class="ui basic icon button red instructrowdelete"><i class="delete icon"></i></button>' +
                    '</td>' +
                    '<td style="text-align:center">' + Transition.state + '</td>' +
                    '<td style="text-align:center; font-weight:' + (eventiscommand ? 'bold' : 'normal') +';">' + Transition.event + '</td>' +
                    '<td style="text-align:center">' + Transition.newstate + '</td>' +
                    '<td style="text-align:center">' +
                    '   <button data-index="' + i + '" class="ui basic icon button ' + ((Transition.actions.length > 0) ? 'orange' : 'blue') + ' instructrowprogram"><i class="cogs icon"></i></button>' +
                    '</td>' +
                '</tr>';
            });

            Automation.commands = newcommands;
        }
        return Result;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreateActionsTable = function (Transition)
    {
        var Result = "";
        if (Transition != undefined)
        {
            $.each(Transition.actions, function (i, Action) {
                Result +=
                '<tr>' +
                    '<td style="text-align:center; vertical-align:top">' +
                        '<button data-index="' + i + '" class="ui basic icon button red actionrowdelete"><i class="delete icon"></i></button>' +
                    '</td>' +
                    '<td style="text-align:center; vertical-align:top"">' +
                    '   <button data-index="' + i + '" class="ui fluid basic button green actionrowedit">' + Action.command + '</button>' +
                    '</td>' +
                    '<td style="text-align:left">' + CreateParametersList(i, Action.command, Action.parameters) + '</td>' +
                '</tr>';
            });
        }
        return Result;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreateParametersList = function (Index, Command, Parameters)
    {
        var Result = "";
        if (Parameters != undefined)
        {
            $.each(Parameters, function (Key, Value) {
                var ValueString = Value;
                if (Value === Object(Value)) ValueString = JSON.stringify(Value);
                Result += ' \
                    <div class="ui fluid labeled input"> \
                        <div class="ui label" style="width:30%">' + Key + '</div> \
                        <input class="actionparameters" data-index="' + Index + '" data-key="' + Key + '" value=\'' + ValueString + '\' placeholder="' + ActionParametersDetails[Command][Key] + '" type="text" > \
                    </div> \
                ';
            });
        }
        return Result;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreateActionSelectionTable = function ()
    {
        var TableContents = "";
        Object.keys(ActionParametersDetails).forEach(function (key) {
            TableContents += ' \
            <tr> \
                <th style="text-align:center"> \
                    <div class="ui basic button blue fluid selectactionbutton" data-action="' + key + '">' + key + '</div> \
                </th> \
                <th style="text-align:left; padding-left:10px"> \
                    ' + ActionParametersDetails[key].help + ' \
                </th> \
            </tr>';
        });
        $('#selectactiontable > tbody').html(TableContents);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ActionParametersDetails = {
        set: {
            help: "Set a Variable to a given Value.",
            key: "The name of the variable",
            value: "The value to set, or a mathematical expression."
        },
        test :{
            help: "Send an event depending on the results of the test.",
            test: "An expression that evaluates to 'true' or 'false'.",
            true_event: "The event to send on true.",
            false_event: "The event to send on false.",
            geobotid: "The Geobot to send the event to (empty for this Geobot)"
        },
        send: {
            help: "Send an event to this or another Geobot.",
            event: "The event to send",
            parameters: "Parameters in JSON format",
            delay: "How long to wait (s) before sending the event",
            geobotid: "The Geobot to send the event to (empty for this Geobot)"
        },
        broadcast: {
            help: "Send an event to Geobots on a channel within a radius of this Geobot.",
            channelid: "The Channel to find the Geobots on (blank for this channel).",
            radius: "The radius around this Geobot to send the event to (meters)",
            event: "The event to send",
            parameters: "Parameters in JSON format",
            delay: "How long to wait (s) before sending the event"
        },
        email: {
            help: "Send an email to list of recipients.",
            recipients: "A comma separated list of recipients.",
            subject: "The subject of the email",
            message: "The message for the email.  Can include html."
        },
        trigger: {
            help: "Trigger a synchronized event across all devices watching this Geobot.  This can be used to start animations, trigger sounds, etc.",
            event: "The triggering event to be sent to all devices."
        },
        log: {
            help: "Log a real-world event for later analysis",
            event: "Event name to log",
            parameters: "Parameters in JSON format to log with the event"
        },
        kill: {
            help: "FOR TESTING ONLY - Kill a specified entity on the Server to check it is properly recreated.",
            type: "companion, channel, geobot or automation"
        }
    }
    var ActionParameters = function (Command)
    {
        var Parameters = {};
        switch (Command)
        {
            case "set":
                Parameters.key = "";
                Parameters.value = "";
            break;
            case "test":
                Parameters.test = "";
                Parameters.true_event = "";
                Parameters.false_event = "";
                Parameters.geobotid = "";
            break;
            case "send":
                Parameters.event = "";
                Parameters.parameters = "";
                Parameters.delay = 0;
                Parameters.geobotid = "";
            break;
            case "broadcast":
                Parameters.channelid = "";
                Parameters.radius = 1000;
                Parameters.event = "";
                Parameters.parameters = "";
                Parameters.delay = 0;
            break;
            case "email":
                Parameters.recipients = "";
                Parameters.subject = "";
                Parameters.message = "";
            break;
            case "trigger":
                Parameters.event = "";
            break;
            case "log":
                Parameters.event = "";
                Parameters.parameters = "";
            break;
            case "kill":
                Parameters.type = "";
                break;
            default:
            break;
        }
        return {command:Command, parameters:Parameters};
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Open = function (Automations)
    {
        CurrentAutomations = Automations.automations;
        CurrentAutomationIndex = 0;

        var jsoneditordiv = document.getElementById("instructautomationjson");
        var AutomationsButtons = ExtractAutomationNames(CurrentAutomations, CurrentAutomationIndex);

        if (CurrentAutomations.length > 0)
        {
            var AutomationsContent = CreateAutomationTable(CurrentAutomations[CurrentAutomationIndex]);
            $('#instructautomationtable > tbody').html(AutomationsContent);

            if (jsoneditor == null)
            {
                jsoneditor = new JSONEditor(jsoneditordiv, jsoneditoroptions);
            }
            jsoneditor.set(CurrentAutomations[CurrentAutomationIndex]);
        }
        else {
            $('#instructautomationtable > tbody').html("");

            if (jsoneditor == null)
            {
                jsoneditor = new JSONEditor(jsoneditordiv, jsoneditoroptions);
            }
        }

        $("#instructautomationtable").show();
        $("#instructautomationjsondiv").hide();

        $("#instructguided").removeClass("basic");
        $("#instructexpert").addClass("basic");

        $("#instructautomationbuttons").html(AutomationsButtons);

        $('#instructcontent').css('height', $(window).height() * 0.8);
        $('#instructbox').modal({allowMultiple: false}).modal("show");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    this.Initialise = Initialise;
    this.Open = Open;
}
