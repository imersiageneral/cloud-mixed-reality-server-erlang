// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Manages UI panel for editing Metadata (and the edit metadata dialog box)
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


IMERSIA.MetadataEdit_UI = function ()
{
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Public Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Private Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    var currentlySelectedMetadata = "";
    var currentlySelectedMetadataType = "";
    var jsoneditor = null;
    var jsoneditoroptions = {
        onChange: function() {
            $('#editmetadatavalue').val(jsoneditor.getText());
        }
    };

    // Pointer to self
    var self = this;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Initialise = function ()
    {
        $("body").on("click", "#editmetadataeditvalue", function (e) {
            $("#editmetadatajsondiv").toggle();
        });

        // Edit Metadata OK
        $("body").on("click", "#editmetadataok", function(e) {
            SetMetadata(e);
        });

        $("body").on('change', '#editmetadatavalue', function(e) {
            try
            {
                jsoneditor.set(JSON.parse($('#editmetadatavalue').val()));
            }
            catch(err)
            {
                jsoneditor.set("Invalid JSON");
            }
        });

        // Edit Metadata Cancel
        $("body").on("click", "#editmetadatacancel", function(e) {
            $('#editmetadatabox').modal().modal("hide");
        });

        // Delete Metadata OK
        $("body").on("click", "#deletemetadataok", function(e) {
            DeleteMetadata(e);
        });

        // Delete Metadata Cancel
        $("body").on("click", "#deletemetadatacancel", function(e) {
            $('#deletemetadatabox').modal().modal("hide");
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // What happens when the edit metadata button is pressed.
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var EditMetadata = function(entityid, entitytype) {
        currentlySelectedMetadata = entityid;
        currentlySelectedMetadataType = entitytype;
        $('#editmetadatakey').val($('#' + currentlySelectedMetadata + "_key").text());
        $('#editmetadatavalue').val($('#' + currentlySelectedMetadata + "_value").text());
        var jsoneditordiv = document.getElementById("editmetadatajson");
        if (jsoneditor == null)
        {
            jsoneditor = new JSONEditor(jsoneditordiv, jsoneditoroptions);
        }
        jsoneditor.set(JSON.parse($('#' + currentlySelectedMetadata + "_value").text()));

        $("#editmetadatajsondiv").hide();
        $('#editmetadatabox').modal().modal("show");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Creating the new metadata
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var NewMetadata = function(entitytype) {
        currentlySelectedMetadata = "new";
        currentlySelectedMetadataType = entitytype;
        $('#editmetadatakey').val("");
        $('#editmetadatavalue').val("");
        var jsoneditordiv = document.getElementById("editmetadatajson");
        if (jsoneditor == null)
        {
            jsoneditor = new JSONEditor(jsoneditordiv, jsoneditoroptions);
        }
        jsoneditor.set({});

        $("#editmetadatajsondiv").hide();
        $('#editmetadatabox').modal().modal("show");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Set the metadata in the table from the dialog box
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetMetadata = function(e) {
        // Get the key and value from the dialog box
        var currentmetadata_key = $('#editmetadatakey').val();
        var currentmetadata_value = "";
        try {
            currentmetadata_value = JSON.parse($('#editmetadatavalue').val());
        } catch (e) {
            currentmetadata_value = $('#editmetadatavalue').val();
        }

        $('#' + currentlySelectedMetadata).addClass("dirty");

        if (currentlySelectedMetadata == "new")
        {
            AddRowToMetadataTable(undefined, currentlySelectedMetadataType, currentmetadata_key, currentmetadata_value);
        }
        else
        {
            // Save the values back to the table
            $('#' + currentlySelectedMetadata + "_key").text(currentmetadata_key);
            $('#' + currentlySelectedMetadata + "_value").text(JSON.stringify(currentmetadata_value));

            // Un-strike out the text if it was.
            $('#' + currentlySelectedMetadata).removeClass("deleted");
            $('#' + currentlySelectedMetadata + "_key").css('text-decoration', '');
            $('#' + currentlySelectedMetadata + "_value").css('text-decoration', '');

            // Ensure the delete button icon is a cross
            $('#' + currentlySelectedMetadata + "_deletebutton").removeClass("undo icon");
            $('#' + currentlySelectedMetadata + "_deletebutton").addClass("delete icon");
        }

        $('#editmetadatabox').modal().modal("hide");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Add a row to the variables / metadata table
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var AddRowToMetadataTable = function(metadataid, metadatatype, key, value)
    {
        var html = "";
        if (metadataid == undefined)
        {
            metadataid = key + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
            html += '<tr class = "new ' + metadatatype + '_variable_record", id="' + metadataid + '">'
        }
        else
        {
            html += '<tr class="' + metadatatype + '_variable_record" id="' + metadataid + '">'
        }

        html += '<td style="padding-right:0">'
        html += '<button class="ui mini basic icon button green edit' + metadatatype + 'metadata"><i class="edit icon"></i></button>'
        html += '</td>';
        html += '<td style="padding-left:0">';
        html += '<button class="ui mini basic icon button red delete' + metadatatype + 'metadata"><i class="delete icon" id="' + metadataid + '_deletebutton"></i></button>'
        html += '</td>';
        html += '<td id="' + metadataid + '_key">' + key + '</td>';
        html += '<td id="' + metadataid + '_value">' + JSON.stringify(value) + '</td>';
        html += "</tr>";

        $("#" + metadatatype +"variablestable").append(html);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Open the delete metadata dialog box
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var DeleteMetadataDialog = function(entityid, entitytype) {
        currentlySelectedMetadata = entityid;
        currentlySelectedMetadataType = entitytype;
        if ($('#' + currentlySelectedMetadata).hasClass("deleted"))
        {
            $('#deletemetadatamessage').text('Restore');
            $('#deletemetadataok').html('Restore');
        }
        else
        {
            $('#deletemetadatamessage').text('Delete');
            $('#deletemetadataok').html('Delete');
        }
        $('#deletemetadatatype').text('Geobot');
        $('#deletemetadatakey').text($('#' + currentlySelectedMetadata + "_key").text());
        $('#deletemetadatabox').modal().modal("show");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Delete metadata from table
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var DeleteMetadata = function(e) {
        if ($('#' + currentlySelectedMetadata).hasClass("deleted"))
        {
            // UN-Strike out the text
            $('#' + currentlySelectedMetadata).removeClass("deleted");
            $('#' + currentlySelectedMetadata + "_key").css('text-decoration', '');
            $('#' + currentlySelectedMetadata + "_value").css('text-decoration', '');

            $('#' + currentlySelectedMetadata + "_deletebutton").removeClass("undo icon");
            $('#' + currentlySelectedMetadata + "_deletebutton").addClass("delete icon");
        }
        else
        {
            // Strike out the text
            $('#' + currentlySelectedMetadata).addClass("deleted");
            $('#' + currentlySelectedMetadata + "_key").css('text-decoration', 'line-through');
            $('#' + currentlySelectedMetadata + "_value").css('text-decoration', 'line-through');

            $('#' + currentlySelectedMetadata + "_deletebutton").removeClass("delete icon");
            $('#' + currentlySelectedMetadata + "_deletebutton").addClass("undo icon");
        }

        $('#deletemetadatabox').modal().modal("hide");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    this.Initialise = Initialise;
    this.EditMetadata = EditMetadata;
    this.NewMetadata = NewMetadata;
    this.AddRowToMetadataTable = AddRowToMetadataTable;
    this.DeleteMetadataDialog = DeleteMetadataDialog;
}
