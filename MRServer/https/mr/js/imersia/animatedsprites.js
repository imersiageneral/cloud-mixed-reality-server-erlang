// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Maintains and adds the animation to texture sprites
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


IMERSIA.AnimatedSprites = function ()
{
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Public Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Private Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Textures currently being animated
    var texturesToAnimate = {};

    // Pointer to self
    var self = this;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Update each sprite to the next frame if time-wise it is appropriate to do so
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var TimedUpdate = function( milliSec )
    {
        // Animate textures
        $.each(texturesToAnimate, function (ID, theTexture) {
            if (APP.playing || theTexture.alwaysplay)
            {
                // Check if the texture is needing to be removed - in which case ignore it
                if (!theTexture.toRemove)
                {
                    // Check if it is currently playing
                    if (theTexture.playing)
                    {
                        // Work out if enough time has elapsed given the framerate, to advance to the next frame
                        theTexture.currentDisplayTime += milliSec;
                        while (theTexture.currentDisplayTime > theTexture.tileDisplayDuration)
                        {
                            theTexture.currentDisplayTime -= theTexture.tileDisplayDuration;
                            theTexture.currentTile++;

                            // Go around to the start again if reached the last tile, and repeating
                            if (theTexture.currentTile >= theTexture.numberOfTiles)
                            {
                                theTexture.currentTile = 0;
                                if (!theTexture.repeat) theTexture.playing = false;
                            }
                        }

                        // Work out which row and column
                        var currentColumn = theTexture.currentTile % theTexture.tilesHorizontal;
                        var currentRow = Math.floor( theTexture.currentTile / theTexture.tilesHorizontal ) ;

                        // Set the texture offset based in the row and column
                        theTexture.texture.offset.x = currentColumn / theTexture.tilesHorizontal;
                        theTexture.texture.offset.y = 1 - ((currentRow + 1) / theTexture.tilesVertical);
                    }
                }
                else
                {
                    // Remove any from the list that have been tagged to be removed.
                    delete texturesToAnimate[ID];
                }
            }
        });
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Add an animated Sprite
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var AddSprite = function (ID, texture, parameters, alwaysplay)
    {
        alwaysplay = alwaysplay || false;
        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.set( 1 / parameters.width, 1 / parameters.height );
        texture.needsUpdate = true;

        var textureDetails = {
            texture : texture,
            tilesHorizontal : parameters.hasOwnProperty("width") ? ((parameters.width <= 0) ? 1 : parameters.width) : 1,
            tilesVertical : parameters.hasOwnProperty("height") ? ((parameters.height <= 0) ? 1 : parameters.height) : 1,
            numberOfTiles : parameters.hasOwnProperty("length") ? ((parameters.length <= 0) ? 1 : parameters.length) : 1,
            tileDisplayDuration : parameters.hasOwnProperty("framerate") ? (Math.abs(Math.floor(1000 / parameters.framerate))) : 100,
            currentDisplayTime: 0,
            currentTile : 0,
            repeat: parameters.hasOwnProperty("repeat") ? parameters.repeat : true,
            playontouch: parameters.hasOwnProperty("playontouch") ? parameters.playontouch : false,
            playonstart: parameters.hasOwnProperty("playonstart") ? parameters.playonstart : true,
            webappontouch: parameters.hasOwnProperty("webappontouch") ? parameters.webappontouch : false,
            toRemove: false,
            alwaysplay : alwaysplay
        };

        // Store the Sprite
        if (texturesToAnimate.hasOwnProperty(ID))
        {
            // If this animated sprite already exists
            textureDetails.playing = texturesToAnimate[ID].playing;
            texturesToAnimate[ID] = textureDetails;
        }
        else
        {
            // If this animated sprite doesn't already exist, check if playing on start or not
            textureDetails.playing = (parameters.hasOwnProperty("playonstart") ? parameters.playonstart : false ) ;
            texturesToAnimate[ID] = textureDetails;
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Remove an animated Sprite
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var RemoveSprite = function (ID)
    {
        // We do it this way to avoid the TimedUpdate function above suddenly finding what it is pointing at has been deleted.
        if (texturesToAnimate.hasOwnProperty(ID)) texturesToAnimate[ID].toRemove = true;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // What to do when this Sprite is touched
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Touch = function (ID)
    {
        // If the sprite exists
        if (texturesToAnimate.hasOwnProperty(ID))
        {
            // If the behaviour is to play the sprite when touched
            if (texturesToAnimate[ID].playontouch)
            {
                if (texturesToAnimate[ID].repeat)
                {
                    // If in repeat mode, then toggle it
                    texturesToAnimate[ID].playing = !texturesToAnimate[ID].playing;
                }
                else
                {
                    // Otherwise, just play it once
                    texturesToAnimate[ID].playing = true;
                }
            }
            else
            {
                if (texturesToAnimate[ID].webappontouch)
                {
                    // If this is to activate the WebApp
                    //RB.Experiences.OpenGeobotasWebpage(ID);
                }
                else
                {
                    // Otherwise, send the event further to the Velocity Engine
                    //RB.SDK.SendEventToEngine(ID, "touch", { userid: RB.SDK.UserID () });
                }
            }
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var PlayOneShot = function(ID)
    {
        if (texturesToAnimate.hasOwnProperty(ID))
        {
            texturesToAnimate[ID].playing = true;
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    this.TimedUpdate = TimedUpdate;
    this.AddSprite = AddSprite;
    this.RemoveSprite = RemoveSprite;
    this.Touch = Touch;
    this.PlayOneShot = PlayOneShot;

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}
