// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Description: Geobot datastructure - a writethrough cache.
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Constructor
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Geobot = function (geobot)
{
    this.geobotid = geobot.geobotid;
    this.channelid = geobot.channelid;
    this.ownerid = geobot.ownerid;
    this.class = geobot.class;
    this.name = geobot.name;
    this.description = geobot.description;
    this.imageurl = geobot.imageurl;
    this.location = geobot.location;
    this.radius = geobot.radius;
    this.created = geobot.created;
    this.modified = geobot.modified;
    this.hidden = geobot.hidden;
    this.automations = new IMERSIA.Automations(geobot.geobotid);
    this.status = {};

    this.metadata = new IMERSIA.Metadata(geobot.geobotid, "geobot");
    this.fileurls = [];

    this.callbacks = {};
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Save the Geobot to the API.  NB Automations saved in Instruct_UI.
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Geobot.prototype.Save = function (sessionid, details, callback, errorcallback)
{
    var self = this;

    APP.SDK.PutToAPI(sessionid, 'geobots', {geobotid : details.geobotid}, details, function (result)
    {
        if (self.metadata)
        {
            self.metadata.Save(sessionid, function () {
                if (callback) callback(result);
            }, function (error) {
                if (errorcallback) errorcallback(error);
            });
        }
        else
        {
            if (callback) callback(result);
        }
    },
    function(error)
    {
        if (errorcallback) errorcallback(error);
    });
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Geobot.prototype.Reload = function (sessionid, callback, errorcallback)
{
    var self = this;

    APP.SDK.GetFromAPI(sessionid, 'geobots', {geobotid : self.geobotid}, function (geobot)
    {
        // Update the core details
        self.class = geobot.class;
        self.name = geobot.name;
        self.description = geobot.description;
        self.imageurl = geobot.imageurl;
        self.location = geobot.location;
        self.radius = geobot.radius;
        self.hidden = geobot.hidden;
        self.created = geobot.created;
        self.modified = geobot.modified;
        self.status = {};

        // Update the Geobot metadata and file structure
        APP.SDK.GetFromAPI(sessionid, 'files', {geobotid : geobot.geobotid}, function(files)
        {
            self.fileurls = files.fileurls;

            self.metadata.Load(sessionid, function () {

                self.automations.Load(sessionid, function () {
                    // Trigger an update on the Automations states
                    APP.companion.SendCommand(geobot.geobotid, "status", {});

                    if (callback) callback();
                }, function (error) {
                    if (errorcallback) errorcallback();

                })
            }, function (error) {
                if (errorcallback) errorcallback();
            });
        });
    },
    function(error)
    {
        if (errorcallback) errorcallback(error);
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Respond to a channel-level wotcha event triggered when changes occur to a Geobot
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Geobot.prototype.Wotcha = function (message)
{
    var thisgeobot = this;

    if (message.hasOwnProperty("geobot_set"))
    {
        if (thisgeobot.geobotid == message.geobot_set.geobotid)
        {
            thisgeobot.name = message.geobot_set.name || thisgeobot.name;
            thisgeobot.description = message.geobot_set.description || thisgeobot.description;
            thisgeobot.class = message.geobot_set.class || thisgeobot.class;
            thisgeobot.imageurl = message.geobot_set.imageurl || thisgeobot.imageurl;
            thisgeobot.location = message.geobot_set.location || thisgeobot.location;
            thisgeobot.radius = message.geobot_set.radius || thisgeobot.radius;
            thisgeobot.hidden = message.geobot_set.hasOwnProperty("hidden") ?  message.geobot_set.hidden : thisgeobot.hidden;

            // Call any other functions watching this Geobot
            Object.keys(thisgeobot.callbacks).forEach (function (ID) {
                thisgeobot.callbacks[ID] ( message );
            });

            // Trigger an update on the Automations states
            APP.companion.SendCommand(thisgeobot.geobotid, "status", {});

        }
    }
    else if (message.hasOwnProperty("geobot_delete"))
    {
        Object.keys(thisgeobot.callbacks).forEach (function (ID) {
            if (thisgeobot.callbacks.hasOwnProperty(ID)) thisgeobot.callbacks[ID] ( message );
        });
    }
    else if (message.hasOwnProperty("geobot_metadata"))
    {
        if (thisgeobot.geobotid == message.geobot_metadata.geobotid)
        {
            // Update the metadata
            try {
                thisgeobot.metadata.metadata[message.geobot_metadata.metadataid] = JSON.parse(message.geobot_metadata); // Convert it if possible
            } catch (e) {
                thisgeobot.metadata.metadata[message.geobot_metadata.metadataid] = message.geobot_metadata;
            };

            // Call any other functions watching this Geobot
            Object.keys(thisgeobot.callbacks).forEach (function (ID) {
                thisgeobot.callbacks[ID] ( message );
            });

            // Trigger an update on the Automations states
            APP.companion.SendCommand(thisgeobot.geobotid, "status", {});
        }
    }
    else if (message.hasOwnProperty("geobot_metadata_delete"))
    {
        if (thisgeobot.geobotid == message.geobot_metadata_delete.geobotid)
        {
            // Delete Geobot metadata
            delete thisgeobot.metadata.metadata[message.geobot_metadata_delete.metadataid];

            // Call any other functions watching this Geobot
            Object.keys(thisgeobot.callbacks).forEach (function (ID) {
                thisgeobot.callbacks[ID] ( message );
            });
        }
    }
    else if (message.hasOwnProperty("geobot_status"))
    {
        if (thisgeobot.geobotid == message.geobot_status.geobotid)
        {
            thisgeobot.status = {};
            // Automation states
            $.each(message.geobot_status.states, function(AutomationID, State)
            {
                var Automation = thisgeobot.automations.Get(AutomationID);
                if (Automation)
                {
                    thisgeobot.status[AutomationID] = {name : Automation.name, state : State};
                }
            })
        }
    }
    else if (message.hasOwnProperty("geobot_state_change"))
    {
        if (thisgeobot.geobotid == message.geobot_state_change.geobotid)
        {
            var AutomationID = message.geobot_state_change.automationid;
            if (thisgeobot.status.hasOwnProperty(AutomationID))
            {
                thisgeobot.status[AutomationID].state = message.geobot_state_change.newstate;
            }

            // Trigger an update on the Automations states (makes sure all is consistent across the UI)
            APP.companion.SendCommand(thisgeobot.geobotid, "status", {});
        }
    }
    else if (message.hasOwnProperty("geobot_automations"))
    {
        if (this.geobotid == message.geobot_automations.geobotid)
        {
            this.automations.Load(APP.companion.sessionid);
        }
    }
    else if (message.hasOwnProperty("trigger"))
    {
        if (thisgeobot.geobotid == message.trigger.geobotid)
        {
            APP.Companion_UI.AddInfoMessage("Trigger [ <i>" + message.trigger.event + "</i> ] received for " + thisgeobot.name);

            // Animated sprite trigger
            if (thisgeobot.class == "com.imersia.sprite")
            {
                var SpriteMetadata = thisgeobot.metadata.GetByKey('com.imersia.sprite');
                // TODO: Replace this with a more fine-grained animation selection
                if (SpriteMetadata && (SpriteMetadata.value.trigger == message.trigger.event))
                {
                    APP.AnimatedSprites.PlayOneShot(thisgeobot.geobotid);
                }
            }

            // Check Audio trigger
            var AudioMetadata = thisgeobot.metadata.GetByKey('com.imersia.audio');
            if (AudioMetadata && (AudioMetadata.value.trigger == message.trigger.event))
            {
                APP.Maps.PlayAudioOneShot(thisgeobot.geobotid);
                if (AudioMetadata.value.hasOwnProperty("vibrate"))
                {
                    if (AudioMetadata.value.vibrate) navigator.vibrate([400, 100, 300, 100, 200, 100, 100]);
                }
            }
        }
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Geobot.prototype.Touch = function ()
{
    var thisgeobot = this;
    var OpenWebApp = function(thisgeobot)
    {
        $('#webappiframe').attr('src', window.location.origin + '/mr.html?' + thisgeobot.geobotid);
        $('#webappheader').text(window.location.origin + '/mr.html?' + thisgeobot.geobotid);
        $('#webappiframe').css('height', $(window).height() * 0.6);
        $('#webappbox').modal("show");
    }

    var DefaultData = thisgeobot.metadata.GetByKey(thisgeobot.class);
    if (DefaultData)
    {
        switch (DefaultData.value.touch)
        {
            case "touch":
                APP.companion.SendEvent(thisgeobot.geobotid, "touch");
                APP.companion.SendCommand(thisgeobot.geobotid, "status", {});
                break;
            case "webapp":
                OpenWebApp(thisgeobot);
                break;
        }
    }
    else {
        OpenWebApp(thisgeobot);
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Geobot.prototype.Register = function (ID, callback)
{
    this.callbacks[ID] = callback;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Geobot.prototype.Deregister = function (ID)
{
    delete this.callbacks[ID];
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
