// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Description: Metadata management in a table as a passthrough cache to the API.
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Constructor
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Metadata = function (id, idtype)
{
    this.id = id;
    this.idtype = (idtype || "geobot") + "id";
    this.metadata = {};
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Process the Metadata to turn into actual numbers, booleans and JSON.  Sometimes these get lost in transfer.
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Metadata.prototype.ProcessMetadata = function (metadataIn)
{
    var self = this;

    metadataIn.forEach(function(metadataRecord)
    {
        // console.log(metadataRecord.value);
        // var value = metadataRecord.value.replace(/\\\"/g,'"'); // Remove extraneous quotes
        try {
            metadataRecord.value = JSON.parse(metadataRecord.value); // Convert it if possible
        } catch (e) {
            metadataRecord.value = metadataRecord.value;
        };

        self.metadata[metadataRecord.metadataid] = metadataRecord;
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Load the Metadata details from the API
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Metadata.prototype.Load = function (sessionid, callback, errorcallback)
{
    var self = this;
    var headers = {};
    headers[self.idtype] = self.id;

    APP.SDK.GetFromAPI(sessionid, "metadata", headers, function (metadata)
    {
        self.ProcessMetadata(metadata);
        if (callback) callback();
    }, function (error) {
        if (errorcallback) errorcallback(error);
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Save the Metadata to the API
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Metadata.prototype.Save = function (sessionid, callback, errorcallback)
{
    var self = this;
    var headers = {};
    headers[self.idtype] = self.id;

    var numItems = self.metadata.length;
    Object.keys(self.metadata).forEach(function (key)
    {
        var metadataRecord = self.metadata[key];
        var Details = {
            key : metadataRecord.key,
            value : metadataRecord.value
        };
        if (metadataRecord.metadataid) Details.metadataid = metadataRecord.metadataid;

        APP.SDK.PutToAPI(sessionid, "metadata", headers, Details, function (result) {
            if ((--numItems <= 0) && callback) callback();
        }, function (error) {
            if (errorcallback) errorcallback(error);
        });
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Set a metadata key / value pair.  If no metadataid is given, a new entry is made, otherwise an existing one is updated.
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Metadata.prototype.Set = function (sessionid, metadataid, key, value, callback, errorcallback)
{
    var self = this;
    var headers = {};
    headers[self.idtype] = self.id;

    var Details = {
        key : key,
        value : value
    };
    if (metadataid) Details.metadataid = metadataid;

    APP.SDK.PostToAPI(sessionid, "metadata", headers, Details, function (result) {
        if (result.metadataid) Details.metadataid = result.metadataid;
        self.metadata[Details.metadataid] = Details;

        if (callback) callback(result.metadataid);
    }, function (error) {
        if (errorcallback) errorcallback(error);
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Delete a metadata key / value pair.
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Metadata.prototype.Delete = function (sessionid, metadataid, callback, errorcallback)
{
    var self = this;
    var headers = {};
    headers[self.idtype] = self.id;
    headers["metadataid"] = metadataid;

    APP.SDK.DeleteFromAPI(sessionid, "metadata", headers, function (result) {
        if (result.ok)
        {
            delete self.metadata[metadataid];
        }

        if (callback) callback();
    }, function (error) {
        if (errorcallback) errorcallback(error);
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Metadata.prototype.GetByID = function (metadataid)
{
    var self = this;

    return (self.metadata.hasOwnProperty(metadataid)) ? self.metadata[metadataid].value : undefined;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Metadata.prototype.GetByKey = function (key)
{
    var self = this;
    var result = undefined;

    Object.keys(self.metadata).some(function (metadataid)
    {
        if (self.metadata[metadataid].key == key)
        {
            result = self.metadata[metadataid];
            return true;
        }
        else {
            return false;
        }
    });

    return result;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
