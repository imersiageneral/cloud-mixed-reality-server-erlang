// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Description: Automations management in a table as a passthrough cache to the API.
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Constructor
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Automations = function (geobotid)
{
    this.id = geobotid;
    this.automations = [];
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Load the Automation details from the API
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Automations.prototype.Load = function (sessionid, callback, errorcallback)
{
    var self = this;
    var headers = {
        geobotid: self.id
    };

    APP.SDK.GetFromAPI(sessionid, "geobots/automations", headers, function (automations)
    {
        self.automations = automations;
        if (callback) callback();
    }, function (error) {
        if (errorcallback) errorcallback(error);
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Save all the Automations to the API
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Automations.prototype.Save = function (sessionid, callback, errorcallback)
{
    var self = this;
    var headers = {
        geobotid: self.id
    };

    var numItems = self.automations.length;
    $.each(self.automations, function (index, automationRecord)
    {
        var Details = {
            name : automationRecord.name,
            description: automationRecord.description,
            commands: automationRecord.commands,
            transitions: automationRecord.transitions
        };
        if (automationRecord.automationid != "new") Details.automationid = automationRecord.automationid;

        APP.SDK.PostToAPI(sessionid, "geobots/automations", headers, Details, function (result) {
            if ((--numItems <= 0) && callback) callback();
        }, function (error) {
            if (errorcallback) errorcallback(error);
        });
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Set an Automation's details
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// IMERSIA.Automations.prototype.Set = function (sessionid, automationid, name, description, commands, transitions, callback, errorcallback)
// {
//     var self = this;
//     var headers = {
//         geobotid: self.id
//     };
//
//     var Details = {
//         name : name,
//         description : description,
//         commands: commands,
//         transitions: transitions
//     };
//     if (automationid) Details.automationid = automationid;
//
//     APP.SDK.PostToAPI(sessionid, "geobots/automations", headers, Details, function (result) {
//         if (result.automationid) Details.automationid = result.automationid;
//         self.automations[Details.automationid] = Details;
//
//         if (callback) callback(result.automationid);
//     }, function (error) {
//         if (errorcallback) errorcallback(error);
//     });
// }
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Delete an Automation
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Automations.prototype.Delete = function (sessionid, automationid, callback, errorcallback)
{
    var self = this;
    var Automation = IMERSIA.Automations.Get(automationid);
    if (Automation == undefined)
    {
        if (errorcallback) errorcallback("Automation doesn't exist");
    }
    else
    {
        var headers = {
            geobotid: self.id
        };
        headers["automationid"] = automationid;

        APP.SDK.DeleteFromAPI(sessionid, "geobots/automations", headers, function (result) {
            if (callback) callback();
        }, function (error) {
            if (errorcallback) errorcallback(error);
        });
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Automations.prototype.Get = function (automationid)
{
    var self = this;

    var returnindex = -1;
    $.each(self.automations, function (index, automationRecord)
    {
        if (automationRecord.automationid == automationid) returnindex = index
    });

    return ((returnindex == -1) ? undefined : self.automations[returnindex]);
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
