// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Description: Maps abstract layer for 2D or 3D maps
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


IMERSIA.Maps = function ()
{
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Public Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    this.mapboxid = "";

    this.currentlatitude = 0;
    this.currentlongitude = 180;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Private Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    var firsttime = true;
    var currentgeobotid = "";

    // Pointer to self
    var self = this;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Initialise = function (maptype, callback)
    {
        AssignFunctions(maptype);

        // Get the map token ids
        APP.SDK.GetFromAPI(APP.companion.sessionid, 'vars', {category: "maptoken", key : "mapbox"}, function (details)
        {
            self.mapboxid = details.value;
            if (callback) callback();
        });

        // Monitor the current GPS location
        APP.SDK.StartLocationMonitoring(function (latitude, longitude) {
            SetCurrentLocation(latitude, longitude);
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var AssignFunctions = function (maptype)
    {
        self.RecentreMap = (maptype == 'threed') ? APP.Mapthreed.RecentreMap : APP.Maptwod.RecentreMap;
        self.GetMapExtent = (maptype == 'threed') ? APP.Mapthreed.GetMapExtent : APP.Maptwod.GetMapExtent;
        self.ResizeCanvas = (maptype == 'threed') ? APP.Mapthreed.ResizeCanvas : APP.Maptwod.ResizeCanvas;
        self.ClearPointsFromMap = (maptype == 'threed') ? APP.Mapthreed.ClearPointsFromMap : APP.Maptwod.ClearPointsFromMap;
        self.AddPointToMap = (maptype == 'threed') ? APP.Mapthreed.AddPointToMap : APP.Maptwod.AddPointToMap;
        self.GetCurrentLocation = (maptype == 'threed') ? APP.Mapthreed.GetCurrentLocation : APP.Maptwod.GetCurrentLocation;
        self.CentreMapOnPoints = (maptype == 'threed') ? APP.Mapthreed.CentreMapOnPoints : APP.Maptwod.CentreMapOnPoints;
        self.PlayAudioOneShot = (maptype == 'threed') ? APP.Mapthreed.PlayAudioOneShot : APP.Maptwod.PlayAudioOneShot;
        self.SetCompanionImage = (maptype == 'threed') ? APP.Mapthreed.SetCompanionImage : APP.Maptwod.SetCompanionImage;
        self.SetRadius = (maptype == 'threed') ? APP.Mapthreed.SetRadius : APP.Maptwod.SetRadius;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetMapType = function(newType)
    {
        APP.Mapthreed.SetMapType(newType);
        APP.Maptwod.SetMapType(newType);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetCurrentGeobot = function (newgeobotid)
    {
        currentgeobotid = newgeobotid;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Set the location of the 'current position' marker
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetCurrentLocation = function(latitude, longitude)
    {
        self.currentlatitude = latitude;
        self.currentlongitude = longitude;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetTwoD = function ()
    {
        AssignFunctions("twod");
        $("#threedmap").hide();
        $("#map").show();
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetThreeD = function ()
    {
        AssignFunctions("threed");
        $("#threedmap").show();
        $("#map").hide();
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    this.Initialise = Initialise;
    this.SetCurrentLocation = SetCurrentLocation;
    this.SetCurrentGeobot = SetCurrentGeobot;
    this.SetMapType = SetMapType;

    this.SetTwoD = SetTwoD;
    this.SetThreeD = SetThreeD;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

}
