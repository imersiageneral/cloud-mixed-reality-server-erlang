// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Description: Maps implemented using THREE.JS
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


IMERSIA.Maptwod = function ()
{
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Public Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Private Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    var mapType;

    var currentgeobotid = "";
    var map = null;
    var mapcanvas;
    var mappoints = {};
    var markerslayer;
    var markerjson;

    var mousedownpos = {};
    var grabbedMarker = null;
    var mousebuttonpressed = false;
    var isDragging = false;
    var isCursorOverPoint = false;
    var searchRadius = null;

    // Pointer to self
    var self = this;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetBearing = function (Bearing)
    {
        if (map) {
            if (!map.isMoving()) map.setBearing (Bearing);
            if (searchRadius) searchRadius.setCenter({lng:APP.SDK.currentlongitude, lat:APP.SDK.currentlatitude} );
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetRadius = function (Radius)
    {
        if (searchRadius) searchRadius.setRadius(Radius);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Change the map provider
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetMapType = function (newType)
    {
        if (newType) mapType = newType;
        switch (mapType)
        {
            case "com.mapbox.streets":
                map.setStyle('mapbox://styles/mapbox/streets-v9');
                break;
            case "com.mapbox.satellite":
                map.setStyle('mapbox://styles/mapbox/satellite-streets-v9');
                break;
            case "com.mapbox.light":
                map.setStyle('mapbox://styles/mapbox/light-v9');
                break;
            case "com.mapbox.dark":
                map.setStyle('mapbox://styles/mapbox/dark-v9');
                break;
            case "com.mapbox.outdoors":
                map.setStyle('mapbox://styles/mapbox/outdoors-v9');
                break;
            default:
                map.setStyle('mapbox://styles/mapbox/light-v9');
                break;
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Resize the map canvas
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ResizeCanvas = function()
    {
        var h = $(window).height();
        var w = $(window).width();

        var mapheight = h - $("#footer").outerHeight() + (APP.threedmode ? 5 : 0);
        $("#twodmap").height(mapheight);
        $("#twodmap").css("height", mapheight);
        $("#twodmap").width(w);
        $("#twodmap").css("width", w);

        if (map) map.resize();
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Adjust the central location of the map
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var AdjustCentre = function()
    {

    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var GetMapExtent = function ()
    {
        if (map)
        {
            var point1 = map.unproject([0, 0]);
            var point2 = map.unproject([100, 100]);

            var distance = getDistance(point1.lat, point1.lng, point2.lat, point2.lng) * 10000;  // distance in m
            return Math.max(1, Math.round(distance * 2));
        }
        else {
            return 1000;
        }
    }

    var getDistance = function (lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var deg2rad = 0.017453293;
        var dLat = deg2rad * (lat2 - lat1);
        var dLon = deg2rad * (lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(deg2rad * lat1) * Math.cos(deg2rad * lat2) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Recentre the map at the given location
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var RecentreMap = function(latitude, longitude)
    {
        WaitUntilMapReady(function(isready)
        {
            if (isready)
            {
                if (latitude == undefined)
                {
                    if (! $(".mapboxgl-ctrl-geolocate").hasClass('mapboxgl-ctrl-geolocate-active'))
                    {
                        $(".mapboxgl-ctrl-geolocate").trigger('click');
                    }
                }
                else
                {
                    map.flyTo( { center: [ longitude, latitude ] } );
                }
            }
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Get the location of the current map centre
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var GetCurrentLocation = function()
    {
        var MapCenter = map.getCenter();
        return ({latitude: MapCenter.lat, longitude: MapCenter.lng});
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var DrawPointsOnMap = function ()
    {
        var markersource = map.getSource('markers');
        if (markersource) markersource.setData(markerjson);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // TODO: Make the sound 3D even in 2D map mode
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var PlayAudioOneShot = function(geobotid)
    {
        if (mappoints.hasOwnProperty(geobotid) && APP.playing)
        {
            var thisGeobot = APP.companion.FindGeobot(geobotid);
            var audiodetails = thisGeobot.metadata.GetByKey("com.imersia.audio");
            if (audiodetails && (audiodetails.value.audiourl != ""))
            {
                var audio = new Audio(audiodetails.value.audiourl);
                audio.play();
            }
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var UpdateGeobotOnMap = function(message)
    {
        if (message.hasOwnProperty("geobot_set"))
        {
            AddPointToMap(message.geobot_set); // Only adds if new point, otherwise modifies
        }
        else if (message.hasOwnProperty("geobot_delete"))
        {
            RemovePointFromMap(message.geobot_delete.geobotid);
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var RemovePointFromMap = function (geobotid)
    {
        var mapobject = mappoints[geobotid];

        if (mapobject != undefined)
        {
            var thisGeobot = mapobject.geobot;
            thisGeobot.Deregister("maptwod");

            var markerfeature = mapobject.marker;
            var index = markerfeature.properties.index;
            markerjson.features.splice(index, 1);
            for (var index2 = 0; index2 < markerjson.features.length; index2++)
            {
                markerjson.features[index2].properties.index = index2;
            }
            delete mappoints[geobotid];
        }

        // Update the map
        DrawPointsOnMap();

        if (APP.currentlySelectedGeobot == geobotid)
        {
            APP.currentlySelectedGeobot = "";
            APP.Geobot_UI.Close();
            APP.GeobotEdit_UI.Close();
            APP.Geobot_UI.ClearOutput();
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var AddPointToMap = function (pointdetails)
    {
        var alreadyExists = (mappoints.hasOwnProperty(pointdetails.geobotid));

        if (alreadyExists)
        {
            var mapobject = mappoints[pointdetails.geobotid];
            mapobject.marker.properties.title = pointdetails.name;
            mapobject.marker.properties.coordinates = [pointdetails.location.longitude, pointdetails.location.latitude];
            mapobject.marker.geometry.coordinates = [pointdetails.location.longitude, pointdetails.location.latitude];
            DrawPointsOnMap();
        }
        else
        {
            WaitUntilMapReady(function (mapready)
            {
                if (mapready)
                {
                    mappoints[pointdetails.geobotid] = {};
                    var newpoint = {
                        type: "Feature",
                        geometry: {
                            type: "Point",
                            coordinates: [pointdetails.location.longitude, pointdetails.location.latitude]
                        },
                        properties: {
                            title: pointdetails.name,
                            geobotid: pointdetails.geobotid,
                            channelid: pointdetails.channelid,
                            index: markerjson.features.length,
                            coordinates: [pointdetails.location.longitude, pointdetails.location.latitude]
                        }
                    };
                    markerjson.features.push(newpoint);

                    mappoints[pointdetails.geobotid].marker = newpoint;

                    var markersource = map.getSource('markers');
                    if (markersource)
                    {
                        markersource.setData(markerjson);
                    }
                    else
                    {
                        map.addSource('markers', {
                            "type": "geojson",
                            "data": markerjson
                        });

                        var markerlayer = map.getLayer('markerslayer');
                        if (markerlayer == undefined)
                        {
                            map.loadImage('images/geobotblue.png', function(error, image) {
                                if (error) throw error;
                                if (!map.hasImage('imersiastar')) map.addImage('imersiastar', image);
                                map.addLayer(
                                    {
                                        "id": "markerslayer",
                                        "type": "symbol",
                                        "source": "markers",
                                        "layout":
                                        {
                                            "icon-image": "imersiastar",
                                            "icon-size" : 0.15,
                                            "text-field": "{title}",
                                            "icon-allow-overlap":true,
                                            "text-allow-overlap":true,
                                            "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
                                            "text-size": 14,
                                            "text-offset": [0, 0.8],
                                            "text-anchor": "top",
                                            "text-optional": true
                                        }
                                    }
                                );
                            });
                        }
                    }

                    // Save the Geobot Datastructure
                    var thisGeobot = APP.companion.channels[pointdetails.channelid].geobots[pointdetails.geobotid];
                    mappoints[pointdetails.geobotid].geobot = thisGeobot;
                    thisGeobot.Register("maptwod", UpdateGeobotOnMap.bind(thisGeobot));
                };
            });
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var WaitUntilMapReady = function(callback)
    {
        CheckMapReady(10, callback);
    }
    var CheckMapReady = function(counter, callback)
    {
        if (counter > 0)
        {
            if (map.isStyleLoaded())
            {
                callback(true);
            }
            else
            {
                setTimeout(function() {CheckMapReady(counter-1, callback)}, 1000);
            }
        }
        else
        {
            callback(false);
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ClearPointsFromMap = function ()
    {
        mapcanvas.style.cursor = '';
        isCursorOverPoint = false;
        grabbedMarker = undefined;
        map.dragPan.enable();
        isDragging = false;
        isCursorOverPoint = true;
        map.off('mousemove', onMove);

        markerjson =
        {
            type: 'FeatureCollection',
            features: [ ]
        };
        DrawPointsOnMap();

        Object.keys(mappoints).forEach(function (mappointid)
        {
            ClearMapPoint(mappointid);
        });

        // Clear the table of points and colliders
        mappoints = {};

        // var markerlayer = map.getLayer('markerslayer');
        // if (markerlayer != undefined)
        // {
        //     map.removeLayer('markerslayer');
        // }
        // var markersource = map.getSource('markers');
        // if (markersource)
        // {
        //     map.removeSource('markers');
        // }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ClearMapPoint = function(mappointid)
    {
        var mapobject = mappoints[mappointid];

        if (mapobject != undefined)
        {
            var thisGeobot = mapobject.geobot;
            thisGeobot.Deregister("maptwod");
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CentreMapOnPoints = function()
    {
        WaitUntilMapReady(function(isready)
        {
            if (isready)
            {
                var bbox = new mapboxgl.LngLatBounds();

                Object.keys(mappoints).forEach(function (mappointid)
                {
                    var lnglat = new mapboxgl.LngLat(mappoints[mappointid].geobot.location.longitude, mappoints[mappointid].geobot.location.latitude);
                    bbox.extend(lnglat);
                });

                map.fitBounds(bbox, {
                  padding: 400
                });
            }
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var handleOrientation = function(event)
    {
        var currentheading = APP.SDK.CalculateOrientation(true);

        if (!APP.threedmode && APP.Maptwod)
        {
            SetBearing(360 - currentheading);
        }

        // $("#footermenu").html("Orientation :" +  (window.orientation || 0) + " Heading :" + (currentheading));
    }

    var Initialise = function ()
    {
        if (undefined != mapboxgl)
        {
            mapboxgl.accessToken = APP.Maps.mapboxid;
            map = new mapboxgl.Map({
                container: 'twodmap',
                style: 'mapbox://styles/mapbox/light-v9'
            });

            mapcanvas = map.getCanvasContainer();

            map.addControl(new mapboxgl.GeolocateControl({
                positionOptions: {
                    enableHighAccuracy: true
                },
                trackUserLocation: true
            }));

            WaitUntilMapReady(function (mapready)
            {
                map.loadImage('images/geobotblue.png', function(error, image) {
                    if (error) throw error;
                    map.addImage('imersiastar', image);
                });

                map.onLoad = function(callback) {
                    if  (map.loaded()) {
                        callback();
                    } else {
                        map.once('load', () => {
                            callback();
                        });
                    }
                };

                map.onLoad( function() {
                    $(".mapboxgl-map").css("overflow", "hidden");
                    ResizeCanvas();

                    searchRadius = new MapboxCircle({lng:APP.SDK.currentlongitude, lat:APP.SDK.currentlatitude}, 1000, {
                        editable: false,
                        fillColor: '#C7C7FF',
                        fillOpacity: 0.1,
                        strokeColor: '#C7C7FF',
                        strokeOpacity: 1,
                        refineStroke: true
                    }).addTo(map);

                    map.on('render', function() {
                        if (searchRadius) searchRadius.setCenter({lng:APP.SDK.currentlongitude, lat:APP.SDK.currentlatitude} );
                    });

                    markerjson =
                    {
                        type: 'FeatureCollection',
                        features: []
                    };

                    // When the cursor enters a feature in the point layer, prepare for dragging.
                    map.on('mouseenter', 'markerslayer', function() {
                        mapcanvas.style.cursor = 'move';
                        isCursorOverPoint = true;
                        map.dragPan.disable();
                    });

                    map.on('mouseleave', 'markerslayer', function() {
                        mapcanvas.style.cursor = '';
                        isCursorOverPoint = false;
                        map.dragPan.enable();
                    });

                    map.on('mousemove', function(e) {
                        if (isDragging) return;

                        if (map.getLayer('markerslayer') != undefined)
                        {
                            var features = map.queryRenderedFeatures(e.point, { layers: ['markerslayer'] });

                            // Change cursor style as a UI indicator
                            // and set a flag to enable other mouse events.
                            if (features.length) {
                                mapcanvas.style.cursor = 'pointer';
                                isCursorOverPoint = true;
                                grabbedMarker = features[0];
                                map.dragPan.disable();
                            } else {
                                mapcanvas.style.cursor = '';
                                isCursorOverPoint = false;
                                grabbedMarker = undefined;
                                map.dragPan.enable();
                            }
                        }
                    });

                    map.on('mousedown', 'markerslayer', mouseDownOnMap);
                });
            });

            if ('ondeviceorientationabsolute' in window)
            {
                // Chrome 50+ specific
                window.addEventListener('deviceorientationabsolute', handleOrientation);
            } else if ('ondeviceorientation' in window)
            {
                window.addEventListener('deviceorientation', handleOrientation);
            }
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Manage the movement of Geobots by the mouse on the map
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    function mouseDownOnMap(e)
    {
        if (isCursorOverPoint)
        {
            // Mouse events
            map.on('mousemove', onMove);
            map.once('mouseup', onUp);
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    function onMove(e)
    {
        // if (!grabbedMarker)
        // {
        //     if (map.getLayer('markerslayer') != undefined)
        //     {
        //         var features = map.queryRenderedFeatures(e.point, { layers: ['markerslayer'] });
        //
        //         // Change cursor style as a UI indicator
        //         // and set a flag to enable other mouse events.
        //         if (features.length) {
        //             mapcanvas.style.cursor = 'pointer';
        //             isCursorOverPoint = true;
        //             grabbedMarker = features[0];
        //             map.dragPan.disable();
        //         } else {
        //             mapcanvas.style.cursor = '';
        //             isCursorOverPoint = false;
        //             grabbedMarker = undefined;
        //             map.dragPan.enable();
        //             return;
        //         }
        //     }
        // };

        if (!APP.playing)
        {
            isDragging = true;

            var coords = e.lngLat;

            // Set a UI indicator for dragging.
            mapcanvas.style.cursor = 'grabbing';

            markerjson.features[grabbedMarker.properties.index].geometry.coordinates = [coords.lng, coords.lat];
            var markersource = map.getSource('markers');
            if (markersource) markersource.setData(markerjson);
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    function onUp(e)
    {
        if (!grabbedMarker) return;

        if (isDragging)
        {
            var coords = e.lngLat;
            var thisGeobotID = grabbedMarker.properties.geobotid;

            var newlocation = {
                latitude: coords.lat,
                longitude: coords.lng,
                altitude: mappoints[thisGeobotID].geobot.location.altitude
            };

            APP.SDK.PutToAPI(APP.companion.sessionid, "geobots", {geobotid:thisGeobotID}, {location:newlocation});
        }
        else
        {
            if (APP.mobilemode)
            {
                var thisGeobotID = grabbedMarker.properties.geobotid;
                // TODO Check Geobot actions - this is just a temporary test
                mappoints[thisGeobotID].geobot.Touch();

                // $('#webappiframe').attr('src', window.location.origin + '/mr.html?' + thisGeobotID);
                // $('#webappheader').text(window.location.origin + '/mr.html?' + thisGeobotID);
                // $('#webappiframe').css('height', $(window).height() * 0.6);
                // $('#webappbox').modal("show");
            }
            else {
                var thisGeobotID = grabbedMarker.properties.geobotid;
                RecentreMap(mappoints[thisGeobotID].geobot.location.latitude, mappoints[thisGeobotID].geobot.location.longitude);
                APP.Geobot_UI.Open();
                APP.currentlySelectedGeobot = thisGeobotID;
                APP.Geobot_UI.ShowGeobot(mappoints[thisGeobotID].geobot);
            }
        }

        mapcanvas.style.cursor = '';
        isDragging = false;
        isCursorOverPoint = true;
        map.off('mousemove', onMove);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    this.RecentreMap = RecentreMap;
    this.GetMapExtent = GetMapExtent;
    this.ResizeCanvas = ResizeCanvas;
    this.Initialise = Initialise;
    this.SetMapType = SetMapType;
    // this.SetMapMarker = SetMapMarker;
    this.ClearPointsFromMap = ClearPointsFromMap;
    this.GetCurrentLocation = GetCurrentLocation;
    this.AddPointToMap = AddPointToMap;
    this.CentreMapOnPoints = CentreMapOnPoints;
    this.PlayAudioOneShot = PlayAudioOneShot;

    this.SetBearing = SetBearing;
    this.SetRadius = SetRadius;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

}
