// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Description: Various functions for getting content to and from the Imersia API.
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.SDK = function ()
{
    this.currentlatitude = 0;
    this.currentlongitude = 180;
    this.currentaltitude = 0;
    this.currentheading = 0;
    this.currentaccuracy = 0;
    this.currentaltitudeAccuracy = 0;
    this.currentspeed = 0;
    this.currentlocation = "xbpbpbpbp";
    this.developerid = "com.imersia.portal";
    this.geoMag = null;
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Authenticate the user in Imersia
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.SDK.prototype.Login = function (username, password, callback, errorcallback)
{
    var self = this;

    var parameters = {
        touch : false,
        location : self.currentlocation,
        developerid : self.developerid,
        useremail : username,
        password : password
    }

    $.ajax({
        url : '/api/sessions',
        headers : parameters,
        type : 'GET',
        processData: false,
        dataType: "json",
        cache: false,
        success : function(data) {
            if (callback) callback(data.sessionid);
        },
        error : function (data) {
            if (errorcallback) errorcallback(data.responseJSON);
        }
    });
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Authenticate the user in Imersia
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.SDK.prototype.Register = function (username, password, firstname, surname, nickname, callback, errorcallback)
{
    var self = this;

    var parameters = {
        touch : false,
        location : self.currentlocation,
        developerid : self.developerid,
        "Content-Type" : "application/json; charset=utf-8"
    };

    var body = {
        useremail: username,
        password: password,
        details: {
            firstname : firstname,
            surname : surname,
            nickname : nickname
        }
    };

    $.ajax({
        url : '/api/user',
        headers : parameters,
        type : 'POST',
        data: JSON.stringify(body),
        processData: false,
        dataType: "json",
        cache: false,
        success : function(data) {
           if (callback) callback(data);
        },
        error : function (data) {
            if (errorcallback) errorcallback(data.responseJSON);
        }
    });
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Get something from the API
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.SDK.prototype.GetFromAPI = function ( sessionid, command, parameters, callback, errorcallback )
{
    var self = this;

    parameters.touch = false;
    parameters.location = self.currentlocation;
    parameters.developerid = self.developerid;
    parameters.sessionid = sessionid;

    $.ajax({
        url : '/api/' + command,
        headers : parameters,
        type : 'GET',
        processData: false,
        dataType: "json",
        cache: false,
        success : function(data) {
           if (callback) callback(data);
        },
        error : function (data) {
           if (errorcallback) errorcallback(data.responseJSON);
        }
    });
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Get something from the API
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.SDK.prototype.GetGeoJSON = function ( sessionid, URL, parameters, callback, errorcallback )
{
    var self = this;

    parameters.touch = true;
    parameters.radius = APP.Maps.GetMapExtent();
    parameters.location = self.currentlocation;
    parameters.developerid = self.developerid;
    parameters.sessionid = sessionid;
    APP.Maps.SetRadius(parameters.radius/2);

    $.ajax({
        type : 'GET',
        url : URL,
        headers : parameters,
        processData: false,
        dataType: "json",
        cache: false,
        success : function(data) {
           if (callback) callback(data);
        },
        error : function (data) {
           if (errorcallback) errorcallback(data.responseJSON);
        }
    });
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Post something to the API
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.SDK.prototype.PostToAPI = function ( sessionid, command, parameters, body, callback, errorcallback )
{
    var self = this;

    parameters.touch = false;
    parameters.location = self.currentlocation;
    parameters.developerid = self.developerid;
    parameters.sessionid = sessionid;
    parameters["Content-Type"] = "application/json; charset=utf-8";

    $.ajax({
        url : '/api/' + command,
        headers : parameters,
        type : 'POST',
        data: encodeURIComponent(JSON.stringify(body)),
        processData: false,
        dataType: "json",
        cache: false,
        success : function(data) {
           if (callback) callback(data);
        },
        error : function (data) {
            if (errorcallback) errorcallback(data.responseJSON);
        }
    });
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Put something to the API
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.SDK.prototype.PutToAPI = function ( sessionid, command, parameters, body, callback, errorcallback )
{
    var self = this;

    parameters.touch = false;
    parameters.location = self.currentlocation;
    parameters.developerid = self.developerid;
    parameters.sessionid = sessionid;
    parameters["Content-Type"] = "application/json; charset=utf-8";

    $.ajax({
        url : '/api/' + command,
        headers : parameters,
        type : 'PUT',
        data: encodeURIComponent(JSON.stringify(body)),
        processData: false,
        dataType: "json",
        cache: false,
        success : function(data) {
           if (callback) callback(data);
        },
        error : function (data) {
           if (errorcallback) errorcallback(data.responseJSON);
        }
    });
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Delete something from the API
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.SDK.prototype.DeleteFromAPI = function ( sessionid, command, parameters, callback, errorcallback )
{
    var self = this;

    parameters.touch = false;
    parameters.location = self.currentlocation;
    parameters.developerid = self.developerid;
    parameters.sessionid = sessionid;
    parameters["Content-Type"] = "application/json; charset=utf-8";

    $.ajax({
        url : '/api/' + command,
        headers : parameters,
        type : 'DELETE',
        processData: false,
        dataType: "json",
        cache: false,
        success : function(data) {
           if (callback) callback(data);
        },
        error : function (data) {
           if (errorcallback) errorcallback(data.responseJSON);
        }
    });
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Post File to the API
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.SDK.prototype.PostFileToAPI = function ( sessionid, command, parameters, theFile, callback, errorcallback)
{
    var self = this;

    parameters.touch = false;
    parameters.location = self.currentlocation;
    parameters.developerid = self.developerid;
    parameters.sessionid = sessionid;

    var formData = new FormData();
    formData.append('inputfile', theFile);

    $.ajax({
        url : '/api/' + command,
        headers : parameters,
        type : 'POST',
        data : formData,
        enctype : "multipart/form-data",
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        cache: false,
        success : function(data) {
           if (callback) callback(data);
        },
        error : function (data) {
           if (errorcallback) errorcallback(data.responseJSON);
        }
    });
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.SDK.prototype.UpdateLocation = function (callback)
{
    var self = this;

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            self.currentlatitude = position.coords.latitude;
            self.currentlongitude = position.coords.longitude;
            self.currentaltitude = position.coords.altitude;
            self.currentaccuracy = position.coords.accuracy;
            self.currentaltitudeAccuracy = position.coords.altitudeAccuracy;
            self.currentspeed = position.coords.speed;
            self.currentlocation = encodeGeoHash(position.coords.latitude, position.coords.longitude);
            // $("#footermenu").html("Heading :" + self.currentheading);
            if (callback) callback(position.coords.latitude, position.coords.longitude);
        });
    }
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.SDK.prototype.CalculateOrientation = function (withdeviceorientation)
{
    var self = this;

    withdeviceorientation = withdeviceorientation || false;

    var currentheading = 0;
    if (event.webkitCompassHeading) { currentheading = 360 - event.webkitCompassHeading; }
    else if (event.absolute) { currentheading = event.alpha || 0; }
    else { currentheading = event.alpha; }

    // Get the Magnegtic Declination for here
    var declinationData = self.geoMag(self.currentlatitude, self.currentlongitude, self.currentaltitude);

    currentheading = Math.round(currentheading - declinationData.dec - (withdeviceorientation ? (window.orientation || 0) : 0)) % 360;
    if (currentheading < 0) currentheading += 360;

    return currentheading;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.SDK.prototype.StartLocationMonitoring = function(callback)
{
    var self = this;

    // Setup the values required to calculate the Magnetic Declination
    var cof = syncXHR('../geomagjs/WMM.COF');
    var newGeomag = new Geomag(cof);
    self.geoMag = newGeomag.mag;

    if (navigator.geolocation) {
        navigator.geolocation.watchPosition(function (position) {
            self.currentlatitude = position.coords.latitude;
            self.currentlongitude = position.coords.longitude;
            self.currentaltitude = position.coords.altitude;
            self.currentaccuracy = position.coords.accuracy;
            self.currentaltitudeAccuracy = position.coords.altitudeAccuracy;
            self.currentspeed = position.coords.speed;
            self.currentlocation = encodeGeoHash(position.coords.latitude, position.coords.longitude);
            // $("#footermenu").html("Heading :" + self.currentheading);
            if (callback) callback(position.coords.latitude, position.coords.longitude);
        });
    }
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.SDK.prototype.CurrentLocation = function ()
{
    var self = this;

    return ({latitude:self.currentlatitude, longitude:self.currentlongitude, altitude:self.currentaltitude});
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
