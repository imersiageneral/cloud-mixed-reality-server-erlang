// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Description: Channel datastructure - a writethrough cache.
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Constructor
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Channel = function (channel)
{
    this.channelid = channel.channelid;
    this.ownerid = channel.ownerid;
    this.class = channel.class;
    this.name = channel.name;
    this.description = channel.description;
    this.imageurl = channel.imageurl;
    this.hidden = channel.hidden;
    this.created = channel.created;
    this.modified = channel.modified;

    this.metadata = new IMERSIA.Metadata(channel.channelid, "channel");
    this.fileurls = [];

    this.geobots = {};

    this.callbacks = {};
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Save changes to a Channel
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Channel.prototype.Save = function (sessionid, details, callback, errorcallback)
{
    var self = this;

    APP.SDK.PutToAPI(sessionid, 'channels', {channelid : details.channelid}, details, function (result)
    {
        if (self.metadata)
        {
            self.metadata.Save(sessionid, function () {
                if (callback) callback(result);
            }, function (error) {
                if (errorcallback) errorcallback(error);
            });
        }
        else
        {
            if (callback) callback(result);
        }
    },
    function(error)
    {
        if (errorcallback) errorcallback(error);
    });
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Channel.prototype.LoadFilesAndMetadata = function (sessionid, callback, errorcallback)
{
    var self = this;

    APP.SDK.GetFromAPI(sessionid, 'files', {channelid : self.channelid}, function(files)
    {
        self.fileurls = files.fileurls;

        self.metadata.Load(sessionid, function () {
            if (callback) callback();
        }, function (error) {
            if (errorcallback) errorcallback();
        });
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Get a list of Geobots for this channel
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Channel.prototype.LoadGeobots = function (sessionid, callback, errorcallback)
{
    var self = this;

    // Clear any previously loaded Geobots
    self.geobots = {};

    // Get the list of geobots for this channel
    APP.SDK.GetFromAPI(sessionid, 'geobots', {channelid : self.channelid}, function (geobots)
    {
        // Create each Geobot
        var geobotsCount = geobots.length;
        geobots.forEach(function (geobot) {
            // Create the Geobot data structure
            var newGeobot = new IMERSIA.Geobot(geobot);
            self.geobots[geobot.geobotid] = newGeobot;

            newGeobot.Reload(sessionid,
                function ()
                {
                    if ((--geobotsCount == 0) && callback) callback();
                },
                function (error)
                {
                    if (errorcallback) errorcallback();
                });
        });
    },
    function(error)
    {
        if (errorcallback) errorcallback(error);
    });
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Define what happens when a wotcha event arrives for this channel
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Channel.prototype.Wotcha = function (message)
{
    var thischannel = this;

    if (message.hasOwnProperty("channel_set"))
    {
        if (thischannel.channelid == message.channel_set.channelid)
        {
            thischannel.name = message.channel_set.name || thischannel.name;
            thischannel.description = message.channel_set.description || thischannel.description;
            thischannel.imageurl = message.channel_set.imageurl || thischannel.imageurl;
            thischannel.class = message.channel_set.class || thischannel.class;
            thischannel.hidden = message.channel_set.hasOwnProperty("hidden") ?  message.channel_set.hidden : thischannel.hidden;

            // Call any other functions watching this channel
            Object.keys(thischannel.callbacks).forEach (function (ID) {
                thischannel.callbacks[ID] ( message );
            });
        }
    }
    else if (message.hasOwnProperty("channel_delete"))
    {
        Object.keys(thischannel.callbacks).forEach (function (ID) {
            if (thischannel.callbacks.hasOwnProperty(ID)) thischannel.callbacks[ID] ( message );
        });
    }
    else if (message.hasOwnProperty("geobot_new"))
    {
        if (message.geobot_new.channelid == APP.currentlySelectedChannel)
        {
            APP.Maps.AddPointToMap(message.geobot_new);
        }
    }
    else if (message.hasOwnProperty("channel_metadata"))
    {
        Object.keys(thischannel.callbacks).forEach (function (ID) {
            thischannel.callbacks[ID] ( message );
        });
    }
    else if (message.hasOwnProperty("geobot_metadata"))
    {
        Object.keys(thischannel.callbacks).forEach (function (ID) {
            thischannel.callbacks[ID] ( message );
        });
    }
    else if (message.hasOwnProperty("geobot_metadata_delete"))
    {
        Object.keys(thischannel.callbacks).forEach (function (ID) {
            thischannel.callbacks[ID] ( message );
        });
    }
    else if (message.hasOwnProperty("geobot_state_change"))
    {

    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Register a callback function that is interested in when a wotcha event occurs for this channel
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Channel.prototype.Register = function (ID, callback)
{
    this.callbacks[ID] = callback;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Remove a callback function from being interested in a wotcha event
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMERSIA.Channel.prototype.Deregister = function (ID)
{
    delete this.callbacks[ID];
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
