// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Description: Maps implemented using THREE.JS
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


IMERSIA.Mapthreed = function ()
{
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Public Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Private Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var imagecache = {};
    var mappoints = {};
    var mappointcolliders = [];

    var mapcentrelatitude = 0;
    var mapcentrelongitude = 0;
    var mapcentrealtitude = 0;

    var mappointslatitude = 0;
    var mappointslongitude = 0;

    var firsttime = true;
    var mousebuttonpressed = false;
    var dragging = false;
    // var movingmap = false;

    var character = "companion_male";
    var transparenttexture;

    var renderer;
    var camera;
    var clock;
    var scene;
    var objectscene;
    var radialbase;
    var canvas;
    var audioListener;
    var controls;
    var maptilecount = 11; // Must be an odd number
    var maplimit = Math.floor(maptilecount / 2);
    var maptilewidth = 3;
    var maptilesize = 1;
    var maptilescale = 1;
    var objectscaledivider = 15;    // Bigger means smaller
    var mapplane;
    var objectplane;
    var fractionalmapZoom = 18;
    var mapZoom = 18;
    var origin = new THREE.Vector3(0,0,0);
    var mapType = "mapboxlight";
    var trackingObject = null;
    var companionObject = null;
    var crosshairObject = null;

    var fixedUpdateDuration = 10;
    var currentFixedUpdateTime = 0;

    var raycaster = new THREE.Raycaster();
    var projectionraycaster = new THREE.Raycaster();
    var mouse = new THREE.Vector2();
    var mousepixel = new THREE.Vector2();
    var mousedownpos = new THREE.Vector2();
    var popup;
    var mousegeobotid = "";
    var mousedowngeobotid = "";

    var forceReload = false;

    var isplaying = false;

    var shaders = {
        fadeout : {
            vertex : `
                varying vec2 vUV;

                void main() {
                  vUV = uv;
                  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
                }
            `,

            fragment : `
                uniform sampler2D backtexture;
                uniform sampler2D texture;
                uniform sampler2D alpha;
                uniform vec2 offset;
                uniform vec2 repeat;

                varying vec2 vUV;

                void main() {
                    vec4 sampleback = texture2D(backtexture, vUV);
                    vec4 sample = texture2D(texture, vUV);
                    vec4 samplealpha = texture2D(alpha, offset + vUV * repeat);

                    if (sample.a < 0.01)
                    {
                        gl_FragColor = vec4( sampleback.rgb, sampleback.a * samplealpha.g );
                    }
                    else
                    {
                        gl_FragColor = vec4( sample.rgb, sample.a * samplealpha.g );
                    }
                }
            `
        },
        //gl_Position = projectionMatrix * (modelViewMatrix * vec4(0.0, 0.0, 0.0, 1.0) + vec4(position.x, position.y, 0.0, 0.0));
        cylindricalbillboard : {
            vertex : `
                void main() {
                    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
                }
            `,

            fragment : `
                uniform sampler2D texture;
                uniform vec2 offset;
                uniform vec2 repeat;

                void main() {
                    vec4 sample = texture2D(texture, offset + texture * repeat);

                    gl_FragColor = vec4( sample.rgba );
                }
            `
        }
    };

    // Pointer to self
    var self = this;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Tile and latitude / longitude conversion funcions.  From http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var long2tile = function (lon,zoom) { return ((lon+180.0)/360.0*Math.pow(2.0,zoom)); }
    var lat2tile = function (lat,zoom)  { return ((1.0-Math.log(Math.tan(lat*Math.PI/180.0) + 1.0/Math.cos(lat*Math.PI/180.0))/Math.PI)/2.0 *Math.pow(2.0,zoom)); }
    var tile2long = function (x,z) { return (x/Math.pow(2.0,z)*360.0-180.0); }
    var tile2lat = function (y,z) { var n=Math.PI-2.0*Math.PI*y/Math.pow(2.0,z); return (180.0/Math.PI*Math.atan(0.5*(Math.exp(n)-Math.exp(-n)))); }
    var tilescale = function (lat, zoom) {return 40075017.0 * Math.cos(lat*Math.PI/180.0)/Math.pow(2.0, zoom+8.0)};
    var deg2rad = 0.017453293;
    var rad2deg = 57.29577951;
    var GetBounds = function ()
    {
       var tilex = Math.floor(long2tile(mapcentrelongitude, mapZoom));
       var tiley = Math.floor(lat2tile(mapcentrelatitude, mapZoom));

       var south = tile2lat(tiley - maplimit, mapZoom);
       var north = tile2lat(tiley + maplimit + 1.0, mapZoom);
       var east = tile2long(tilex - maplimit, mapZoom);
       var west = tile2long(tilex + maplimit + 1.0, mapZoom);

       return ({north : north, south : south, west : west, east : east});
    }
    var getDistance = function (lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad * (lat2-lat1);
        var dLon = deg2rad * (lon2-lon1);
        var a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(deg2rad * lat1) * Math.cos(deg2rad * lat2) *
            Math.sin(dLon/2) * Math.sin(dLon/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c; // Distance in km
        return d;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Change the map provider
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetMapType = function (newType)
    {
        if (newType) mapType = newType;
        RecentreMap(mapcentrelatitude, mapcentrelongitude);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Resize the map canvas
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ResizeCanvas = function()
    {
        // return setInterval(function() {
        var h = $(window).height();
        var w = $(window).width();

        var mapheight = h - $("#footer").outerHeight() + (APP.threedmode ? 5 : 0);
        $("#threedmap").height(mapheight);
        $("#threedmap").width(w);

        var aspect = w / mapheight;

        camera.fov = 30;

        // notify the renderer of the size change
        renderer.setSize( w, mapheight );

        // update the camera
        camera.aspect = aspect;
        camera.updateProjectionMatrix();
        // }, 100);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Adjust the central location of the map
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var AdjustCentre = function()
    {
        var theBounds = GetBounds();
        var y2 = theBounds.north;
        var x2 = theBounds.west;
        var y1 = theBounds.south;
        var x1 = theBounds.east;
        var xpos = 0;
        var ypos = 0;

        var tilex = Math.floor(long2tile(mapcentrelongitude, mapZoom));
        var tiley = Math.floor(lat2tile(mapcentrelatitude, mapZoom));
        var centrelat = tile2lat(tiley + 0.5, mapZoom);
        var centrelong = tile2long(tilex + 0.5, mapZoom);

        var latdiff = y2 - y1;
        var longdiff = x2 - x1;

        var mapWidthInPixels = maptilecount * maptilesize;

        var rellat = mapcentrelatitude - centrelat;
        var rellong = mapcentrelongitude - centrelong;

        xpos = (rellong * mapWidthInPixels / longdiff);
        ypos = (rellat * mapWidthInPixels / latdiff);

        mapplane.position.set(-xpos, 0, -ypos);
        objectplane.position.set(-xpos, 0, -ypos);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var GetMapExtent = function ()
    {
        var point1 = ProjectFromMap({x:0, y:0});
        var point2 = ProjectFromMap({x:0.1, y:0.1});

        var distance = getDistance(point1.latitude, point1.longitude, point2.latitude, point2.longitude) * 10000;  // 10s of meters

        return Math.max(1, Math.round(distance * 2));
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Recentre the map at the given location
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var prevtilex = -1;
    var prevtiley = -1;
    var RecentreMap = function(latitude, longitude)
    {
        if (latitude == undefined) latitude = APP.Maps.currentlatitude;
        if (longitude == undefined) longitude = APP.Maps.currentlongitude;

        var tiley = Math.floor(lat2tile(latitude, mapZoom));
        var tilex = Math.floor(long2tile(longitude, mapZoom));

        mapcentrelatitude = latitude;
        mapcentrelongitude = longitude;

        // if ((tilex != prevtilex) || (tiley != prevtiley)) {
            SetMapTileImages (tilex, tiley);
        // }

        prevtilex = tilex;
        prevtiley = tiley;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Load a texture and cache it for later (so we only need to load it once)
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var LoadTexture = function (imageURL, callbackFunction, passthrough, forceReload, dontCache)
    {
        var theTexture = null;
        if (forceReload == undefined) forceReload = false;
        if (dontCache == undefined) dontCache = false;
        if (passthrough == undefined) passthrough = {};

        if (imagecache.hasOwnProperty(imageURL) && !forceReload)
        {
            if (callbackFunction) callbackFunction (imagecache[imageURL], passthrough);
        }
        else
        {
            var loader = new THREE.TextureLoader();
            loader.load(
                imageURL,
                function (theTexture)
                {
                    if (!dontCache) { imagecache[imageURL] = theTexture };
                    if (callbackFunction) { callbackFunction (theTexture, passthrough) };
                },
            	// Function called when download progresses
            	function ( xhr ) {
            		//console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
            	},
            	// Function called when download errors
            	function ( xhr ) {
            		console.log( 'An error loading image ' + imageURL );
                    if (callbackFunction) callbackFunction (null, passthrough);
            	}
            );
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var makeTextSprite = function( message, parameters )
    {
    	if ( parameters === undefined ) parameters = {};

    	var fontface = parameters.hasOwnProperty("fontface") ?
    		parameters["fontface"] : "Arial";

    	var fontsize = parameters.hasOwnProperty("fontsize") ?
    		parameters["fontsize"] : 18;

    	var borderThickness = parameters.hasOwnProperty("borderThickness") ?
    		parameters["borderThickness"] : 2;

    	var borderColor = parameters.hasOwnProperty("borderColor") ?
    		parameters["borderColor"] : { r:0, g:0, b:0, a:1.0 };

    	var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?
    		parameters["backgroundColor"] : { r:255, g:255, b:255, a:1.0 };

    	var canvas = document.createElement('canvas');
    	var context = canvas.getContext('2d');
    	context.font = "Bold " + fontsize + "px " + fontface;

    	// get size data (height depends only on font size)
    	var metrics = context.measureText( message );
    	var textWidth = metrics.width;

    	// background color
    	context.fillStyle   = "rgba(" + backgroundColor.r + "," + backgroundColor.g + ","
    								  + backgroundColor.b + "," + backgroundColor.a + ")";
    	// border color
    	context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + ","
    								  + borderColor.b + "," + borderColor.a + ")";

    	context.lineWidth = borderThickness;
    	roundRect(context, borderThickness/2, borderThickness/2, textWidth + borderThickness, fontsize * 1.4 + borderThickness, 6);
    	// 1.4 is extra height factor for text below baseline: g,j,p,q.

    	// text color
    	context.fillStyle = "rgba(0, 90, 185, 1.0)";

    	context.fillText( message, borderThickness, fontsize + borderThickness);

    	// canvas contents will be used for a texture
    	var texture = new THREE.Texture(canvas)
    	texture.needsUpdate = true;

        var geometry = new THREE.PlaneGeometry( 1, 1, 1, 1 );
        var material = new THREE.MeshBasicMaterial( {map: texture, side: THREE.FrontSide, transparent: true, depthTest: false} );
        var sprite = new THREE.Mesh( geometry, material );

    	// var spriteMaterial = new THREE.SpriteMaterial( { map: texture } );
    	// var sprite = new THREE.Sprite( spriteMaterial );
    	sprite.scale.set(100, 50, 1.0);
    	return sprite;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // function for drawing rounded rectangles
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var roundRect = function(ctx, x, y, w, h, r)
    {
        ctx.beginPath();
        ctx.moveTo(x+r, y);
        ctx.lineTo(x+w-r, y);
        ctx.quadraticCurveTo(x+w, y, x+w, y+r);
        ctx.lineTo(x+w, y+h-r);
        ctx.quadraticCurveTo(x+w, y+h, x+w-r, y+h);
        ctx.lineTo(x+r, y+h);
        ctx.quadraticCurveTo(x, y+h, x, y+h-r);
        ctx.lineTo(x, y+r);
        ctx.quadraticCurveTo(x, y, x+r, y);
        ctx.closePath();
        ctx.fill();
    	ctx.stroke();
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Set the images on the map tiles with the given tile numbers being the centre
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetMapTileImages = function (tilex, tiley)
    {
        var numTiles = maptilecount * maptilecount;
        var numTilesLoading = 0;

        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Local function to load a texture onto a tile, taking into account the various options
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        function CheckAndCache(maptype, ownmap, cachemap, mapzoom, actualx, actualy, localurl, remoteurl, center, callback)
        {
            if (cachemap)
            {
                LoadTexture ( localurl + "?url=" + remoteurl, function (thetexture, parameters)
                    {
                        callback(thetexture, parameters);
                    },
                    {
                        actualx:actualx, actualy:actualy, mapzoom:mapzoom, maptype:maptype, timestamp:new Date()
                    }
                );
            }
            else
            {
                LoadTexture ( remoteurl, function (thetexture, parameters) { callback(thetexture, parameters); }, {actualx:actualx, actualy:actualy, mapzoom:mapzoom, maptype:maptype, timestamp:new Date()} );
            }
        };
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        function LoadTileTexture(tileurl, ownmap, cachemap, alphatexture, maptype, mapzoom, actualx, actualy, mapchild, center)
        {
            var localtilebackurl = 'https://' + location.host + '/maptile/mapboxlight/' + mapzoom + '/' + actualx + '/' + actualy;
            var tilebackurl = 'https://api.mapbox.com/styles/v1/mapbox/light-v9/tiles/512/' + mapZoom + '/' + actualx + '/' + actualy + '?access_token=' + APP.Maps.mapboxid;
            var localtileurl = 'https://' + location.host + '/maptile/' + maptype + '/' + mapzoom + '/' + actualx + '/' + actualy;

            // Check back texture
            CheckAndCache("mapboxlight", false, true, mapzoom, actualx, actualy, localtilebackurl, tilebackurl, center, function (backtexture) {
                CheckAndCache(maptype, ownmap, cachemap, mapzoom, actualx, actualy, localtileurl, tileurl, center, function (maintexture, parameters)
                {
                    // Due to the asynchronous nature of texture loading, we need to ensure the texture put on the tile is
                    // the latest one and for this current zoom level since an earlier one might take longer and thus overwrite the current tile image
                    // if ((parameters.mapzoom == mapZoom) && (parameters.timestamp >= mapchild.timestamp) && (parameters.maptype == maptype))
                    if ((parameters.mapzoom == mapZoom) && (parameters.maptype == maptype))
                    {
                        var mapx = mapchild.mapindex[0];
                        var mapy = -mapchild.mapindex[1];

                        // console.log(JSON.stringify(parameters));
                        var offset = new THREE.Vector2((mapx + maplimit) / maptilecount, (mapy + maplimit) / maptilecount);
                        var repeat = new THREE.Vector2(1 / maptilecount, 1 / maptilecount);

                        var uniforms = {
                            texture: { type: 't', value: maintexture },
                            backtexture: { type: 't', value: backtexture },
                            alpha: { type: 't', value: alphatexture },
                            offset: {type: 'v2', value: offset },
                            repeat: {type: 'v2', value: repeat }
                        };
                        var material = new THREE.ShaderMaterial( {
                            uniforms:       uniforms,
                            vertexShader:   shaders.fadeout.vertex,
                            fragmentShader: shaders.fadeout.fragment,
                            side: THREE.FrontSide,
                            transparent: true
                        });

                        mapchild.material = material;
                        mapchild.material.needsUpdate = true;
                        mapchild.visible = true;
                        // mapchild.timestamp = new Date();
                        mapchild.timestamp = parameters.timestamp;
                    }
                });
            });
        };
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        LoadTexture
        (
            "images/maproundcutout.png",
            function (alphatexture)
            {
                $.each(mapplane.children, function (i, mapchild) {
                    if (mapchild.hasOwnProperty("mapindex"))
                    {
                        var mapx = mapchild.mapindex[0];
                        var mapy = mapchild.mapindex[1];
                        var actualx = tilex + mapx;
                        if (actualx > (Math.pow(2, mapZoom) - 1)) actualx = actualx - (Math.pow(2, mapZoom));
                        if (actualx < 0) actualx = actualx + (Math.pow(2, mapZoom));
                        var actualy = tiley + mapy;

                        var tileurl = "";
                        var cachemap = true;
                        var ownmap = false;

                        switch (mapType)
                        {
                            case "com.tomtom.default":
                                tileurl = 'https://api.tomtom.com/map/1/tile/basic/main/' + mapZoom + '/' + actualx + '/' + actualy + '.png?tileSize=512&key=' + APP.Maps.tomtomid;
                                break;
                            case "com.openstreetmap.default":
                                tileurl = "http://c.tile.openstreetmap.org/" + mapZoom + "/" + actualx + "/" + actualy + ".png";
                                break;
                            case "com.arcgis.default":
                                tileurl = "http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/" + mapZoom + "/" + actualy + "/" + actualx    ;
                                break;
                            case "com.mapbox.streets":
                                tileurl = 'https://api.mapbox.com/styles/v1/mapbox/streets-v9/tiles/512/' + mapZoom + '/' + actualx + '/' + actualy + '?access_token=' + APP.Maps.mapboxid;
                                break;
                            case "com.mapbox.light":
                                tileurl = 'https://api.mapbox.com/styles/v1/mapbox/light-v9/tiles/512/' + mapZoom + '/' + actualx + '/' + actualy + '?access_token=' + APP.Maps.mapboxid;
                                break;
                            case "com.mapbox.outdoors":
                                tileurl = 'https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/512/' + mapZoom + '/' + actualx + '/' + actualy + '?access_token=' + APP.Maps.mapboxid;
                                break;
                            case "com.stamen.watercolor":
                                tileurl = 'http://b.tile.stamen.com/watercolor/' + mapZoom + '/' + actualx + '/' + actualy + '.jpg';
                                break;
                            case "com.mapbox.dark":
                                tileurl = 'https://api.mapbox.com/styles/v1/mapbox/dark-v9/tiles/512/' + mapZoom + '/' + actualx + '/' + actualy + '?access_token=' + APP.Maps.mapboxid;
                                break;
                            case "com.mapbox.satellite":
                                // tileurl = "https://mts1.google.com/vt/lyrs=y&x=" + actualx + "&y=" + actualy + "&z=" + mapZoom ;
                                tileurl = 'https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v9/tiles/512/' + mapZoom + '/' + actualx + '/' + actualy + '?access_token=' + APP.Maps.mapboxid;
                                break;
                            default:
                                cachemap = false;
                                tileurl = "images/grid.png";
                                break;
                        };

                        // NB For google maps, use lyrs=
                        // h = roads only
                        // m = standard roadmap
                        // p = terrain
                        // r = somehow altered roadmap
                        // s = satellite only
                        // t = terrain only
                        // y = hybrid

                        // Load a temporary image whilst the other is loading
                        LoadTexture(
                            // ((mapx == 0) && (mapy == 0)) ? "images/loading.png" : "images/blank.png",
                            "images/grid.png",
                            function (texture, parameters)
                            {
                                // if ((parameters.mapzoom == mapZoom) && (parameters.timestamp >= mapchild.timestamp) && (parameters.maptype == mapType))
                                if ((parameters.mapzoom == mapZoom) && (parameters.maptype == mapType))
                                {
                                    var offset = new THREE.Vector2((parameters.mapx + maplimit) / maptilecount, (parameters.mapy + maplimit) / maptilecount);
                                    var repeat = new THREE.Vector2(1 / maptilecount, 1 / maptilecount);

                                    var uniforms = {
                                        texture: { type: 't', value: texture },
                                        backtexture: { type: 't', value: transparenttexture },
                                        alpha: { type: 't', value: alphatexture },
                                        offset: {type: 'v2', value: offset },
                                        repeat: {type: 'v2', value: repeat }
                                    };
                                    var material = new THREE.ShaderMaterial( {
                                        uniforms:       uniforms,
                                        vertexShader:   shaders.fadeout.vertex,
                                        fragmentShader: shaders.fadeout.fragment,
                                        side: THREE.FrontSide,
                                        transparent: true
                                    });

                                    mapchild.material = material;
                                    mapchild.material.needsUpdate = true;
                                    mapchild.visible = true;
                                    // mapchild.timestamp = new Date();
                                    mapchild.timestamp = parameters.timestamp;
                                };
                            },
                            {mapx:mapx, mapy:-mapy, mapzoom:mapZoom, maptype:mapType, timestamp:new Date()}
                        );

                        LoadTileTexture(tileurl, ownmap, cachemap, alphatexture, mapType, mapZoom, actualx, actualy, mapchild, ((mapx == 0) && (mapy == 0)));
                    }
                });
            }
        );
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Adjust the size of the map tiles in order to keep the relative size of the imagery on the screen scaling in proportion between the different zoom levels
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ResizeMapTiles = function ()
    {
        $.each(mapplane.children, function (i, mapchild) {
            if (mapchild.hasOwnProperty("mapindex"))
            {
                var mapx = mapchild.mapindex[0];
                var mapy = mapchild.mapindex[1];
                mapchild.position.set(mapx * maptilesize, -mapy * maptilesize, 0);
                mapchild.scale.set(maptilescale, maptilescale, 1);
            }
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Works out and returns a vector of the given position in coordinates that would put it in the
    // correct place on the map (or zero vector if no map)
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ProjectToMap = function (objLatitude, objLongitude, objAltitude)
    {
        if (objAltitude == undefined) objAltitude = 0;

        // Get tile positions for this location
        var xtile = long2tile(objLongitude, mapZoom);
        var ytile = lat2tile(objLatitude, mapZoom);

        // Get the ratio across this tile of this position.
        var xratio = (xtile - Math.floor(xtile) - 0.5);
        var yratio = (ytile - Math.floor(ytile) - 0.5);

        var objtilex = Math.floor(xtile) - Math.floor(long2tile(mapcentrelongitude, mapZoom));
        var objtiley = Math.floor(ytile) - Math.floor(lat2tile(mapcentrelatitude, mapZoom));

        var xpos = (objtilex + xratio) * maptilesize;
        var ypos = (objtiley + yratio) * maptilesize;
        var zpos = objAltitude / 4;

        return new THREE.Vector3(xpos, -ypos, zpos );
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Given the relative mouse position of the mouse, return the map coordinates
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ProjectFromMap = function (mouseposition)
    {
        projectionraycaster.setFromCamera( mouseposition, camera );
        var mapx = Math.floor(long2tile(mapcentrelongitude, mapZoom));
        var mapy = Math.floor(lat2tile(mapcentrelatitude, mapZoom));

        // calculate objects intersecting the picking ray
        var intersects = projectionraycaster.intersectObjects( mapplane.children );

        if (intersects.length > 0)
        {
            intersects[0].object.updateMatrixWorld();
            var localposition = intersects[0].point.clone();
            intersects[0].object.worldToLocal(localposition);

            var relx = ((localposition.x + maptilewidth/2) / maptilewidth);
            var rely = ((localposition.y + maptilewidth/2) / maptilewidth);

            var tilex = mapx + intersects[0].object.mapindex[0] + relx;
            var tiley = mapy + intersects[0].object.mapindex[1] + (1-rely);

            var mouselat = tile2lat(tiley, mapZoom);
            var mouselong = tile2long(tilex, mapZoom);
        }

        return({latitude: mouselat, longitude: mouselong});
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Get the location of the current map centre
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var GetCurrentLocation = function()
    {
        return ({latitude: mapcentrelatitude, longitude: mapcentrelongitude});
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var DrawPointsOnMap = function ()
    {
        function GetRotation (mapobject)
        {
            var b = mapobject.position.clone();
            var v = camera.position.clone();
            var yhat = new THREE.Vector3 (0,1,0);
            var rotaxis = new THREE.Vector3(0,0,0);

            v.sub (b);  // v-b
            v.sub (yhat.multiplyScalar(v.dot(yhat))); v.normalize();  // pxz

            var rotangle = Math.acos (v.dot(new THREE.Vector3(0,0,1)));
            rotaxis.crossVectors (new THREE.Vector3(0,0,1),v);
            if (rotaxis.dot(new THREE.Vector3(0,1,0)) < 0) rotangle *= -1;

            return rotangle;
        }

        var objectscale = Math.max(0.1, camera.position.distanceTo(origin)/objectscaledivider);
        if (companionObject)
        {
            var newPos = ProjectToMap(mapcentrelatitude, mapcentrelongitude);
            companionObject.scale.set(objectscale, objectscale, objectscale);
            companionObject.position.set(newPos.x, newPos.y, newPos.z + objectscale/2);
            companionObject.rotation.set(Math.PI/2, GetRotation(companionObject), 0);
        }

        var mappointsids = Object.keys(mappoints);
        for (var i = 0; i < mappointsids.length; i++)
        {
            var id = mappointsids[i];
            var point = mappoints[id];

            var pointposition = ProjectToMap(point.geobot.location.latitude, point.geobot.location.longitude, point.geobot.location.altitude);

            var angdist = 1 / 6371000; // / Math.PI;
            var lat1rad = point.geobot.location.latitude * deg2rad;
            var lat2rad = Math.asin(Math.sin(lat1rad) * Math.cos(angdist) + Math.cos(lat1rad) * Math.sin(angdist));
            var pointposition2 = ProjectToMap(lat2rad * rad2deg, point.geobot.location.longitude, point.geobot.location.altitude);
            var xdiff = pointposition2.x - pointposition.x;
            var ydiff = pointposition2.y - pointposition.y;
            var pointScale = Math.sqrt(xdiff * xdiff + ydiff * ydiff);

            pointposition.z = (pointposition.z + (APP.firstpersonmode?4:0)) * pointScale * 20 + objectscale / 2;

            if (point.hasOwnProperty("mapobject"))
            {
                point.mapobject.visible = true;
                point.mapobject.position.set(pointposition.x, pointposition.y, 0);
                point.mapobject.scale.set(objectscale, objectscale, objectscale);
            }

            if (point.hasOwnProperty("mapsprite"))
            {
                point.mapsprite.rotation.set(Math.PI/2, GetRotation(point.mapsprite), 0);
                if (point.geobot.class == "com.imersia.sprite")
                {
                    var spritescale = objectscale;
                    var spriteratio = point.spriteratio;
                    var spritedetails = point.geobot.metadata.GetByKey("com.imersia.sprite");
                    if (spritedetails != undefined)
                    {
                        spritescale *= spritedetails.value.scale;
                        spriteratio = point.spriteratio * spritedetails.value.height / spritedetails.value.width;
                    }
                    point.mapsprite.scale.set(spritescale * spriteratio, spritescale, spritescale)
                    point.mapsprite.position.set(pointposition.x, pointposition.y, pointposition.z);
                }
                else
                {
                    point.mapsprite.scale.set(objectscale/2, objectscale/2, objectscale/2)
                    point.mapsprite.position.set(pointposition.x, pointposition.y, pointposition.z);
                }
            }

            if (point.hasOwnProperty("mappin"))
            {
                if (point.geobot.class == "com.imersia.default")
                {
                    point.mappin.visible = true;
                    point.mappin.position.set(pointposition.x, pointposition.y, pointposition.z);
                    point.mappin.rotation.set(Math.PI/2, GetRotation(point.mappin), 0);
                    point.mappin.scale.set(objectscale, objectscale, objectscale)
                }
                else
                {
                    point.mappin.visible = false;
                }
            }

            if (point.hasOwnProperty("mapradius"))
            {
                if (APP.playing)
                {
                    point.mapradius.visible = false;
                }
                else {
                    point.mapradius.visible = true;
                    var radiusScale = Math.max(0.1, point.geobot.radius) * pointScale;

                    point.mapradius.position.set(pointposition.x, pointposition.y, 0);
                    point.mapradius.scale.set(radiusScale, radiusScale, radiusScale);
                }
            }

            if (point.hasOwnProperty("maplineobject"))
            {
                if (APP.playing)
                {
                    point.maplineobject.visible = false;
                }
                else
                {
                    var lineScale = point.geobot.location.altitude * pointScale;
                    var lineWidth = objectscale / 100;
                    point.maplineobject.visible = true;
                    point.maplineobject.position.set(pointposition.x, pointposition.y, pointposition.z / 2);
                    point.maplineobject.scale.set(lineWidth, lineWidth, pointposition.z );
                }
            }
        };

        isplaying = APP.playing;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var UpdateGeobotOnMap = function(message)
    {
        if (message.hasOwnProperty("geobot_set"))
        {
            AddPointToMap(message.geobot_set); // Only adds if new point, otherwise modifies
        }
        else if (message.hasOwnProperty("geobot_delete"))
        {
            RemovePointFromMap(message.geobot_delete.geobotid);
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var RemovePointFromMap = function (geobotid)
    {
        // Remove points from the object plane
        var the3DObject = mappoints[geobotid];

        if (the3DObject.hasOwnProperty("mapobject"))
        {
            // Update the colliders
            var newColliders = [];
            mappointcolliders.forEach(function (mappointcollider) {
                if (the3DObject.mapobject != mappointcollider) newColliders.push(mappointcollider);
            });
            mappointcolliders = newColliders;
            objectplane.remove(the3DObject.mapobject);
            delete the3DObject.mapobject;
        }
        if (the3DObject.hasOwnProperty("maplineobject"))
        {
            objectplane.remove(the3DObject.maplineobject);
            delete the3DObject.maplineobject;
        }
        if (the3DObject.hasOwnProperty("mapsprite"))
        {
            objectplane.remove(the3DObject.mapsprite);
            delete the3DObject.mapsprite;
        }
        if (the3DObject.hasOwnProperty("mappin"))
        {
            objectplane.remove(the3DObject.mappin);
            delete the3DObject.mappin;
        }
        if (the3DObject.hasOwnProperty("mapradius"))
        {
            objectplane.remove(the3DObject.mapradius);
            delete the3DObject.mapradius;
        }

        if (the3DObject.hasOwnProperty('audio'))
        {
            delete the3DObject.audio;
        }

        var thisGeobot = the3DObject.geobot;
        thisGeobot.Deregister("mapthreed");

        // Remove the sprite (if there is one)
        APP.AnimatedSprites.RemoveSprite(thisGeobot.geobotid);

        // Remove the line (if there is one)
        // APP.LineParticles.RemoveLine(objectplane, obj.userData.geobotid);

        // Remove the object from the scenegraph
        objectplane.remove(the3DObject);

        // Clear the point from the table and array of colliders
        delete mappoints[geobotid];

        // Update the map
        DrawPointsOnMap();

        if (APP.currentlySelectedGeobot == geobotid)
        {
            APP.Geobot_UI.Close();
            APP.GeobotEdit_UI.Close();
            APP.Geobot_UI.ClearOutput();
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var AddPointToMap = function (pointdetails)
    {
        var alreadyExists = (mappoints.hasOwnProperty(pointdetails.geobotid));

        if (!alreadyExists) mappoints[pointdetails.geobotid] = {};

        // Grab the Geobot Datastructure
        var thisGeobot = APP.companion.channels[pointdetails.channelid].geobots[pointdetails.geobotid];
        mappoints[pointdetails.geobotid].geobot = thisGeobot;

        // Load the basic textures for the line, pin, radius and map icon
        LoadTexture
        (
            "images/line.png",
            function ( texture ) {
                if (mappoints[pointdetails.geobotid].maplineobject)
                {
                    mappoints[pointdetails.geobotid].maplineobject.material.map = texture;
                    mappoints[pointdetails.geobotid].maplineobject.material.needsUpdate = true;
                }
                else
                {
                    var geometry = new THREE.BoxGeometry( 1, 1, 1 );
                    var material = new THREE.MeshBasicMaterial( {map: texture, side: THREE.FrontSide, transparent: true, depthTest: false} );
                    mappoints[pointdetails.geobotid].maplineobject = new THREE.Mesh( geometry, material );
                    objectplane.add(mappoints[pointdetails.geobotid].maplineobject);
                }
            }
        );

        LoadTexture
        (
            "images/shadow.png",
            function ( texture ) {
                if (mappoints[pointdetails.geobotid].mapobject)
                {
                    mappoints[pointdetails.geobotid].mapobject.material.map = texture;
                    mappoints[pointdetails.geobotid].mapobject.material.needsUpdate = true;
                }
                else
                {
                    var geometry = new THREE.PlaneGeometry( 1, 1, 1, 1 );
                    var material = new THREE.MeshBasicMaterial( {map: texture, side: THREE.FrontSide, transparent: true, depthTest: false} );
                    mappoints[pointdetails.geobotid].mapobject = new THREE.Mesh( geometry, material );
                    mappoints[pointdetails.geobotid].mapobject.userData = pointdetails;
                    objectplane.add(mappoints[pointdetails.geobotid].mapobject);
                    mappointcolliders.push(mappoints[pointdetails.geobotid].mapobject);
                }
            }
        );

        LoadTexture
        (
            "images/pin.png",
            function (texture ) {
                if (mappoints[pointdetails.geobotid].mappin)
                {
                    mappoints[pointdetails.geobotid].mappin.material.map = texture;
                    mappoints[pointdetails.geobotid].mappin.material.needsUpdate = true;
                }
                else
                {
                    var geometry = new THREE.PlaneGeometry( 1, 1, 1, 1 );
                    var material = new THREE.MeshBasicMaterial( {map: texture, side: THREE.FrontSide, transparent: true, depthTest: false} );
                    mappoints[pointdetails.geobotid].mappin = new THREE.Mesh( geometry, material );
                    objectplane.add(mappoints[pointdetails.geobotid].mappin);
                }
            }
        );

        // The radius circle
        if (!mappoints[pointdetails.geobotid].mapradius)
        {
            var geometry = new THREE.CircleGeometry( 1, 32 );
            var material = new THREE.MeshBasicMaterial( { color: 0x222288, side: THREE.FrontSide, transparent: true, depthTest: false, opacity: 0.1 } );
            mappoints[pointdetails.geobotid].mapradius = new THREE.Mesh( geometry, material );
            objectplane.add( mappoints[pointdetails.geobotid].mapradius );
        }

        // Load the image for inside the pin
        var mapimageurl = "";
        var spritedetails = null;
        if (pointdetails.class == "com.imersia.sprite")
        {
            spritedetails = thisGeobot.metadata.GetByKey("com.imersia.sprite");
            if (spritedetails != undefined)
            {
                mapimageurl = spritedetails.value.imageurl;
            }
            else
            {
                mapimageurl = APP.Classes.InterpretContentURL(pointdetails.geobotid, pointdetails.imageurl, "size=small&shape=round");
            }
        }
        else {
            mapimageurl = APP.Classes.InterpretContentURL(pointdetails.geobotid, pointdetails.imageurl, "size=small&shape=round");
        }

        LoadTexture
        (
            mapimageurl,
            function (texture1) {
                function DoLoadTexture(texture)
                {
                    if (mappoints[pointdetails.geobotid].mapsprite)
                    {
                        mappoints[pointdetails.geobotid].mapsprite.material.map = texture;
                        mappoints[pointdetails.geobotid].mapsprite.material.needsUpdate = true;
                        if (spritedetails)
                        {
                            APP.AnimatedSprites.AddSprite(pointdetails.geobotid, texture, spritedetails.value, false);
                        }
                    }
                    else
                    {
                        var geometry = new THREE.PlaneGeometry( 1, 1, 1, 1 );
                        var material = new THREE.MeshBasicMaterial( {map: texture, side: THREE.FrontSide, transparent: true, depthTest: false} );

                        mappoints[pointdetails.geobotid].mapsprite = new THREE.Mesh( geometry, material );
                        mappoints[pointdetails.geobotid].spriteratio = texture.image.width / texture.image.height;
                        objectplane.add(mappoints[pointdetails.geobotid].mapsprite);
                        mappoints[pointdetails.geobotid].mapsprite.material.needsUpdate = true;
                        if (spritedetails)
                        {
                            APP.AnimatedSprites.AddSprite(pointdetails.geobotid, texture, spritedetails.value, false);
                        }
                        mappoints[pointdetails.geobotid].mapsprite.userData = pointdetails;
                        mappointcolliders.push(mappoints[pointdetails.geobotid].mapsprite);
                    };
                }

                if (texture1 == null)
                {
                    LoadTexture(
                        '/files?url=images/noimage.png&size=small&shape=round',
                        function (texture2)
                        {
                            DoLoadTexture(texture2);
                        }
                    )
                }
                else
                {
                    DoLoadTexture(texture1);
                }
            }, {}, true, true
        );

        // Load the Audio if it exists
        var audiodetails = thisGeobot.metadata.GetByKey("com.imersia.audio");
        if (audiodetails && (audiodetails.value.audiourl != ""))
        {
            if (mappoints[pointdetails.geobotid].hasOwnProperty("audio"))
            {
                mappoints[pointdetails.geobotid].mapsprite.remove( mappoints[pointdetails.geobotid].audio );
                mappoints[pointdetails.geobotid].audio = null;
            }
            mappoints[pointdetails.geobotid].audio = new THREE.PositionalAudio( audioListener );
            var audioLoader = new THREE.AudioLoader();
            audioLoader.load(audiodetails.value.audiourl, function ( buffer ) {
                mappoints[pointdetails.geobotid].audio.setBuffer( buffer );
                mappoints[pointdetails.geobotid].audio.setRefDistance( 0.1 );
                mappoints[pointdetails.geobotid].audio.setRolloffFactor( 1 );
                mappoints[pointdetails.geobotid].audio.setDistanceModel( "exponential" );
                mappoints[pointdetails.geobotid].audio.setLoop(audiodetails.value.repeat == true);
                mappoints[pointdetails.geobotid].mapsprite.add( mappoints[pointdetails.geobotid].audio );
            });
        }
        else
        {
            if (mappoints[pointdetails.geobotid].hasOwnProperty("audio"))
            {
                mappoints[pointdetails.geobotid].audio.stop();
                delete mappoints[pointdetails.geobotid].audio;
            }
        }

        // Trigger an update on the Automations states
        APP.companion.SendCommand(pointdetails.geobotid, "status", {});

        // Set up the wotching for this Geobot
        thisGeobot.Register("mapthreed", UpdateGeobotOnMap.bind(thisGeobot));

        // if (pointdetails.previd != "") APP.LineParticles.AddLine(objectplane, pointdetails.previd, pointdetails.geobotid);


        // Calculate the centre of the points on the map
        if (!alreadyExists)
        {
            if (Object.keys(mappoints).length == 1) {
                mappointslatitude = pointdetails.location.latitude;
                mappointslongitude = pointdetails.location.longitude;
            }
            else {
                mappointslatitude = (mappointslatitude + pointdetails.location.latitude) / 2;
                mappointslongitude = (mappointslongitude + pointdetails.location.longitude) / 2;
            }
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ClearPointsFromMap = function ()
    {
        Object.keys(mappoints).forEach(function (mappointid)
        {
            ClearMapPoint(mappointid);
        });

        // Clear the table of points and colliders
        mappoints = {};
        mappointcolliders = [];

        // Update the map
        DrawPointsOnMap();

        // Clear the central point for the map based on the points
        mappointslatitude = 0;
        mappointslongitude = 0;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ClearMapPoint = function(mappointid)
    {
        var the3DObject = mappoints[mappointid];

        if (the3DObject.hasOwnProperty("mapobject"))
        {
            objectplane.remove(the3DObject.mapobject);
            delete the3DObject.mapobject;
        }
        if (the3DObject.hasOwnProperty("maplineobject"))
        {
            objectplane.remove(the3DObject.maplineobject);
            delete the3DObject.maplineobject;
        }
        if (the3DObject.hasOwnProperty("mapsprite"))
        {
            objectplane.remove(the3DObject.mapsprite);
            delete the3DObject.mapsprite;
        }
        if (the3DObject.hasOwnProperty("mappin"))
        {
            objectplane.remove(the3DObject.mappin);
            delete the3DObject.mappin;
        }
        if (the3DObject.hasOwnProperty("mapradius"))
        {
            objectplane.remove(the3DObject.mapradius);
            delete the3DObject.mapradius;
        }

        if (the3DObject.hasOwnProperty('audio'))
        {
            delete the3DObject.audio;
        }

        var thisGeobot = the3DObject.geobot;
        thisGeobot.Deregister("mapthreed");

        // Remove the sprite (if there is one)
        APP.AnimatedSprites.RemoveSprite(thisGeobot.geobotid);

        // Remove the line (if there is one)
        // APP.LineParticles.RemoveLine(objectplane, obj.userData.geobotid);

        // Remove the object from the scenegraph
        objectplane.remove(the3DObject);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var PlayAudioOneShot = function(geobotid)
    {
        if (mappoints.hasOwnProperty(geobotid) && APP.playing)
        {
            if (mappoints[geobotid].audio)
            {
                mappoints[geobotid].audio.play();
            }
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CentreMapOnPoints = function()
    {
        RecentreMap(mappointslatitude, mappointslongitude);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var onMouseMove = function( event )
    {
        var rect = canvas.getBoundingClientRect();

        var x = event.clientX - rect.left;
        var y = event.clientY - rect.top;

        mousepixel.x = event.clientX;
        mousepixel.y = event.clientY - 20;

        // calculate mouse position in normalized device coordinates
        // (-1 to +1) for both components

        mouse.x = ( x / rect.width ) * 2 - 1;
        mouse.y = ( y / rect.height ) * -2 + 1;

        if (mousedownpos.distanceTo(mousepixel) > 5)
        {
            if (mousebuttonpressed)
            {
                if (mousegeobotid == '')
                {
                    mousebuttonpressed = false;
                }
                else
                {
                    if (mousedowngeobotid == "") mousedowngeobotid = mousegeobotid;
                    if (!APP.playing)
                    {
                        dragging = true;
                        crosshairObject.visible = true;
                    }
                }
            };
            // movingmap = true;
        }
        else {
            event.stopPropagation();
        }


        if (dragging)
        {
            var newlocation = ProjectFromMap(mouse);

            if (crosshairObject)
            {
                var objectscale = Math.max(0.1, camera.position.distanceTo(origin)/objectscaledivider);

                var newPos = ProjectToMap(newlocation.latitude, newlocation.longitude);
                crosshairObject.scale.set(objectscale, objectscale, objectscale)
                crosshairObject.position.set(newPos.x, newPos.y, newPos.z);
            }

            event.stopPropagation();
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var onMouseDown = function( event )
    {
        mousedownpos.x = mousepixel.x;
        mousedownpos.y = mousepixel.y;

        mousebuttonpressed = true;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var onMouseUp = function( event )
    {
        // Finished dragging a Geobot
        if (dragging)
        {
            // Save Geobot position
            var newlocation = ProjectFromMap(mouse);
            newlocation.altitude = mappoints[mousedowngeobotid].geobot.location.altitude;
            crosshairObject.visible = false;

            APP.SDK.PutToAPI(APP.companion.sessionid, "geobots", {geobotid:mousedowngeobotid}, {location:newlocation});

            mousedowngeobotid = "";
        }

        // Clicked on Geobot on map
        if ((mousegeobotid != "") && !dragging)
        {
            if (APP.playing)
            {
                mappoints[mousegeobotid].geobot.Touch();
            }
            else
            {
                RecentreMap(mappoints[mousegeobotid].geobot.location.latitude, mappoints[mousegeobotid].geobot.location.longitude);
                APP.Geobot_UI.Open();
                APP.currentlySelectedGeobot = mousegeobotid;
                APP.Geobot_UI.ShowGeobot(mappoints[mousegeobotid].geobot);
            }
        }

        // Reset for next time
        dragging = false;
        // movingmap = false;
        mousebuttonpressed = false;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var onDoubleClick = function (event)
    {
        if (!APP.playing)
        {
            var newlocation = ProjectFromMap(mouse);
            RecentreMap(newlocation.latitude, newlocation.longitude);
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Called regularly
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var FixedUpdate = function (milliSec)
    {
        currentFixedUpdateTime += milliSec;
        while (currentFixedUpdateTime > fixedUpdateDuration)
        {
            currentFixedUpdateTime -= fixedUpdateDuration;

            if (currentFixedUpdateTime < fixedUpdateDuration)
            {

            }
        }
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetCompanionImage = function (newimage)
    {
        LoadTexture
        (
            "images/" + newimage,
            function ( texture ) {
                companionObject.material.map = texture;
                companionObject.material.needsUpdate = true;
            }
        );
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetRadius = function(radius)
    {

    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetThirdPerson = function ()
    {
        controls.target = origin;
        controls.minDistance = 1;
        controls.maxDistance = 5000000;
        controls.enableZoom = true;
    }
    var SetFirstPerson = function ()
    {
        var newtarget = new THREE.Vector3(0,1,0);
        controls.target = newtarget;
        controls.minDistance = 0.001;
        controls.maxDistance = 0.001;
        controls.enableZoom = false;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetUpScenegraph = function()
    {
        var width = $("body").width();
        var height = $("body").height()-5;

        $("#threedmap").width(width);
        // $("#threedmap").height(height);

        scene = new THREE.Scene();
        scene.background = new THREE.Color( 0xc8daed );
        // scene.background = new THREE.Color( 0x000000 );
        objectscene = new THREE.Scene();
        camera = new THREE.PerspectiveCamera( 40, width/height, 1, 3000 );
        clock = new THREE.Clock();

        camera.position.y = 10;
        camera.position.x = 0;
        camera.position.z = 10;

        renderer = new THREE.WebGLRenderer({ antialias: true , alpha: true });
        renderer.setClearColor(0x000000, 0);
        canvas = renderer.domElement;

        renderer.setSize( width, height );
        renderer.setViewport(0, 0, width, height );
        renderer.autoClear = false;
        $("#threedmap").append(canvas);

        canvas.width  = width;
        canvas.height = height;
        canvas.setAttribute("style", 'width:100%; height:100%;');

        controls = new THREE.OrbitControls( camera, canvas );
        // controls.enableDamping = true;
        // controls.dampingFactor = 0.1;
        controls.enableZoom = true;
        // controls.zoomSpeed = 0.5;
        // controls.rotateSpeed = 0.1;
        controls.enablePan = false;
        controls.minDistance = 1;
        controls.maxDistance = 5000000;
        controls.minPolarAngle = 0;
        controls.maxPolarAngle = Math.PI / 2.07;
        controls.target = origin;

        // LIGHTS
        var light = new THREE.PointLight(0xffffff, 0.5, 1000);
        light.position.set(0, 0, 5);
        scene.add(light);

        // Create the map plane
        mapplane = new THREE.Object3D();
        objectplane = new THREE.Object3D();
        objectplane.position.set(0, 0, 0);
        objectplane.rotation.set(-Math.PI/2, 0, 0);
        objectplane.name = 'mapobjects';
        mapplane.position.set(0, 0, 0);
        mapplane.rotation.set(-Math.PI/2, 0, 0);
        mapplane.name = 'map';
        scene.add(mapplane);
        objectscene.add(objectplane);
        mapplane.visible = true;

        LoadTexture
        (
            "images/companion_male.png",
            function ( texture ) {
                var geometry = new THREE.PlaneGeometry( 1, 1, 1, 1 );
                var material = new THREE.MeshBasicMaterial( {map: texture, side: THREE.FrontSide, transparent: true, depthTest: false} );
                companionObject = new THREE.Mesh( geometry, material );
                companionObject.name = "companionobject";
                audioListener = new THREE.AudioListener();
                companionObject.add(audioListener);
                objectplane.add(companionObject);
            }
        );
        LoadTexture
        (
            "images/crosshair2.png",
            function ( texture ) {
                var geometry = new THREE.PlaneGeometry( 1, 1, 1, 1 );
                var material = new THREE.MeshBasicMaterial( {map: texture, side: THREE.FrontSide, transparent: true, depthTest: false} );
                crosshairObject = new THREE.Mesh( geometry, material );
                crosshairObject.name = "crosshair";
                crosshairObject.visible = false;
                objectplane.add(crosshairObject);
            }
        );
        LoadTexture
        (
            "images/pulse00_6x4.png",
            function ( texture ) {
                var geometry = new THREE.PlaneGeometry( 1, 1, 1, 1 );
                var material = new THREE.MeshBasicMaterial( {map: texture, side: THREE.FrontSide, transparent: true, depthTest: false} );
                trackingObject = new THREE.Mesh( geometry, material );
                trackingObject.name = "trackingobject";
                objectplane.add(trackingObject);
                var parameters = {
                    width: 6,
                    height: 4,
                    length: 24,
                    framerate: 20,
                    playonstart: true
                };
                APP.AnimatedSprites.AddSprite ("locator", texture, parameters, true);
            }
        );

        // Set up some listeners
        canvas.addEventListener('mousemove', onMouseMove, false);
        canvas.addEventListener('mousedown', onMouseDown, false);
        canvas.addEventListener('mouseup', onMouseUp, false);
        canvas.addEventListener('dblclick', onDoubleClick, false);

        // Render loop
        var render = function ()
        {
            var delta = clock.getDelta();
            requestAnimationFrame( render );

            // Only draw 3D stuff if 3D mode is active
            if (APP.threedmode)
            {
                // Update the animation and controls
                controls.update();

                // Work out the current scales of the tiles and camera distance
                var cameraDistance = camera.position.distanceTo(origin);
                fractionalmapZoom = Math.min(18, Math.max(4, 20 - Math.log(cameraDistance) * 1.1));
                var newMapZoom = Math.round(fractionalmapZoom);
                maptilesize = maptilewidth * (19 - fractionalmapZoom) * tilescale(mapcentrelatitude, newMapZoom);
                maptilescale = (19 - fractionalmapZoom) * tilescale(mapcentrelatitude, newMapZoom);

                camera.near = cameraDistance / 8;
                camera.far = cameraDistance * 8;
                camera.updateProjectionMatrix();

                // Reload the tiles if the zoom level changes
                if (newMapZoom != mapZoom)
                {
                    mapZoom = newMapZoom;
                    RecentreMap(mapcentrelatitude, mapcentrelongitude);
                }

                // Adjust the tile size and map centre to keep things lined up visually
                ResizeMapTiles();
                AdjustCentre();

                // Scale for objects on the map to keep a uniform size despite zoom level
                var objectscale = Math.max(0.1, cameraDistance/objectscaledivider);

                // Draw in the current location
                if (trackingObject)
                {
                    var newPos = ProjectToMap(APP.Maps.currentlatitude, APP.Maps.currentlongitude);
                    trackingObject.scale.set(objectscale, objectscale, objectscale)
                    trackingObject.position.set(newPos.x, newPos.y, newPos.z);
                    if (APP.playing && APP.mobilemode)
                    {
                        RecentreMap();
                    }
                    // if (APP.playing)
                    // {
                    //     trackingObject.visible = APP.firstpersonmode;
                    // }
                    // else
                    // {
                    //     trackingObject.visible = true;
                    //
                    //     if (APP.firstpersonmode)
                    //     {
                    //         RecentreMap();
                    //     }
                    // }
                }

                // Adjust the current set of points on the map
                DrawPointsOnMap();

                // update the picking ray with the camera and mouse position
                raycaster.setFromCamera( mouse, camera );

                // calculate objects intersecting the picking ray
                var intersects = raycaster.intersectObjects( mappointcolliders );

                if (intersects.length > 0)
                {
                    for ( var i = 0; i < intersects.length; i++ ) {
                        mousegeobotid = intersects[0].object.userData.geobotid;
                        if (!APP.playing)
                        {
                            $('#popup').css({position:'absolute',top:mousepixel.y,left:mousepixel.x}).show();
                            $('#popup').html(mappoints[mousegeobotid].geobot.name + "<br>" + drawstatus(mappoints[mousegeobotid].geobot.status));
                        }
                    }
                }
                else
                {
                    $('#popup').hide();
                    mousegeobotid = "";
                }

                APP.AnimatedSprites.TimedUpdate(1000 * delta);
                // APP.LineParticles.TimedUpdate(1000 * delta, objectscale);
            };

            // Draw everything in 3D
            renderer.clear();
            renderer.render(scene, camera);
            renderer.clearDepth();
            renderer.render(objectscene, camera);
        };

        ResizeMapTiles();
        AdjustCentre();
        render();
    }

    var drawstatus = function(GeobotStatus)
    {
        var html = '<table class="ui celled table"><tbody>';
        $.each(GeobotStatus, function (key, value) {
            html += '<tr><td>' + value.name + '</td><td>' + value.state + '</td></tr>'
        });
        html += '</table>';
        return html;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Initialise = function ()
    {
        var maptilesize = maptilewidth * (19 - mapZoom);

        SetUpScenegraph ();

        // Add the tiles for the map images
        LoadTexture
        (
            "images/maproundcutout.png",
            function (alphatexture)
            {
                var t = maptilecount;
                var x = 0; var y = 0; var dx = 0;
                var dy = -1;
                var maxI = t*t;

                for(var i = 0; i < maxI; i++){
                    if ((-maptilecount/2 <= x) && (x <= maptilecount/2) && (-maptilecount/2 <= y) && (y <= maptilecount/2))
                    {
                        LoadTexture
                        (
                            "images/imersialogo.png",
                            function (texture, parameters)
                            {
                                var offset = new THREE.Vector2((parameters.mapx + maplimit) / maptilecount, (parameters.mapy + maplimit) / maptilecount);
                                var repeat = new THREE.Vector2(1 / maptilecount, 1 / maptilecount);

                                var geometry = new THREE.PlaneGeometry(maptilesize, maptilesize);

                                var uniforms = {
                                    texture: { type: 't', value: texture },
                                    backtexture: { type: 't', value: transparenttexture },
                                    alpha: { type: 't', value: alphatexture },
                                    offset: {type: 'v2', value: offset },
                                    repeat: {type: 'v2', value: repeat }
                                };
                                var material = new THREE.ShaderMaterial( {
                                    uniforms:       uniforms,
                                    vertexShader:   shaders.fadeout.vertex,
                                    fragmentShader: shaders.fadeout.fragment,
                                    side: THREE.FrontSide,
                                    transparent: true
                                });

                                var maptile = new THREE.Mesh(geometry, material);
                                mapplane.add(maptile);
                                maptile.mapindex = [parameters.mapx, -parameters.mapy];
                                maptile.position.set(parameters.mapx * maptilesize, parameters.mapy * maptilesize, 0);
                                maptile.timestamp = new Date();
                            },
                            {mapx:x, mapy:y}
                        );
                    }
                    if( (x == y) || ((x < 0) && (x == -y)) || ((x > 0) && (x == 1-y)))
                    {
                        t = dx;
                        dx = -dy;
                        dy = t;
                    }
                    x += dx;
                    y += dy;
                }
            }
        );
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Preload some images
    LoadTexture("images/blank.png", function (texture) { transparenttexture = texture; });
    LoadTexture("images/noimage.png");

    this.RecentreMap = RecentreMap;
    this.GetMapExtent = GetMapExtent;
    this.ResizeCanvas = ResizeCanvas;
    this.Initialise = Initialise;
    // this.LoadTexture = LoadTexture;
    this.SetMapType = SetMapType;
    this.ProjectToMap = ProjectToMap;
    // this.SetCurrentLocation = SetCurrentLocation;
    this.GetCurrentLocation = GetCurrentLocation;
    this.ClearPointsFromMap = ClearPointsFromMap;
    this.AddPointToMap = AddPointToMap;
    this.PlayAudioOneShot = PlayAudioOneShot;
    this.CentreMapOnPoints = CentreMapOnPoints;
    this.SetCompanionImage = SetCompanionImage;
    this.SetThirdPerson = SetThirdPerson;
    this.SetFirstPerson = SetFirstPerson;

    this.SetRadius = SetRadius;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

}
