// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Manages UI panel for a Geobot
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


IMERSIA.GeobotEdit_UI = function ()
{
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Public Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Private Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    var panelside = "right";
    var panelname = "geoboteditsidebar";
    var currentlySelectedMetadata = "";
    var audioPlayer = null;
    var jsoneditor = null;

    // Pointer to self
    var self = this;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Initialise = function ()
    {
        $('.'+panelname).sidebar({dimPage:false, closable: false, transition: "overlay", mobileTransition: "overlay"});
        $('.pointing.menu .item').tab();

        // Save Geobot
        $("body").on("click", "#savegeobotbutton", function (e) {
            SaveGeobot();
        });

        // Add new Metadata
        $("body").on("click", "#addgeobotmetadatabutton", function (e) {
            APP.MetadataEdit_UI.NewMetadata("geobot");
        });

        // Edit Metadata
        $("body").on("click", ".editgeobotmetadata", function (e)
        {
            currentlySelectedMetadata = e.currentTarget.parentNode.parentNode.id;
            APP.MetadataEdit_UI.EditMetadata(currentlySelectedMetadata, "geobot");
        });

        // Delete Metadata
        $("body").on("click", ".deletegeobotmetadata", function(e) {
            currentlySelectedMetadata = e.currentTarget.parentNode.parentNode.id;
            APP.MetadataEdit_UI.DeleteMetadataDialog(currentlySelectedMetadata, "geobot");
        });

        // Delete File
        $("body").on("click", ".deletegeobotfile", function(e) {
            var FileToDelete = e.currentTarget.name;
            APP.SDK.DeleteFromAPI(APP.companion.sessionid, "files", {geobotid:APP.currentlySelectedGeobot, filename:FileToDelete},
                function(result)
                {
                    UpdateFileTable();
                    APP.Companion_UI.AddInfoMessage("File " + FileToDelete + " deleted.");
                },
                function(error)
                {
                    APP.Companion_UI.AddErrorMessage("File " + FileToDelete + " NOT deleted.");
                }
            );
        });

        // Play the Audio when the button is clicked
        $("body").on("click", "#com_imersia_audio_audioplaybutton", function(e) {
            var audiometadata = APP.companion.channels[APP.currentlySelectedChannel].geobots[APP.currentlySelectedGeobot].metadata.GetByKey("com.imersia.audio");
            if (audiometadata != undefined)
            {
                // if (audioPlayer == null)
                // {
                //     // Audio element not yet defined, so load it and then play
                //     audioPlayer = new Audio(audiometadata.value.audiourl);
                //     audioPlayer.play();
                // }
                // else
                // {
                //     if (audioPlayer.src.indexOf(audiometadata.value.audiourl) != -1)
                //     {
                //         // Same as existing Audio file, so either pause or play
                //         if (audioPlayer.paused)
                //         {
                //             audioPlayer.play();
                //         }
                //         else
                //         {
                //             audioPlayer.pause();
                //         }
                //     }
                //     else
                //     {
                //         // New Audio file, so replace it and start playing
                //         audioPlayer.src = audiometadata.value.audiourl;
                //         audioPlayer.play();
                //     }
                // }
            }
        });

        // Changing the class
        $("body").on("change", "#com_imersia_geobot_class", function(e) {
            var Geobotclass = $('#com_imersia_geobot_class').val();
            var classdetails = APP.companion.channels[APP.currentlySelectedChannel].geobots[APP.currentlySelectedGeobot].metadata.GetByKey(Geobotclass);
            var classhtml = {};
            if (classdetails != undefined)
            {
                classhtml = APP.Classes.ConvertClassToHTML(Geobotclass, classdetails.value);
            }
            else {
                classhtml = APP.Classes.ConvertClassToHTML(Geobotclass, {});
            }
            $('#geoboteditclass').html(classhtml);
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Open = function ()
    {
        // Close any LHS panels
        $(".sidebar."+panelside).not("."+panelname).sidebar("hide");

        // Open this one.
        $("."+panelname).sidebar("show");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Close = function ()
    {
        $("."+panelname).sidebar("hide");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Save Geobot, taking stuff from the UI and adjusting metadata on the way
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SaveGeobot = function (callback)
    {
        var Geobotclass = $('#com_imersia_geobot_class').val();
        var thisGeobot = APP.companion.channels[APP.currentlySelectedChannel].geobots[APP.currentlySelectedGeobot];

        // Get the metadata object for this geobot
        var Geobotmetadata = thisGeobot.metadata;

        var MetadataUpdates = {};
        // Create a set of metadata from the main UI and the variables that can then be saved out in one go to the Geobot

        // Go through each element of the Variables UI and gather the Metadata
        $(".geobot_variable_record").each(function (index, DOMelement) {
            var metadataid = DOMelement.id;

            // Get the key
            var key = $('#' + metadataid + "_key").text();

            // Get the value, trying to parse it as JSON
            var value = null;
            try {
                value = JSON.parse($('#' + metadataid + "_value").text());
            } catch (e) {
                value = $('#' + metadataid + "_value").text();
            }

            // Check what is to be done each metadata variable
            if ($('#' + metadataid).hasClass("deleted"))
            {
                MetadataUpdates[key] = {action:"delete", metadataid:metadataid};
            }
            else
            {
                if ($('#' + metadataid).hasClass("new"))
                {
                    MetadataUpdates[key] = {action:"new", key:key, value:value};
                }
                else
                {
                    MetadataUpdates[key] = {action:"update", metadataid:metadataid, key:key, value:value};
                }
            }
        });

        // Grab the class details from the UI
        var geobotClassDetailsFromUI = GrabDetailsFromClass(Geobotclass);

        // Set the class details
        MetadataUpdates[Geobotclass] = {action:"update", key:Geobotclass, value:geobotClassDetailsFromUI};

        // Set some special metadata values from the main part of the UI
        if (MetadataUpdates.hasOwnProperty("com.imersia.default"))
        {
            var existingvalue = MetadataUpdates["com.imersia.default"].value;
            existingvalue.previd = $("#com_imersia_geobot_previd").val();

            MetadataUpdates["com.imersia.default"].value = existingvalue;
        }
        else
        {
            MetadataUpdates["com.imersia.default"] = {action:"update", key:"com.imersia.default", value:{ previd : $("#com_imersia_geobot_previd").val() }};
        }

        // Set the audio metadata Details
        var geobotAudioDetailsFromUI = GrabDetailsFromClass("com.imersia.audio");

        // Set the audio details
        MetadataUpdates["com.imersia.audio"] = {action:"update", key:"com.imersia.audio", value:geobotAudioDetailsFromUI};

        // Go through all the Metadata and make the appropriate changes
        Object.keys(MetadataUpdates).forEach(function(key, index)
        {
            var MetadataDetails = MetadataUpdates[key];

            switch (MetadataDetails.action)
            {
                case "delete":
                    Geobotmetadata.Delete(APP.companion.sessionid, MetadataDetails.metadataid);
                    break;
                case "update":
                    Geobotmetadata.Set(APP.companion.sessionid, MetadataDetails.metadataid, MetadataDetails.key, MetadataDetails.value);
                    break;
                case "new":
                    Geobotmetadata.Set(APP.companion.sessionid, undefined, MetadataDetails.key, MetadataDetails.value);
                break;
            }
        });

        // Set up the main Geobot Details
        var Geobotdetails = {
            geobotid:$('#com_imersia_geobot_geobotid').val(),
            channelid:$('#com_imersia_geobot_channelid').val(),
            name:$('#com_imersia_geobot_name').val(),
            description:$('#com_imersia_geobot_description').val(),
            class:$('#com_imersia_geobot_class').val(),
            location: { longitude : +$('#com_imersia_geobot_longitude').val(), latitude : +$('#com_imersia_geobot_latitude').val(), altitude : +$('#com_imersia_geobot_altitude').val() },
            radius: +$('#com_imersia_geobot_radius').val(),
            hidden:$('#com_imersia_geobot_hidden').is(':checked'),
            imageurl:$('#com_imersia_geobot_imageurl').val()
        };

        // Save the Geobot
        thisGeobot.Save(APP.companion.sessionid, Geobotdetails, function (result) {
        }, function (error) {
            APP.Companion_UI.AddErrorMessage("error saving Geobot");
        });

        Close();
        APP.Geobot_UI.Open();
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var GrabDetailsFromClass = function(ClassName)
    {
        var returnData = {};
        Object.keys(APP.Classes.ClassDetails(ClassName)).forEach(function(key, index)
        {
            var hideit = false;
            if (APP.Classes.ClassDetails(ClassName)[key].hasOwnProperty("hide"))
            {
                hideit = APP.Classes.ClassDetails(ClassName)[key].hide;
            }

            if (!hideit)
            {
                switch (APP.Classes.ClassDetails(ClassName)[key].type)
                {
                    case "number":
                    returnData[key] = +$('#' + ClassName.replace(/\./g, "_") + "_" + key).val();
                    break;
                    case "check":
                    returnData[key] = ($('#' + ClassName.replace(/\./g, "_") + "_" + key).is(":checked"));
                    break;
                    case "radio":
                    // TODO Get which Radio button is checked
                    break;
                    default:
                    returnData[key] = $('#' + ClassName.replace(/\./g, "_") + "_" + key).val();
                    break;
                }
            }
        });

        return returnData;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetDetails = function(details) {
        details.id = details.geobotid;
        details.image = APP.Classes.InterpretContentURL(details.geobotid, details.imageurl, "size=medium");
        details.latitude = details.location.latitude;
        details.longitude = details.location.longitude;
        details.altitude = details.location.altitude;
        details.geohash = details.location.geohash;
        details.radius = details.radius;

        $('#geoboteditmain').html(
            APP.Classes.ConvertClassToHTML("com.imersia.geobot", details)
        );

        var classdetails = details.metadata.GetByKey(details.class);
        var classhtml = {};
        if (classdetails != undefined)
        {
            if (details.class == "com.imersia.sprite")
            {
                var spritedetails = "";

                if (classdetails.value.imageurl)
                {
                    if (classdetails.value.imageurl == "")
                    {
                        spritedetails = '/files?size=medium';
                    }
                    else if (classdetails.value.imageurl.indexOf("http") == -1)
                    {
                        spritedetails = classdetails.value.imageurl + '?size=medium';
                    }
                    else
                    {
                        spritedetails = '/files/' + details.geobotid + '?url=' + classdetails.value.imageurl + '&size=medium';
                    }
                }
                else
                {
                    spritedetails = '/files?size=medium';
                }
                classdetails.value.sprite = spritedetails;
            }
            classhtml = APP.Classes.ConvertClassToHTML(details.class, classdetails.value);
        }
        else {
            classhtml = APP.Classes.ConvertClassToHTML("com.imersia.default", {});
        }
        $('#geoboteditclass').html(classhtml);

        var audiometadata = details.metadata.GetByKey("com.imersia.audio");
        var audiohtml = {};
        if (audiometadata == undefined)
        {
            audiohtml = APP.Classes.ConvertClassToHTML("com.imersia.audio", {});
        }
        else
        {
            audiohtml = APP.Classes.ConvertClassToHTML("com.imersia.audio", audiometadata.value);
        }
        $('#geoboteditaudio').html(audiohtml);

        CreateTableFromMetadata(details.metadata);

        CreateTableFromFiles(details.fileurls);

    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreateTableFromMetadata = function(metadata)
    {
        var html = "";

        html += '<div style="overflow-x:scroll"><table style="width:650px" class="ui compact table">';
        html += '<thead> <tr> <th></th> <th></th> <th style="width:300px">Key</th> <th style="width:300px">Value</th> </tr> </thead>';
        html += '<tbody id="geobotvariablestable">';
        html += '</tbody>'
        html += '<tfoot class="full-width"> <tr> <th colspan="4"> \
                    <div class="ui basic button green" id="addgeobotmetadatabutton">Add</div> \
                    </th> </tr> </tfoot>';
        html += '</table></div>';

        $('#geoboteditvariables').html(html);

        Object.keys(metadata.metadata).forEach(function (metadataid)
        {
            APP.MetadataEdit_UI.AddRowToMetadataTable(metadataid, "geobot", metadata.metadata[metadataid].key, metadata.metadata[metadataid].value);
        });

        return html;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Add a row to the files table
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var AddRowToFilesTable = function(fileurl)
    {
        var filename = fileurl.split("/")[3];
        var html = "";

        html += '<tr class="file_record">';

        html += '<td>'
        html += '<button name="' + filename + '" class="ui mini basic icon button red deletegeobotfile"><i class="delete icon"></i></button>'
        html += '</td>';
        html += '<td>' + filename + '</td>';
        html += "</tr>";

        $("#geobotfilesstable").append(html);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreateTableFromFiles = function(fileurls)
    {
        var html = "";

        html += '<div style="overflow-x:scroll"><table class="ui compact table">';
        html += '<thead> <tr> <th></th> <th></th> <th></th> </tr> </thead>';
        html += '<tbody id="geobotfilesstable">';
        html += '</tbody>'
        html += '<tfoot class="full-width"> <tr> <th colspan="4"> \
                    <div class="ui basic button green fileloader" id="com_imersia_geobot_file">Load File</div> \
                    </th> </tr> </tfoot>';
        html += '</table></div>';

        $('#geoboteditfiles').html(html);

        if (fileurls != undefined)
        {
            for (let fileurl of fileurls) {
                AddRowToFilesTable(fileurl);
            }
        };

        return html;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var UpdateImage = function (details, thefile, callback)
    {
        var geobotid = details.geobotid;

        APP.SDK.PostFileToAPI(APP.companion.sessionid, 'files', details, thefile, function (result) {
            if (result.hasOwnProperty("fileurls"))
            {
                var fileurl = result.fileurls[0];
                APP.SDK.PutToAPI(APP.companion.sessionid, "geobots", {geobotid: geobotid}, {imageurl:fileurl}, function (result) {
                    APP.companion.channels[APP.currentlySelectedChannel].geobots[APP.currentlySelectedGeobot].imageurl = fileurl;
                    callback (fileurl);
                },
                function (error) {
                    callback ("");
                });
            };
        }, function (error) {
            callback("")
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var UpdateFileTable = function ()
    {
        APP.SDK.GetFromAPI(APP.companion.sessionid, 'files', {geobotid : APP.currentlySelectedGeobot}, function(files)
        {
            var CurrentGeobot = APP.companion.channels[APP.currentlySelectedChannel].geobots[APP.currentlySelectedGeobot];
            CurrentGeobot.fileurls = files.fileurls;

            CreateTableFromFiles(CurrentGeobot.fileurls);
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var UpdateSprite = function (details, thefile, callback)
    {
        var geobotid = details.geobotid;
        details.id = details.geobotid;

        APP.SDK.PostFileToAPI(APP.companion.sessionid, 'files', details, thefile, function (result) {
            if (result.hasOwnProperty("fileurls"))
            {
                var fileurl = result.fileurls[0];
                var thisGeobot = APP.companion.channels[APP.currentlySelectedChannel].geobots[APP.currentlySelectedGeobot];
                APP.SDK.PutToAPI(APP.companion.sessionid, "geobots", {geobotid: geobotid}, {imageurl:fileurl}, function (result) {
                    if (thisGeobot.hasOwnProperty("metadata"))
                    {
                        var spritedetails = thisGeobot.metadata.GetByKey("com.imersia.sprite");
                        if (spritedetails != undefined)
                        {
                            spritedetails.imageurl = fileurl;
                            callback(fileurl);
                        }
                        else {
                            callback ("");
                        }
                    }
                    else {
                        callback ("");
                    }
                },
                function (error) {
                    callback ("");
                });
            };
        }, function (error) {
            callback("")
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ClearOutput = function()
    {
        $('#geoboteditmain').html("");
        $('#geoboteditclass').html("");
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    this.Initialise = Initialise;
    this.Open = Open;
    this.Close = Close;
    this.SetDetails = SetDetails;
    this.ClearOutput = ClearOutput;
    this.UpdateImage = UpdateImage;
    this.UpdateSprite = UpdateSprite;
    this.UpdateFileTable = UpdateFileTable;
}
