// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Manages communications with the Companion
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


IMERSIA.Companion_UI = function ()
{
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Public Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Private Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    var verbose = false;
    var panelside = "right";
    var panelname = "companionsidebar";

    var outputlines = [];

    // Pointer to self
    var self = this;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Initialise = function ()
    {
        var SendCommand = function (e) {
            if (e.keyCode == 13) {
                if (APP.currentlySelectedGeobot != "")
                {
                    var Parameters = {};
                    try {
                        Parameters = JSON.parse($('#companioninputparameters').val()); // Convert it if possible
                    } catch (e) {
                        Parameters = $('#companioninputparameters').val();
                    };

                    APP.companion.SendEvent(APP.currentlySelectedGeobot, $('#companioninput').val(), Parameters);
                    APP.companion.SendCommand(APP.currentlySelectedGeobot, "status", {});
                }
                // $('#companioninput').val("");
            }
        };


        $('.'+panelname).sidebar({dimPage:false, closable: false, transition: "overlay", mobileTransition: "overlay"});

        ClearOutput();

        SetupCommandButtons();

        $("body").on("keyup", "#companioninputparameters", function (e)
        {
            SendCommand(e);
        });

        $("body").on("keyup", "#companioninput", function (e)
        {
            SendCommand(e);
        });

        $("body").on("change", '#companion_verbose', function(e) {
            if($(this).is(":checked")) {
                verbose = true;
                SetVerbose(true);
            }
            else {
                verbose = false;
                SetVerbose(false);
            }
        });

        $("body").on("click", "#companion_clear", function (e)
        {
            ClearOutput();
        });

        $("body").on("click", ".geobot_link", function (e)
        {
            var entityid = e.currentTarget.name;
            var thisgeobot = APP.companion.FindGeobot(entityid);
            if (thisgeobot != null)
            {
                var geobotid = thisgeobot.geobotid;
                var channelid = thisgeobot.channelid;

                // Select this channel
                APP.currentlySelectedChannel = channelid;
                APP.currentlySelectedGeobot = geobotid;
                APP.ChannelList_UI.SelectChannel(channelid);
                APP.ChannelList_UI.LoadGeobotsOnMap(channelid);

                // Move map to show this Geobot
                APP.Maps.RecentreMap(thisgeobot.location.latitude, thisgeobot.location.longitude);

                // Open the Geobot UI
                APP.Geobot_UI.ShowGeobot(thisgeobot);
                APP.Geobot_UI.Open();
            }
        });

        $("body").on("click", ".companion_commandbutton", function (e)
        {
            var theCommand = e.currentTarget.innerText.replace(/\s/g, '');  // Remove spaces that might have been added
            if (APP.currentlySelectedGeobot != "")
            {
                APP.companion.SendEvent(APP.currentlySelectedGeobot, theCommand);
                APP.companion.SendCommand(APP.currentlySelectedGeobot, "status", {});
            }
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Open = function ()
    {
        // Close any RHS panels
        $(".sidebar."+panelside).not("."+panelname).sidebar("hide");

        SetupCommandButtons();

        // Open this one.
        $("."+panelname).sidebar("show");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Close = function ()
    {
        $("."+panelname).sidebar("hide");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var IsOpen = function ()
    {
        return ($("."+panelname).hasClass("visible"));
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetupCommandButtons = function ()
    {
        var commands = [];
        if (APP.currentlySelectedGeobot != "")
        {
            var currentGeobot = APP.companion.FindGeobot(APP.currentlySelectedGeobot);

            // Check we have an automations field
            if (currentGeobot.hasOwnProperty("automations"))
            {
                // For each Automation
                var i = currentGeobot.automations.automations.length;
                while (i--)
                {
                    var currentAutomation = currentGeobot.automations.automations[i];
                    // If there is a commands field
                    if (currentAutomation.hasOwnProperty("commands"))
                    {
                        // For each command in the list of commands
                        var j = currentAutomation.commands.length;
                        while (j--)
                        {
                            var thisCommand = currentAutomation.commands[j];
                            // Add the command to the array only if it is not already there (ie a set)
                            var k = commands.length;
                            var commandalreadyinlist = false;
                            while (k--)
                            {
                                if (commands[k] == thisCommand) commandalreadyinlist = true;
                            }
                            if (!commandalreadyinlist) commands.push(thisCommand);
                        }
                    }
                }
            }
        }

        // For each command that has been collected, create a button
        var companioncommandbuttonshtml = "";
        var l = commands.length;
        while (l--)
        {
            var the_command = make_nice(commands[l]);
            companioncommandbuttonshtml += '<div class="ui basic blue fluid button companion_commandbutton" style="margin-top:10px;">' + the_command + '</div>'
        }
        $("#companioncommandbuttons").html(companioncommandbuttonshtml);
    }

    var make_nice = function (command_in)
    {
        // var command_out = "";
        // var i = 0;
        // var was_capital = false;
        // while (i < command_in.length)
        // {
        //     if ((command_in[i] >= "A") && (command_in[i] <= "Z") && !was_capital && (i>0))
        //     {
        //         command_out += " " + command_in[i];
        //         was_capital = true;
        //     }
        //     else
        //     {
        //         if (!((command_in[i] >= "A") && (command_in[i] <= "Z")))
        //         {
        //             was_capital = false;
        //         }
        //         command_out += command_in[i];
        //     }
        //     i++;
        // }
        // return command_out;
        return command_in;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetVerbose = function(NewVerbosity)
    {
        verbose = NewVerbosity;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var AddCompanionMessage = function(txt, imageurl, name, entityid) {
        var image = (imageurl == "") ? '<i class="pencil icon"></i>' : '<img src="' + imageurl + '">';
        AddToOutputQueue(
            '<div class="event">' +
                '<div class="label">' +
                    image +
                '</div>' +
                '<div class="content">' +
                    '<div class="summary">' +
                        '<a class="user ' + ((entityid) ? 'geobot_link" name="' + entityid + '"' : '"') + '>' +
                            name +
                        '</a> ' + txt +
                    '</div>' +
                '</div>' +
            '</div>'
        );
    };

    var AddUserMessage = function(txt, showbox) {
        showbox = showbox || false;
        var image = '<i class="smile icon"></i>';
        AddToOutputQueue(
            '<div class="event">' +
                '<div class="label">' +
                    image +
                '</div>' +
                '<div class="content">' +
                    '<div class="summary">' +
                        '<a class="user">' +
                            'You' +
                        '</a> say ' + txt +
                    '</div>' +
                '</div>' +
            '</div>'
        );
        if (showbox)
        {
            $('#messageboxmessage').text(txt);
            $('#messagebox').modal({closable:false, allowMultiple:false}).modal("show");
        }
    };

    var AddErrorMessage = function(txt, showbox) {
        showbox = showbox || true;
        var image = '<i class="warning icon"></i>';
        AddToOutputQueue(
            '<div class="event">' +
                '<div class="label">' +
                    image +
                '</div>' +
                '<div class="content">' +
                    '<div class="summary">' +
                        '<i>' + txt +'</i>' +
                    '</div>' +
                '</div>' +
            '</div>'
        );
        if (showbox)
        {
            $('#messageboxmessage').text(txt);
            $('#messagebox').modal({closable:false, allowMultiple:false}).modal("show");
        }
    };

    var AddInfoMessage = function(txt, showbox) {
        showbox = showbox || false;
        var image = '<i class="info icon"></i>';
        AddToOutputQueue(
            '<div class="event">' +
                '<div class="label">' +
                    image +
                '</div>' +
                '<div class="content">' +
                    '<div class="summary">' +
                        '<i>' + txt +'</i>' +
                    '</div>' +
                '</div>' +
            '</div>'
        );
        if (showbox)
        {
            $('#messageboxmessage').text(txt);
            $('#messagebox').modal({closable:false, allowMultiple:false}).modal("show");
        }
    };

    var ClearOutput = function()
    {
        outputlines = [];
        $('#companionoutput').html("");
    };

    var AddToOutputQueue = function(html) {
        if (outputlines.length > 20) { outputlines.shift(); }
        outputlines.push(html);
        var texttoprint = "";
        for (var i = outputlines.length-1; i>=0; i--)
        {
            texttoprint += outputlines[i];
        }
        $('#companionoutput').html(texttoprint);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    this.Initialise = Initialise;
    this.Open = Open;
    this.Close = Close;
    this.IsOpen = IsOpen;
    this.AddCompanionMessage = AddCompanionMessage;
    this.AddUserMessage = AddUserMessage;
    this.AddErrorMessage = AddErrorMessage;
    this.AddInfoMessage = AddInfoMessage;
    this.ClearOutput = ClearOutput;
    this.SetVerbose = SetVerbose;
}
