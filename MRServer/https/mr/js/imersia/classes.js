// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Description: Geobot Classes
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


IMERSIA.Classes = function ()
{
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Public Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    this.geobotclasses = ["com.imersia.default", "com.imersia.sprite", "com.imersia.panorama", "com.imersia.threesixtyvideo", "com.imersia.vr"];
    this.channelclasses = ["com.imersia.default", "com.imersia.geophoto", "com.imersia.storyline", "com.imersia.virtuandize", "com.imersia.treasurehunt", "com.imersia.contextad"];
    this.mapstyles = ["com.mapbox.light", "com.mapbox.dark", "com.mapbox.streets", "com.mapbox.satellite", "com.mapbox.outdoors", "com.stamen.watercolor"]

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Private Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var classdetails = {};
    var classjsonmetadata = {};

    // Pointer to self
    var self = this;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ConvertToHTML = function ( details, initialvalue )
    {
        var returnstring = "";
        details.label = details.label || "";

        switch (details.type)
        {
            case "string":
            initialvalue = ValidateInput (initialvalue, details );
            // returnstring = '<div class="field validate_field"><label>' + details.label + '</label><div><input id="' + details.id + '" value = "' + initialvalue + '" style="width: ' + (details.hasOwnProperty("hint") ? '210' : '300') + 'px; margin-right: 10px;"' + (details.hasOwnProperty("readonly") ? (details.readonly ? 'readonly' : '') : '') + '/>' + (details.hasOwnProperty("hint") ? details.hint : "") + '</div></div>';
            returnstring = '<div class="field validate_field"><label>' + details.label + '</label><div><div class="ui ' + (details.hasOwnProperty("pasteicon") ? 'icon' : "") + ' input"><input type="text" id="' + details.id + '" value = "' + initialvalue + '" style="width: ' + (details.hasOwnProperty("hint") ? '210px' : '100%') + '; margin-right: 10px;" ' + (details.hasOwnProperty("readonly") ? 'readonly' : '') + '/>' + (details.hasOwnProperty("pasteicon") ? '<i class="paste icon"></i>' : "") + (details.hasOwnProperty("hint") ? details.hint : "") + '</div></div></div>';
            break;

            case "text":
            initialvalue = ValidateInput (initialvalue, details );
            returnstring = '<div class="field validate_field"><label>' + details.label + '</label><div><textarea id="' + details.id + '" style="width: ' + (details.hasOwnProperty("hint") ? '210px' : '100%') + '; margin-right: 10px;">' + initialvalue + '</textarea>'+ (details.hasOwnProperty("hint") ? details.hint : "") + '</div></div>';
            // returnstring = '<div class="field validate_field"><label>' + details.label + '</label><div><textarea id="' + details.id + '">' + initialvalue + '</textarea>'+ (details.hasOwnProperty("hint") ? details.hint : "") + '</div></div>';
            break;

            case "number":
            initialvalue = ValidateInput (initialvalue, details );
            returnstring = '<div class="field validate_field"><label>' + details.label + '</label><div><input id="' + details.id + '" value = "' + initialvalue + '" style="width: ' + (details.hasOwnProperty("hint") ? '210px' : '100%') + '; margin-right: 10px;"' + (details.hasOwnProperty("readonly") ? (details.readonly ? 'readonly' : '') : '') + '/>' + (details.hasOwnProperty("hint") ? details.hint : "") + '</div></div>';
            break;

            case "image":
            initialvalue = ValidateInput (initialvalue, details );
            returnstring = '<div class="field validate_field"><label>' + details.label + '</label><div><img id="' + details.id + '" src = "' + initialvalue + '" style="width: ' + (details.hasOwnProperty("hint") ? '210px' : '100%') + '; margin-right: 10px;"/>' + (details.hasOwnProperty("hint") ? details.hint : "") + '</div></div>';
            break;

            case "button":
            initialvalue = ValidateInput (initialvalue, details );
            returnstring = '<div class="field validate_field"><label>' + details.label + '</label><div class="buttonbackground"><button class="ui basic button blue ' + (details.hasOwnProperty("cssclass")?details.cssclass:"") + '" name="' + (details.hasOwnProperty("filetypes")?details.filetypes:"") + '" id="' + details.id + '" style="margin : 0px; width: ' + (details.hasOwnProperty("hint") ? '210px' : '100%') + '; margin-right: 10px;">' + initialvalue + "</button>" + (details.hasOwnProperty("hint") ? details.hint : "") + '</div></div>';
            break;

            case "select":
            initialvalue = ValidateInput (initialvalue, details);
            returnstring = '<div class="field validate_field"><label>' + details.label + '</label><div><select id="' + details.id + '" style="width: ' + (details.hasOwnProperty("hint") ? '210px' : '300px') + '; margin-right: 10px;">';
            details.options.forEach (function (element) {
                returnstring += '<option value="' + element + '" ' + ((element == initialvalue)?"selected":"") + '>' + element + '</option>';
            });
            returnstring += '</select>' + (details.hasOwnProperty("hint") ? details.hint : '') + '</div></div>';
            break;

            case "check":
            initialvalue = ValidateInput (initialvalue, details );
            returnstring = '<div class="field validate_field"><label>' + details.label + '</label><div><input type="checkbox" ' + ((initialvalue)?"checked":"") + ' id="' + details.id + '" style="width: ' + (details.hasOwnProperty("hint") ? '20' : '300') + 'px; margin-right: 10px;">' + (details.hasOwnProperty("hint") ? details.hint : "") + '</div></div>';
            break;

            case "comment":
            returnstring = '<div class="field validate_field"><label>' + details.label + '</label><div style="font-size:small">' + details.text + '</div></div>';
            break;
        }

        return (returnstring);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ConvertClassToHTML = function ( theclass, initialvalues )
    {
        var returnstring = "";
        initialvalues = initialvalues || {};
        if (!classdetails.hasOwnProperty(theclass)) theclass = "com.imersia.default";

        Object.keys(classdetails[theclass]).forEach (function (key) {
            classdetails[theclass][key].id = theclass.replace(/\./g, '_') + "_" + key;
            // if (classdetails[theclass][key].hasOwnProperty("for"))
            // {
            //     var keyfor = classdetails[theclass][key]["for"];
            //     var defaultvalue = (initialvalues.hasOwnProperty(keyfor)) ? initialvalues[keyfor] : classdetails[theclass][keyfor].default;
            //     returnstring += ConvertToHTML (classdetails[theclass][key], defaultvalue);
            // }
            // else {
                var defaultvalue = (initialvalues.hasOwnProperty(key)) ? initialvalues[key] : classdetails[theclass][key].default;
                returnstring += ConvertToHTML (classdetails[theclass][key], defaultvalue);
            // }
        });

        return returnstring;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var GetClassesAsHTML = function (ItemType)
    {
        var returnstring = "";
        if(ItemType == "channel")
        {
            this.channelclasses.forEach (function (element) {
                returnstring += '<option value="' + element + '">' + element + '</option>';
            });
            return returnstring;
        }
        else {
            this.geobotclasses.forEach (function (element) {
                returnstring += '<option value="' + element + '">' + element + '</option>';
            });
        }
        return returnstring;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ValidateInput = function ( value, checkfunction )
    {
        if (value == undefined) value = "";
        if (checkfunction.check != undefined)
        {
            return (checkfunction.check (value));
        }
        else {
            return (value);
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ClassDetails = function (theclass)
    {
        var returndetails = {};
        if (classdetails.hasOwnProperty(theclass)) returndetails = classdetails[theclass];
        return (returndetails);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var GetClassAsJSON = function (theclass, values)
    {
        var returnobject = {};
        values = values || {};

        if (classdetails.hasOwnProperty(theclass))
        {
            Object.keys(classdetails[theclass]).forEach (function (key)
            {
                if (classdetails[theclass][key].hasOwnProperty("default"))
                {
                    if (classdetails[theclass][key].hasOwnProperty("hide"))
                    {
                        if (!classdetails[theclass][key].hide)
                        {
                            returnobject[key] = (values.hasOwnProperty(key)) ? values[key] : classdetails[theclass][key].default;
                        }
                    }
                    else
                    {
                        returnobject[key] = (values.hasOwnProperty(key)) ? values[key] : classdetails[theclass][key].default;
                    }
                }
            });
        }

        return returnobject;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var InterpretContentURL = function (ItemID, ContentURL, Parameters, nodefault)
    {
        var Result = "";
        var QueryParameters = (Parameters) ? ((Parameters == "") ? "" : ("?" + Parameters)) : "";
        if (!nodefault) nodefault = false;

        if (ContentURL)
        {
            if (ContentURL == "")
            {
                Result = (nodefault ? '' : '/files' + QueryParameters);
            }
            else if (ContentURL.indexOf("http") == -1)
            {
                if (ContentURL[0] == "/")
                {
                    Result = ContentURL + QueryParameters;
                }
                else
                {
                    Result = '/files/' + ItemID + '/' + ContentURL + QueryParameters;
                }
            }
            else
            {
                Result = '/files/' + ItemID + '?url=' + ContentURL + QueryParameters;
            }
        }
        else
        {
            Result = (nodefault ? '' : '/files' + QueryParameters);
        }

        return Result;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Geobot Class
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    classdetails["com.imersia.geobot"] = {
        name: {
            type: "string",
            label: "Name",
            default: ""
        },
        image : {
            type: "image",
            label: "Image URL",
            hide: true,
            default: "",
            for: "imageurl"
        },
        imageurl: {
            type: "string",
            default: "",
            pasteicon: true
        },
        imagebutton: {
            type: "button",
            hide: true,
            cssclass: "fileloader",
            filetypes: ".png, .png, .jpeg",
            default: "Load Image",
            for: "imageurl"
        },
        description: {
            type: "text",
            label: "Description",
            default: ""
        },
        class: {
            type: "select",
            label: "Class",
            options: self.geobotclasses,
            default: "com.imersia.default"
        },
        latitude: {
            type: "number",
            label: "Location",
            hint: "latitude",
            default: 0,
            check : function (value) { return ((value > 90) ? (value - 180) : ((value < -90) ? (value + 180) : value)); }
        },
        longitude: {
            type: "number",
            hint: "longitude",
            default: 0,
            check : function (value) { return ((value > 180) ? (value - 360) : ((value < -180) ? (value + 360) : value)); }
        },
        geohash: {
            type: "string",
            hint: "geohash",
            default: "",
            readonly: true
        },
        altitude: {
            type: "number",
            label: "Altitude",
            hint: "meters",
            default: 0
        },
        radius: {
            type: "number",
            label: "Radius",
            hint: "meters",
            default: 0,
            check : function (value) { return (Math.abs(value)); }
        },
        hidden: {
            type: "check",
            label: "Hidden",
            hint: "Hide geobot from public view.",
            default: false
        },
        geobotid: {
            type: "string",
            label: "ID",
            default: "",
            readonly: true
        },
        // previd: {
        //     type: "string",
        //     label: "Previous Geobot ID",
        //     default: ""
        // },
        channelid: {
            type: "string",
            label: "Channel ID",
            default: "",
            readonly: true
        }
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Channel Class
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    classdetails["com.imersia.channel"] = {
        name: {
            type: "string",
            label: "Name",
            default: ""
        },
        image : {
            type: "image",
            label: "Image URL",
            hide: true,
            default: "",
            for: "imageurl"
        },
        imageurl: {
            type: "string",
            default: "",
            pasteicon: true
        },
        imagebutton: {
            type: "button",
            hide: true,
            cssclass: "fileloader",
            filetypes: ".png, .png, .jpeg",
            default: "Load Image",
            for: "imageurl"
        },
        description: {
            type: "text",
            label: "Description",
            default: ""
        },
        class: {
            type: "select",
            label: "Class",
            options: self.channelclasses,
            default: "com.imersia.default"
        },
        hidden: {
            type: "check",
            label: "Hidden",
            hint: "Hide channel from public view.",
            default: false
        },
        channelid: {
            type: "string",
            label: "ID",
            default: "",
            readonly: true
        }
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Default Geobot Class
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    classdetails["com.imersia.default"] = {
        touch: {
            type: "select",
            label: "Touch action",
            options: ["webapp", "touch"],
            default: "webapp"
        },
        images: {
            type: "array",
            label: "Images",
            array: {
                size: 5,
                item: {
                    type: "string",
                    default: ""
                }
            }
        }
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Default Experience GUI Class
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    classdetails["com.imersia.experiencegui"] = {
        topbannerimage : {
            type: "image",
            label: "Top Banner",
            hide: true,
            default: "",
            for: "topbannerurl"
        },
        bottombannerimage : {
            type: "image",
            label: "Bottom Banner",
            hide: true,
            default: "",
            for: "bottombannerurl"
        },
        topbannerurl: {
            type: "string",
            label: "Top Banner URL",
            default: "",
            pasteicon: true
        },
        topbannerbutton: {
            type: "button",
            hide: true,
            cssclass: "fileloader",
            filetypes: ".png, .jpg, .jpeg",
            label: "",
            default: "Load Top Banner File",
            for: "topbannerurl"
        },
        bottombannerurl: {
            type: "string",
            label: "Bottom Banner URL",
            default: "",
            pasteicon: true
        },
        bottombannerbutton: {
            type: "button",
            hide: true,
            cssclass: "fileloader",
            filetypes: ".png, .jpg, .jpeg",
            label: "",
            default: "Load Bottom Banner File",
            for: "bottombannerurl"
        }
    };

    classdetails["com.imersia.experienceguimap"] = {
        mapstyle: {
            type: "select",
            label: "Map style",
            options: self.mapstyles,
            default: "com.mapbox.streets"
        },
        mapiconimage : {
            type: "image",
            label: "Map icon",
            hide: true,
            default: "",
            for: "mapiconurl"
        },
        mapiconurl: {
            type: "string",
            label: "Map icon URL",
            default: "",
            pasteicon: true
        },
        mapiconbutton: {
            type: "button",
            hide: true,
            cssclass: "fileloader",
            filetypes: ".png, .jpg, .jpeg",
            label: "",
            default: "Load Map Icon",
            for: "mapiconurl"
        }
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Audio Class
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    classdetails["com.imersia.audio"] = {
        audiourl: {
            type: "string",
            label: "Audio URL",
            default: "",
            pasteicon: true
        },
        audiobutton: {
            type: "button",
            hide: true,
            cssclass: "fileloader",
            filetypes: ".mp3",
            label: "",
            default: "Load Audio File",
            for: "audiourl"
        },
        // audioplaybutton : {
        //     type: "button",
        //     hide: true,
        //     label: "",
        //     default: "Play / Pause Audio",
        //     for: "audiourl"
        // },
        comment1: {
            type: "comment",
            text: "Note that large audio files can cause problems for mobile devices, so it is best to break audio into smaller chunks.  The best format to use is MP3.",
            check : function () {}
        },
        // playonstart: {
        //     type: "check",
        //     label: "Play on start",
        //     hint: "Start playing when loaded.",
        //     default: true
        // },
        trigger: {
            type: 'string',
            label: "Trigger"
        },
        vibrate: {
            type: "check",
            label: "Vibrate",
            hint: "Vibrate device when playing audio (only phones)",
            default: false
        }
        // comment0: {
        //     type: "comment",
        //     text: "Start playing when this trigger event is received."
        // },
        // repeat: {
        //     type: "check",
        //     label: "Repeat",
        //     hint: "Repeatedly cycle the audio once playing.",
        //     default: false
        // },
        // positional: {
        //     type: "check",
        //     label: "Positional",
        //     hint: "Position the audio in the real world (ticked), or play in the background (unticked).",
        //     default: true
        // }
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Sprite Class
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    classdetails["com.imersia.sprite"] = {
        touch: {
            type: "select",
            label: "Touch action",
            options: ["touch", "play", "webapp"],
            default: "touch"
        },
        imageurl: {
            type: "string",
            label: "Sprite URL",
            default: "",
            pasteicon: true
        },
        playonstart: {
            type: "check",
            label: "Play on start",
            hint: "Start playing when sprite is loaded.",
            default: true
        },
        repeat: {
            type: "check",
            label: "Repeat",
            hint: "Repeatedly cycle the animation once playing.",
            default: true
        },
        trigger: {
            type: 'string',
            label: "Trigger"
        },
        comment0: {
            type: "comment",
            text: "The animation is played once when this trigger is received."
        },
        scale: {
            type: 'number',
            label: "Scale",
            default: 1,
            hint: ">=0",
            check: function (value) { return (Math.round(Math.abs(value))); }
        },
        comment1: {
            type: "comment",
            text: "A sprite sheet has width by height cells and has length number of frames.<br>It cycles at framerate frames per second.<br>A non-animated sprite has only 1 cell.",
            check : function () {}
        },
        width: {
            type: 'number',
            label: "Width",
            hint: ">= 1",
            default: 1,
            check: function (value) { return (Math.max(1, Math.round(Math.abs(value)))); }
        },
        height: {
            type: 'number',
            label: "Height",
            hint: ">= 1",
            default: 1,
            check: function (value) { return (Math.max(1, Math.round(Math.abs(value)))); }
        },
        length: {
            type: 'number',
            label: "Length",
            hint: ">= 1",
            default: 1,
            check: function (value) { return (Math.max(1, Math.round(Math.abs(value)))); }
        },
        framerate: {
            type: 'number',
            label: "Framerate",
            hint: "per second",
            default: 30,
            check: function (value) { return (Math.max(1, Math.round(Math.abs(value)))); }
        },
        spritebutton: {
            type: "button",
            hide: true,
            cssclass: "fileloader",
            filetypes: ".png, .png, .jpeg",
            label: "",
            default: "Load Sprite Sheet",
            for: "imageurl"
        },
        sprite : {
            type: "image",
            hide: true,
            label: "",
            default: "",
            for: "imageurl"
        }
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Panorama Class
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    classdetails["com.imersia.panorama"] = {
        touch: {
            type: "select",
            label: "Touch action",
            options: ["enter"],
            default: "enter",
            check: function (value) { return (value); }
        },
        imageurl: {
            type: "string",
            label: "Image URL",
            hint: "360 image",
            default: "",
            check : function (value) { return (value); },
            pasteicon: true
        },
        imagebutton: {
            type: "button",
            hide: true,
            cssclass: "fileloader",
            filetypes: ".png, .png, .jpeg",
            default: "Load 360 Image",
            for: "imageurl"
        },
        image : {
            type: "image",
            hide: true,
            default: "",
            for: "imageurl"
        },
        audiourl: {
            type: "string",
            label: "Audio URL",
            default: "",
            check : function (value) { return (value); }
        },
        audiobutton: {
            type: "button",
            hide: true,
            cssclass: "fileloader",
            filetypes: ".mp3",
            default: "Load Audio",
            for: "audiourl"
        },
        audio : {
            type: "audio",
            hide: true,
            default: "",
            for: "audiourl"
        }
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Threesixty Video Class
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    classdetails["com.imersia.threesixtyvideo"] = {
        touch: {
            type: "select",
            label: "Touch action",
            options: ["enter"],
            default: "enter",
            check: function (value) { return (value); }
        },
        videourl: {
            type: "string",
            label: "Video URL",
            hint: "mp4 360 video",
            default: "",
            check : function (value) { return (value); }
        },
        videobutton: {
            type: "button",
            hide: true,
            cssclass: "fileloader",
            filetypes: ".mp4",
            default: "Load 360 Video",
            for: "videourl"
        },
        video : {
            type: "video",
            hide: true,
            default: "",
            for: "videourl"
        }
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    this.ConvertToHTML = ConvertToHTML;
    this.ConvertClassToHTML = ConvertClassToHTML;
    this.ValidateInput = ValidateInput;
    this.GetClassesAsHTML = GetClassesAsHTML;
    this.GetClassAsJSON = GetClassAsJSON;
    this.ClassDetails = ClassDetails;
    this.InterpretContentURL = InterpretContentURL;
}
