// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Manages extra UI for mobile mode
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


IMERSIA.Mobile_UI = function ()
{
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Public Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Private Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var scanner = null;

    // Pointer to self
    var self = this;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Initialise = function ()
    {
        // 2D/3D Button switch is in index.js

        $("body").on("click", "#reloadexperiencebutton", function (e) {
            LoadExperience();
        });

        $("body").on("click", "#experiencelistboxcancel", function (e) {
            $('#experiencelistbox').modal({allowMultiple: false}).modal("hide");
        });

        $("body").on("click", "#aroundherebutton", function (e) {
            $('#mobileurlinput').val("https://" + window.location.hostname);
            LoadExperience();
        });

        $("body").on("click", "#qrcodebutton", function (e) {
            scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
            scanner.addListener('scan', function (content) {
                $('#mobileurlinput').val(content);
                $("#previewbox").modal({allowMultiple: false}).modal("hide");
                scanner.stop();
                LoadExperience();
            });
            Instascan.Camera.getCameras().then(function (cameras) {
                if (cameras.length > 1) {
                    $("#previewbox").modal({allowMultiple: false}).modal("show");
                    scanner.start(cameras[1]);
                } else if (cameras.length > 0) {
                    $("#previewbox").modal({allowMultiple: false}).modal("show");
                    scanner.start(cameras[0]);
                } else {
                    console.error('No cameras found.');
                }
            }).catch(function (e) {
                console.error(e);
            });
        });

        $("body").on("click", "#previewboxcancel", function (e) {
            if (scanner) scanner.stop();
            $("#previewbox").modal({allowMultiple: false}).modal("hide");
        });

        $("body").on("keyup", "#mobileurlinput", function (e)
        {
            if (e.keyCode === 13) {
                LoadExperience();
            }
        });

        $("body").on("click", ".mobileexperiencebutton", function (e) {
            var channelname = e.currentTarget.id.split("|")[1];
            var currentURL = $('#mobileurlinput').val();
            $('#mobileurlinput').val(currentURL + "/" + channelname);
            $('#experiencelistbox').modal({allowMultiple: false}).modal("hide");
            LoadExperience();
        });

        $("body").on("click", ".favouriteexperiencebutton", function (e) {
            var URLtoload = e.currentTarget.id.split("|")[1];
            $('#mobileurlinput').val(URLtoload);
            $('#favouritesbox').modal({allowMultiple: false}).modal("hide");
            LoadExperience();
        });

        // When the Favourites button is pressed
        $("body").on("click", "#favouritesbutton", function(e)
        {
            APP.showfavouritesafterlogin = true;
            if (!APP.loggedin)
            {
                $("#loginboxmessage").html("Please log in so we can load<br>and save your favourite Experiences.");

                var loginemail = $.cookie('loginemail') || "";
                $("#loginemail").val(loginemail);

                $('#loginbox').modal({closable:false, allowMultiple:false}).modal("show");
            }
            else {
                LoadFavourites();
            }
        });

        // When the mobile login button is pressed
        $("body").on("click", "#loginmobilebutton", function(e)
        {
            APP.showfavouritesafterlogin = false;
            if (!APP.loggedin)
            {
                $("#loginboxmessage").html("Please log in.");

                var loginemail = $.cookie('loginemail') || "";
                $("#loginemail").val(loginemail);

                $('#loginbox').modal({closable:false, allowMultiple:false}).modal("show");
            }
        });

        // Adding a URL to the user's favourites
        $("body").on("click", "#favouritesboxadd", function (e) {
            var currentURL = $('#favouritesboxurl').val();
            if (currentURL == "")
            {
                APP.Companion_UI.AddErrorMessage("Please load an Experience first.");
            }
            else
            {
                if (APP.companion.hasOwnProperty("com.imersia.realitybrowser"))
                {
                    if (APP.companion["com.imersia.realitybrowser"].hasOwnProperty("favourites"))
                    {
                        if (Array.isArray(APP.companion["com.imersia.realitybrowser"].favourites))
                        {
                            APP.companion["com.imersia.realitybrowser"].favourites.push(currentURL);
                        }
                    }
                    else
                    {
                        APP.companion["com.imersia.realitybrowser"].favourites = [currentURL];
                    }
                }
                else
                {
                    APP.companion["com.imersia.realitybrowser"] = {favourites: [currentURL]};
                }

                APP.companion["com.imersia.realitybrowser"].favourites = RemoveDuplicates(APP.companion["com.imersia.realitybrowser"].favourites);

                APP.SDK.PostToAPI(APP.companion.sessionid, "metadata", {userid: APP.companion.userid}, {key: "com.imersia.realitybrowser", value: APP.companion["com.imersia.realitybrowser"]}, function(result)
                {
                    APP.Companion_UI.AddInfoMessage("Favourites saved.", true);
                },function (e)
                {
                    APP.Companion_UI.AddErrorMessage("Favourites NOT saved.");
                });
            }
        });

        $("body").on("click", "#favouritesboxcancel", function (e) {
            $('#favouritesbox').modal({allowMultiple: false}).modal("hide");
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var LoadFavourites = function()
    {
        var currentURL = $('#mobileurlinput').val();
        if (currentURL != "")
        {
            var parsedURL = ExtractURL(currentURL);
            var remadeURL = parsedURL.remadeurl;
            $('#favouritesboxurl').val(remadeURL);
        }

        $('#favouritesboxcontent').html("");

        if (APP.companion.hasOwnProperty("com.imersia.realitybrowser"))
        {
            if (APP.companion["com.imersia.realitybrowser"].hasOwnProperty("favourites"))
            {
                // Load Favourites
                for (var i=0; i < APP.companion["com.imersia.realitybrowser"].favourites.length; i++)
                {
                    var URL = APP.companion["com.imersia.realitybrowser"].favourites[i];
                    LoadFavouriteExperienceDetails(URL, function (ExperienceDetails, URL, urlBase)
                    {
                        if (ExperienceDetails)
                        {
                            $('#favouritesboxcontent').append( FavouriteItem(new IMERSIA.Channel(ExperienceDetails), URL, urlBase));
                        }
                    });
                }
            }
        };

        $('#favouritesboxcontent').css('height', $(window).height() * 0.5);
        $('#favouritesbox').modal({allowMultiple: false}).modal("show");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var LoadFavouriteExperienceDetails = function (URL, callback)
    {
        var parsedURL = ExtractURL(URL);
        var path = (parsedURL.path == "/") ? "" : parsedURL.path;
        var urlBase = parsedURL.urlbase;
        var geoJSONURL = urlBase + "/geojson" + path + "?" + encodeURIComponent(parsedURL.query);
        if (path == "")
        {
            // No Experience path
            callback(null, URL, urlBase);
        }
        else {
            APP.SDK.GetGeoJSON(APP.companion.sessionid, geoJSONURL, {}, function (geoJSON)
            {
                callback(geoJSON.details, URL, urlBase);
            },
            function(error)
            {
                // Some other Error
                callback(null, URL, urlBase);
            });
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var FavouriteItem = function (details, URL, urlBase)
    {
        var imagedetails = "";

        if (details.imageurl == "")
        {
            imagedetails = urlBase + '/files?size=medium';
        }
        else if (details.imageurl.indexOf("http") == -1)
        {
            imagedetails = urlBase + details.imageurl + '?size=medium';
        }
        else
        {
            imagedetails = '/files/' + details.channelid + '?url=' + urlBase + details.imageurl + '&size=medium';
        }

        var spliturl = urlBase.split("://");
        var urlwithouthttps = "";
        if (spliturl.length >= 2)
        {
            urlwithouthttps = spliturl[1];
        }

        return (
            '<div class="item favouriteexperiencebutton" id="' + details.channelid + '|' + URL + '">' +
                '<div class="image">' +
                    '<img src="' + imagedetails + '">' +
                '</div>' +
                '<div class="content">' +
                    '<div class="center aligned header">' +
                        details.name +
                    '</div>' +
                    '<div class="center aligned description">' +
                        details.description +
                    '</div>' +
                    '<div class="center aligned extra content">' +
                        ConvertClass(details.class) +
                    '</div>' +
                    '<div class="center aligned extra content">' +
                        'on ' + urlwithouthttps +
                    '</div>' +
                '</div>' +
            '</div>'
        );
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var RemoveDuplicates = function (theArray)
    {
        var outArray = [];
        $.each(theArray, function(i, el){
            if($.inArray(el, outArray) === -1) outArray.push(el);
        });
        return outArray;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var LoadExperience = function ()
    {
        $('#mobileurlinput').blur();
        $("#mobileurlinputicon").removeClass("green").addClass("grey");

        NProgress.start();
        var URL = $('#mobileurlinput').val();
        APP.Maps.ClearPointsFromMap();

        var parsedURL = ExtractURL(URL);

        if (parsedURL.host == "")
        {
            // Is not a URL, so do a search instead.
            // search on local system for keywords
            APP.Companion_UI.AddErrorMessage("Not a valid Experience URL");
            NProgress.done();
        }
        else
        {
            var remadeQuery = parsedURL.query;
            var path = (parsedURL.path == "/") ? "" : parsedURL.path;
            var urlBase = parsedURL.urlbase;
            var geoJSONURL = urlBase + "/geojson" + path + "?" + encodeURIComponent(remadeQuery);
            var remadeURL = parsedURL.remadeurl;

            // Put it back in the input box for reference
            $('#mobileurlinput').val(remadeURL);

            // Attempt to get GeoJSON from URL
            APP.SDK.GetGeoJSON(APP.companion.sessionid, geoJSONURL, {}, function (geoJSON)
            {
                NProgress.inc();
                // Display GeoJSON result
                // Create the channel data structure

                if (geoJSON.hasOwnProperty("channels"))
                {
                    if (path != "")
                    {
                        NProgress.done();
                        APP.Companion_UI.AddErrorMessage("Channel " + path.substr(1) + " does not exist.");
                    }
                    else {
                        LoadExperiences(geoJSON.channels, urlBase);
                        NProgress.done();
                        $('#experiencelistbox').modal({allowMultiple: false}).modal("show");
                    }
                }
                else
                {
                    var newChannel = new IMERSIA.Channel(geoJSON.details);
                    newChannel.metadata.metadata = geoJSON.details.metadata;
                    if (newChannel.imageurl != "")
                    {
                        newChannel.imageurl = urlBase + newChannel.imageurl;
                    }
                    newChannel.baseuri = urlBase;
                    APP.companion.channels[newChannel.channelid] = newChannel;

                    // Set the UI skin
                    var ExperienceGUI = newChannel.metadata.GetByKey("com.imersia.experiencegui");
                    if (ExperienceGUI)
                    {
                        SetUISkin(ExperienceGUI.value);
                    }
                    else
                    {
                        SetUISkin(null);
                    }

                    // Wotch this channel (will currently only work if same server as logged into)
                    if (APP.companion.websocket)
                    {
                        APP.companion.websocket.send(JSON.stringify({ command : 'wotcha', sessionid : APP.companion.sessionid, parameters : { id : newChannel.channelid } }));
                        $("#mobileurlinputicon").removeClass("grey").addClass("green");
                    }

                    // Setup the Geobots
                    for (var i = 0; i < geoJSON.features.length; i++)
                    {
                        NProgress.inc();

                        var newGeobot = new IMERSIA.Geobot(geoJSON.features[i].properties);
                        newGeobot.metadata.metadata = geoJSON.features[i].properties.metadata;

                        if (newGeobot.imageurl != "")
                        {
                            newGeobot.imageurl = urlBase + newGeobot.imageurl;
                        }
                        newGeobot.baseuri = urlBase;

                        APP.companion.channels[newChannel.channelid].geobots[newGeobot.geobotid] = newGeobot;
                        APP.Maps.AddPointToMap(APP.companion.channels[newChannel.channelid].geobots[newGeobot.geobotid]);
                    }

                    // Select this channel to view
                    APP.currentlySelectedChannel = newChannel.channelid;
                    APP.Maps.RecentreMap();
                    NProgress.done();

                    // Wotch the channel for any changes
                    //self.websocket.send(JSON.stringify({ command : 'wotcha', sessionid : self.sessionid, parameters : { id : channel.channelid } }));
                }
            }, function()
            {
                APP.Companion_UI.AddErrorMessage("Unable to load Experience from that location.");
                NProgress.done();
            });
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetUISkin = function (SkinMetadata)
    {
        if (SkinMetadata)
        {
            $('#uimobiletopbanner').attr("src", SkinMetadata.topbannerurl || "");
            $('#uimobilebottombanner').attr("src", SkinMetadata.bottombannerurl || "");
            APP.Maps.SetMapType(SkinMetadata.mapstyle || "com.mapbox.streets");
        }
        else
        {
            $('#uimobiletopbanner').attr("src", "");
            $('#uimobilebottombanner').attr("src", "");
            APP.Maps.SetMapType('com.mapbox.streets');
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ExtractURL = function (URL)
    {
        // Check if url begins with http(s).  If not, add https
        if (!/^(f|ht)tps?:\/\//i.test(URL)) {
           URL = "https://" + URL;
        }

        // Check if it is a URL
        var parsedURL = parseUri(URL);
        var path = (parsedURL.path == "/") ? "" : parsedURL.path;

        parsedURL.urlbase = parsedURL.protocol + "://" + parsedURL.host + ((parsedURL.port == "") ? "" : ":" + parsedURL.port);
        parsedURL.remadeurl = parsedURL.urlbase + path;

        return parsedURL;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Build a UI element from the details given
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var LoadExperiences = function (ListOfChannels, urlBase)
    {
        $('#experiencelistboxcontent').css('height', $(window).height() * 0.5);
        $('#experiencelistboxcontent').html("");
        ListOfChannels.forEach (function (Channel) {
            if (Channel.class != "com.imersia.default")
            {
                $('#experiencelistboxcontent').append( ExperienceItem(Channel, urlBase));
            }
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ExperienceItem = function (details, urlBase)
    {
        var imagedetails = "";

        if (details.imageurl == "")
        {
            imagedetails = urlBase + '/files?size=medium';
        }
        else if (details.imageurl.indexOf("http") == -1)
        {
            imagedetails = urlBase + details.imageurl + '?size=medium';
        }
        else
        {
            imagedetails = '/files/' + details.channelid + '?url=' + urlBase + details.imageurl + '&size=medium';
        }

        return (
            '<div class="item mobileexperiencebutton" id="' + details.channelid + '|' + details.name + '">' +
                '<div class="image">' +
                    '<img src="' + imagedetails + '">' +
                '</div>' +
                '<div class="content">' +
                    '<div class="center aligned header">' +
                        details.name +
                    '</div>' +
                    '<div class="center aligned description">' +
                        details.description +
                    '</div>' +
                    '<div class="center aligned extra content">' +
                        ConvertClass(details.class) +
                    '</div>' +
                '</div>' +
            '</div>'
        );
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ConvertClass = function(theClass)
    {
        var returnText = "";
        switch (theClass)
        {
            case "com.imersia.geophoto":
                returnText = "Geophoto Library";
                break;
            case "com.imersia.storyline":
                returnText = "Storyline";
                break;
            case "com.imersia.virtuandize":
                returnText = "Augmented Virtuandize";
                break;
            case "com.imersia.treasurehunt":
                returnText = "Treasure Hunt";
                break;
            case "com.imersia.contextad":
                returnText = "Contextual Ad";
                break;
        }
        return (returnText);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    function parseUri (str) {
    	var	o   = parseUri.options,
    		m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
    		uri = {},
    		i   = 14;

    	while (i--) uri[o.key[i]] = m[i] || "";

    	uri[o.q.name] = {};
    	uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
    		if ($1) uri[o.q.name][$1] = $2;
    	});

    	return uri;
    };

    parseUri.options = {
    	strictMode: true,
    	key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
    	q:   {
    		name:   "queryKey",
    		parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    	},
    	parser: {
    		strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
    		loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    	}
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    this.Initialise = Initialise;
    this.LoadFavourites = LoadFavourites;
}
