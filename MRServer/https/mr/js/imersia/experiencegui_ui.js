// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Manages UI panel for the Experience GUI
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


IMERSIA.ExperienceGUI_UI = function ()
{
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Public Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Private Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    var panelside = "right";
    var panelname = "experienceguisidebar";

    // Pointer to self
    var self = this;
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Initialise = function ()
    {
        $('.'+panelname).sidebar({dimPage:false, closable: false, transition: "overlay", mobileTransition: "overlay"});
        // $('.pointing.menu .item').tab();

        // Save Channel
        $("body").on("click", "#saveexperienceguibutton", function (e)
        {
            SaveExperience();
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Open = function ()
    {
        // Close any LHS panels
        $(".sidebar."+panelside).not("."+panelname).sidebar("hide");

        // Open this one.
        $("."+panelname).sidebar("show");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Close = function ()
    {
        $("."+panelname).sidebar("hide");
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetDetails = function (experienceguimetadata)
    {
        var experienceguidetails = {};
        if (experienceguimetadata == undefined)
        {
            experienceguidetails.topbannerurl = "";
            experienceguidetails.bottombannerurl = "";
            experienceguidetails.mapstyle = "";
            experienceguidetails.mapiconurl = "";
        }
        else {
            var topbannerurl = APP.Classes.InterpretContentURL(APP.currentlySelectedChannel, experienceguimetadata.value.topbannerurl || "", "size=medium", true);
            var bottombannerurl = APP.Classes.InterpretContentURL(APP.currentlySelectedChannel, experienceguimetadata.value.bottombannerurl || "", "size=medium", true);
            var mapiconurl = APP.Classes.InterpretContentURL(APP.currentlySelectedChannel, experienceguimetadata.value.mapiconurl || "", "size=small", true);
            var mapstyle = experienceguimetadata.value.mapstyle || "" ;

            experienceguidetails.topbannerimage = topbannerurl;
            experienceguidetails.topbannerurl = topbannerurl;
            experienceguidetails.bottombannerurl = bottombannerurl;
            experienceguidetails.bottombannerimage = bottombannerurl;
            experienceguidetails.mapstyle = mapstyle;
            experienceguidetails.mapiconimage = mapiconurl;
            experienceguidetails.mapiconurl = mapiconurl;
        }

        $('#experienceguimain').html(
            APP.Classes.ConvertClassToHTML("com.imersia.experiencegui", experienceguidetails)
        );

        $('#experienceguimap').html(
            APP.Classes.ConvertClassToHTML("com.imersia.experienceguimap", experienceguidetails)
        );

        APP.ExperienceGUI_UI.Open();
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var ClearOutput = function()
    {
        // $('#geobotoutput').html("");
        // if ((APP.currentlySelectedChannel != "") && (APP.currentlySelectedGeobot != ""))
        // {
        //     var thisGeobot = APP.companion.channels[APP.currentlySelectedChannel].geobots[APP.currentlySelectedGeobot];
        //     thisGeobot.Deregister("geobotui");
        // }
    };
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Upload an image for the Geobot experience and change the imageurl
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var UpdateMetadata = function (bannerposition, details, thefile, callback)
    {
        var thisChannel = APP.companion.channels[details.channelid];
        APP.SDK.PostFileToAPI(APP.companion.sessionid, 'files', details, thefile, function (result) {
            if (result.hasOwnProperty("fileurls"))
            {
                if (thisChannel.hasOwnProperty("metadata"))
                {
                    var experienceguidetails = thisChannel.metadata.GetByKey("com.imersia.experiencegui");
                    if (experienceguidetails)
                    {
                        var updatedexperienceguidetails = experienceguidetails.value;
                        updatedexperienceguidetails[bannerposition] = result.fileurls[0];
                        thisChannel.metadata.Set(APP.companion.sessionid, experienceguidetails.metadataid, "com.imersia.experiencegui", updatedexperienceguidetails,
                            function (result) {},
                            function (error) {}
                        )
                    }
                    else
                    {
                        var newexperienceguidetails = {
                            topbannerurl : "",
                            bottombannerurl : "",
                            mapiconurl : "",
                            mapstyle : "com.mapbox.streets"
                        };
                        newexperienceguidetails[bannerposition] = result.fileurls[0];
                        thisChannel.metadata.Set(APP.companion.sessionid, null, "com.imersia.experiencegui", newexperienceguidetails,
                            function (result) {
                            },
                            function (error) {
                            }
                        )
                    }
                }
                else {
                    callback ("");
                }
                callback (result.fileurls[0]);
            }
            else {
                callback ("");
            }
        }, function (error) {
            callback("")
        });

        APP.ExperienceGUI_UI.Open();
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Save the results of editing the experience details
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SaveExperience = function (callback)
    {
        // Save Channel
        var thisChannel = APP.companion.channels[APP.currentlySelectedChannel];

        var MetadataUpdates = {
            topbannerurl : $('#com_imersia_experiencegui_topbannerurl').val(),
            bottombannerurl : $('#com_imersia_experiencegui_bottombannerurl').val(),
            mapiconurl: $('#com_imersia_experienceguimap_mapiconurl').val(),
            mapstyle: $('#com_imersia_experienceguimap_mapstyle').val()
        };


        // Save it to the API
        thisChannel.metadata.Set(APP.companion.sessionid, null, "com.imersia.experiencegui", MetadataUpdates,
            function (result) {
            },
            function (error) {
                APP.Companion_UI.AddErrorMessage("Error saving experience GUI");
            }
        );

        // Close this menu and open the companion to show the result of saving
        APP.ExperienceGUI_UI.Close();
        APP.ChannelList_UI.SelectChannel();
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    this.Initialise = Initialise;
    this.Open = Open;
    this.Close = Close;
    this.SetDetails = SetDetails;
    this.ClearOutput = ClearOutput;
    this.UpdateMetadata = UpdateMetadata;
}
