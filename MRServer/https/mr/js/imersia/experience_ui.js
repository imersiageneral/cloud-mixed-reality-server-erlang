// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Manages the Experiences UI
// ****************************************************************************************************************************************************************************************************
"use strict";
var IMERSIA = IMERSIA || {};


IMERSIA.Experience_UI = function ()
{
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Public Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Private Variables
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    // Pointer to self
    var self = this;
    var CurrentExperienceType = "";
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Setup all the interactions etc
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Initialise = function ()
    {
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // Main Experience Box
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $("body").on("click", ".newexperience", function (e) {
            CurrentExperienceType = e.currentTarget.dataset.type;

            var ExperienceTypeUI = CreateExperienceTypeUI(CurrentExperienceType);

            $('#createexperienceboxcontent').css('height', $(window).height() * 0.6);
            $('#createexperienceboxcontent').html(ExperienceTypeUI.html);
            $('#createexperienceboxtitle').text(ExperienceTypeUI.title);
            $('#createexperiencebox').modal({allowMultiple: false}).modal("show");
        });

        $("body").on("click", "#createexperienceboxcancel", function (e) {
            $('#newexperiencebox').modal({allowMultiple: false}).modal("hide");
            $('#createexperiencebox').modal({allowMultiple: false}).modal("hide");
        });

        $("body").on("click", "#newexperienceboxcancel", function (e) {
            $('#newexperiencebox').modal({allowMultiple: false}).modal("hide");
            $('#createexperiencebox').modal({allowMultiple: false}).modal("hide");
        });

        $("body").on("click", "#mobilenewexperienceboxcancel", function (e) {
            $('#mobilenewexperiencebox').modal({allowMultiple: false}).modal("hide");
            // $('#mobilecreateexperiencebox').modal({allowMultiple: false}).modal("hide");
        });

        $("body").on("click", "#createexperienceboxcreate", function (e) {
            switch (CurrentExperienceType)
            {
                case "geophotolibrary":
                    CreatePhotoLibraryStep1();
                break;
                case "storyline":
                    ProcessStroyLineUI();
                break;
            }
        });

        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var Open = function ()
    {
        var ExperienceTypes = LoadExperienceTypes();

        $('#newexperienceboxcontent').css('height', $(window).height() * 0.6);
        $('#newexperienceboxcontent').html(ExperienceTypes);

        if (APP.mobilemode)
        {
            $('#mobilenewexperiencebox').modal({allowMultiple: false}).modal("show");
        }
        else
        {
            $('#newexperiencebox').modal({allowMultiple: false}).modal("show");
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Create Experience Types UI
    // TODO: Load these dynamically from a database
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var LoadExperienceTypes = function()
    {
        var html = ' \
            <div class="ui three stackable cards"> \
                <div class="card"> \
                    <div class="image"> \
                        <img src="images/geophotolibrary2.png"> \
                    </div> \
                    <div class="content"> \
                        <div class="header">Geophoto Library</div> \
                        <div class="description"> \
                            A geo-photo library consists of images located at places, sometimes with narrative, music or extra information. \
                        </div> \
                    </div> \
                    <div class="ui bottom attached button newexperience" data-type="geophotolibrary"> \
                        <i class="add icon"></i> \
                            Create \
                    </div> \
                </div> \
                <div class="card"> \
                    <div class="image"> \
                        <img src="images/songline.jpg"> \
                    </div> \
                    <div class="content"> \
                        <div class="header">Storyline</div> \
                        <div class="description"> \
                            A storyline is a trail through a place, accompanied by a "walking narrative". It may be augmented with images and videos if desired.\
                        </div> \
                    </div> \
                    <div class="ui bottom attached disabled button newexperience" data-type="storyline"> \
                        <i class="add icon"></i> \
                            coming soon \
                    </div> \
                </div> \
                <div class="card"> \
                    <div class="image"> \
                        <img src="images/virtuandize.png"> \
                    </div> \
                    <div class="content"> \
                        <div class="header">Augmented Virtuandize</div> \
                        <div class="description"> \
                             Augment any image with 3D content, videos, links and sound.  Can be used with posters, t-shirts, mugs, caps - in fact any printed media.\
                        </div> \
                    </div> \
                    <div class="ui bottom attached disabled button newexperience" data-type="virtuandize"> \
                        <i class="add icon"></i> \
                            coming soon \
                    </div> \
                </div> \
                <div class="card"> \
                    <div class="image"> \
                        <img src="images/treasurehunt.jpg"> \
                    </div> \
                    <div class="content"> \
                        <div class="header">Treasure Hunt</div> \
                        <div class="description"> \
                             Create a linked series of clues that automatically records who has been at each clue, and manages prizes for those who complete the hunt.\
                        </div> \
                    </div> \
                    <div class="ui bottom attached disabled button newexperience" data-type="treasurehunt"> \
                        <i class="add icon"></i> \
                            coming soon \
                    </div> \
                </div> \
                <div class="card"> \
                    <div class="image"> \
                        <img src="images/contextualadvertising.jpg"> \
                    </div> \
                    <div class="content"> \
                        <div class="header">Contextual Advertising</div> \
                        <div class="description"> \
                             Contextual Advertising allows you to use AI to target a specific message to a potential client when they are most receptive to your product or service.\
                        </div> \
                    </div> \
                    <div class="ui bottom attached disabled button newexperience" data-type="contextad"> \
                        <i class="add icon"></i> \
                            coming soon \
                    </div> \
                </div> \
                <div class="card"> \
                    <div class="image"> \
                        <img src="images/geotemporalreminder.png"> \
                    </div> \
                    <div class="content"> \
                        <div class="header">Geotemporal Reminder</div> \
                        <div class="description"> \
                             Set a reminder for a specific time and place.  Add documents relevant to then and there.  For example, a shopping list for when you reach the supermarket.\
                        </div> \
                    </div> \
                    <div class="ui bottom attached disabled button newexperience" data-type="contextad"> \
                        <i class="add icon"></i> \
                            coming soon \
                    </div> \
                </div> \
            </div>';
        return html;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreateExperienceTypeUI = function (ExperienceType)
    {
        switch (ExperienceType)
        {
            case "geophotolibrary":
                return CreatePhotoLibraryUI();
            break;
            case "storyline":
                return CreateStroyLineUI();
            break;
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreatePhotoLibraryUI = function ()
    {
        var html = ' \
            <div class="grid"> \
                <div id="photolibrarynamediv" class="ui fluid labeled input"> \
                    <div class="ui label" style="width:20%">Name</div> \
                    <input id="photolibraryname" placeholder="Name of Geophoto Library" type="text"> \
                </div> \
                <div class="ui divider"></div> \
                The Geophoto Library name should be around 10 characters long, and describes the Library.  It has to be unique on this Mixed Reality Server. \
                <div class="ui divider"></div> \
                <div id="photolibrarymainimagefilediv" class="ui fluid labeled input"> \
                    <div class="ui label" style="width:20%">Main Image</div> \
                    <input id="photolibrarymainimagefile" type="file"/> \
                </div> \
                <div class="ui divider"></div> \
                The Geophoto Library Main Image will adorn the experience when people are looking for it, and should be easy to recognize. \
                <div class="ui divider"></div> \
                <div id="photolibrarycsvfilediv" class="ui fluid labeled input"> \
                    <div class="ui label" style="width:20%">Description File</div> \
                    <input id="photolibrarycsvfile" type="file"/> \
                </div> \
                <div class="ui divider"></div> \
                The Geophoto Library Description File is a CSV file describing the Photo Library.  Each row of the file represents a geo-spatially located photolibrary image. \
                <div class="ui divider"></div> \
                <div id="photolibraryimagefilesdiv" class="ui fluid labeled input"> \
                    <div class="ui label" style="width:20%">Photo Files</div> \
                    <input id="photolibraryimagefiles" type="file" multiple="multiple"/> \
                </div> \
                <div class="ui divider"></div> \
                When setting up the Geophoto Library, create a folder on your computer, place all the photos in the same folder, \
                load the CSV file and photo files above (making sure the photo file names in the CSV file match the photos loaded), and press the "Create" button. \
                <div class="ui divider"></div> \
                <div class="ui progress" id="photolibraryprogress"> \
                    <div class="bar"> \
                        <div class="progress"></div> \
                    </div> \
                    <div id="photolibraryprogresslabel" class="label">Waiting to start</div> \
                </div> \
            </div>';

        return {html:html, title:"Geophoto Library"};
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreatePhotoLibraryStep1 = function ()
    {
        // Grab the data from the dialog box
        var photolibraryname = $("#photolibraryname").val();

        var photolibraryimagefileInput = $("#photolibrarymainimagefile");
        var libraryimageFile = photolibraryimagefileInput[0].files[0];

        var csvfileInput = $("#photolibrarycsvfile");
        var csvFile = csvfileInput[0].files[0];

        var imagefilesInput = $("#photolibraryimagefiles");
        var imagefiles = imagefilesInput[0].files;

        // Assume all good to start with...
        var inputerror = false;

        // Do some checks, and produce an error if something is amiss
        if (photolibraryname == "") { $("#photolibrarynamediv").addClass("error"); inputerror = true; } else { $("#photolibrarynamediv").removeClass("error"); };
        if (libraryimageFile == undefined) { $("#photolibrarymainimagefilediv").addClass("error"); inputerror = true; } else { $("#photolibrarymainimagefilediv").removeClass("error"); };
        if (csvfileInput == undefined) { $("#photolibrarycsvfilediv").addClass("error"); inputerror = true; } else { $("#photolibrarycsvfilediv").removeClass("error"); };
        if (imagefiles.length == 0) { $("#photolibraryimagefilesdiv").addClass("error"); inputerror = true; } else { $("#photolibraryimagefilesdiv").removeClass("error"); };

        // Tell the user if there were errors, otherwise process the files
        if (inputerror)
        {
            APP.Companion_UI.AddErrorMessage("Some fields still need to be filled in.")
        }
        else
        {
            // Try to get this channel by name
            APP.SDK.GetFromAPI(APP.companion.sessionid, 'channels/' + photolibraryname, {}, function (details)
            {
                // Only proceed if this channel is owned by the logged in user
                if (details.ownerid == APP.companion.userid)
                {
                    // Channel exists, delete all the existing Geobots on this channel
                    APP.SDK.GetFromAPI(APP.companion.sessionid, 'geobots', {channelid:details.channelid}, function (geobotlist)
                    {
                        // Keep track of items deleted, because this is all asynchronous
                        var numgeobots = geobotlist.length;
                        if (numgeobots > 0)
                        {
                            var geobotcounter = 0;
                            for (var i = 0; i < numgeobots; i++)
                            {
                                APP.SDK.DeleteFromAPI(APP.companion.sessionid, 'geobots', {geobotid:geobotlist[i].geobotid}, function ()
                                {
                                    if (++geobotcounter >= numgeobots)
                                    {
                                        // All deleted, now create new Geobots
                                        CreatePhotoLibraryStep2 (details.channelid, photolibraryname, libraryimageFile, csvFile, imagefiles);
                                    }
                                }, function (error)
                                {
                                    // Something went wrong in the deletion process (could be the geobot was deleted by someone else)
                                    if (++geobotcounter >= numgebots)
                                    {
                                        // All deleted, now create new Geobots
                                        CreatePhotoLibraryStep2 (details.channelid, photolibraryname, libraryimageFile, csvFile, imagefiles);
                                    }
                                });
                            }
                        }
                        else
                        {
                            // There aren't any Geobots in this Channel
                            CreatePhotoLibraryStep2 (details.channelid, photolibraryname, libraryimageFile, csvFile, imagefiles);
                        }
                    });
                }
                else
                {
                    alert ("An Experience with this name already exists on this MR Server, but is not owned by you.  Please choose a different name for the GeoPhoto Library");
                }
            },
            function (error)
            {
                // A channel with this name desn't exist
                CreatePhotoLibraryStep2 (undefined, photolibraryname, libraryimageFile, csvFile, imagefiles);
            });
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreatePhotoLibraryStep2 = function (channelid, photolibraryname, libraryimageFile, csvFile, imagefiles)
    {
        // Create a new channel if one doesn't already exist
        if (channelid)
        {
            CreatePhotoLibraryStep3 (channelid, photolibraryname, libraryimageFile, csvFile, imagefiles);
        }
        else
        {
            // Create the channel with the given name
            APP.SDK.PostToAPI(APP.companion.sessionid, 'channels', {}, {name:photolibraryname}, function (result)
            {
                CreatePhotoLibraryStep3 (result.channelid, photolibraryname, libraryimageFile, csvFile, imagefiles);
            },
            function (error)
            {
                if (error.error == "name")
                {
                    alert ("An Experience with this name already exists on this MR Server, but is not owned by you.  Please choose a different name for the Geophoto Library.");
                }
                else
                {
                    alert ("An error orccurred, please contact your administrator.");
                }
            });
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreatePhotoLibraryStep3 = function (channelid, photolibraryname, libraryimageFile, csvFile, imagefiles)
    {
        // Upload the geophoto library image
        APP.SDK.PostFileToAPI(APP.companion.sessionid, 'files', {channelid : channelid}, libraryimageFile, function (result)
        {
            if (result.hasOwnProperty("fileurls"))
            {
                // And set the imageurl and class
                APP.SDK.PutToAPI(APP.companion.sessionid, 'channels', {channelid:channelid}, {imageurl:result.fileurls[0], class:"com.imersia.geophoto"}, function (result)
                {
                    CreatePhotoLibraryStep4 (channelid, photolibraryname, libraryimageFile, csvFile, imagefiles);
                }, function (error)
                {
                    alert ("An error orccurred, please contact your administrator.");
                });
            }
            else
            {
                alert ("An error orccurred, please contact your administrator.");
            }
        }, function (error)
        {
            APP.Companion_UI.AddErrorMessage("An error occurred loading the Geophoto Library main image.  Perhaps it is too large or not an image format.")
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreatePhotoLibraryStep4 = function (channelid, photolibraryname, libraryimageFile, csvFile, imagefiles)
    {
        var reader = new FileReader();
        reader.onload = function (imageFiles) {
            var csvfilecontents = reader.result;

            // Load the CSV File
            var ParsedCSVFile = Papa.parse(csvfilecontents, {header:true});

            var NumGeobots = ParsedCSVFile.data.length;
            var NewGeobotsCounter = 0;
            if (NumGeobots > 0)
            {
                // Go through each line in the file, Create a Geeobot
                for (var i=0; i<NumGeobots; i++)
                {
                    var geobotdetails = ParsedCSVFile.data[i];

                    // Check the geobot details has the core elements
                    if (geobotdetails.hasOwnProperty("name") && geobotdetails.hasOwnProperty("image") && geobotdetails.hasOwnProperty("latitude") && geobotdetails.hasOwnProperty("longitude"))
                    {
                        CreatePhotoLibraryStep5(channelid, geobotdetails, imagefiles, function ()
                        {
                            SetProgressBar((NewGeobotsCounter+1) / NumGeobots * 100, "Loading " + geobotdetails.name);
                            if (++NewGeobotsCounter >= NumGeobots)
                            {
                                SetProgressBar(100, "Finished");
                                alert ("New Geophoto Library successfully created.");
                                $('#newexperiencebox').modal({allowMultiple: false}).modal("hide");
                                $('#createexperiencebox').modal({allowMultiple: false}).modal("hide");
                            }
                        }, function (error)
                        {
                            if (++NewGeobotsCounter >= NumGeobots)
                            {
                                alert ("There was an error creating one of the Photos.");
                            }
                        });
                    }
                    else
                    {
                        APP.Companion_UI.AddErrorMessage("The Geophoto Library CSV file is not in the correct format.  Download example file to ensure this is all filled in correctly with the right headers.");
                    }
                }
            }
            else
            {
                APP.Companion_UI.AddErrorMessage("There were no Photos in the Geophoto Library CSV file, or the file format was incorrect.  See the example file for more details.")
            }
        };
        reader.readAsText(csvFile, 'utf8');
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var CreatePhotoLibraryStep5 = function (channelid, geobotdetails, imageFiles, callback, errorcallback)
    {
        // Create the new Geobot (Photo)
        APP.SDK.PostToAPI(APP.companion.sessionid, 'geobots', {channelid:channelid}, {name:geobotdetails.name, description: geobotdetails.description, location:{latitude:geobotdetails.latitude, longitude:geobotdetails.longitude, altitude:0}}, function (newgeobot)
        {
            // See if the image exists in the list of images
            var thisImage = GetImageFromFileList (geobotdetails.image, imageFiles);
            if (thisImage)
            {
                // Upload the geophoto image
                APP.SDK.PostFileToAPI(APP.companion.sessionid, 'files', {geobotid:newgeobot.geobotid}, thisImage, function (result)
                {
                    if (result.hasOwnProperty("fileurls"))
                    {
                        // And set the imageurl
                        APP.SDK.PutToAPI(APP.companion.sessionid, 'geobots', {geobotid:newgeobot.geobotid}, {imageurl:result.fileurls[0]}, function (result)
                        {
                            // All good, return to previous function
                            if (callback) callback();
                        }, function (error)
                        {
                            alert ("An error orccurred, please contact your administrator.");
                            if (errorcallback) errorcallback(error);
                        });
                    }
                    else
                    {
                        alert ("An error orccurred, please contact your administrator.");
                        if (errorcallback) errorcallback(error);
                    }
                }, function (error)
                {
                    alert ("An error occurred loading a Geolibrary Photo.  Perhaps it is too large or not an image format.")
                    if (errorcallback) errorcallback(error);
                });
            }
            else
            {
                // No image to upload
                if (callback) callback();
            }
        },
        function (error)
        {
            if (errorcallback) errorcallback(error);
        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var GetImageFromFileList = function(imageName, imagefiles)
    {
        var returnFile = null;
        var i = 0;
        var foundit=false;
        while ((i < imagefiles.length) && !foundit)
        {
            if (imagefiles[i].name == imageName)
            {
                returnFile = imagefiles[i];
                foundit = true;
            }
            i++;
        }
        return returnFile;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var SetProgressBar = function (Percent, Message)
    {
        $('#photolibraryprogress').progress({
          percent: Percent
        });
        $('#photolibraryprogresslabel').text(Message);
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    this.Initialise = Initialise;
    this.Open = Open;
}
