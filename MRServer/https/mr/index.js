// ****************************************************************************************************************************************************************************************************
// Copyright Imersia Ltd 2019
// Author: Dr. Roy C. Davies
// Description: A tool for managing content on Mixed Reality Servers
// ****************************************************************************************************************************************************************************************************
"use strict";

// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// APP Sections
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var APP = {
    // Links to the other pieces of code required (like having require())
    SDK :               new IMERSIA.SDK(),
    Companion_UI :      new IMERSIA.Companion_UI(),
    ChannelList_UI :    new IMERSIA.ChannelList_UI(),
    ExperienceList_UI : new IMERSIA.ExperienceList_UI(),
    Analytics_UI :      new IMERSIA.Analytics_UI(),
    ChannelEdit_UI :    new IMERSIA.ChannelEdit_UI(),
    Geobot_UI :         new IMERSIA.Geobot_UI(),
    GeobotEdit_UI :     new IMERSIA.GeobotEdit_UI(),
    MetadataEdit_UI:    new IMERSIA.MetadataEdit_UI(),
    Instruct_UI :       new IMERSIA.Instruct_UI(),
    Experience_UI :     new IMERSIA.Experience_UI(),
    ExperienceGUI_UI :  new IMERSIA.ExperienceGUI_UI(),
    Mapthreed :         new IMERSIA.Mapthreed(),
    Maptwod :           new IMERSIA.Maptwod(),
    Maps :              new IMERSIA.Maps(),
    AnimatedSprites :   new IMERSIA.AnimatedSprites(),
    Mobile_UI :         new IMERSIA.Mobile_UI(),
    Classes :           new IMERSIA.Classes(),

    // Some high-level global variables that affect overall behaviour
    companion : {},
    currentlySelectedGeobot : '',
    currentlySelectedChannel : '',
    currentlySelectedExperience : '',
    loggedin : false,
    playing : false,
    threedmode : false,
    mobilemode : false,
    mobiledevice : false,
    firstpersonmode : false,
    usermode : "guided",
    copiedtext : "",
    showfavouritesafterlogin: false
};
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// * Ready - called when document is ready and open
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$(document).ready(function () { StageOne(); });
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var StageOne = function() {

    var htmlelements = [
        "header",
        "login",
        "register",
        "webapp",
        "message",
        "checkdelete",
        "metadata",
        "instructgeobot",
        "createexperiences",
        "sidebars",
        "experiencelists",
        "qrcodepreview",
        "pusher",
        "footer"
    ];

    // Load the additional html elements required
    var counter = htmlelements.length;
    $.each(htmlelements, function (index, htmlelementname)
    {
        var filename = 'js/imersia/portalui/' + htmlelementname + '.html';
        $("body").append('<div id="' + htmlelementname + '"></div>');
        $('#' + htmlelementname).load(filename, function() {
            // Flatten the dev to make it a direct child of the body
            $(this).children(':first').unwrap();
            // Activate the rest of the UI once all is loaded
            if (--counter <= 0) StageTwo();
        });
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var StageTwo = function()
{
    // THREE.Cache.enabled = true;

    APP.SetUIItems();

    APP.Companion_UI.Initialise();
    APP.ChannelList_UI.Initialise();
    APP.ExperienceList_UI.Initialise();
    APP.Analytics_UI.Initialise();
    APP.ChannelEdit_UI.Initialise();
    APP.GeobotEdit_UI.Initialise();
    APP.Geobot_UI.Initialise();
    APP.MetadataEdit_UI.Initialise();
    APP.Instruct_UI.Initialise();
    APP.Experience_UI.Initialise();
    APP.ExperienceGUI_UI.Initialise();
    APP.Mobile_UI.Initialise();
    APP.companion = new IMERSIA.Companion();
    APP.Maps.Initialise('twod', function () {
        StageThree();
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var StageThree = function ()
{
    APP.Maptwod.Initialise();
    APP.Mapthreed.Initialise();
    APP.Maps.SetTwoD();
    APP.Maps.RecentreMap();

    SetUpMenuInteractions();
    SetUpLoginInteractions();
    SetUpRegisterInteractions();
    SetUpFileLoaders();

    // Check if running on a mobile device
    if (/Mobi/.test(navigator.userAgent)) {
        // Go to mobile mode
        GoPlayMode();
        GoMobileMode();
        APP.mobiledevice = true;
    }
    else {
        // Set the login box type and start with it showing.
        $("#loginboxmessage").html("Please log in.");

        var loginemail = $.cookie('loginemail') || "";
        $("#loginemail").val(loginemail);

        $('#loginbox').modal({closable:false, allowMultiple:false}).modal("show");
        GoGuidedMode(false);
        GoDesktopMode();
        APP.mobiledevice = false;
    };

    // Set the Version details on the window
    APP.SDK.GetFromAPI(APP.companion.sessionid, 'vars', {category: "mrserver", key : "version"}, function (details)
    {
        $("#imersiaversiondiv").text(details.value);
    });

    // Stop touch on screen moving stuff around on iOS
    $(window).on('touchmove', function(e) { e.preventDefault(); });

    // Ensure UI size and map adjusts if the window changes
    ResizeMenuBar();
    APP.Maps.ResizeCanvas();
    $( window ).resize(function() {
        ResizeMenuBar();
        APP.Maps.ResizeCanvas();
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var SetUpLoginInteractions = function()
{
    $("body").on("click", "#logincancel", function (e)
    {
        $('#loginbox').modal("hide");
    });

    $("body").on("click", "#loginregister", function (e)
    {
        $('#loginbox').modal("hide");
        $('#registerbox').modal({closable:false, allowMultiple:false}).modal("show");
    });

    $("body").on("keypress", "#loginemail", function(e)
    {
        if(e.keyCode==13) $('#loginok').click();
    });
    $("body").on("keypress", "#loginpassword", function(e)
    {
        if(e.keyCode==13) $('#loginok').click();
    });
    $("body").on("click", "#webappdone", function(e)
    {
        $('#webappbox').modal("hide");
    });

    $("body").on("click", "#loginok", function (e)
    {
        $('#loginbox').modal("hide");
        // ToggleFullscreen();

        var useremail = $("#loginemail").val().toLowerCase();
        var password = $("#loginpassword").val();

        $.cookie('loginemail', useremail, { expires: 7, path: '/' });

        APP.SDK.Login(useremail, password, function (sessionid) {
            APP.companion.SetLogin(sessionid, useremail);
            APP.currentlySelectedGeobot = "";
            APP.currentlySelectedChannel = "";

            APP.loggedin = true;

            APP.companion.Load();

            APP.SetUIItems();

            $("#loginbuttonicon").removeClass("sign in alt").addClass("sign out alt");
            $("#login").attr("data-tooltip", "Log out");
            $("#login").removeClass("blue").addClass("grey");
            $("#createexperiencebutton").removeClass("disabled");
            $("#uimobiledropdownmenu2icon").removeClass("grey").addClass("green");

            setTimeout(function () {
                APP.Maps.ClearPointsFromMap();
                APP.Maps.SetMapType('com.mapbox.streets');
                if (!APP.mobilemode)
                {
                    OpenAppropriateSidebars(false);
                }
                else
                {

                    if (APP.showfavouritesafterlogin)
                    {
                        APP.Mobile_UI.LoadFavourites();
                    }
                }
            }, 1000);
        }, function (error) {
            // ToggleFullscreen();
            APP.Companion_UI.AddErrorMessage("Error logging in");
        });
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var SetUpRegisterInteractions = function()
{
    $("body").on("click", "#registercancel", function (e)
    {
        $('#registerbox').modal("hide");
    });

    $("body").on("click", "#registerok", function (e)
    {
        $('#registerbox').modal("hide");

        var useremail = $("#registeremail").val();
        var password = $("#registerpassword").val();
        var password2 = $("#registerpassword2").val();
        var firstname = $("#registerfirstname").val();
        var surname = $("#registersurname").val();
        var nickname = $("#registernickname").val();

        if (password == password2)
        {
            APP.SDK.Register(useremail, password, firstname, surname, nickname, function (sessionid) {
                $('#loginbox').modal({closable:false, allowMultiple:false}).modal("show");
            }, function (error) {
                APP.Companion_UI.AddErrorMessage("Error registering");
            });
        }
        else {
            APP.Companion_UI.AddErrorMessage("Passwords are not the same");
        }
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var SetUpMenuInteractions = function()
{
    // The Fullscreen toggle button
    $("body").on("click", "#fullscreenbuttondesktop", function (e)
    {
        ToggleFullscreen();
    });
    $("body").on("click", "#fullscreenbuttonmobile", function (e)
    {
        ToggleFullscreen();
    });

    $("body").on("click", "#messageboxcancel", function (e)
    {
        $('#messagebox').modal("hide");
    });

    // The Channel List button
    $("body").on("click", "#chooserbutton", function (e)
    {
        OpenAppropriateSidebars(true);
    });

    // The Channels Reload button
    $("body").on("click", "#channelsreload", function (e) { ReloadChannels(); });
    $("body").on("click", "#experiencesreload", function (e) { ReloadChannels(); });

    // The Geobot Reload button
    $("body").on("click", "#geobotreload", function (e)
    {
        $(".sidebar.right").sidebar("hide");

        APP.companion.channels[APP.currentlySelectedChannel].geobots[APP.currentlySelectedGeobot].Reload(APP.companion.sessionid, function (result) {
            setTimeout(function () {
                APP.Geobot_UI.Open();
            }, 1000);
        });
    });

    // The Companion button
    $("body").on("click", "#companionbutton", function (e)
    {
        if (APP.Companion_UI.IsOpen())
        {
            APP.Companion_UI.Close();
        }
        else
        {
            APP.Companion_UI.Open();
        }
    });

    // The Play and Stop buttons
    $("body").on("click", "#playbutton", function (e) { GoPlayMode(); });
    $("body").on("click", "#stopbutton", function (e) { GoEditMode(); });

    // The Desktop / Mobile mode
    $("body").on("click", "#mobilebutton", function (e) { GoPlayMode(); GoMobileMode(); });
    $("body").on("click", "#desktopbutton", function (e) { GoEditMode(); GoDesktopMode(); });

    // The 2D 3D map selector
    $("body").on("click", "#twodeebutton", function (e) { GoTwoDeeMode(); });
    $("body").on("click", "#threedeebutton", function (e) { GoThreeDeeMode(); });
    $("body").on("click", "#2D3Dswitch", function(e) { if (APP.threedmode) { GoTwoDeeMode(); } else { GoThreeDeeMode(); } });

    // The Guided / Expert mode selector
    $("body").on("click", "#guidedmodebutton", function (e) { GoGuidedMode(); });
    $("body").on("click", "#expertmodebutton", function (e) { GoExpertMode(); });
    $("body").on("click", "#analyticsmodebutton", function (e) { GoAnalyticsMode(); });

    // The Mobile Create Experience button
    $("body").on("click", "#createexperiencebutton", function (e) { if (APP.loggedin) { APP.Experience_UI.Open(); } });

    $("body").on("click", "#1stpersontoggle", function (e) { if (APP.firstpersonmode) { APP.Mapthreed.SetThirdPerson(); } else { APP.Mapthreed.SetFirstPerson(); };  APP.firstpersonmode = !APP.firstpersonmode; });


    // The Login button
    $("body").on("click", "#login", function (e)
    {
        if (!APP.loggedin)
        {
            $('#loginboxmessage').html("Please log in.");

            var loginemail = $.cookie('loginemail') || "";
            $("#loginemail").val(loginemail);

            $('#loginbox').modal({closable:false, allowMultiple:false}).modal("show");
        }
        else
        {
            APP.Logout();
        }
    });

    // Set the dropdown menu behaviours
    $('.ui.dropdown.mobileleftdropdown').dropdown({
        action: 'hide'
    });
    $('.ui.dropdown.mobilerightdropdown').dropdown({
        action: 'hide'
    });

    $('.ui.dropdown.maptype').dropdown({
        action: 'hide',
        onChange: function(value, text, $selectedItem) {
            ResizeMenuBar();
            APP.Maps.ResizeCanvas();
            APP.Maps.SetMapType(value);
            if (APP.currentlySelectedChannel == "")
            {
                APP.Maps.RecentreMap();
            }
            else
            {
                APP.ChannelList_UI.LoadGeobotsOnMap(APP.currentlySelectedChannel);
            }
        }
    });
    $('.ui.dropdown.recentre').dropdown({
        action: 'hide',
        onChange: function(value, text, $selectedItem) {
            switch (value) {
                case "channel" :
                    APP.Maps.CentreMapOnPoints();
                    break;
                case "location" :
                    APP.Maps.RecentreMap();
            }
        }
    });
    $('.ui.dropdown.avatar').dropdown({
        action: 'hide',
        onChange: function(value, text, $selectedItem) {
            switch (value)
            {
                case "male" :
                    APP.Maps.SetCompanionImage("companion_male.png");
                    break;
                case "female" :
                    APP.Maps.SetCompanionImage("companion_female.png");
                    break;
            }
        }
    });
}


var ReloadChannels = function ()
{
    var sessionid = APP.companion.sessionid;
    var useremail = APP.companion.useremail;
    APP.companion = new IMERSIA.Companion();
    APP.companion.SetLogin(sessionid, useremail);
    APP.currentlySelectedGeobot = "";
    APP.currentlySelectedChannel = "";

    APP.ChannelList_UI.ClearOutput();
    APP.ExperienceList_UI.ClearOutput();

    $(".sidebar.left").sidebar("hide");
    $(".sidebar.right").sidebar("hide");

    APP.companion.Load();
    APP.Maps.ClearPointsFromMap();
    APP.Maps.SetMapType('com.mapbox.streets');

    setTimeout(function () {
        OpenAppropriateSidebars(false);
        APP.Companion_UI.Open();
    }, 1000);
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Controllers for the various menu item's visibility based on mode
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var ChooseTwoDee            = function () { $(".nottwodee").hide();     $(".twodee").show(); }
var ChooseThreeDee          = function () { $(".notthreedee").hide();   $(".threedee").show(); }
var ChooseGuidedMode        = function () { $(".notguidedmode").hide(); $(".guidedmode").show(); }
var ChooseExpertMode        = function () { $(".notexpertmode").hide(); $(".expertmode").show(); }
var ChooseStatsMode         = function () { $(".notstatsmode").hide();  $(".statsmode").show(); }
var ChoosePlaying           = function () { $(".notplaying").hide();    $(".playing").show(); }
var ChooseLoggedIn          = function () { $(".notloggedin").hide();   $(".loggedin").show(); }

var ChooseMobileMode        = function () { $(".notmobilemode").hide(); $(".mobilemode").show(); }
var ChooseDesktopMode       = function () { $(".mobilemode").hide();    $(".notmobilemode").show(); }

APP.SetUIItems = function ()
{
    if (APP.threedmode) { ChooseThreeDee(); } else { ChooseTwoDee(); };
    switch (APP.usermode) { case "guided" : ChooseGuidedMode(); break; case "expert" : ChooseExpertMode(); break; case "stats" : ChooseStatsMode(); break; };
    if (APP.playing) { ChoosePlaying(); };
    if (!APP.loggedin) { ChooseLoggedIn(); };
    if (APP.mobilemode) {ChooseMobileMode(); } else { ChooseDesktopMode(); };
}

var OpenAppropriateSidebars = function (dotoggle)
{
    if (APP.loggedin)
    {
        var closing = false;
        switch (APP.usermode)
        {
            case "guided":
                if (dotoggle) {
                    if (APP.ExperienceList_UI.IsOpen())
                    { APP.ExperienceList_UI.Close(); $(".sidebar.right").sidebar("hide"); closing=true;}
                    else { APP.ExperienceList_UI.Open(); }
                } else {
                    APP.ExperienceList_UI.Open();
                }
            break;
            case "expert":
                if (dotoggle) {
                    if (APP.ChannelList_UI.IsOpen())
                    { APP.ChannelList_UI.Close(); $(".sidebar.right").sidebar("hide"); closing=true;}
                    else { APP.ChannelList_UI.Open(); }
                } else {
                    APP.ChannelList_UI.Open();
                }
            break;
            case "analytics":
                if (dotoggle) {
                    if (APP.Analytics_UI.IsOpen())
                    { APP.Analytics_UI.Close(); $(".sidebar.right").sidebar("hide"); closing=true;}
                    else { APP.Analytics_UI.Open(); }
                } else {
                    APP.Analytics_UI.Open();
                }
            break;
        }

        if (closing)
        {
            APP.Companion_UI.Open();
        }
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
APP.Logout = function ()
{
    $(".sidebar.left").sidebar("hide");
    $(".sidebar.right").sidebar("hide");

    APP.loggedin = false;
    APP.currentlySelectedGeobot = "";
    APP.currentlySelectedChannel = "";

    APP.companion.Disconnect();
    APP.companion = new IMERSIA.Companion();
    APP.Maps.ClearPointsFromMap();

    $("#loginbuttonicon").removeClass("sign out alt").addClass("sign in alt");
    $("#login").attr("data-tooltip", "Log in");
    $("#login").removeClass("grey").addClass("blue");
    $("#createexperiencebutton").addClass("disabled");
    $("#uimobiledropdownmenu2icon").removeClass("green").addClass("grey");
    $("#mobileurlinputicon").removeClass("green").addClass("grey");

    APP.Maps.SetMapType('com.mapbox.light');
    APP.SetUIItems();
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var GoTwoDeeMode = function ()
{
    APP.threedmode = false;

    APP.SetUIItems();

    $("#twodeebutton").removeClass("basic");
    $("#threedeebutton").addClass("basic");
    APP.Maps.SetTwoD();
    ResizeMenuBar();
    APP.Maps.ResizeCanvas();
    APP.Maps.SetMapType();
    if (APP.currentlySelectedChannel == "")
    {
        APP.Maps.RecentreMap();
    }
    else
    {
        APP.ChannelList_UI.LoadGeobotsOnMap(APP.currentlySelectedChannel);
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var GoThreeDeeMode = function ()
{
    APP.threedmode = true;
    $("#threedeebutton").removeClass("basic");
    $("#twodeebutton").addClass("basic");
    APP.Maps.SetThreeD();
    ResizeMenuBar();
    APP.Maps.ResizeCanvas();
    APP.Maps.SetMapType();

    APP.SetUIItems();

    if (APP.currentlySelectedChannel == "")
    {
        APP.Maps.RecentreMap();
    }
    else
    {
        APP.ChannelList_UI.LoadGeobotsOnMap(APP.currentlySelectedChannel);
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var GoGuidedMode = function (opensidebar)
{
    opensidebar = (opensidebar==undefined)?true:opensidebar;
    APP.usermode = "guided";
    $("#guidedmodebutton").removeClass("basic");
    $("#expertmodebutton").addClass("basic");
    $("#analyticsmodebutton").addClass("basic");

    APP.SetUIItems();

    if (opensidebar) APP.ExperienceList_UI.Open();
    $(".modeindicator").css("border-color", "rgba(" + (APP.usermode=="expert" ? "255" : "100") + "," + (APP.usermode=="analytics" ? "255" : "100") + "," + (APP.usermode=="guided" ? "255" : "100") + ", 0.8)");
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var GoExpertMode = function ()
{
    APP.usermode = "expert";
    $("#guidedmodebutton").addClass("basic");
    $("#expertmodebutton").removeClass("basic");
    $("#analyticsmodebutton").addClass("basic");

    APP.SetUIItems();

    if (!APP.mobilemode) APP.ChannelList_UI.Open();
    $(".modeindicator").css("border-color", "rgba(" + (APP.usermode=="expert" ? "255" : "100") + "," + (APP.usermode=="analytics" ? "255" : "100") + "," + (APP.usermode=="guided" ? "255" : "100") + ", 0.8)");
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var GoAnalyticsMode = function ()
{
    APP.usermode = "analytics";
    $("#guidedmodebutton").addClass("basic");
    $("#expertmodebutton").addClass("basic");
    $("#analyticsmodebutton").removeClass("basic");

    APP.SetUIItems();

    if (!APP.mobilemode) APP.Analytics_UI.Open();
    $(".modeindicator").css("border-color", "rgba(" + (APP.usermode=="expert" ? "255" : "100") + "," + (APP.usermode=="analytics" ? "255" : "100") + "," + (APP.usermode=="guided" ? "255" : "100") + ", 0.8)");
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var GoPlayMode = function ()
{
    APP.playing = true;
    $("#stopbutton").removeClass("red");
    $("#stopbutton").addClass("grey");
    $("#playbutton").removeClass("grey");
    $("#playbutton").addClass("green");
    $("#playbutton").removeClass("basic");

    APP.SetUIItems();

    $(".sidebar.left").sidebar("hide");
    $(".sidebar.right").sidebar("hide");

    APP.Maps.ClearPointsFromMap();
    $("#portalui").removeClass("expanded");
    $(".modeindicator").css("border-color", "rgba(100,100,100,0.8)");
    if (APP.loggedin && (APP.currentlySelectedChannel != ""))
    {
        Object.keys(APP.companion.channels[APP.currentlySelectedChannel].geobots).forEach (function (geobotid) {
            APP.Maps.AddPointToMap(APP.companion.channels[APP.currentlySelectedChannel].geobots[geobotid]);
        });
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var GoEditMode = function ()
{
    APP.playing = false;

    $("#playbutton").removeClass("green");
    $("#playbutton").addClass("grey");
    $("#stopbutton").removeClass("grey");
    $("#stopbutton").addClass("red");
    $("#playbutton").addClass("basic");

    APP.SetUIItems();

    APP.Maps.ClearPointsFromMap();
    $("#portalui").addClass("expanded");
    $(".modeindicator").css("border-color", "rgba(" + (APP.usermode=="expert" ? "255" : "100") + "," + (APP.usermode=="analytics" ? "255" : "100") + "," + (APP.usermode=="guided" ? "255" : "100") + ", 0.8)");
    if (APP.loggedin && (APP.currentlySelectedChannel != ""))
    {
        Object.keys(APP.companion.channels[APP.currentlySelectedChannel].geobots).forEach (function (geobotid) {
            APP.Maps.AddPointToMap(APP.companion.channels[APP.currentlySelectedChannel].geobots[geobotid]);
        });
    }
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var GoMobileMode = function ()
{
    APP.mobilemode = true;
    APP.Logout();

    $(".sidebar.left").sidebar("hide");
    $(".sidebar.right").sidebar("hide");

    $("#portalui").addClass("expanded");
    $(".modeindicator").css("border-color", "rgba(100,100,100,0.8)");
    APP.SetUIItems();
    ResizeMenuBar();
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var GoDesktopMode = function ()
{
    APP.mobilemode = false;
    APP.Logout();

    $(".modeindicator").css("border-color", "rgba(" + (APP.usermode=="expert" ? "255" : "100") + "," + (APP.usermode=="analytics" ? "255" : "100") + "," + (APP.usermode=="guided" ? "255" : "100") + ", 0.8)");
    APP.SetUIItems();
    ResizeMenuBar();
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var ToggleFullscreen = function()
{
    if (((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen))) // || !isFullscreen())
    {
        // w2ui['mainportal_main_toolbar'].set('fullscreen', { text: 'Exit Fullscreen' });
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        // w2ui['mainportal_main_toolbar'].set('fullscreen', { text: 'Fullscreen' });
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    };
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// All the file loaders in the sub menus are managed by one fileloader element in the html file.  This function splits them up again.
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var SetUpFileLoaders = function()
{
    // The class of buttons for loading and changing files
    var selectedFileButtonType = "";
    $("body").on("click", ".fileloader", function ( e )
    {
        //e.preventDefault();
        selectedFileButtonType = e.target.id;
        if (e.target.name) {
            $('#hidden_fileinput').attr("accept", e.target.name);
        }
        else {
            $('#hidden_fileinput').attr("accept", "*");
        };
        $('#hidden_fileinput').trigger('click');
    });

    // After an image is selected, what to do to load it to the right place
    $('body').on('change', "#hidden_fileinput", function( e )
    {
        var reader = new FileReader();
        reader.onloadend = function( e ) {
            var fileInput = $("#hidden_fileinput");
            // var fileDetails = fileInput[0].files[0];
            var theFile = fileInput[0].files[0];

            if (selectedFileButtonType == "com_imersia_geobot_imagebutton")
            {
                var geobotid = $('#com_imersia_geobot_geobotid').val();
                APP.GeobotEdit_UI.UpdateImage ({geobotid:geobotid}, theFile, function (fileurl) {
                    if (fileurl === "")
                    {
                        APP.Companion_UI.AddErrorMessage("Error loading image to Geobot");
                    }
                    else
                    {
                        // Change the Image on the UI
                        $('#com_imersia_geobot_imageurl').val(fileurl);
                        $("#com_imersia_geobot_image").attr("src", reader.result);
                    }
                });
            }
            else if (selectedFileButtonType == 'com_imersia_channel_imagebutton')
            {
                var channelid = $('#com_imersia_channel_channelid').val();
                APP.ChannelEdit_UI.UpdateImage ({channelid: channelid}, theFile, function (fileurl) {
                    if (fileurl === "")
                    {
                        APP.Companion_UI.AddErrorMessage("Error loading image to Channel");
                    }
                    else
                    {
                        // Change the Image on the UI
                        $('#com_imersia_channel_imageurl').val(fileurl);
                        $("#com_imersia_channel_image").attr("src", reader.result);
                    }
                });
            }
            else if (selectedFileButtonType == "com_imersia_sprite_spritebutton")
            {
                var geobotid = $('#com_imersia_geobot_geobotid').val();
                APP.GeobotEdit_UI.UpdateSprite ({geobotid:geobotid}, theFile, function (fileurl) {
                    if (fileurl === "")
                    {
                        APP.Companion_UI.AddErrorMessage("Error loading image to Sprite");
                    }
                    else
                    {
                        // Change the Image on the UI
                        $('#com_imersia_sprite_imageurl').val(fileurl);
                        $("#com_imersia_sprite_sprite").attr("src", reader.result);
                    }
                });
            }
            else if (selectedFileButtonType == "com_imersia_geobot_file")
            {
                var geobotid = $('#com_imersia_geobot_geobotid').val();
                APP.SDK.PostFileToAPI(APP.companion.sessionid, 'files', {geobotid : geobotid}, theFile, function (result) {
                    if (result.hasOwnProperty("fileurls"))
                    {
                        APP.GeobotEdit_UI.UpdateFileTable();
                    };
                }, function (error) {
                });
            }
            else if (selectedFileButtonType == "com_imersia_channel_file")
            {
                var channelid = $('#com_imersia_channel_channelid').val();
                APP.SDK.PostFileToAPI(APP.companion.sessionid, 'files', {channelid : channelid}, theFile, function (result) {
                    if (result.hasOwnProperty("fileurls"))
                    {
                        APP.ChannelEdit_UI.UpdateFileTable();
                    };
                }, function (error) {
                });
            }
            else if (selectedFileButtonType == "com_imersia_audio_audiobutton")
            {
                var geobotid = $('#com_imersia_geobot_geobotid').val();
                APP.SDK.PostFileToAPI(APP.companion.sessionid, 'files', {geobotid : geobotid}, theFile, function (result) {
                    if (result.hasOwnProperty("fileurls"))
                    {
                        $('#com_imersia_audio_audiourl').val(result.fileurls[0]);
                    }
                    else
                    {
                        APP.Companion_UI.AddErrorMessage("Error loading Audio");
                    }
                }, function (error) {
                });
            }
            else if (selectedFileButtonType == "com_imersia_experiencegui_topbannerbutton")
            {
                var channelid = $('#com_imersia_channel_channelid').val();
                APP.ExperienceGUI_UI.UpdateMetadata ("topbannerurl", {channelid: channelid}, theFile, function (fileurl) {
                    if (fileurl === "")
                    {
                        APP.ExperienceGUI_UI.AddErrorMessage("Error loading image to UI");
                    }
                    else
                    {
                        // Change the Image on the UI
                        $('#com_imersia_experiencegui_topbannerurl').val(fileurl);
                        $("#com_imersia_experiencegui_topbannerimage").attr("src", fileurl);
                    }
                });
            }
            else if (selectedFileButtonType == "com_imersia_experiencegui_bottombannerbutton")
            {
                var channelid = $('#com_imersia_channel_channelid').val();
                APP.ExperienceGUI_UI.UpdateMetadata ("bottombannerurl", {channelid: channelid}, theFile, function (fileurl) {
                    if (fileurl === "")
                    {
                        APP.ExperienceGUI_UI.AddErrorMessage("Error loading image to UI");
                    }
                    else
                    {
                        // Change the Image on the UI
                        $('#com_imersia_experiencegui_bottombannerurl').val(fileurl);
                        $("#com_imersia_experiencegui_bottombannerimage").attr("src", fileurl);
                    }
                });
            }
            else if (selectedFileButtonType == "com_imersia_experienceguimap_mapiconbutton")
            {
                var channelid = $('#com_imersia_channel_channelid').val();
                APP.ExperienceGUI_UI.UpdateMetadata ("mapiconurl", {channelid: channelid}, theFile, function (fileurl) {
                    if (fileurl === "")
                    {
                        APP.ExperienceGUI_UI.AddErrorMessage("Error loading map icon to UI");
                    }
                    else
                    {
                        // Change the Image on the UI
                        $('#com_imersia_experienceguimap_mapiconurl').val(fileurl);
                        $("#com_imersia_experienceguimap_mapiconimage").attr("src", fileurl);
                    }
                });
            }

            // Clear the File input value to ensure it changes even if same file chosen next time.
            $("#hidden_fileinput").val("");
        }
        reader.readAsDataURL(this.files[0]);
    });
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var isFullscreen = function(){

  if($.browser.opera){

    var fs=$('<div class="fullscreen"></div>');
    $('body').append(fs);

    var check=fs.css('display')=='block';
    fs.remove();

    return check;
  }

  var st=screen.top || screen.availTop || window.screenTop;

  if(st!=window.screenY){

    return false;
  }

  return window.fullScreen==true || screen.height-document.documentElement.clientHeight<=30;
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Adjust the menu bar size as the window size changes.
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var ResizeMenuBar = function()
{
    var w = $(window).width();
    var mobilesidemenuswidth = $("#uimobiledropdownmenu1").outerWidth() + $("#reloadexperiencediv").outerWidth() + $("#createexperiencediv").outerWidth() + $("#uimobiledropdownmenu2").outerWidth();
    $("#mobileurlinputdiv").css("width", w - mobilesidemenuswidth);
}
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
