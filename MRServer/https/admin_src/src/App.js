// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// The main App for the Imersia Admin Portal
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { } from 'semantic-ui-react'

import Main  from './Main.js';
import Login from './Login.js';
import GeobotViewer from './GeobotViewer.js';
import ImersiaSDK from './ImersiaSDK.js';
import MetaTags from 'react-meta-tags';

// import './App.css';

// --------------------------------------------------------------------------------------------------------
// The main App for the experience.
// --------------------------------------------------------------------------------------------------------
class App extends Component
{
    // ----------------------------------------------------------------------------------------------------
    // Construct the App object
    // ----------------------------------------------------------------------------------------------------
    constructor (props)
    {
        super (props);

        this.state = {
            loggedin: false,
            entityid: {
                checking: true,
                exists : false,
                id : ""
            },
            userid: ""
        };

        this.imersiasdk = new ImersiaSDK();
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    componentDidMount = () =>
    {
        this.getEntityID();
        document.addEventListener("updategps", this.processWotchaEvent);
    }
    // ----------------------------------------------------------------------------------------------------
    componentWillUnmount = () => {
        document.removeEventListener("updategps", this.processWotchaEvent);
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    processWotchaEvent = (message) => {
        if (message.detail)
        {
            if (!message.detail.connected && this.state.loggedin)
            {
                this.logout();
            }
        }
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    getEntityID = () =>
    {
        var url = new URL(window.location.href);
        var entityid = url.searchParams.get("id");

        // let pathnames = window.location.pathname.split("/");
        // let entityid = (pathnames.length > 0) ? pathnames[pathnames.length-1] : "";
        if (entityid !== "")
        {
            this.imersiasdk.HEAD("geobots", {geobotid:entityid})
            .then ((resp) => {
                this.setState({entityid:{id:entityid, checking: false, exists:true}});
            })
            .catch ((err) => {
                this.setState({entityid:{id:"", checking: false, exists:false}});
            });
        }
        else
        {
            this.setState({entityid:{id:"", checking: false, exists:false}});
        }
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Set the result of logging in
    // ----------------------------------------------------------------------------------------------------
    setLoggedIn = (value) =>
    {
        this.setState({loggedin:value});
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Initiate logging in, for content viewers and editors that need to be logged in before proceeding.
    // ----------------------------------------------------------------------------------------------------
    doLogin = () =>
    {
        this.setState({loggedin:false});
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Logout
    // ----------------------------------------------------------------------------------------------------
    logout = () =>
    {
        this.setState({loggedin: false});
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    metatags = () =>
    {
        return (
            <MetaTags>
                <meta name = "viewport" content = "initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width"/>
                <meta name = "apple-mobile-web-app-capable" content = "yes"/>
                <meta name = "apple-touch-fullscreen" content = "yes"/>
                <meta name = "mobile-web-app-capable" content = "yes"/>
                <meta name = "apple-mobile-web-app-status-bar-style" content="black-translucent"/>
                <meta name = "apple-mobile-web-app-title" content="Admin Portal"/>
            </MetaTags>
        )
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // The main render loop
    // ----------------------------------------------------------------------------------------------------
    render ()
    {
        if (this.state.entityid.checking)
        {
            return(
                <>
                    {this.metatags()}
                    Loading ...
                </>);
        }
        else {
            if (this.state.entityid.exists)
            {
                if (this.state.entityid.id !== "")
                {
                    // Load the Geobot with this ID
                    return (
                        <>
                            {this.metatags()}
                            <GeobotViewer
                                imersiasdk={this.imersiasdk}
                                geobotid={this.state.entityid.id}
                            />
                        </>
                    );
                }
            }
            else
            {
                if (this.state.loggedin)
                {
                    return (
                        <>
                            {this.metatags()}
                            <Main
                                imersiasdk={this.imersiasdk}
                                logout={this.logout}
                                doLogin={this.doLogin}
                            />
                        </>
                    )
                }
                else
                {
                    return (
                        <>
                            {this.metatags()}
                            <Login
                                imersiasdk={this.imersiasdk}
                                loginMessage="Please log in."
                                setLoggedIn={this.setLoggedIn}
                                loginIfRemembered={false}
                            />
                        </>
                    )
                }
            }
        }
    }
    // ----------------------------------------------------------------------------------------------------
}
// --------------------------------------------------------------------------------------------------------

export default App;
