// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// The Content viewer and manager for Admins
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Icon, Header, Button, Card } from 'semantic-ui-react'

import Users from './Users.js';
import Channels from './Channels.js';
import Geobots from './Geobots.js';
import ChannelTitle from './ChannelTitle.js';
import Geobot from './Geobot.js';
import GeobotTitle from './GeobotTitle.js';
import Map from './Map.js';


// --------------------------------------------------------------------------------------------------------
// The main class
// --------------------------------------------------------------------------------------------------------
class AdminContentViewer extends Component {
    // ----------------------------------------------------------------------------------------------------
    // Set up the object
    // props expected are sessionid and userid
    // ----------------------------------------------------------------------------------------------------
    constructor (props)
    {
        super (props);

        this.container = React.createRef();

        this.state = {
            currentstate: "none",
            showingmap: false,
            maplocation: "xbpbpbpbp",
            map_style: {
                    position:"absolute",
                    top:"120px",
                    bottom:"11px",
                    left:"11px",
                    right:"11px"
                }
        }

        this.data = {
            currentuser: {},
            currentchannel: {},
            currentgeobot: {},
            users: [],
            channels: [],
            geobots: []
        };
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // What to start with when the page loads
    // ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
    {
        // document.addEventListener("channel_new", this.processWotchaEvent);
        this.loadUsers();
    }
    componentWillUnmount = () =>
    {
        // document.removeEventListener("channel_new", this.processWotchaEvent);
        // this.closeChannels ();
    }
	// ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    closeChannels = () =>
    {
        this.data.channels.forEach((channel) => {
            // this.props.imersiasdk.CLOSE (channel.channelid);
            document.removeEventListener(channel.channelid, this.processWotchaEvent);
        });
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Load each user's details
    // ----------------------------------------------------------------------------------------------------
    loadUsers = () =>
    {
        this.props.imersiasdk.GET("admin", {command:"users", parameters:{}})
        .then ((resp) => {
            this.data.users = resp.json;
            this.setState({currentstate:"reload_users"});
        })
        .catch((err) => {
            // Do nothing
        });
    }
    // ----------------------------------------------------------------------------------------------------




    // ----------------------------------------------------------------------------------------------------
    // Load each channel's details and set up the wotcha events
    // ----------------------------------------------------------------------------------------------------
    loadChannels = () =>
    {
        this.props.imersiasdk.GET("admin", {command:"channels", parameters:{"userid":this.state.currentuser.userid}})
        .then ((resp) => {
            this.data.channels = resp.json;
            this.data.channels.forEach((channel) => {
                // Make sure our Companion is wotching these channels
                this.props.imersiasdk.OPEN (channel.channelid);
                // Listen for the relevant channel events
                document.addEventListener(channel.channelid, this.processWotchaEvent);
            });
            this.setState({currentstate:"reload_channels"});
        })
        .catch((err) => {
            // Do nothing
        });
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    loadGeobots = () =>
    {
        this.props.imersiasdk.GET("geobots", {channelid:this.data.currentchannel.channelid})
        .then ((resp) => {
            this.data.geobots = resp.json;
            this.setState({currentstate: "reload_geobots"});
        })
        .catch((err) => {
            // Do nothing
        });
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Note when showing a list of channels or geobots when a new one is created, or one is deleted
    // and go back a step to the channel when the geobot currently being viewed is deleted.
    // ----------------------------------------------------------------------------------------------------
    processWotchaEvent = (message) =>
    {
        switch (this.state.currentstate)
        {
            case "channels":
                if (message.detail.wotcha.hasOwnProperty("channel_delete"))
                {
                    this.closeChannels ();
                    this.loadChannels ();
                }
                else if (message.detail.wotcha.hasOwnProperty("channel_new"))
                {
                    this.closeChannels ();
                    this.loadChannels ();
                }
            break;
            case "geobots":
            case "geobot":
                if (message.detail.wotcha.hasOwnProperty("channel_delete"))
                {
                    // If the channel we are currently viewing has just been deleted
                    if (message.detail.wotcha.channel_delete.channelid === this.data.currentchannel.channelid)
                    {
                        this.closeChannels ();
                        this.loadChannels ();
                    }
                }
                else if (!this.state.showingmap)
                {
                    if (message.detail.wotcha.hasOwnProperty("geobot_delete"))
                    {
                        this.loadGeobots ();
                    }
                    else if (message.detail.wotcha.hasOwnProperty("geobot_new"))
                    {
                        this.loadGeobots ();
                    }
                    else if (message.detail.wotcha.hasOwnProperty("geobot_set"))
                    {
                        if (this.data.currentgeobot.geobotid === message.detail.wotcha.geobot_set.geobotid)
                        {
                            this.data.currentgeobot = message.detail.wotcha.geobot_set;
                        }
                    }
                }
            break;
            default:
            break;
        }
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // where to go back to with the back button
    // ----------------------------------------------------------------------------------------------------
    goback = () =>
    {
        switch (this.state.currentstate)
        {
            case "geobots":
                this.closeChannels ();
                this.loadChannels ();
            break;
            case "geobot":
                this.loadGeobots ();
            break;
            default:
            break;
        }
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Open a list of geobots from a list of channels
    // ----------------------------------------------------------------------------------------------------
    openChannel = (chosenchannel) =>
    {
        this.props.imersiasdk.GET("geobots", {channelid:chosenchannel.channelid})
        .then ((resp) => {
            this.data.currentchannel = chosenchannel;
            this.data.geobots = resp.json;
            document.addEventListener(this.data.currentchannel.channelid, this.processWotchaEvent);
            this.setState({currentstate: "geobots"});
        })
        .catch((err) => {
            // Do nothing
        });
    }
    closeChannel = () =>
    {
        this.closeChannels ();
        this.loadChannels ();
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Show a set of Channels
    // ----------------------------------------------------------------------------------------------------
    channelsContent = () =>
    {
        return (
            <>
                <Header style={{height:"50px"}}>
                    <span><Icon link large name='home' /></span>
                    <Button.Group floated='right'>
                        <Button basic color="green" onClick={this.newChannel} >
                            <Icon name='magic'/> new
                        </Button>
                    </Button.Group>
                </Header>
                <Channels
                    imersiasdk={this.props.imersiasdk}
                    channels={this.data.channels}
                    openChannel={this.openChannel}
                    closeChannel={this.closeChannel}
                />
            </>
        )
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Show a set of users
    // ----------------------------------------------------------------------------------------------------
    usersContent = () =>
    {
        return (
            <>
                <Header style={{height:"50px"}}>
                    <span><Icon link large name='home' /></span>
                    {/*} <Button.Group floated='right'>
                        <Button basic color="green" onClick={this.newChannel} >
                            <Icon name='magic'/> new
                        </Button>
                    </Button.Group> */}
                </Header>
                <Users
                    imersiasdk={this.props.imersiasdk}
                    users={this.data.users}
                    openUser={this.openUser}
                />
            </>
        )
    }
    // ----------------------------------------------------------------------------------------------------




    // ----------------------------------------------------------------------------------------------------
    // Open a Geobot when clicked on in the GeobotsContent set
    // ----------------------------------------------------------------------------------------------------
    openGeobot = (chosengeobot) =>
    {
        this.props.imersiasdk.GET("geobots/automations", {geobotid:chosengeobot.geobotid})
        .then ((resp) => {
            this.data.currentgeobot = chosengeobot;
            this.data.currentgeobot_automations = resp.json;
            this.setState({currentstate: "geobot"});
        })
        .catch ((err) => {
            // Do nothing
        });
    }
    closeGeobot = () =>
    {
        this.loadGeobots ();
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    newChannel = () =>
    {
        let channelName = "Channel_" + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);

        this.props.imersiasdk.POST("channels", {}, {name:channelName})
        .then ((resp) => {

        })
        .catch((err) => {
            // Do nothing
        });
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    newGeobot = () =>
    {
        let geobotName = "Geobot_" + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        let headers = {
            channelid:this.data.currentchannel.channelid
        }
        if (this.state.showingmap)
        {
            headers.location = this.state.maplocation;
        }

        this.props.imersiasdk.POST("geobots", headers, {name:geobotName})
        .then ((resp) => {

        })
        .catch((err) => {
            // Do nothing
        });
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    toggleMap = (e) =>
    {
        // Reload geobots if we're going back from map view in case there were some changes
        if (this.state.showingmap) { this.loadGeobots(); }

        // Change the state
        this.setState({showingmap:!this.state.showingmap});
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Store the map center from the Map object
    // ----------------------------------------------------------------------------------------------------
    mapcenter = (newmaplocation) =>
    {
        this.setState({maplocation:newmaplocation});
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Show a set of Geobots either in the list or on the map
    // ----------------------------------------------------------------------------------------------------
    geobotsOrMap = () =>
    {
        if (this.state.showingmap)
        {
            // A little delay to allow the parentnode to draw so we can get its dimensions for the map
            setTimeout(() => {
                let rect = this.container.current.parentNode.getBoundingClientRect();
                this.setState({map_style: {
                        position:"absolute",
                        top:"120px",
                        bottom:"11px",
                        left:rect.left,
                        right:"11px"
                    }
                });
            }, 100);

            return (
                <div style={this.state.map_style}>
                    <Map
                        imersiasdk={this.props.imersiasdk}
                        channelid={this.data.currentchannel.channelid}
                        geobots={this.data.geobots}
                        mapcenter={this.mapcenter}
                    />
                </div>
            )
        }
        else
        {
            return (
                <Geobots
                    imersiasdk={this.props.imersiasdk}
                    geobots={this.data.geobots}
                    currentchannel={this.data.currentchannel}
                    openGeobot={this.openGeobot}
                    closeGeobot={this.closeGeobot}
                />
            )
        }
    }

    geobotsContent = () =>
    {
        return (
            <>
                <Header style={{height:"50px"}}>
                    <span><Icon link large name='arrow left' onClick={this.goback} /></span>
                    <ChannelTitle
                        imersiasdk={this.props.imersiasdk}
                        channelid={this.data.currentchannel.channelid}
                    />
                    <Button.Group floated='right'>
                    {
                        this.state.showingmap
                        ?<>
                            <Button value={false} basic color="blue" onClick={this.toggleMap }>
                                <Icon name='picture' /> list
                            </Button>
                            <Button value={true} color="blue" onClick={this.toggleMap }>
                                <Icon name='map' /> map
                            </Button>
                        </>:<>
                            <Button value={false} color="blue" onClick={this.toggleMap }>
                                <Icon name='picture' /> list
                            </Button>
                            <Button value={true} basic color="blue" onClick={this.toggleMap }>
                                <Icon name='map' /> map
                            </Button>
                        </>
                    }
                    <Button basic color="green" floated="right" onClick={this.newGeobot} >
                        <Icon name='magic'/> new
                    </Button>
                    </Button.Group>
                </Header>
                {this.geobotsOrMap()}
            </>
        )
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Show a single Geobot
    // ----------------------------------------------------------------------------------------------------
    geobotContent = () =>
    {
        return (
            <>
                <Header style={{height:"50px"}}>
                    <span><Icon link large name='arrow left' onClick={this.goback} /></span>
                    <ChannelTitle
                        imersiasdk={this.props.imersiasdk}
                        channelid={this.data.currentchannel.channelid}
                    />
                    <span> | </span>
                    <GeobotTitle
                        imersiasdk={this.props.imersiasdk}
                        geobotid={this.data.currentgeobot.geobotid}
                    />
                </Header>
                <Card.Group centered itemsPerRow={3} stackable doubling>
                    <Geobot
                        imersiasdk={this.props.imersiasdk}
                        currentchannel={this.data.currentchannel}
                        geobotdetails={this.data.currentgeobot}
                        automations={this.data.currentgeobot_automations}
                        doLogin={this.props.doLogin}
                    />
                </Card.Group>
            </>
        )
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Makes sure the content tab is rendering the correct output depending on the current state
    // ----------------------------------------------------------------------------------------------------
    showCorrectContent = () =>
    {
        let returnvalue = "";
        switch (this.state.currentstate)
        {
            case "users":
                returnvalue = (this.usersContent());
            break;
            case "reload_users":
                returnvalue = (<></>);
                this.setState({currentstate: "users"});
            break;
            case "channels":
                returnvalue = (this.channelsContent());
            break;
            case "reload_channels":
                returnvalue = (<></>);
                this.setState({currentstate: "channels"});
            break;
            case "geobots":
                returnvalue = (this.geobotsContent());
            break;
            case "reload_geobots":
                returnvalue = (<></>);
                this.setState({currentstate: "geobots"});
            break;
            case "geobot":
                returnvalue = (this.geobotContent());
            break;
            default:
            break;
        }
        return returnvalue;
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // The main render loop
    // ----------------------------------------------------------------------------------------------------
    render ()
    {
        return (
            <div ref={this.container}>{this.showCorrectContent()}</div>
        );
    }
};
// --------------------------------------------------------------------------------------------------------


export default AdminContentViewer;
