// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// A simple file manager for Channels, Geobots and Users
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Table, Button, Icon } from 'semantic-ui-react';
import ImageExtensions from './patterns/ImageExtensions.json';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import ImageImersia from './ImageImersia.js';

import Dropzone from 'react-dropzone';
import noimage from './images/noimage.png';


// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class FileManager extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.styles = {
			dropzone: {
				display:'flex',
				'justify-content': 'center',
				'align-items': 'center',
				height:"100px",
				'border-style': 'dashed',
				'border-color': 'grey',
				'border-width': "1px",
				'border-radius': "5px"
			}
		}

		this.state = {
			files: [],
			clipboardMessage: "Copy filename to clipboard."
		};
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		this.props.imersiasdk.GET("files", this.props.entityid)
		.then ((resp) => {
			this.setState({files: resp.json.fileurls});
		})
		.catch ((err) => {
			// Do nothing
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentWillUnmount = () =>
	{
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	imageOrIcon = (filename) =>
	{
		let extension = filename.split('.').pop();
		if (ImageExtensions.indexOf(extension) > -1)
		{
			return (
				<ImageImersia src={filename} default={noimage} imersiasdk={this.props.imersiasdk} size="mini" centered/>
			)
		}
		else {
			return (
				<></>
			)
		}
	}
	// ----------------------------------------------------------------------------------------------------




	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	deleteFile = (e, data) =>
	{
		let filename = this.state.files[data.index].split('/').pop();
		let headers = this.props.entityid;
		headers.filename = filename;
		this.props.imersiasdk.DELETE("files", headers)
		.then ((resp) => {
			this.props.imersiasdk.GET("files", this.props.entityid)
			.then ((resp) => {
				this.setState({files: resp.json.fileurls});
			})
			.catch ((err) => {
				// Do nothing
			});
		})
		.catch ((err) => {
			// Do nothing
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Upload files and set the parameter
	// ----------------------------------------------------------------------------------------------------
	onDrop = (acceptedFiles, rejectedFiles, e) => {
		// Do something with files
		this.props.imersiasdk.POSTFILES(this.props.entityid, acceptedFiles)
		.then ((resp) => {
			this.props.imersiasdk.GET("files", this.props.entityid)
			.then ((resp) => {
				this.setState({files: resp.json.fileurls});
			})
			.catch ((err) => {
				// Do nothing
			});
		})
		.catch ((err) => {
			// Do nothing
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	changeMessage = () => {
		this.setState({clipboardMessage:"Filename copied."})
		setTimeout(() => {
			this.setState({clipboardMessage:"Copy filename to clipboard."});
		}, 1000);
	}
	// ----------------------------------------------------------------------------------------------------




	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	fileList = () =>
	{
		let metadatahtml = [];
		this.state.files.forEach ((item, i) => {
			metadatahtml.push (
				<Table.Row>
					<Table.Cell textAlign='center' width={1}>
						<Button index={i} icon color="red" basic onClick={this.deleteFile}><Icon name="delete"/></Button>
					</Table.Cell>
					<Table.Cell textAlign='center' width={2}>
						{this.imageOrIcon(item)}
					</Table.Cell>
					<Table.Cell width={15}>
						<a rel="noopener noreferrer" target="_blank" href={"https://" + window.location.host + item}>{item}</a>
						<CopyToClipboard index={i} text={item} onCopy={this.changeMessage}>
							<Button basic floated="right" circular icon="copy" />
						</CopyToClipboard>
					</Table.Cell>
				</Table.Row>
			)
		});
		return (metadatahtml);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		return (
			<>
				<Dropzone name="fileloader" onDrop={this.onDrop}>
					{({ getRootProps, getInputProps }) => (
						<div style={this.styles.dropzone} {...getRootProps({ refKey: 'innerRef' })}>
							<input {...getInputProps()} />
							<p>Click or drop files here to Upload.</p>
						</div>
					)}
				</Dropzone>
				<Table celled unstackable>
					<Table.Header>
						<Table.HeaderCell textAlign='center' width={1}>
						</Table.HeaderCell>
						<Table.HeaderCell textAlign='center' width={2}>Icon</Table.HeaderCell>
						<Table.HeaderCell width={15}>Filename</Table.HeaderCell>
					</Table.Header>
					<Table.Body>
						{this.fileList ()}
					</Table.Body>
				</Table>
			</>
		);
	}
};
// --------------------------------------------------------------------------------------------------------

export default FileManager;
