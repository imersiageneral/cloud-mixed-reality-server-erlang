// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// The Login dialog box.  Returns either an empty sessionid if there was an error,or the sessionid after
// logging in successfully.
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import Register from './Register.js';
import ChangePassword from './ChangePassword.js';
import { Segment, Form, Checkbox, Divider, Grid, Button, Modal, Image, Header } from 'semantic-ui-react';
import cookie from 'react-cookies'

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class Login extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		let loadfromcookie = cookie.load('rememberme') === "checked";

		this.state = {
			open : true,
			useremail : (loadfromcookie)?cookie.load('useremail'):"",
			password : (loadfromcookie)?cookie.load('password'):"",
			rememberme: (loadfromcookie)?cookie.load('rememberme'):"",
			passcode: "",
			loginMessage : this.props.loginMessage,
			registering: false,
			changingpassword: false,
			login_mode : "password",
			login_stage : 1
		};

		this.styles = {
			actionStyle : { paddingLeft: '1.5em', paddingRight: '1.5em' }
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		let loadfromcookie = cookie.load('rememberme') === "checked";

		if ((this.props.loginIfRemembered) && (loadfromcookie))
		{
			this.doLogin();
		}
		else {
			this.props.imersiasdk.GET("vars", {"category":"authentication", "key":"login"})
			.then((resp) => {
				if (resp.json.value === "passcode")
				{
					this.setState({login_mode:"passcode", login_stage:1});
				}
				else if (resp.json.value === "twofactor")
				{
					this.setState({login_mode:"twofactor", login_stage:1});
				}
				else
				{
					this.setState({login_mode:"password", login_stage:1});
				}
			})
			.catch((err) => {
				this.setState({login_mode:"password", login_stage:1});
			});
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Perform a login and either close the dialog or show a message, depending on the outcome.
	// ----------------------------------------------------------------------------------------------------
	doLogin = () => {
		if (this.state.rememberme === "checked")
		{
			cookie.save("useremail", this.state.useremail, {maxAge:31536000}); // 1 year
			cookie.save("password", this.state.password, {maxAge:31536000});
			cookie.save("rememberme", this.state.rememberme, {maxAge:31536000});
		}
		else
		{
			cookie.remove("useremail");
			cookie.remove("password");
			cookie.remove("rememberme");
		}

		this.props.imersiasdk.LOGIN(this.state.useremail, this.state.password, this.state.passcode.toUpperCase())
		.then ((resp) => {
			this.props.setLoggedIn(true);
		}).catch ((err) => {
			switch (err.json.error)
			{
				case "password":
					this.setState({loginMessage:"Incorrect password.", login_stage: 1});
					break;
				case "invalid":
					this.setState({loginMessage:"Invalid passcode - make sure you use the latest."});
					break;
				case "timeout":
					this.setState({loginMessage:"Passcode timed out - request a new one."});
					break;
				case "mumtries":
					this.setState({loginMessage:"Too many attempts - request a new passcode."});
					break;
				case "used":
					this.setState({loginMessage:"Passcode already used - request a new one."});
					break;
				case "database":
				case "smtp":
					this.setState({loginMessage:"MR Server error - contact your administrator."});
					break;
				default:
					this.setState({loginMessage:"Something went wrong - try requesting a new passcode."});
					break;
			}
			this.props.setLoggedIn(false);
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	getPasscode = () => {
		this.props.imersiasdk.GET("passcodes", {"useremail":this.state.useremail})
		.then((resp) => {
			this.setState({login_stage:2, passcode: ""});
		})
		.catch((err) => {
			this.setState({login_stage:1, passcode: ""});
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	doRegister = () => {
		this.setState({registering:true, login_stage:1});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	doNewPassword = () => {
		this.setState({changingpassword:true, login_stage:1});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	doneRegistering = () => {
		this.setState({registering:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	doneChanging = () => {
		this.setState({changingpassword:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Set the local variables when the input fields are changed, ready for when the user presses Login
	// ----------------------------------------------------------------------------------------------------
	handleChange = (e, { name, value, checked }) => {
		if (name === "rememberme")
		{
			this.setState({rememberme:(checked)?"checked":"notchecked"});
			if (!checked)
			{
				this.setState({useremail:"", password:""});
				cookie.remove("useremail");
				cookie.remove("password");
				cookie.remove("rememberme");
			}
		}
		else
		{
			this.setState({ [name]: value })
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Check for the enter key
	// ----------------------------------------------------------------------------------------------------
	handleKeyPress = (e) => { if (e.charCode === 13) { this.doLogin(e); } }
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	login_dialogbox = () =>
	{
		return (
			<Modal open={this.state.open} size='tiny' >
				<Modal.Content>
					<Image fluid size='medium' src='/images/logo.png' centered />
					<Header as='h4' textAlign='center'>
						<span>{this.state.loginMessage}</span>
					</Header>
					<Divider />
					<Grid columns='one'>
						<Grid.Row>
							<Grid.Column>
								<Form>
									{ this.login_inputs() }
								</Form>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Modal.Content>
				<Modal.Actions style={this.styles.actionStyle}>
					{ this.login_buttons() }
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------
	login_inputs = () =>
	{
		switch (this.state.login_stage)
		{
			case 1: return (
				<>
					<Form.Field>
						<Form.Input placeholder="Email Address" name="useremail" type="text" value={this.state.useremail} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
					</Form.Field>
					{ this.show_password_or_not() }
					<Form.Field>
						<Checkbox label='Remember me' name="rememberme" defaultChecked={this.state.rememberme === "checked"} onChange={this.handleChange}/>
					</Form.Field>
					<Segment secondary mini textAlign="center">
						We use browser cookies to remember your settings for this site.  If you don't wish us to do that, do NOT click 'remember me'.
					</Segment>
				</>
			);
			break;
			case 2: return (
				<>
					<Segment secondary mini textAlign="center">
						A passcode has now been sent to your email address.  Please check for this and enter it here.
					</Segment>
					<Form.Field>
						<Form.Input placeholder="Passcode" name="passcode" type="text" value={this.state.passcode} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
					</Form.Field>
				</>
			);
			break;
			default: return (
				<></>
			);
		}
	}
	// ----------------------------------------------------------------------------------------------------
	show_password_or_not = () =>
	{
		if (this.state.login_mode !== "passcode")
		{
			return (
				<Form.Field>
					<Form.Input placeholder="Password" name="password" type="password" value={this.state.password} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
					<Button fluid basic onClick={this.doNewPassword}>I've forgotten my password ...</Button>
				</Form.Field>
			)
		}
		else {
			return (
				<></>
			)
		}
	}
	// ----------------------------------------------------------------------------------------------------
	login_buttons = () =>
	{
		if ((this.state.login_mode !== "passcode") && (this.state.login_mode !== "twofactor"))
		{
			return (
				<Grid columns='two'>
					<Grid.Row>
						<Grid.Column>
							<Button fluid basic color='blue' onClick={this.doRegister}>Register</Button>
						</Grid.Column>
						<Grid.Column>
							<Button fluid basic color='green' onClick={this.doLogin}>Log in</Button>
						</Grid.Column>
					</Grid.Row>
				</Grid>
			)
		}
		switch (this.state.login_stage)
		{
			case 1: return (
				<Grid columns='two'>
					<Grid.Row>
						<Grid.Column>
							<Button fluid basic color='blue' onClick={this.doRegister}>Register</Button>
						</Grid.Column>
						<Grid.Column>
							<Button fluid basic color='green' onClick={this.getPasscode}>Get Passcode</Button>
						</Grid.Column>
					</Grid.Row>
				</Grid>
			);
			break;
			case 2: return (
				<Grid columns='two'>
					<Grid.Row>
						<Grid.Column>
							<Button fluid basic color='orange' onClick={this.getPasscode}>Get New Passcode</Button>
						</Grid.Column>
						<Grid.Column>
							<Button fluid basic color='green' onClick={this.doLogin}>Log in</Button>
						</Grid.Column>
					</Grid.Row>
				</Grid>
			);
			break;
			default: return (
				<></>
			)
		}
	}
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		if (this.state.changingpassword)
		{
			return (
				<ChangePassword message="Change Password" useremail={this.state.useremail} doneChanging={this.doneChanging} imersiasdk={this.props.imersiasdk}/>
			)
		}
		else if (this.state.registering)
		{
			return (
				<Register message="Please register" doneRegistering={this.doneRegistering} imersiasdk={this.props.imersiasdk}/>
			)
		}
		else
		{
			return (this.login_dialogbox());
		}
	}
};
// --------------------------------------------------------------------------------------------------------

export default Login;
