// --------------------------------------------------------------------------------------------------------
// A dynamically updating channel title.
// --------------------------------------------------------------------------------------------------------
import React, { Component } from 'react';
import { } from 'semantic-ui-react'

class UserTokens extends Component {
    constructor (props)
    {
        super (props);
        this.state = {
            tokens : ""
        }
    }


    componentDidMount = () => {
        this.props.imersiasdk.GET("user/tokens", {userid: this.props.imersiasdk.userid})
        .then ((resp) => {
            this.setState({tokens:resp.json.tokens});
            document.addEventListener(this.props.imersiasdk.userid, this.processWotchaEvent);
        })
        .catch((err) => {
            this.setState({tokens:""});
            // Do nothing
        });
    }


    componentWillUnmount = () => {
        document.removeEventListener(this.props.imersiasdk.userid, this.processWotchaEvent);
    }


    processWotchaEvent = (message) =>
    {
		if (message.detail.wotcha.hasOwnProperty("user_tokens"))
		{
			this.setState({tokens: message.detail.wotcha.user_tokens.tokens});
		}
    }


    render ()
    {
        return (<span>{this.state.tokens}</span>);
    }
}
// --------------------------------------------------------------------------------------------------------


export default UserTokens;
