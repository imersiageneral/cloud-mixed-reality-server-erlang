// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draws a channel card given the details
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Modal, Card, Label, Icon, Menu, Button, Dropdown, Segment} from 'semantic-ui-react';
import JSONEditor from './JSONEditor.js';
import Channel from './patterns/Channel.json';
import MetadataEdit from './MetadataEdit.js';
import ContextsEdit from './ContextsEdit.js';
import FileManager from './FileManager.js';
import ImageImersia from './ImageImersia.js';

import noimage from './images/noimage.png';

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class ChannelCard extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			channeldetails: JSON.parse(JSON.stringify(this.props.channeldetails)),
			editedchanneldetails: {},
			channelmetadata:[],
			channelcontexts:[],
			editing: false,
			deleting: false,
			editingmetadata: false,
			editingcontexts: false,
			filemanager: false,
			error: false,
			error_message: ""
		};
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () => {
		document.addEventListener(this.props.channeldetails.channelid, this.processWotchaEvent);
    }
    componentWillUnmount = () => {
		document.removeEventListener(this.props.channeldetails.channelid, this.processWotchaEvent);
    }
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	processWotchaEvent = (message) =>
	{
		if (message.detail.wotcha.hasOwnProperty("channel_set"))
		{
			this.setState({channeldetails: message.detail.wotcha.channel_set});
		}
		else if (message.detail.wotcha.hasOwnProperty("channel_rename"))
		{
			this.setState({channeldetails: message.detail.wotcha.channel_rename});
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Pass back up to the channel list which channel was clicked on
	// ----------------------------------------------------------------------------------------------------
	openChannel = () =>
	{
		this.props.openChannel(this.state.channeldetails);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleEditClick = () =>
	{
		this.setState({editing:true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleMetadataClick = () =>
	{
		this.setState({editingmetadata:true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleContextsClick = () =>
	{
		this.setState({editingcontexts:true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	cancelMetadataEdit = () =>
	{
		this.setState({editingmetadata:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	updateMetadata = (newMetadata) =>
	{
		this.setState({channelmetadata:newMetadata});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	saveMetadata = () =>
	{
		let counter = this.state.channelmetadata.length;
		this.state.channelmetadata.forEach((metadata) => {
			if (metadata.deleted)
			{
				this.props.imersiasdk.DELETE("metadata", {channelid:this.state.channeldetails.channelid, metadataid:metadata.metadataid})
				.then ((resp) => {
					if (--counter <= 0) {this.setState({editingmetadata:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.setState({editingmetadata:false});}
				});
			}
			else if (metadata.new)
			{
				this.props.imersiasdk.POST("metadata", {channelid:this.state.channeldetails.channelid}, metadata)
				.then ((resp) => {
					if (--counter <= 0) {this.setState({editingmetadata:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.setState({editingmetadata:false});}
				});
			}
			else
			{
				this.props.imersiasdk.PUT("metadata", {channelid:this.state.channeldetails.channelid, metadataid:metadata.metadataid}, metadata)
				.then ((resp) => {
					if (--counter <= 0) {this.setState({editingmetadata:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.setState({editingmetadata:false});}
				});
			}
		})
	}
	// ----------------------------------------------------------------------------------------------------




	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	changeDetails = (newdetails) =>
	{
		this.setState({editedchanneldetails:JSON.parse(JSON.stringify(newdetails))});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	cancelEdit = () => {
		this.setState({editing:false});
	}

	saveEdit = () => {
		this.props.imersiasdk.PUT("channels", {channelid:this.state.editedchanneldetails.channelid}, this.state.editedchanneldetails)
		.then ((resp) => {
			this.setState({channeldetails:this.state.editedchanneldetails, editing:false});
		})
		.catch ((resp) => {
			if (resp.err === "name")
			{
				this.setState({error:true, error_message:"A Channel with that name already exists on this Domain."});
			}
			else {
				this.setState({error:true, error_message:"An error occurred saving the Channel."});
			}
		});
	}
	askDeleteChannel = () => {
		this.setState({deleting:true});
	}
	deleteChannel = () => {
		this.props.imersiasdk.DELETE("channels", {channelid:this.state.channeldetails.channelid})
		.then ((resp) => {
			this.props.closeChannel();
		})
		.catch((err) => {
			this.setState({deleting:false});
		});
	}
	cancelDelete = () => {
		this.setState({deleting:false});
	}
	errorOK = () => {
		this.setState({error:false})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	editModal = () =>
	{
		return (
			<Modal open={this.state.editing} centered={false}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Edit</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content scrolling>
					<JSONEditor
						imersiasdk={this.props.imersiasdk}
						pattern={Channel["com.imersia.channel"]}
						details={this.state.channeldetails}
						parameters={{channelid:this.state.channeldetails.channelid}}
						changeDetails={this.changeDetails}
					/>
		  		</Modal.Content>
				<Modal.Actions>
					<Button basic negative onClick={this.askDeleteChannel}>
						<Icon name='delete' /> Delete
					</Button>
					<Button basic color="blue" onClick={this.cancelEdit}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic positive onClick={this.saveEdit}>
						Save <Icon name='right chevron' />
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	askDeleteModal = () =>
	{
		return (
			<Modal size="tiny" open={this.state.deleting} centered={true}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Delete Channel</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content>
					Delete Channel and all its Geobots? - there is no UNDO.
				</Modal.Content>
				<Modal.Actions>
					<Button basic positive onClick={this.cancelDelete}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic negative onClick={this.deleteChannel}>
						<Icon name='cancel' /> DELETE
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	metadataModal = () =>
	{
		return (
			<Modal open={this.state.editingmetadata} centered={false} >
				<Modal.Header>
					Metadata
				</Modal.Header>
				<Modal.Content scrolling>
					<MetadataEdit
						imersiasdk={this.props.imersiasdk}
						entityid={{channelid:this.state.channeldetails.channelid}}
						updateMetadata={this.updateMetadata}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button basic color="blue" onClick={this.cancelMetadataEdit}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic positive onClick={this.saveMetadata}>
						<Icon name='save' /> Save
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	contextsModal = () =>
	{
		return (
			<Modal open={this.state.editingcontexts} centered={false} >
				<Modal.Header>
					Contexts
				</Modal.Header>
				<Modal.Content>
					<Segment secondary mini textAlign="justify">
						Channels can have attributes: read (to read details), add (to add geobots), remove (to remove geobots), list (to list geobots).
					</Segment>
				</Modal.Content>
				<Modal.Content scrolling>
					<ContextsEdit
						imersiasdk={this.props.imersiasdk}
						entityid={{channelid:this.state.channeldetails.channelid}}
						updateContexts={this.updateContexts}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button basic color="blue" onClick={this.cancelContextsEdit}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic positive onClick={this.saveContexts}>
						<Icon name='save' /> Save
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	cancelContextsEdit = () =>
	{
		this.setState({editingcontexts:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	updateContexts = (newContexts) =>
	{
		this.setState({channelcontexts:newContexts});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	saveContexts = () =>
	{
		let counter = this.state.channelcontexts.length;
		this.state.channelcontexts.forEach((context) => {
			if (context.deleted)
			{
				this.props.imersiasdk.DELETE("context", {channelid:this.state.channeldetails.channelid, contextid:context.contextid})
				.then ((resp) => {
					if (--counter <= 0) {this.setState({editingcontexts:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.setState({editingcontexts:false});}
				});
			}
			else if (context.new)
			{
				this.props.imersiasdk.POST("context", {channelid:this.state.channeldetails.channelid}, context)
				.then ((resp) => {
					if (--counter <= 0) {this.setState({editingcontexts:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.setState({editingcontexts:false});}
				});
			}
			else
			{
				this.props.imersiasdk.PUT("context", {channelid:this.state.channeldetails.channelid, contextid:context.contextid}, context)
				.then ((resp) => {
					if (--counter <= 0) {this.setState({editingcontexts:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.setState({editingcontexts:false});}
				});
			}
		})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleFilesClick = () =>
	{
		this.setState({filemanager:true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	closeFilesModal = () =>
	{
		this.setState({filemanager:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	filesModal = () =>
	{
		return (
			<Modal open={this.state.filemanager} centered={false} >
				<Modal.Header>
					Files
				</Modal.Header>
				<Modal.Content scrolling>
					<FileManager
						imersiasdk={this.props.imersiasdk}
						entityid={{channelid:this.state.channeldetails.channelid}}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button basic positive onClick={this.closeFilesModal}>
						<Icon name='check' /> Done
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	errorModal = () =>
	{
		return (
			<Modal size="tiny" open={this.state.error} centered={true}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Error</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content>
					{this.state.error_message}
				</Modal.Content>
				<Modal.Actions>
					<Button basic positive onClick={this.errorOK}>
						OK
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	mainBit = () =>
	{
		const imageurl = this.state.channeldetails.imageurl;
		const trigger = (
			<Button style={{left:"0px", "background-color": "white", "color": "grey", opacity: 0.8}} compact circular icon="ellipsis vertical"/>
		)
		const options = [
			{ value: 'edit', text: 'Edit Details', icon: 'edit' },
			{ value: 'metadata', text: 'Metadata', icon: 'list' },
			{ value: 'files', text: 'Files', icon: 'file alternate outline' },
			{ value: 'contexts', text: 'Contexts', icon: 'key' }
		]

		return (
			<>
				<Button
					basic compact
					onClick={this.openChannel}>
					<ImageImersia src={imageurl} default={noimage} imersiasdk={this.props.imersiasdk} fluid/>
				</Button>
				{
					this.state.channeldetails.ownerid === this.props.imersiasdk.userid ?
					(
						<div style={{ position: "absolute", top: "15px", left: "20px" }}>
							<Dropdown trigger={trigger} options={options} pointing='top left' icon={null} onChange={this.handleMenuChoice}/>
						</div>
					):(
						<></>
					)
				}
			</>
		)
	}
	handleMenuChoice = (e, data) =>
	{
		switch (data.value)
		{
			case "edit":
			this.handleEditClick ();
			break;
			case "metadata":
			this.handleMetadataClick ();
			break;
			case "files":
			this.handleFilesClick ();
			break;
			case "contexts":
			this.handleContextsClick ();
			break;
			default:
			break;
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		return (
			<Card style={{minHeight: "350px"}}>
				{this.mainBit()}
				<Card.Content textAlign="center">
					<Card.Header>{this.state.channeldetails.name}</Card.Header>
					<Card.Meta>{this.state.channeldetails.class}</Card.Meta>
					<Card.Description>{this.state.channeldetails.description}</Card.Description>
				</Card.Content>
				<Card.Content extra textAlign="center">
					<Label fluid>
						<Icon name={(this.state.channeldetails.hidden) ? "eye slash" : "eye"} />
						{(this.state.channeldetails.hidden) ? "hidden" : "visible"}
					</Label>
				</Card.Content>

				{this.state.editing ?			this.editModal() 		: <></>}
				{this.state.error ?				this.errorModal()		: <></>}
				{this.state.deleting ?			this.askDeleteModal()	: <></>}
				{this.state.editingmetadata ?	this.metadataModal()	: <></>}
				{this.state.filemanager ?		this.filesModal()		: <></>}
				{this.state.editingcontexts ?	this.contextsModal()	: <></>}
			</Card>
		)
	}
};
// --------------------------------------------------------------------------------------------------------

export default ChannelCard;
