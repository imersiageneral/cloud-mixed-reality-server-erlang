// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draws a Geobot card given the details
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Card, Image as SemImage, Button} from 'semantic-ui-react';
import Spritesheet from 'react-responsive-spritesheet';
import ReactAudioPlayer from 'react-audio-player';

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class Geobot extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.spriteimage = null;
		this.spritesheet = null;
		this.audioplayer = null;

		this.state = {
			geobotdetails: this.props.geobotdetails,
			automations: this.props.automations,
			classdetails: null,
			audiodetails: null,
			linkdetails: null,
			currentcommand : ""
		};
	}
	// ----------------------------------------------------------------------------------------------------


	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () => {
		document.addEventListener(this.props.geobotdetails.geobotid, this.processGeobotUpdate);

		this.loadMetadata ();
	}
	componentWillUnmount = () => {
		document.removeEventListener(this.props.geobotdetails.geobotid, this.processGeobotUpdate);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	processGeobotUpdate = (message) =>
	{
		if (message.detail.wotcha.hasOwnProperty("geobot_set"))
		{
			this.setState({geobotdetails: message.detail.wotcha.geobot_set});
		}
		else if (message.detail.wotcha.hasOwnProperty("geobot_automations"))
		{
			this.setState({automations:message.detail.wotcha.geobot_automations.automations});
		}
		else if (message.detail.wotcha.hasOwnProperty("trigger"))
		{
			this.actOnTrigger (message.detail.wotcha.trigger.event);
		}

		// Reload the class-related metadata
		this.loadMetadata ();
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	loadMetadata = () =>
	{
		this.props.imersiasdk.GET("metadata", {geobotid:this.state.geobotdetails.geobotid, key: this.state.geobotdetails.class})
		.then ((resp) => {
			if (this.state.geobotdetails.class === "com.imersia.sprite")
			{
				let image = new Image();
				image.onload = (() => {
					this.spriteimage = image;
					this.setState({classdetails: resp.json.value});
				});
				image.onerror = (() => {
					this.setState({classdetails: null});
				})
				image.src = resp.json.value.imageurl;
			};
		})
		.catch ((err) => {
			this.setState({classdetails: null});
		});

		this.props.imersiasdk.GET("metadata", {geobotid:this.state.geobotdetails.geobotid, key: "com.imersia.audio"})
		.then ((resp) => {
			this.setState({audiodetails: resp.json.value});
		})
		.catch ((err) => {
			this.setState({audiodetails: null});
		});

		this.props.imersiasdk.GET("metadata", {geobotid:this.state.geobotdetails.geobotid, key: "com.imersia.links"})
		.then ((resp) => {
			this.setState({linkdetails: resp.json.value});
		})
		.catch ((err) => {
			this.setState({linkdetails: null});
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	sendEvent = (e, details) =>
	{
		// {geobotid, command, parameters} = details;
		this.setState({currentcommand: details.command});
		this.props.imersiasdk.SEND(details.geobotid, details.command, details.parameters);
		setTimeout( () => {
			this.setState({currentcommand: ""});
		}, 1000);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	openLink = (e, details) =>
	{
		window.open(details.url);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	isSendingEvent = (command) =>
	{
		return (this.state.currentcommand === command);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	automationButtons = (automations) =>
	{
		var commands = [];
		automations.forEach((automation) => {
			automation.commands.forEach((command) => {
				if (commands.indexOf(command) === -1) {
					commands.push( command );
				}
			});
		});

		if (this.props.imersiasdk.loggedIn())
		{
			return (
				commands.map(
					command =>
					<Card.Content extra>
						<Button
							fluid basic
							geobotid={this.state.geobotdetails.geobotid}
							command={command}
							parameters={{}}
							loading={this.isSendingEvent(command)}
							onClick={this.sendEvent}>
								{command}
						</Button>
					</Card.Content>
				)
			);
		}
		else if (this.props.imersiasdk.loggingIn())
		{
			return (
				<Card.Content extra textAlign="center">
					<Button basic fluid onClick={this.props.doLogin}>Logging in...</Button>
				</Card.Content>
			);
		}
		else
		{
			return (
				<Card.Content extra textAlign="center">
					<Button basic fluid onClick={this.props.doLogin}>Log in</Button>
				</Card.Content>
			);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	linkButtons = (links) =>
	{
		if (links == null)
		{
			return (
				<></>
			)
		}
		else {
			return (
				links.map(
					link =>
					<Card.Content extra>
						<Button
							fluid basic
							url={link.url}
							parameters={{}}
							loading={this.isSendingEvent(link)}
							onClick={this.openLink}>
								{link.name}
						</Button>
					</Card.Content>
				)
			);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	actOnTrigger = (triggerevent) =>
	{
		if ((this.state.geobotdetails.class === "com.imersia.sprite") && (this.state.classdetails) && this.spritesheet)
		{
			if (this.state.classdetails.trigger === triggerevent)
			{
				this.spritesheet.goToAndPlay(1);
			}
		}
		if ((this.state.audiodetails) && this.audioplayer)
		{
			if (this.state.audiodetails.trigger === triggerevent)
			{
				this.audioplayer.audioEl.play();
				if (this.state.audiodetails.vibrate)
				{
					navigator.vibrate([300]);
				}
			}
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	imageOrSprite = () =>
	{
		var imageurl = this.state.geobotdetails.imageurl;
		if (imageurl === "") imageurl = 'images/noimage.png';

		if ((this.state.geobotdetails.class === "com.imersia.sprite") && (this.state.classdetails) && (this.spriteimage))
		{
			return (
				<Spritesheet
					image={this.state.classdetails.imageurl}
					widthFrame={this.spriteimage.width / this.state.classdetails.width}
					heightFrame={this.spriteimage.height / this.state.classdetails.height}
					steps={this.state.classdetails.length}
					fps={this.state.classdetails.framerate}
					autoplay={this.state.classdetails.playonstart}
        			loop={this.state.classdetails.repeat}
					startAt={1}
					endAt={1}
					direction="forward"
					onInit={this.initSpritesheet}
				/>
			)
		}
		else {
			return (
				<SemImage fluid src={imageurl}/>
			)
		}
	}
	initSpritesheet = ((spritesheet) => {
		this.spritesheet = spritesheet;
		this.spritesheet.goToAndPause(1);
	});
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	audioPlayer = () => {
		if (this.state.audiodetails)
		{
			return (
				<ReactAudioPlayer
					src={this.state.audiodetails.audiourl}
					loop={this.state.audiodetails.repeat}
					autoPlay={this.state.audiodetails.playonstart}
					volume={this.state.audiodetails.volume}
					ref={(element) => { this.audioplayer = element; }}
				/>
			)
		}
		else {
			return (
				<> </>
			)
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		return (
            <Card>
				<Button
					compact basic
					geobotid={this.state.geobotdetails.geobotid}
					loading={this.isSendingEvent("touch")}
					command="touch"
					parameters={{}}
					onClick={this.sendEvent}
				>
				{this.imageOrSprite ()}
				</Button>
				{this.audioPlayer ()}
                <Card.Content textAlign="center">
                    <Card.Header>{this.state.geobotdetails.name}</Card.Header>
					<Card.Description>{this.state.geobotdetails.description}</Card.Description>
                </Card.Content>
				{this.automationButtons(this.state.automations)}
				{this.linkButtons(this.state.linkdetails)}
            </Card>
		)
	}
};
// --------------------------------------------------------------------------------------------------------

export default Geobot;
