// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Edit the metadata for the give entity
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Table, Button, Icon, Input, Modal } from 'semantic-ui-react'
import { JsonEditor } from 'jsoneditor-react';
import 'jsoneditor-react/es/editor.min.css';


// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class MetadataEdit extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			editingmetadata: false,
			editingJSONindex: 0,
			metadata: []
		};
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		this.props.imersiasdk.GET("metadata", this.props.entityid)
		.then ((resp) => {
			this.setState({metadata: resp.json});
			this.props.updateMetadata(resp.json);
		})
		.catch ((err) => {
			// Do nothing
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentWillUnmount = () =>
	{
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	newMetadata = (e) =>
	{
		let currentmetadata = this.state.metadata;
		currentmetadata.push({key:"", value:"", metadataid:"", new:true});
		this.setState({metadata: currentmetadata});
		this.props.updateMetadata(currentmetadata);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	deleteMetadata = (e, data) =>
	{
		let currentmetadata = this.state.metadata;
		currentmetadata[data.index].deleted = true;
		this.setState({metadata: currentmetadata});
		this.props.updateMetadata(currentmetadata);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Set the local variables when the input fields are changed, ready for saving to the automations object
	// ----------------------------------------------------------------------------------------------------
	handleChange = (e, data) => {
		let currentmetadata = this.state.metadata;

		if (data.name === "value")
		{
			currentmetadata[data.index][data.name] = this.parseJSON(data.value);
		}
		else
		{
			currentmetadata[data.index][data.name] = data.value;
		}
		this.setState({metadata: currentmetadata});
		this.props.updateMetadata(currentmetadata);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleJSONChange = (data) => {
		let currentmetadata = this.state.metadata;
		currentmetadata[this.state.editingJSONindex].value = data;
		this.setState({metadata: currentmetadata});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	openJsonEditor = (e, data) =>
	{
		this.setState({editingJSONindex: data.index});
		this.setState({valueToEdit: this.parseJSON(this.state.metadata[data.index].value), editingmetadata: true});
	}
	saveJSONValue = (e) =>
	{
		let currentmetadata = this.state.metadata;
		// currentmetadata[this.state.editingJSONindex].value = this.parseJSON(data.value);
		this.setState({metadata: currentmetadata, editingmetadata: false});
		this.props.updateMetadata(currentmetadata);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	metadataList = () =>
	{
		let metadatahtml = [];
		this.state.metadata.forEach ((item, i) => {
			if (!item.deleted)
			{
				metadatahtml.push (
					<Table.Row>
						<Table.Cell textAlign='center' width={1}>
							<Button index={i} icon color="red" basic onClick={this.deleteMetadata}><Icon name="delete"/></Button>
						</Table.Cell>
						<Table.Cell textAlign='center' width={5}>
							<Input fluid index={i} name="key" value={item.key} placeholder="Metadata key" onChange={this.handleChange}/>
						</Table.Cell>
						<Table.Cell width={12}>
							<Input fluid index={i} name="value" value={this.stringifyJSON(item.value)} placeholder="Metadata value (JSON, number, boolean or string)" label={<Button index={i} onClick={this.openJsonEditor}><Icon name="edit"/></Button>} onChange={this.handleChange} />
						</Table.Cell>
					</Table.Row>
				)
			}
		});
		return (metadatahtml);
	}
	parseJSON = (value) =>
	{
		let converted = null;
		try {
			converted = JSON.parse(value);
		}
		catch (err)
		{
			converted = value;
		}
		return (converted);
	}
	stringifyJSON = (value) =>
	{
		if (typeof value === 'string' || value instanceof String)
			{ return value }
		else
		 	{ return (JSON.stringify(value)) };
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	editJSONValueModal = () => {
		if (this.state.editingmetadata)
		{
			return (
				<Modal open={this.state.editingmetadata} centered={false}>
					<Modal.Header>
						JSON Editor
					</Modal.Header>
					<Modal.Content scrolling>
						<JsonEditor
							value={this.state.metadata[this.state.editingJSONindex].value}
							onChange={this.handleJSONChange}
						/>
					</Modal.Content>
					<Modal.Actions>
						<Button basic positive onClick={this.saveJSONValue}>
							Update <Icon name='right chevron' />
						</Button>
					</Modal.Actions>
				</Modal>
			);
		}
		else {
			return (<></>);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		return (
			<>
				<Table celled unstackable>
					<Table.Header>
						<Table.HeaderCell textAlign='center' width={1}>
							<Button basic color="blue" onClick={this.newMetadata}>add</Button>
						</Table.HeaderCell>
						<Table.HeaderCell width={5}>Key</Table.HeaderCell>
						<Table.HeaderCell width={12}>Value</Table.HeaderCell>
					</Table.Header>
					<Table.Body>
						{this.metadataList ()}
					</Table.Body>
				</Table>
				{this.editJSONValueModal ()}
			</>
		);
	}
};
// --------------------------------------------------------------------------------------------------------

export default MetadataEdit;
