// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draw a lot of channel cards
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { } from 'semantic-ui-react'
import { Line } from 'react-chartjs-2'

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class Analytics extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// props:
	//		entityid
	//		startdate
	//		enddate
	//		events
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			analytics: null,
			loading: true
		};
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		if (this.props.entityid)
		{
			let headers = this.props.entityid;
			if (this.props.startdate) headers.startdate = this.props.startdate;
			if (this.props.enddate) headers.enddate = this.props.enddate;

			// Get Analytics from geobot
			if (this.props.imersiasdk.isadmin)
			{
				this.props.imersiasdk.GET("admin", {"command":"analytics", "parameters":JSON.stringify(this.props.entityid)})
				.then ((resp) => {
					this.setState({analytics: resp.json, loading:false});
				})
				.catch ((err) => {
					// Do nothing
				});
			}
			else {
				this.props.imersiasdk.GET("analytics", headers)
				.then ((resp) => {
					this.setState({analytics: resp.json, loading:false});
				})
				.catch ((err) => {
					// Do nothing
				});
			}
		}
	}

	componentWillUnmount = () =>
	{
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Extract the unique event names from the analytics data
	// ----------------------------------------------------------------------------------------------------
	eventNames = () =>
	{
		let dropdownitems = [];
		if (this.state.analytics != null)
		{
			let eventnames = [];

			// Collate unique events
			this.state.analytics.forEach( (entry) => {
				if (eventnames.indexOf(entry.event) === -1)
				{
					eventnames.push(entry.event);
				}
			});

			// Create a dropdown for each of these
			eventnames.forEach((eventname) => {
				dropdownitems.push( {key: eventname, text: eventname, value: eventname} );
			});

		}
		return (dropdownitems);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Process the analytics into a form more suitable for interrogating for the graph
	// ----------------------------------------------------------------------------------------------------
	processAnalytics = () =>
	{
		let analyticshash = {};
		if (this.state.analytics)
		{
			this.state.analytics.forEach((entry) => {
				if (analyticshash.hasOwnProperty(entry.event))
				{
					if (analyticshash[entry.event].hasOwnProperty(entry.created))
					{
						analyticshash[entry.event][entry.created] += entry.tally;
					}
					else
					{
						analyticshash[entry.event][entry.created] = entry.tally;
					}
				}
				else
				{
					analyticshash[entry.event] = {};
					analyticshash[entry.event][entry.created] = entry.tally;
				}
			});
		}

		return (analyticshash);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Given a date in milliseconds, create a nice-looking hourly date / time.
	// ----------------------------------------------------------------------------------------------------
	dateLabel = (rawDate) =>
	{
		let theDate = new Date(rawDate);
		return (theDate.getFullYear() + "-" + (theDate.getMonth()+1) + "-" + theDate.getDate() + " " + theDate.getHours() + ":00");
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Create a dataset for the graph
	// ----------------------------------------------------------------------------------------------------
	createDataset = (colour, label, labels, entry) => {
		let data = [];
		Object.keys(entry).forEach((date) => {
			let theDate = new Date(date).getTime();
			let dateString = theDate; //this.dateLabel(theDate);
			data.push({x:dateString, y:entry[date]});
		});

		data.sort((a,b) => {return(a.x-b.x)});

		return (
			{
				label: label,
				fill: false,
				lineTension: 0.05,
				backgroundColor: colour,
				borderColor: colour,
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				maintainAspectRatio: false,
				pointBorderColor: colour,
				pointBackgroundColor: '#fff',
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: colour,
				pointHoverBorderColor: colour,
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 10,
				data: data
			}
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Create a range of nice-looking labels from the earliest to the latest dates, at hourly intervals
	// ----------------------------------------------------------------------------------------------------
	createLabels = () => {
		let nicelabels = [];

		if (this.state.analytics)
		{
			if (this.state.analytics.length > 0)
			{
				// Find max and min dates
				let startDate = new Date(this.state.analytics[0].created).getTime();
				let endDate = new Date(this.state.analytics[0].created).getTime();

				this.state.analytics.forEach( (entry) => {
					let thisDate = new Date(entry.created).getTime();
					startDate = Math.min(startDate, thisDate);
					endDate = Math.max(endDate, thisDate);
				});

				// Create a date entry for each hour
				for (let currentDate = startDate; currentDate <= endDate; currentDate += 3600000)
				{
					let thisDate = new Date(currentDate);
					// nicelabels.push(this.dateLabel(thisDate));
					nicelabels.push(thisDate);
				}

				// If there was only one date entry, add another to give something to graph into
				if (nicelabels.length === 1)
				{
					let thisDate = new Date(startDate+3600000);
					nicelabels.push(thisDate);
				}
			}
		}

		return (nicelabels);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		let datasets = [];
		let labels = this.createLabels();
		let processedanalytics = this.processAnalytics();
		let colours = ["#CFF09E", "#A8DBA8", "#79BD9A", "#3B8686", "#0B486B"];
		let colourcounter = 0;
		Object.keys(processedanalytics).forEach((key) => {
			datasets.push(this.createDataset(colours[colourcounter], key, labels, processedanalytics[key]));
			colourcounter = (colourcounter + 1) % 5;
		});

		const data = {
		  datasets: datasets
		};

		const options = {
		    scales: {

				xAxes: [{
					display: true,
					type: "time",
					time: {
						displayFormats: {
							hour: "D MMM, YYYY - hA",
							day: "D MMM, YYYY - hA"
						},
						minUnit:"minute"
					}
				}]
			}
		}

		if (this.state.loading)
		{
			return (<>Loading... </>);
		}
		else {
			if (this.state.analytics && this.state.analytics.length > 0)
			{
				let height = (this.props.height) ? this.props.height + "vh" : "70vh";
				return (
					<div style={{ height:height, width:"100%"}}>
						<Line data={data} options={options}/>
					</div>
				)
			}
			else
			{
				return (<>There are no analytics entries... </>);
			}
		}
	}
};
// --------------------------------------------------------------------------------------------------------

export default Analytics;
