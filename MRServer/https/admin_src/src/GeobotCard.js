// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draws a Geobot card given the details
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Card, Modal, Label, Icon, Menu, Button, Dropdown, Segment } from 'semantic-ui-react'
import JSONEditor from './JSONEditor.js';
import Geobot from './patterns/Geobot.json';
import Audio from './patterns/Audio.json';
import GeobotProgram from './GeobotProgram.js';
import MetadataEdit from './MetadataEdit.js';
import ContextsEdit from './ContextsEdit.js';
import FileManager from './FileManager.js';
import Analytics from './Analytics.js';
import HeatMap from './HeatMap.js';
import ImageImersia from './ImageImersia.js';
import { DatesRangeInput } from 'semantic-ui-calendar-react';

import noimage from './images/noimage.png';

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class GeobotCard extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			currentchannel: this.props.currentchannel,
			geobotdetails: JSON.parse(JSON.stringify(this.props.geobotdetails)),
			audiodetails: {},
			geobotautomations: [],
			geobotmetadata:[],
			geobotcontexts:[],
			editedgeobotdetails: {},
			editMode: 0,
			editing: false,
			deleting: false,
			programming: false,
			editingmetadata: false,
			editingcontexts: false,
			editingaudio: false,
			filemanager:false,
			analyticsexplorer_update:false,
			analyticsexplorer:false,
			showheatmaps: false,
			error: false,
			error_message: "",
			datesRange: ''
		};
	}
	// ----------------------------------------------------------------------------------------------------


	// ----------------------------------------------------------------------------------------------------
	// Register and deregister the event listeners
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () => {
		document.addEventListener(this.props.geobotdetails.geobotid, this.processGeobotUpdate);
		document.addEventListener(this.props.currentchannel.channelid, this.processChannelUpdate);
	}
	// ----------------------------------------------------------------------------------------------------
	componentWillUnmount = () => {
		document.removeEventListener(this.props.geobotdetails.geobotid, this.processGeobotUpdate);
		document.removeEventListener(this.props.currentchannel.channelid, this.processChannelUpdate);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Process the messages coming from the websocket
	// ----------------------------------------------------------------------------------------------------
	processGeobotUpdate = (message) =>
	{
		if (message.detail.wotcha.hasOwnProperty("geobot_set"))
		{
			this.setState({geobotdetails: message.detail.wotcha.geobot_set});
		}
		else if (message.detail.wotcha.hasOwnProperty("geobot_delete"))
		{
			// Leave this session
		}
	}
	// ----------------------------------------------------------------------------------------------------
	processChannelUpdate = (message) =>
	{
		if (message.detail.wotcha.hasOwnProperty("channel_set"))
		{
			this.setState({currentchannel: message.detail.wotcha.channel_set});
		}
		else if (message.detail.wotcha.hasOwnProperty("channel_rename"))
		{
			this.setState({currentchannel: message.detail.wotcha.channel_rename});
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Pass back up to the geobot list which geobot was clicked on
	// ----------------------------------------------------------------------------------------------------
	openGeobot = () =>
	{
		this.props.openGeobot(this.state.geobotdetails);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Extract the default settings from a pattern
	// ----------------------------------------------------------------------------------------------------
	getDefaults = (pattern) =>
	{
		let returnvalue = {};
		pattern.forEach((element) => {
			if (element.type === "object")
			{
				returnvalue[element.name] = this.getDefaults(pattern.object);
			}
			else {
				returnvalue[element.name] = (element.default ? element.default : "");
			}
		});
		return returnvalue;
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Capture the changes to the details that occur in a sub component
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	contextsModal = () =>
	{
		return (
			<Modal open={this.state.editingcontexts} centered={false} >
				<Modal.Header>
					Contexts
				</Modal.Header>
				<Modal.Content>
					<Segment secondary mini textAlign="justify">
						Geobots can have attributes: read (to read details), edit (to alter details, metadata and files), log (to allow generating an analytics event)
					</Segment>
				</Modal.Content>
				<Modal.Content scrolling>
					<ContextsEdit
						imersiasdk={this.props.imersiasdk}
						entityid={{geobotid:this.state.geobotdetails.geobotid}}
						updateContexts={this.updateContexts}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button basic color="blue" onClick={this.cancelContextsEdit}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic positive onClick={this.saveContexts}>
						<Icon name='save' /> Save
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	cancelContextsEdit = () =>
	{
		this.setState({editingcontexts:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	updateContexts = (newContexts) =>
	{
		this.setState({geobotcontexts:newContexts});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	saveContexts = () =>
	{
		let counter = this.state.geobotcontexts.length;
		this.state.geobotcontexts.forEach((context) => {
			if (context.deleted)
			{
				this.props.imersiasdk.DELETE("context", {geobotid:this.state.geobotdetails.geobotid, contextid:context.contextid})
				.then ((resp) => {
					if (--counter <= 0) {this.setState({editingcontexts:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.setState({editingcontexts:false});}
				});
			}
			else if (context.new)
			{
				this.props.imersiasdk.POST("context", {geobotid:this.state.geobotdetails.geobotid}, context)
				.then ((resp) => {
					if (--counter <= 0) {this.setState({editingcontexts:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.setState({editingcontexts:false});}
				});
			}
			else
			{
				this.props.imersiasdk.PUT("context", {geobotid:this.state.geobotdetails.geobotid, contextid:context.contextid}, context)
				.then ((resp) => {
					if (--counter <= 0) {this.setState({editingcontexts:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.setState({editingcontexts:false});}
				});
			}
		})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Metadata Modal Editor
	// ----------------------------------------------------------------------------------------------------
	metadataModal = () =>
	{
		return (
			<Modal open={this.state.editingmetadata} centered={false} >
				<Modal.Header>
					Metadata
				</Modal.Header>
				<Modal.Content scrolling>
					<MetadataEdit
						imersiasdk={this.props.imersiasdk}
						entityid={{geobotid:this.state.geobotdetails.geobotid}}
						updateMetadata={this.updateMetadata}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button basic color="blue" onClick={this.cancelMetadataEdit}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic positive onClick={this.saveMetadata}>
						<Icon name='save' /> Save
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------
	cancelMetadataEdit = () =>
	{
		this.setState({editingmetadata:false});
	}
	// ----------------------------------------------------------------------------------------------------
	updateMetadata = (newMetadata) =>
	{
		this.setState({geobotmetadata:newMetadata});
	}
	// ----------------------------------------------------------------------------------------------------
	saveMetadata = () =>
	{
		let counter = this.state.geobotmetadata.length;
		this.state.geobotmetadata.forEach((metadata) => {
			if (metadata.deleted)
			{
				this.props.imersiasdk.DELETE("metadata", {geobotid:this.state.geobotdetails.geobotid, metadataid:metadata.metadataid})
				.then ((resp) => {
					if (--counter <= 0) {this.setState({editingmetadata:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.setState({editingmetadata:false});}
				});
			}
			else if (metadata.new)
			{
				this.props.imersiasdk.POST("metadata", {geobotid:this.state.geobotdetails.geobotid}, metadata)
				.then ((resp) => {
					if (--counter <= 0) {this.setState({editingmetadata:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.setState({editingmetadata:false});}
				});
			}
			else
			{
				this.props.imersiasdk.PUT("metadata", {geobotid:this.state.geobotdetails.geobotid, metadataid:metadata.metadataid}, metadata)
				.then ((resp) => {
					if (--counter <= 0) {this.setState({editingmetadata:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.setState({editingmetadata:false});}
				});
			}
		})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Edit main details of the Geobot
	// ----------------------------------------------------------------------------------------------------
	editModal = () =>
	{
		return (
			<Modal open={this.state.editing} centered={false} >
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Edit</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content scrolling>
					<JSONEditor
						imersiasdk={this.props.imersiasdk}
						pattern={Geobot["com.imersia.geobot"]}
						details={this.state.geobotdetails}
						parameters={{geobotid:this.state.geobotdetails.geobotid}}
						changeDetails={this.changeDetails}
					/>
		  		</Modal.Content>
				<Modal.Actions>
					<Button basic negative onClick={this.askDeleteGeobot}>
						<Icon name='cancel' /> Delete
					</Button>
					<Button basic color="blue" onClick={this.cancelEdit}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic positive onClick={this.saveEdit}>
						Save <Icon name='right chevron' />
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------
	changeDetails = (newdetails) =>
	{
		this.setState({editedgeobotdetails:JSON.parse(JSON.stringify(newdetails))});
	}
	// ----------------------------------------------------------------------------------------------------
	cancelEdit = () => {
		this.setState({editing:false});
	}
	// ----------------------------------------------------------------------------------------------------
	saveEdit = () => {
		this.props.imersiasdk.PUT("geobots", {geobotid:this.state.editedgeobotdetails.geobotid}, this.state.editedgeobotdetails)
		.then ((resp) => {
			this.setState({geobotdetails:this.state.editedgeobotdetails, editing:false});
		})
		.catch ((resp) => {
			this.setState({error:true, error_message:"An error occurred saving the Geobot."});
		});
	}
	// ----------------------------------------------------------------------------------------------------
	askDeleteGeobot = () => {
		this.setState({deleting:true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Edit the Audio details of the Geobot
	// ----------------------------------------------------------------------------------------------------
	editAudioModal = () =>
	{
		return (
			<Modal open={this.state.editingaudio} centered={false} >
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Audio</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content scrolling>
					<JSONEditor
						imersiasdk={this.props.imersiasdk}
						pattern={Audio["com.imersia.audio"]}
						details={this.state.audiodetails.value}
						parameters={{geobotid:this.state.geobotdetails.geobotid}}
						changeDetails={this.changeAudioDetails}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button basic color="blue" onClick={this.cancelEditAudio}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic positive onClick={this.saveEditAudio}>
						Save <Icon name='right chevron' />
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------
	cancelEditAudio = () =>
	{
		this.setState({editingaudio:false});
	}
	// ----------------------------------------------------------------------------------------------------
	saveEditAudio = () =>
	{
		this.props.imersiasdk.PUT("metadata", {geobotid:this.state.geobotdetails.geobotid}, this.state.audiodetails)
		.then ((resp) => {
			this.setState({editingaudio:false});
		})
		.catch ((err) => {
		});
	}
	// ----------------------------------------------------------------------------------------------------
	changeAudioDetails = (newaudiodetails) =>
	{
		let storedaudiodetails = this.state.audiodetails;
		storedaudiodetails.value = JSON.parse(JSON.stringify(newaudiodetails));
		this.setState({audiodetails:storedaudiodetails});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Delete the Geobot
	// ----------------------------------------------------------------------------------------------------
	askDeleteModal = () =>
	{
		return (
			<Modal size="tiny" open={this.state.deleting} centered={true}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Delete Geobot</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content>
					Delete Geobot? - there is no UNDO.
				</Modal.Content>
				<Modal.Actions>
					<Button basic color="blue" positive onClick={this.cancelDelete}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic negative onClick={this.deleteGeobot}>
						<Icon name='cancel' /> DELETE
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------
	deleteGeobot = () => {
        this.props.imersiasdk.DELETE("geobots", {geobotid:this.state.geobotdetails.geobotid})
        .then ((resp) => {
			this.props.closeGeobot();
        })
        .catch((err) => {
			this.setState({deleting:false});
        });
	}
	// ----------------------------------------------------------------------------------------------------
	cancelDelete = () => {
		this.setState({deleting:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Program the Geobot
	// ----------------------------------------------------------------------------------------------------
	programmingModal = () =>
	{
		return (
			<Modal open={this.state.programming} centered={false}>
				<Modal.Header>Programming
					<Button.Group floated="right" >
						<Button icon basic={this.state.editMode !== 0} color="grey" onClick={this.toggleJsonMode}>
							<Icon name="wizard"/>
						</Button>
						<Button icon basic={this.state.editMode !== 1} color="grey" onClick={this.toggleJsonMode}>
							<Icon name="list"/>
						</Button>
						<Button icon basic={this.state.editMode !== 2} color="grey" onClick={this.toggleJsonMode}>
							<Icon name="file code outline"/>
						</Button>
					</Button.Group>
				</Modal.Header>
				<Modal.Content scrolling>
					<GeobotProgram
						geobotdetails={this.state.geobotdetails}
						geobotautomations={this.state.geobotautomations}
						editMode={this.state.editMode}
						changeProgram={this.changeProgram}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button basic color="blue" onClick={this.cancelProgram}>
						<Icon name='left chevron' /> Cancel
					</Button>
					<Button basic positive onClick={this.saveProgram}>
						<Icon name='save' /> Save
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------
	toggleJsonMode = () =>
	{
		this.setState({editMode: ((this.state.editMode + 1) % 3)});
	}
	cancelProgram = () =>
	{
		this.reloadAutomations ();
		this.setState({programming:false});
	}
	// ----------------------------------------------------------------------------------------------------
	saveProgram = () =>
	{
		let counter = this.state.geobotautomations.length;
		this.state.geobotautomations.forEach((automation) => {
			if (automation.deleted)
			{
				this.props.imersiasdk.DELETE("geobots/automations", {geobotid:this.state.geobotdetails.geobotid, automationid:automation.automationid})
				.then ((resp) => {
					if (--counter <= 0) {this.reloadAutomations (); this.setState({programming:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.reloadAutomations (); this.setState({programming:false});}
				});
			}
			else if (automation.new)
			{
				this.props.imersiasdk.POST("geobots/automations", {geobotid:this.state.geobotdetails.geobotid}, automation)
				.then ((resp) => {
					if (--counter <= 0) {this.reloadAutomations (); this.setState({programming:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.reloadAutomations (); this.setState({programming:false});}
				});
			}
			else
			{
				this.props.imersiasdk.PUT("geobots/automations", {geobotid:this.state.geobotdetails.geobotid, automationid:automation.automationid}, automation)
				.then ((resp) => {
					if (--counter <= 0) {this.reloadAutomations (); this.setState({programming:false});}
				})
				.catch ((err) => {
					if (--counter <= 0) {this.reloadAutomations (); this.setState({programming:false});}
				});
			}
		})
	}
	// ----------------------------------------------------------------------------------------------------
	reloadAutomations = () =>
	{
		if (this.props.imersiasdk.isadmin)
		{
			this.props.imersiasdk.GET("admin", {command:"automations", parameters:JSON.stringify({geobotid:this.state.geobotdetails.geobotid})})
			.then((resp) => {
				this.setState({geobotautomations: resp.json});
			})
			.catch((err) => {

			})
		}
		else
		{
			this.props.imersiasdk.GET("geobots/automations", {geobotid:this.state.geobotdetails.geobotid})
			.then ((resp) => {
				this.setState({geobotautomations: resp.json});
			})
			.catch ((err) => {
				// Do nothing
			});
		}
	}
	// ----------------------------------------------------------------------------------------------------
	changeProgram = (newautomations) =>
	{
		this.setState({geobotautomations:newautomations});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Modal dialog for managing the files on this Geobot
	// ----------------------------------------------------------------------------------------------------
	filesModal = () =>
	{
		return (
			<Modal open={this.state.filemanager} centered={false} >
				<Modal.Header>
					Files
				</Modal.Header>
				<Modal.Content scrolling>
					<FileManager
						imersiasdk={this.props.imersiasdk}
						entityid={{geobotid:this.state.geobotdetails.geobotid}}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button basic positive onClick={this.closeFilesModal}>
						<Icon name='check' /> Done
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------
	closeFilesModal = () =>
	{
		this.setState({filemanager:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Modal dialog for exploring the analytics for this Geobot
	// ----------------------------------------------------------------------------------------------------
	analyticsModal = () =>
	{
		if (this.state.analyticsexplorer_update)
		{
			this.setState({analyticsexplorer_update:false});
			return (<></>);
		}
		else {
			return (
				<Modal size="large" open={this.state.analyticsexplorer} centered={false} >
					<Modal.Header>
						Analytics
						<Button.Group floated='right'>
							<Button compact basic style={{padding:"0px"}}>
								<DatesRangeInput
									name="datesRange"
									floated="right"
									placeholder="Start date - End date"
									value={this.state.datesRange}
									iconPosition="left"
									onChange={this.handleDateChange}
								/>
							</Button>
							{ this.state.showheatmaps
							?<>
								<Button icon value={false} basic color="blue" onClick={this.setanalyticsmode }>
									<Icon name='line graph' />
								</Button>
								<Button icon value={true} color="blue" onClick={this.setanalyticsmode }>
									<Icon name='world' />
								</Button>
							</>:<>
								<Button icon value={false} color="blue" onClick={this.setanalyticsmode }>
									<Icon name='line graph' />
								</Button>
								<Button icon value={true} basic color="blue" onClick={this.setanalyticsmode }>
									<Icon name='world' />
								</Button>
							</>
							}
						</Button.Group>
					</Modal.Header>
					<Modal.Content style={{overflow:"hidden"}}>
						{ this.state.showheatmaps ? (
							<HeatMap
								imersiasdk={this.props.imersiasdk}
								entityid={{geobotid:this.state.geobotdetails.geobotid}}
								startdate={this.extractStartdate(this.state.datesRange)}
								enddate={this.extractEnddate(this.state.datesRange)}
							/>
						) : (
							<Analytics
								imersiasdk={this.props.imersiasdk}
								entityid={{geobotid:this.state.geobotdetails.geobotid}}
								startdate={this.extractStartdate(this.state.datesRange)}
								enddate={this.extractEnddate(this.state.datesRange)}
							/>
						)}
					</Modal.Content>
					<Modal.Actions>
						<Button basic positive onClick={this.closeAnalyticsModal}>
							<Icon name='check' /> Done
						</Button>
					</Modal.Actions>
				</Modal>
			)
		}
	}
	// ----------------------------------------------------------------------------------------------------
	closeAnalyticsModal = () =>
	{
		this.setState({analyticsexplorer:false});
	}
	setanalyticsmode = (e, data) =>
	{
		this.setState({showheatmaps:data.value});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// General Error modal dialog
	// ----------------------------------------------------------------------------------------------------
	errorModal = () =>
	{
		return (
			<Modal size="tiny" open={this.state.error} centered={true}>
				<Modal.Header>
					<Menu borderless>
						<Menu.Item>
							<h3>Error</h3>
						</Menu.Item>
					</Menu>
				</Modal.Header>
				<Modal.Content>
					{this.state.error_message}
				</Modal.Content>
				<Modal.Actions>
					<Button basic positive onClick={this.errorOK}>
						OK
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------
	errorOK = () => {
		this.setState({error:false})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleDateChange = (event, {name, value}) => {
		if (this.state.hasOwnProperty(name)) {
			this.setState({ [name]: value });
			if ((value.length >= 16) || (value.length === 0)) this.setState({analyticsexplorer_update: true});
		}
    }
	extractStartdate = (datesRange) => {
		let elements = datesRange.split('-');
		if (elements.length >= 3)
		{
			return (elements[2].trim() + elements[1].trim() + elements[0].trim() + "");
		}
		else {
			return ""
		}
	}
	extractEnddate = (datesRange) => {
		let elements = datesRange.split('-');
		if (elements.length >= 6)
		{
			return (elements[5].trim() + elements[4].trim() + elements[3].trim() + "");
		}
		else {
			return ""
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Handle the selection of the menu option
	// ----------------------------------------------------------------------------------------------------
	handleEditClick = () =>
	{
		this.setState({editing:true});
	}
	// ----------------------------------------------------------------------------------------------------
	handleAudioClick = () =>
	{
		this.props.imersiasdk.GET("metadata", {geobotid:this.state.geobotdetails.geobotid, key: "com.imersia.audio"})
		.then ((resp) => {
			this.setState({audiodetails: resp.json});
			this.setState({editingaudio: true});
		})
		.catch ((err) => {
			this.setState({audiodetails: {key:"com.imersia.audio", value:this.getDefaults(Audio["com.imersia.audio"])}});
			this.setState({editingaudio: true});
		});
	}
	// ----------------------------------------------------------------------------------------------------
	handleProgramClick = () =>
	{
		if (this.props.imersiasdk.isadmin)
		{
			this.props.imersiasdk.GET("admin", {command:"automations", parameters:JSON.stringify({geobotid:this.state.geobotdetails.geobotid})})
			.then((resp) => {
				this.setState({geobotautomations: resp.json});
				this.setState({programming:true});
			})
			.catch((err) => {

			})
		}
		else
		{
			this.props.imersiasdk.GET("geobots/automations", {geobotid:this.state.geobotdetails.geobotid})
			.then ((resp) => {
				this.setState({geobotautomations: resp.json});
				this.setState({programming:true});
			})
			.catch ((err) => {
				// Do nothing
			});
		}
	}
	// ----------------------------------------------------------------------------------------------------
	handleMetadataClick = () =>
	{
		this.setState({editingmetadata:true});
	}
	// ----------------------------------------------------------------------------------------------------
	handleFilesClick = () =>
	{
		this.setState({filemanager:true});
	}
	// ----------------------------------------------------------------------------------------------------
	handleAnalyticsClick = () =>
	{
		this.setState({analyticsexplorer:true});
	}
	// ----------------------------------------------------------------------------------------------------
	handleContextsClick = () =>
	{
		this.setState({editingcontexts:true});
	}
	// ----------------------------------------------------------------------------------------------------




	// ----------------------------------------------------------------------------------------------------
	// Choose which modal to open depending on the menu item selected
	// ----------------------------------------------------------------------------------------------------
	handleMenuChoice = (e, data) =>
	{
		switch (data.value)
		{
			case "edit":
			this.handleEditClick ();
			break;
			case "audio":
			this.handleAudioClick ();
			break;
			case "program":
			this.handleProgramClick ();
			break;
			case "metadata":
			this.handleMetadataClick ();
			break;
			case "files":
			this.handleFilesClick ();
			break;
			case "analytics":
			this.handleAnalyticsClick ();
			break;
			case "contexts":
			this.handleContextsClick ();
			break;
			default:
			break;
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		var imageurl = this.state.geobotdetails.imageurl;

		const trigger = (
			<Button style={{left:"0px", "background-color": "white", "color": "grey", opacity: 0.8}} compact circular icon="ellipsis vertical"/>
		)
		const options = [
			{ value: 'edit', text: 'Edit Details', icon: 'edit' },
			{ value: 'audio', text: 'Audio', icon: 'sound'},
			{ value: 'program', text: 'Program', icon: 'pencil' },
			{ value: 'metadata', text: 'Metadata', icon: 'list' },
			{ value: 'files', text: 'Files', icon: 'file alternate outline' },
			{ value: 'analytics', text: 'Analytics', icon: 'question circle' },
			{ value: 'contexts', text: 'Contexts', icon: 'key' }
		]

		return (
            <Card>
				<Button
					basic compact
					onClick={this.openGeobot}>
					<ImageImersia src={imageurl} default={noimage} imersiasdk={this.props.imersiasdk} fluid/>
				</Button>
				{
					this.state.geobotdetails.ownerid === this.props.imersiasdk.userid ?
					(
						<div style={{ position: "absolute", top: "15px", left: "20px" }}>
							<Dropdown trigger={trigger} options={options} pointing='top left' icon={null} onChange={this.handleMenuChoice}/>
						</div>
					):(
						<></>
					)
				}
                <Card.Content textAlign="center">
                    <Card.Header>{this.state.geobotdetails.name}</Card.Header>
					<Card.Meta>{this.state.geobotdetails.class}</Card.Meta>
					<Card.Description>{this.state.geobotdetails.description}</Card.Description>
                </Card.Content>
				<Card.Content extra textAlign="center">
					<Label fluid>
						<Icon name={(this.state.currentchannel.hidden) ? "eye slash" : "eye"} />
						channel
					</Label> | <Label fluid>
						<Icon name={(this.state.geobotdetails.hidden) ? "eye slash" : "eye"} />
						geobot
					</Label>
				</Card.Content>
				<Card.Content extra textAlign="center">
					<a rel="noopener noreferrer" target="_blank" href={"https://" + window.location.host + "?id=" + this.state.geobotdetails.geobotid}>direct link</a>
				</Card.Content>

				{this.state.editing ?			this.editModal() 		: <></>}
				{this.state.editingaudio ?		this.editAudioModal()	: <></>}
				{this.state.deleting ?			this.askDeleteModal()	: <></>}
				{this.state.error ?				this.errorModal()		: <></>}
				{this.state.programming ?		this.programmingModal()	: <></>}
				{this.state.editingmetadata ?	this.metadataModal()	: <></>}
				{this.state.filemanager ?		this.filesModal()		: <></>}
				{this.state.analyticsexplorer ?	this.analyticsModal()	: <></>}
				{this.state.editingcontexts ?	this.contextsModal()	: <></>}
            </Card>
		)
	}
};
// --------------------------------------------------------------------------------------------------------

export default GeobotCard;
