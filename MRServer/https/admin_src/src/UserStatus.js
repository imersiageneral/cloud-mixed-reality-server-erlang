// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Current location and UserID in a box
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Table } from 'semantic-ui-react'

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class UserStatus extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object - expect link to imersiasdk object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			userid: this.props.imersiasdk.userid,
			sessionid: this.props.imersiasdk.sessionid,
			geohash: this.props.imersiasdk.geohash,
			connected: this.props.imersiasdk.connected,
			version: this.props.imersiasdk.version,
			isadmin: this.props.imersiasdk.isadmin
		};
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () => {
		document.addEventListener("updategps", this.processWotchaEvent);
    }
    componentWillUnmount = () => {
		document.removeEventListener("updategps", this.processWotchaEvent);
    }
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	processWotchaEvent = (message) => {
		this.setState({userid: message.detail.userid, sessionid: message.detail.sessionid, location: message.detail.location, connected: message.detail.connected});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		return (
			<Table celled stackable definition size='small' compact>
				<Table.Body>
					<Table.Row>
						<Table.Cell>UserID</Table.Cell>
						<Table.Cell>{this.state.userid}</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>Admin</Table.Cell>
						<Table.Cell>{this.state.isadmin ? "yes" : "no"}</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>SessionID</Table.Cell>
						<Table.Cell>{this.state.sessionid}</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>Geohash</Table.Cell>
						<Table.Cell>{this.state.location}</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>Connected</Table.Cell>
						<Table.Cell>{this.state.connected ? "yes" : "no"}</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>Version</Table.Cell>
						<Table.Cell>{this.state.version}</Table.Cell>
					</Table.Row>
				</Table.Body>
			</Table>
		);
	}
};
// --------------------------------------------------------------------------------------------------------

export default UserStatus;
