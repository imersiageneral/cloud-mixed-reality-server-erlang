// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draws a geobot card given the details
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Segment, Modal, Button, Icon, Table, Grid, Form, Divider, Input, Checkbox, Label } from 'semantic-ui-react';
import Commands from './patterns/Commands.json';
import { JsonEditor } from 'jsoneditor-react';
import 'jsoneditor-react/es/editor.min.css';

// import brace from 'brace';
import AceEditor from 'react-ace';
// import 'brace/mode/json';
// import 'brace/theme/github';

class GeobotProgram extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			automationNameModal: false,
			editTransitionModal: false,
			editActionsModal: false,
			selectActionModal: false,
			automations: JSON.parse(JSON.stringify(props.geobotautomations)),
			currentAutomation: -1,
			currentTransition: -1,
			automationName: "",
			automationDescription: "",
			editAutomation: false,				// True - editing automation, false - new automation.

			// holding variables for the dialog boxes
			state: "",
			event: "",
			visible: false,
			newstate: "",
			updateJSON: true
		};
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		if (this.state.automations.length > 0)
		{
			this.setState({currentAutomation:0});
		}
	}

	componentWillUnmount = () =>
	{
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	parseJSON = (value) =>
	{
		let converted = null;
		try {
			converted = JSON.parse(value);
		}
		catch (err)
		{
			converted = value;
		}
		return (converted);
	}
	stringifyJSON = (value) =>
	{
		if (typeof value === 'string' || value instanceof String)
			{ return value }
		else
			{ return (JSON.stringify(value)) };
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Set the local variables when the input fields are changed, ready for saving to the automations object
	// ----------------------------------------------------------------------------------------------------
	handleChange = (e, { name, value, checked }) => {
		if (value)
		{
			this.setState({ [name]: value })
		}
		else
		{
			this.setState({ [name]: checked});
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Get an array of Automation names
	// ----------------------------------------------------------------------------------------------------
	automationNames = () =>
	{
		let namebuttons = [];
		for (let i=0; i<this.state.automations.length; i++)
		{
			if (!this.state.automations[i].deleted)
			{
				namebuttons.push(
					<Button
						active={i === this.state.currentAutomation}
						index={i}
						onClick={this.setAutomation}>
							{this.state.automations[i].name}
					</Button>
				)
			}
		}
		return (namebuttons);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Create a new automation structure from data entered in the dialog box
	// ----------------------------------------------------------------------------------------------------
	newAutomation = () =>
	{
		if (this.state.automationName !== "")
		{
			let updatedautomations = this.state.automations;
			let newautomation = {
				name:this.state.automationName,
				automationid: "",
				new: true,
				description:this.state.automationDescription,
				commands:[],
				transitions:[
					{state:"init", event:"start", newstate:"idle", actions:[]}
				]
			}
			updatedautomations.push(newautomation);
			this.setState({automations:updatedautomations, automationNameModal: false, currentAutomation:updatedautomations.length-1, currentTransition:0, updateJSON: true});
			this.props.changeProgram(updatedautomations);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Edit the automations structure after data entered into the dialog box
	// ----------------------------------------------------------------------------------------------------
	editAutomation = () =>
	{
		if (this.state.automationName !== "")
		{
			let updatedautomations = this.state.automations;
			let thisautomation = updatedautomations[this.state.currentAutomation];
			thisautomation.name = this.state.automationName;
			thisautomation.description = this.state.automationDescription;
			this.setState({automations:updatedautomations, automationNameModal: false, updateJSON: true});
			this.props.changeProgram(updatedautomations);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Delete an automation from the data structure
	// ----------------------------------------------------------------------------------------------------
	deleteAutomation = () =>
	{
		let updatedautomations = this.state.automations;
		if (updatedautomations[this.state.currentAutomation].new)
		{
			updatedautomations.splice(this.state.currentAutomation, 1);
		}
		else
		{
			updatedautomations[this.state.currentAutomation].deleted = true;
		}
		this.setState({automations:updatedautomations, currentAutomation:Math.max(-1,this.state.currentAutomation-1), currentTransition:-1, updateJSON: true});
		this.props.changeProgram(updatedautomations);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Choose which automation to currently edit
	// ----------------------------------------------------------------------------------------------------
	setAutomation = (e, data) =>
	{
		this.setState({currentAutomation:data.index, currentTransition:0, updateJSON: true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Create a new Transition structure in the selected Automation
	// ----------------------------------------------------------------------------------------------------
	newTransition = () =>
	{
		if (this.state.currentAutomation >= 0)
		{
			let updatedautomations = this.state.automations;
			let thisautomation = updatedautomations[this.state.currentAutomation];
			let newtransition = {
				state:"", event:"", newstate:"", actions:[]
			}
			thisautomation.transitions.push(newtransition);
			this.setState({automations:updatedautomations});
			this.props.changeProgram(updatedautomations);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Copy the data from the currently selected transition to the UI for editing
	// ----------------------------------------------------------------------------------------------------
	editTransition = (e, data) =>
	{
		let transition = this.state.automations[this.state.currentAutomation].transitions[data.index];
		this.setState({
			currentTransition:data.index,
			state: transition.state,
			event: transition.event,
			newstate: transition.newstate,
			visible: (this.state.automations[this.state.currentAutomation].commands.indexOf(transition.event) > -1),
			editTransitionModal:true
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Delete the selected transition
	// ----------------------------------------------------------------------------------------------------
	deleteTransition = (e, data) =>
	{
		let updatedautomations = this.state.automations;
		let thisautomation = updatedautomations[this.state.currentAutomation];
		thisautomation.transitions.splice(data.index, 1);
		this.setState({automations:updatedautomations, currentTransition:this.state.currentTransition-1});
		this.props.changeProgram(updatedautomations);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Update the transition from the UI
	// ----------------------------------------------------------------------------------------------------
	saveTransition = (e) =>
	{
		// Grab the existing automations
		let updatedautomations = this.state.automations;

		// Remove the previous event from the list of actions if it was there
		let previousevent = this.state.automations[this.state.currentAutomation].transitions[this.state.currentTransition].event;
		let prevCommandPos = this.state.automations[this.state.currentAutomation].commands.indexOf(previousevent);
		if (prevCommandPos !== -1)
		{
			updatedautomations[this.state.currentAutomation].commands.splice(prevCommandPos,1);
		}

		// Update the automation based on the entered data
		updatedautomations[this.state.currentAutomation].transitions[this.state.currentTransition] = {
			state: this.state.state,
			event: this.state.event,
			newstate: this.state.newstate,
			actions: updatedautomations[this.state.currentAutomation].transitions[this.state.currentTransition].actions
		};

		// Add the event as an action if appropriate
		let commandPos = updatedautomations[this.state.currentAutomation].commands.indexOf(this.state.event);
		if (this.state.visible && (commandPos === -1))
		{
			// Add the command if not already there
			updatedautomations[this.state.currentAutomation].commands.push(this.state.event);
		}

		// Save it all back to the state
		this.setState({automations:updatedautomations, editTransitionModal:false});
		this.props.changeProgram(updatedautomations);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Choose the currently selected Action to edit
	// ----------------------------------------------------------------------------------------------------
	editActions = (e, data) =>
	{
		this.setState({currentTransition: data.index, editActionsModal:true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Create a new action on the currently selected transition
	// ----------------------------------------------------------------------------------------------------
	newAction = (e, data) =>
	{
		if ((this.state.currentAutomation >= 0) && (this.state.currentTransition >= 0))
		{
			let updatedautomations = this.state.automations;
			let newaction = {
				command:data.name, parameters:JSON.parse(JSON.stringify(Commands[data.name]))
			}
			updatedautomations[this.state.currentAutomation].transitions[this.state.currentTransition].actions.push(newaction);
			this.setState({selectActionModal:false, automations:updatedautomations});
			this.props.changeProgram(updatedautomations);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Delete the chosen action on the chosen transition and update the data structures
	// ----------------------------------------------------------------------------------------------------
	deleteAction = (e, data) =>
	{
		if ((this.state.currentAutomation >= 0) && (this.state.currentTransition >= 0))
		{
			let updatedautomations = this.state.automations;
			updatedautomations[this.state.currentAutomation].transitions[this.state.currentTransition].actions.splice(data.index, 1);
			this.setState({automations:updatedautomations});
			this.props.changeProgram(updatedautomations);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Move an action up a step in the list
	// ----------------------------------------------------------------------------------------------------
	upAction = (e, data) =>
	{
		if ((this.state.currentAutomation >= 0) && (this.state.currentTransition >= 0))
		{
			let updatedautomations = this.state.automations;
			if (data.index > 0)
			{
				let thisAction = updatedautomations[this.state.currentAutomation].transitions[this.state.currentTransition].actions[data.index];
				updatedautomations[this.state.currentAutomation].transitions[this.state.currentTransition].actions.splice(data.index, 1);
				updatedautomations[this.state.currentAutomation].transitions[this.state.currentTransition].actions.splice(data.index-1, 0, thisAction);
				this.setState({automations:updatedautomations});
				this.props.changeProgram(updatedautomations);
			}
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Move an action down a step in the list
	// ----------------------------------------------------------------------------------------------------
	downAction = (e, data) =>
	{
		if ((this.state.currentAutomation >= 0) && (this.state.currentTransition >= 0))
		{
			let updatedautomations = this.state.automations;
			if (data.index < updatedautomations[this.state.currentAutomation].transitions[this.state.currentTransition].actions.length-1)
			{
				let thisAction = updatedautomations[this.state.currentAutomation].transitions[this.state.currentTransition].actions[data.index];
				updatedautomations[this.state.currentAutomation].transitions[this.state.currentTransition].actions.splice(data.index, 1);
				updatedautomations[this.state.currentAutomation].transitions[this.state.currentTransition].actions.splice(data.index+1, 0, thisAction);
				this.setState({automations:updatedautomations});
				this.props.changeProgram(updatedautomations);
			}
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Chose an action to edit
	// ----------------------------------------------------------------------------------------------------
	editAction = (e, data) =>
	{
		if ((this.state.currentAutomation >= 0) && (this.state.currentTransition >= 0))
		{
			this.setState({selectActionModal:true});
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Save changes to actions edits
	// ----------------------------------------------------------------------------------------------------
	saveActions = (e, data) =>
	{
		this.props.changeProgram(this.state.automations);
		this.setState({editActionsModal:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Cancel actions editing and discard the changes
	// ----------------------------------------------------------------------------------------------------
	cancelAction = (e, data) =>
	{
		this.setState({selectActionModal:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	transitionsList = () =>
	{
		let transitions = [];
		if (this.state.currentAutomation >= 0)
		{
			for (let i=0; i<this.state.automations[this.state.currentAutomation].transitions.length; i++)
			{
				transitions.push(
					<Table.Row>
						<Table.Cell textAlign='center' width={3}>
							<Button.Group>
								<Button index={i} icon color="green" basic onClick={this.editTransition}><Icon name="edit"/></Button>
								<Button index={i} icon color="red" basic onClick={this.deleteTransition}><Icon name="delete"/></Button>
							</Button.Group>
						</Table.Cell>
						<Table.Cell textAlign='center' width={4}>{this.state.automations[this.state.currentAutomation].transitions[i].state}</Table.Cell>
						<Table.Cell style={{"font-weight":(this.state.automations[this.state.currentAutomation].commands.indexOf(this.state.automations[this.state.currentAutomation].transitions[i].event) !== -1)?"bold":"normal"}} textAlign='center' width={4}>{this.state.automations[this.state.currentAutomation].transitions[i].event}</Table.Cell>
						<Table.Cell textAlign='center' width={4}>{this.state.automations[this.state.currentAutomation].transitions[i].newstate}</Table.Cell>
						<Table.Cell textAlign='center' width={1}>
							<Button
								index={i}
								icon color={(this.state.automations[this.state.currentAutomation].transitions[i].actions.length > 0) ? "blue" : "default"}
								basic
								onClick={this.editActions}>
								<Icon name="edit"/>
							</Button>
						</Table.Cell>
					</Table.Row>
				)
			}
		}
		return (transitions);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	actionsList = () =>
	{
		let actions = [];
		if ((this.state.currentAutomation >= 0) && (this.state.currentTransition >= 0))
		{
			if (this.state.currentAutomation < this.state.automations.length)
			{
				if (this.state.currentTransition < this.state.automations[this.state.currentAutomation].transitions.length)
				{
					for (let i=0; i<this.state.automations[this.state.currentAutomation].transitions[this.state.currentTransition].actions.length; i++)
					{
						actions.push(
							<Table.Row verticalAlign='top'>
								<Table.Cell textAlign='center' width={1}>
									<Button index={i} icon color="red" basic onClick={this.deleteAction}><Icon name="delete"/></Button>
								</Table.Cell>
								<Table.Cell textAlign='center' width={3}>
									<Label size="large" style={{width:"100%", paddingTop:"0.85em", paddingBottom:"0.85em"}}>
										{this.state.automations[this.state.currentAutomation].transitions[this.state.currentTransition].actions[i].command}
									</Label>
								</Table.Cell>
								<Table.Cell textAlign='center' width={12}>
									{
										this.actionParameters ( this.state.automations[this.state.currentAutomation].transitions[this.state.currentTransition].actions[i], i)
									}
								</Table.Cell>
								<Table.Cell>
									<Button.Group vertical>
										{
											(i>0) ?
											 	(<Button index={i} icon basic onClick={this.upAction}><Icon name="angle up"/></Button>)
											:
												(<></>)
										}
										{
											(i<this.state.automations[this.state.currentAutomation].transitions[this.state.currentTransition].actions.length-1) ?
												(<Button index={i} icon basic onClick={this.downAction}><Icon name="angle down"/></Button>)
											:
												(<></>)
										}
									</Button.Group>
								</Table.Cell>
							</Table.Row>
						)
					}
				}
			}
		}
		return (actions);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Change the temporary values of the parameter being typed into
	// ----------------------------------------------------------------------------------------------------
	handleParameterChange = (e, data) =>
	{
		let thisAction = this.state.automations[this.state.currentAutomation].transitions[this.state.currentTransition].actions[data.actionindex];
		thisAction.parameters[data.parameter] = this.parseJSON(data.value);
		this.setState({automations:this.state.automations});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	//
	// ----------------------------------------------------------------------------------------------------
	actionEntry = (command, name, value, placeholder, index) =>
	{
		return (
			<Input fluid label={name} actionindex={index} value={value} command={command} parameter={name} placeholder={placeholder} onChange={this.handleParameterChange}>
				<Label style={{"width":"20%"}}>{name}</Label>
				<input />
			</Input>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	actionParameters = (action, index) =>
	{
		// Create the html from that
		let parameters_html = [];
		switch (action.command)
		{
			case "choose":
				break;
			case "set":
				parameters_html.push(this.actionEntry("set", "key", action.parameters["key"], "The name of the variable.", index));
				parameters_html.push(this.actionEntry("set", "value", action.parameters["value"], "The value to set, or a mathematical expression.", index));
				break;
			case "test":
				parameters_html.push(this.actionEntry("test", "test", action.parameters["test"], "An expression that evaluates to true or false.", index));
				parameters_html.push(this.actionEntry("test", "true_event", action.parameters["true_event"], "The event to send on true.", index));
				parameters_html.push(this.actionEntry("test", "false_event", action.parameters["false_event"], "The event to send on false.", index));
				parameters_html.push(this.actionEntry("test", "geobotid", action.parameters["geobotid"], "The Geobot to send the event to (empty for this Geobot).", index));
				break;
			case "send":
				parameters_html.push(this.actionEntry("send", "event", action.parameters["event"], "The event to send.", index));
				parameters_html.push(this.actionEntry("send", "url", action.parameters["url"], "Remote MRServer URL (without the 'https://').", index));
				parameters_html.push(this.actionEntry("send", "parameters", this.stringifyJSON(action.parameters["parameters"]), "Parameters in JSON format.", index));
				parameters_html.push(this.actionEntry("send", "delay", action.parameters["delay"], "How long to wait (s) before sending the event.", index));
				parameters_html.push(this.actionEntry("send", "geobotid", action.parameters["geobotid"], "The Geobot to send the event to (empty for this Geobot).", index));
				break;
			case "broadcast":
				parameters_html.push(this.actionEntry("broadcast", "event", action.parameters["event"], "Event to send.", index));
				parameters_html.push(this.actionEntry("broadcast", "parameters", this.stringifyJSON(action.parameters["parameters"]), "Parameters in JSON format.", index));
				parameters_html.push(this.actionEntry("broadcast", "delay", action.parameters["delay"], "How long to wait (s) before sending the event.", index));
				parameters_html.push(this.actionEntry("broadcast", "radius", action.parameters["radius"], "The radius around this Geobot to send the even to (meters).", index));
				parameters_html.push(this.actionEntry("broadcast", "channelid", action.parameters["channelid"], "The channel to find the Geobots on (blank for this channel).", index));
				break;
			case "email":
				parameters_html.push(this.actionEntry("email", "recipients", action.parameters["recipients"], "A comma separated list of recipients.", index));
				parameters_html.push(this.actionEntry("email", "subject", action.parameters["subject"], "The subject of the email.", index));
				parameters_html.push(this.actionEntry("email", "message", action.parameters["message"], "The message for the email.  Can include html.", index));
				break;
			case "trigger":
				parameters_html.push(this.actionEntry("trigger", "event", action.parameters["event"], "The triggering event to be sent to all devices.", index));
				parameters_html.push(this.actionEntry("trigger", "parameters", this.stringifyJSON(action.parameters["parameters"]), "Parameters in JSON format.", index));
				break;
			case "spend":
				parameters_html.push(this.actionEntry("spend", "tokens", action.parameters["tokens"], "The number of tokens to spend.", index));
				parameters_html.push(this.actionEntry("spend", "description", action.parameters["description"], "A short message to attach to the spend.", index));
				break;
			case "log":
				parameters_html.push(this.actionEntry("log", "event", action.parameters["event"], "Event name to log.", index));
				parameters_html.push(this.actionEntry("log", "parameters", this.stringifyJSON(action.parameters["parameters"]), "Parameters in JSON format to log with the event.", index));
				break;
			case "entangle":
				parameters_html.push(this.actionEntry("entangle", "url", action.parameters["url"], "Remote MRServer URL (without the 'https://')", index));
				parameters_html.push(this.actionEntry("entangle", "geobotid", action.parameters["geobotid"], "Geobot ID to entangle on Remote MRServer.", index));
				parameters_html.push(this.actionEntry("entangle", "developerid", action.parameters["developerid"], "Valid developerid for Remote MRServer.", index));
				parameters_html.push(this.actionEntry("entangle", "useremail", action.parameters["useremail"], "User login email on Remote MRServer.", index));
				parameters_html.push(this.actionEntry("entangle", "password", action.parameters["password"], "User password on Remote MRServer.", index));
				break;
			case "disentangle":
				parameters_html.push(this.actionEntry("disentangle", "url", action.parameters["url"], "Remote MRServer URL (without the 'https://')", index));
				parameters_html.push(this.actionEntry("disentangle", "geobotid", action.parameters["geobotid"], "Geobot ID to disentangle from.", index));
				parameters_html.push(this.actionEntry("disentangle", "developerid", action.parameters["developerid"], "Valid developerid for Remote MRServer.", index));
				break;
			case "load":
				parameters_html.push(this.actionEntry("load", "name", action.parameters["name"], "Metadata Key from which to load the Automation.", index));
				parameters_html.push(this.actionEntry("load", "geobotid", action.parameters["geobotid"], "Geobot ID to load Automation from (fill in either ChannelID or GeobotID, or leave both blank).", index));
				parameters_html.push(this.actionEntry("load", "channelid", action.parameters["channelid"], "Channel ID to load Automation from (if both blank, loads from this Geobot's Metadata).", index));
				parameters_html.push(this.actionEntry("load", "contextids", this.stringifyJSON(action.parameters["contextids"]), "List of ContextIDs if required for the Geobot or Channel being read from.", index));
				break;
			case "save":
				parameters_html.push(this.actionEntry("save", "name", action.parameters["name"], "Metadata Key to which the Automation will be saved.", index));
				parameters_html.push(this.actionEntry("save", "geobotid", action.parameters["geobotid"], "Geobot ID to save Automation to (fill in either ChannelID or GeobotID, or leave both blank).", index));
				parameters_html.push(this.actionEntry("save", "channelid", action.parameters["channelid"], "Channel ID to save Automation to (if both blank, saves to this Geobot's Metadata).", index));
				parameters_html.push(this.actionEntry("save", "contextids", this.stringifyJSON(action.parameters["contextids"]), "List of ContextIDs if required for the Geobot or Channel being saved to.", index));
				break;
			default:
				break;
		}
		return ( parameters_html );
	}
	// ----------------------------------------------------------------------------------------------------




	// ----------------------------------------------------------------------------------------------------
	// Modal dialog box for editing the automations name and description
	// ----------------------------------------------------------------------------------------------------
	automationNameModal = () =>
	{
		return (
			<Modal size="tiny" open={this.state.automationNameModal} centered={false}>
				<Modal.Header>
					Automation
				</Modal.Header>
				<Modal.Content scrolling>
					<Form>
						<Form.Field>
							<Form.Input placeholder="Automation Name" name="automationName" type="text" value={this.state.automationName} onChange={this.handleChange}/>
						</Form.Field>
						<Form.Field>
							<Form.TextArea placeholder="Automation Description" name="automationDescription" value={this.state.automationDescription} onChange={this.handleChange}/>
						</Form.Field>
					</Form>
				</Modal.Content>
				<Modal.Actions>
					<Button basic colour="red" onClick={this.cancelAutomationNameModal}>
						cancel
					</Button>
					<Button basic colour="green" onClick={this.saveAutomationName}>
						save
					</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Open the Automation Name dialog box for a new automation
	// ----------------------------------------------------------------------------------------------------
	openNewAutomationNameModal = () =>
	{
		this.setState({
			automationNameModal:true,
			automationName: "",
			automationDescription: "",
			editAutomation:false
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Open the Automation Name dialog box for an existing automation
	// ----------------------------------------------------------------------------------------------------
	openEditAutomationNameModal = () =>
	{
		this.setState({
			automationNameModal:true,
			automationName: this.state.automations[this.state.currentAutomation].name,
			automationDescription: this.state.automations[this.state.currentAutomation].description,
			editAutomation:true
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Cancel the naming of an automation
	// ----------------------------------------------------------------------------------------------------
	cancelAutomationNameModal = () =>
	{
		this.setState({automationNameModal:false});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Save the automation name
	// ----------------------------------------------------------------------------------------------------
	saveAutomationName = () =>
	{
		if (this.state.editAutomation)
		{
			this.editAutomation();
		}
		else {
			this.newAutomation();
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Dialog box for editing the state, event and newstate of a transition
	// ----------------------------------------------------------------------------------------------------
	transitionModal = () =>
	{
		return (
			<Modal size="tiny" open={this.state.editTransitionModal} centered={false}>
				<Modal.Header>
					Transition
				</Modal.Header>
				<Modal.Content scrolling>
					<Input fluid label="state" value={this.state.state} name="state" placeholder="Starting state" onChange={this.handleChange}/>
					<div>Any text to represent a state, or '*' to match all states, eg: 'on' or 'off'.  An automation always starts in the 'init' state.</div>
					<Divider />
					<Input fluid label="event" value={this.state.event} name="event" placeholder="Event to cause a state change" onChange={this.handleChange}/>
					<div>Any text to represent an event, eg: 'turnon' or 'turnoff'.  The first event to fire when an automation starts is the 'start' event.</div>
					<Divider />
					<Checkbox name="visible" checked={this.state.visible} onChange={this.handleChange} label="Make this event visible as a selectable command to users of the WebApp"/>
					<Divider />
					<Input fluid label="newstate" value={this.state.newstate} name="newstate" placeholder="State to change to" onChange={this.handleChange}/>
					<div>Any text to represent the state to change to, eg: 'on' or 'off'.</div>
				</Modal.Content>
				<Modal.Actions>
					<Button basic color="green" onClick={this.saveTransition}>update</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Dialog box for editing actions details
	// ----------------------------------------------------------------------------------------------------
	actionsModal = () =>
	{
		return (
			<Modal size="medium" open={this.state.editActionsModal} centered={false}>
				<Modal.Header>
					Actions
				</Modal.Header>
				<Modal.Content scrolling>
					<Table celled unstackable>
						<Table.Header>
							<Table.HeaderCell textAlign='center' width={1}>
								<Button basic color="blue" onClick={this.editAction}>add</Button>
							</Table.HeaderCell>
							<Table.HeaderCell textAlign='center' width={3}>Action</Table.HeaderCell>
							<Table.HeaderCell textAlign='center' width={10}>Parameters</Table.HeaderCell>
							<Table.HeaderCell textAlign='center' width={2}>Move</Table.HeaderCell>
						</Table.Header>
						<Table.Body>
							{this.actionsList ()}
						</Table.Body>
					</Table>
				</Modal.Content>
				<Modal.Actions>
					<Button basic color="green" onClick={this.saveActions}>update</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	editActionModal = () =>
	{
		return (
			<Modal size="small" open={this.state.selectActionModal} centered={false}>
				<Modal.Header>
					Select Action
				</Modal.Header>
				<Modal.Content scrolling>
				<Table celled unstackable>
					<Table.Header>
						<Table.HeaderCell textAlign='center' width={3}>Action</Table.HeaderCell>
						<Table.HeaderCell textAlign='center' width={14}>Description</Table.HeaderCell>
					</Table.Header>
					<Table.Body>
						{this.editActionRow("set", "Set a Variable to a given Value.")}
						{this.editActionRow("test", "Send an event depending on the results of the test.")}
						{this.editActionRow("send", "Send an event to this or another Geobot, either on this MRServer or one this Geobot is entangled with.")}
						{this.editActionRow("broadcast", "Send an event to Geobots on a Channel within a radius of this Geobot.")}
						{this.editActionRow("email", "Send an email to list of recipients.")}
						{this.editActionRow("trigger", "Trigger a synchronized event across all devices watching this Geobot. This can be used to start animations, trigger sounds, etc.")}
						{this.editActionRow("spend", "Spend the user's tokens.  Can be associated with a specific action that they should know they are paying for.")}
						{this.editActionRow("log", "Log a real-world event for later analysis.")}
						{this.editActionRow("entangle", "Entangle this Geobot with a Geobot on another MRServer.")}
						{this.editActionRow("disentangle", "Disentangle this Geobot from a Geobot on another MRServer.")}
						{this.editActionRow("load", "Load an Automation from a Metadata value on a Channel or Geobot.")}
						{this.editActionRow("save", "Save an Automation to a Metadata value on a Channel or Geobot.")}
					</Table.Body>
				</Table>
				</Modal.Content>
				<Modal.Actions>
					<Button basic color="red" onClick={this.cancelAction}>cancel</Button>
				</Modal.Actions>
			</Modal>
		)
	}
	editActionRow = (name, comment) =>
	{
		return (
			<Table.Row>
				<Table.Cell textAlign='center' width={3}>
					<Button fluid name={name} icon color="blue" basic onClick={this.newAction}>{name}</Button>
				</Table.Cell>
				<Table.Cell textAlign='left' width={14}>
					{comment}
				</Table.Cell>
			</Table.Row>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	showAutomations = () =>
	{
		return (
			<>
				<Grid>
					<Grid.Column width={8}>
						<Button.Group>
							{this.automationNames ()}
						</Button.Group>
					</Grid.Column>
					<Grid.Column width={8}>
						<Button.Group floated="right">
							<Button basic color="green" onClick={this.openEditAutomationNameModal}>edit</Button>
							<Button basic color="blue" onClick={this.openNewAutomationNameModal}>new</Button>
							<Button basic color="red" onClick={this.deleteAutomation}>delete</Button>
						</Button.Group>
					</Grid.Column>
				</Grid>
				{ this.chooseEditor(this.props.editMode) }
			</>
		)
	}

	chooseEditor = (editMode) =>
	{
		let returnValue = "";
		switch (this.props.editMode)
		{
			case 0: returnValue = (this.automationTable ()); break;
			case 1: returnValue = (this.automationJSON ()); break;
			default: returnValue = (this.automationText ()); break;
		}
		return (returnValue);
	}

	automationTable = () =>
	{
		return (
			<Table celled unstackable>
				<Table.Header>
					<Table.HeaderCell textAlign='center' width={3}>
						<Button basic color="blue" onClick={this.newTransition}>add</Button>
					</Table.HeaderCell>
					<Table.HeaderCell textAlign='center' width={4}>State</Table.HeaderCell>
					<Table.HeaderCell textAlign='center' width={4}>Event</Table.HeaderCell>
					<Table.HeaderCell textAlign='center' width={4}>NewState</Table.HeaderCell>
					<Table.HeaderCell textAlign='center' width={1}>Actions</Table.HeaderCell>
				</Table.Header>
				<Table.Body>
					{this.transitionsList ()}
				</Table.Body>
			</Table>
		)
	}

	automationJSON = () =>
	{
		if ((this.state.automations.length > 0) && (this.state.currentAutomation >= 0))
		{
			let thisAutomation = JSON.parse(JSON.stringify(this.state.automations[this.state.currentAutomation]));
			delete thisAutomation.automationid;
			delete thisAutomation.new;

			if (this.state.updateJSON)
			{
				this.setState({updateJSON: false});
				return (<></>)
			}
			else {
				return (
					<Segment>
						<JsonEditor
							value={thisAutomation}
							onChange={this.handleJSONChange}
						/>
					</Segment>
				)
			}
		}
		else
		{
			return (<></>)
		}
	}

	automationText = () =>
	{
		if ((this.state.automations.length > 0) && (this.state.currentAutomation >= 0))
		{
			let thisAutomation = JSON.parse(JSON.stringify(this.state.automations[this.state.currentAutomation]));
			delete thisAutomation.automationid;
			delete thisAutomation.new;

			if (this.state.updateJSON)
			{
				this.setState({updateJSON: false});
				return (<></>)
			}
			else {
				return (
					<Segment>
						<AceEditor
							mode="json"
							theme="github"
							onChange={this.handleTextChange}
							value={JSON.stringify(thisAutomation, null, 4)}
							width={"100%"}
					    />
					</Segment>
				)
			}
		}
		else
		{
			return (<></>)
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleJSONChange = (data) => {
		let updatedautomations = this.state.automations;
		Object.keys(data).forEach((key) => {
			updatedautomations[this.state.currentAutomation][key] = data[key];
		})
		this.setState({automations: updatedautomations});
		this.props.changeProgram(updatedautomations);
	}

	handleTextChange = (data) => {
		try {
			let updatedautomations = this.state.automations;
			let dataJSON = JSON.parse(data);
			Object.keys(dataJSON).forEach((key) => {
				updatedautomations[this.state.currentAutomation][key] = dataJSON[key];
			})
			this.setState({automations: updatedautomations});
			this.props.changeProgram(updatedautomations);
		}
		catch(err) {
			// Do nothing
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		return (
			<>
				{this.showAutomations ()}
				{this.automationNameModal ()}
				{this.transitionModal ()}
				{this.actionsModal ()}
				{this.editActionModal ()}
			</>
		);
	}
}

export default GeobotProgram;
