// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Edit the Contexts for the give entity
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Table, Button, Icon, Input, Modal, Label } from 'semantic-ui-react'
import { JsonEditor } from 'jsoneditor-react';
import 'jsoneditor-react/es/editor.min.css';


// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class ContextsEdit extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			editingcontext: false,
			editingJSONindex: 0,
			contexts: []
		};
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		this.props.imersiasdk.GET("context", this.props.entityid)
		.then ((resp) => {
			this.setState({contexts: resp.json});
			this.props.updateContexts(resp.json);
		})
		.catch ((err) => {
			// Do nothing
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentWillUnmount = () =>
	{
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	newContext = (e) =>
	{
		let currentcontexts = this.state.contexts;
		currentcontexts.push({attributes:"", contextid:"", new:true});
		this.setState({contexts: currentcontexts});
		this.props.updateContexts(currentcontexts);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	deleteContext = (e, data) =>
	{
		let currentcontexts = this.state.contexts;
		currentcontexts[data.index].deleted = true;
		this.setState({contexts: currentcontexts});
		this.props.updateContexts(currentcontexts);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Set the local contexts when the input fields are changed, ready for saving to the automations object
	// ----------------------------------------------------------------------------------------------------
	handleChange = (e, data) => {
		let currentcontexts = this.state.contexts;

		if (data.name === "attributes")
		{
			currentcontexts[data.index][data.name] = this.parseJSON(data.value);
		}
		else
		{
			currentcontexts[data.index][data.name] = data.value;
		}
		this.setState({contexts: currentcontexts});
		this.props.updateContexts(currentcontexts);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleJSONChange = (data) => {
		let currentcontexts = this.state.contexts;
		currentcontexts[this.state.editingJSONindex].attributes = data;
		this.setState({contexts: currentcontexts});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	openJsonEditor = (e, data) =>
	{
		this.setState({editingJSONindex: data.index});
		this.setState({valueToEdit: this.parseJSON(this.state.contexts[data.index].attributes), editingcontext: true});
	}
	saveJSONValue = (e) =>
	{
		let currentcontexts = this.state.contexts;
		this.setState({contexts: currentcontexts, editingcontext: false});
		this.props.updateContexts(currentcontexts);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	contextsList = () =>
	{
		let contextshtml = [];
		this.state.contexts.forEach ((item, i) => {
			if (!item.deleted)
			{
				contextshtml.push (
					<Table.Row>
						<Table.Cell textAlign='center' width={1}>
							<Button index={i} icon color="red" basic onClick={this.deleteContext}><Icon name="delete"/></Button>
						</Table.Cell>
						<Table.Cell textAlign='center' width={7}>
							<Label>{item.contextid}</Label>
						</Table.Cell>
						<Table.Cell width={10}>
							<Input fluid index={i} name="attributes" value={this.stringifyJSON(item.attributes)} placeholder="Attributes Array" label={<Button index={i} onClick={this.openJsonEditor}><Icon name="edit"/></Button>} onChange={this.handleChange} />
						</Table.Cell>
					</Table.Row>
				)
			}
		});
		return (contextshtml);
	}
	parseJSON = (value) =>
	{
		let converted = null;
		try {
			converted = JSON.parse(value);
		}
		catch (err)
		{
			converted = value;
		}
		return (converted);
	}
	stringifyJSON = (value) =>
	{
		if (typeof value === 'string' || value instanceof String)
			{ return value }
		else
		 	{ return (JSON.stringify(value)) };
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	editJSONAttributesModal = () => {
		if (this.state.editingcontext)
		{
			return (
				<Modal open={this.state.editingcontext} centered={false}>
					<Modal.Header>
						JSON Editor
					</Modal.Header>
					<Modal.Content scrolling>
						<JsonEditor
							value={this.state.contexts[this.state.editingJSONindex].attributes}
							onChange={this.handleJSONChange}
						/>
					</Modal.Content>
					<Modal.Actions>
						<Button basic positive onClick={this.saveJSONValue}>
							Update <Icon name='right chevron' />
						</Button>
					</Modal.Actions>
				</Modal>
			);
		}
		else {
			return (<></>);
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		return (
			<>
				<Table celled unstackable>
					<Table.Header>
						<Table.HeaderCell textAlign='center' width={1}>
							<Button basic color="blue" onClick={this.newContext}>add</Button>
						</Table.HeaderCell>
						<Table.HeaderCell textAlign='center' width={7}>ContextID</Table.HeaderCell>
						<Table.HeaderCell width={10}>Attributes</Table.HeaderCell>
					</Table.Header>
					<Table.Body>
						{this.contextsList ()}
					</Table.Body>
				</Table>
				{this.editJSONAttributesModal ()}
			</>
		);
	}
};
// --------------------------------------------------------------------------------------------------------

export default ContextsEdit;
