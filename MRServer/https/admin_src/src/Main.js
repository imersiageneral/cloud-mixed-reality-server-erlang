// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// The main screen after logging in
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';

import { Sidebar, Icon, Segment, Menu, Image, Button } from 'semantic-ui-react'

import UserCard from './UserCard.js';
import UserStatus from './UserStatus.js';
import DashboardFrame from './DashboardFrame.js';
import UserContentViewer from './UserContentViewer.js';
// import AdminContentViewer from './AdminContentViewer.js';
import EntityList from './EntityList.js';
import Analytics from './Analytics.js';
import {isMobile} from 'react-device-detect';

// --------------------------------------------------------------------------------------------------------
// The main class
// --------------------------------------------------------------------------------------------------------
class Main extends Component {
    // ----------------------------------------------------------------------------------------------------
    // Set up the object
    // ----------------------------------------------------------------------------------------------------
    constructor (props)
    {
        super (props);

        this.state = {
            currenttab: "dashboard",
            menuvisible: !isMobile,
            userdetails: null
        };

        this.websocket = null;

        this.styles = {
            contentStyleOpen : {"padding-left": "270px", "padding-top":"10px", "padding-right":"10px", "padding-bottom":"10px"},
            contentStyleClosed : {"padding-left": "10px", "padding-top":"10px", "padding-right":"10px", "padding-bottom":"10px"}
        }
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // What to start with when the page loads
    // ----------------------------------------------------------------------------------------------------
    componentDidMount = () =>
    {
        this.props.imersiasdk.GET("user", {useremail: this.props.imersiasdk.useremail})
        .then ((resp) => {
            this.setState({ userdetails : resp.json });
        })
        .catch((err) => {
            // Do nothing
        });
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Dashboard Tab content
    // ----------------------------------------------------------------------------------------------------
    dashboardContent = () =>
    {
        return (
            <div>
                <Menu borderless>
                    <Menu.Item>
                        <Icon link name='bars' onClick={this.toggleSidebar} />
                    </Menu.Item>
                    <Menu.Item>
                        <h3>Dashboard</h3>
                    </Menu.Item>
                </Menu>
                <DashboardFrame
                    imersiasdk={this.props.imersiasdk}
                />
            </div>
        )
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Analytics Tab content
    // ----------------------------------------------------------------------------------------------------
    analyticsContent = () =>
    {
        return (
            <div>
                <Menu borderless>
                    <Menu.Item>
                        <Icon link name='bars' onClick={this.toggleSidebar} />
                    </Menu.Item>
                    <Menu.Item>
                        <h3>Analytics</h3>
                    </Menu.Item>
                </Menu>
                <Analytics/>
            </div>
        )
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    userContentViewer = () =>
    {
        return (
            <div>
                <Menu borderless>
                    <Menu.Item>
                        <Icon link name='bars' onClick={this.toggleSidebar} />
                    </Menu.Item>
                    <Menu.Item>
                        <h3>Content</h3>
                    </Menu.Item>
                </Menu>
                <UserContentViewer
                    imersiasdk={this.props.imersiasdk}
                    doLogin={this.props.doLogin}
                />
            </div>
        )
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------
    adminContentViewer = () =>
    {
        return (
            <div>
                <Menu borderless>
                    <Menu.Item>
                        <Icon link name='bars' onClick={this.toggleSidebar} />
                    </Menu.Item>
                    <Menu.Item>
                        <h3>Content Administrator</h3>
                    </Menu.Item>
                </Menu>
                <EntityList
                    imersiasdk={this.props.imersiasdk}
                />
            </div>
        )
    }
    // ----------------------------------------------------------------------------------------------------




    // ----------------------------------------------------------------------------------------------------
    // Update the contents when the menu item / tab is selected
    // ----------------------------------------------------------------------------------------------------
    handleChange = (e, {name}) =>
    {
        var newstate = name;
        switch (name)
        {
            case "dashboard":
            break;
            case "analytics":
            break;
            case "content":
                newstate = "content_reload";
            break;
            default:
            break;
        }
        this.setState({currenttab: newstate});
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Makes sure the content tab is rendering the correct output depending on then current state
    // ----------------------------------------------------------------------------------------------------
    showCorrectContent = () =>
    {
        var returnvalue = "";
        switch (this.state.currenttab)
        {
            case "dashboard":
                returnvalue = (this.dashboardContent());
            break;
            case "analytics":
                returnvalue = (this.analyticsContent());
            break;
            case "content":
                returnvalue = (this.userContentViewer());
            break;
            case "content_reload":
                this.setState({currenttab: "content"});
            break;
            case "allcontent":
                returnvalue = (this.adminContentViewer());
            break;
            default:
                returnvalue = (this.dashboardContent());
            break;
        }
        return returnvalue;
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Toggle the sidebar menu
    // ----------------------------------------------------------------------------------------------------
    toggleSidebar = () =>
    {
        this.setState({menuvisible: !this.state.menuvisible});
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // Hide the sidebar in mobile mode, when clicked outside the sidebar
    // ----------------------------------------------------------------------------------------------------
    hideSidebar = (e) =>
    {
        if (isMobile && this.state.menuvisible) this.setState({menuvisible: false});
    }
    // ----------------------------------------------------------------------------------------------------



    // ----------------------------------------------------------------------------------------------------
    // The main render loop
    /*<Menu.Item name="analytics" onClick={this.handleChange}>
        Analytics
    </Menu.Item>*/
    // ----------------------------------------------------------------------------------------------------
    render ()
    {
        return (
            <Sidebar.Pushable as={Segment} currenttab={this.state.currenttab} style={{height:'100vh'}}>
                <Sidebar as={Menu} animation='overlay' vertical visible={this.state.menuvisible}>
                    <Menu.Item>
                        <Image fluid src='/images/logo.png' centered />
                    </Menu.Item>
                    <Menu.Item name="user">
                        <UserCard imersiasdk={this.props.imersiasdk} userdetails={this.state.userdetails} />
                    </Menu.Item>
                    <Menu.Item name="dashboard" onClick={this.handleChange}>
                        Dashboard
                    </Menu.Item>
                    <Menu.Item name="content" onClick={this.handleChange}>
                        {this.props.imersiasdk.isadmin?"Own Content":"Content"}
                    </Menu.Item>
                    {this.props.imersiasdk.isadmin
                        ?(
                            <Menu.Item name="allcontent" onClick={this.handleChange}>
                                All Content
                            </Menu.Item>
                        ):(
                            <></>
                        )
                    }
                    <Menu.Item>
                        <Segment><Button basic fluid onClick={this.props.logout}>log out</Button></Segment>
                    </Menu.Item>
                    <Menu.Item>
                        <UserStatus imersiasdk={this.props.imersiasdk}/>
                    </Menu.Item>
                </Sidebar>

                <Sidebar.Pusher dimmed={isMobile && this.state.menuvisible} onClick={this.hideSidebar}>
                    <div style={((this.state.menuvisible && !isMobile) ? this.styles.contentStyleOpen : this.styles.contentStyleClosed)}>
                        {this.showCorrectContent()}
                    </div>
                </Sidebar.Pusher>
            </Sidebar.Pushable>
        );
    }
};
// --------------------------------------------------------------------------------------------------------

export default Main;
