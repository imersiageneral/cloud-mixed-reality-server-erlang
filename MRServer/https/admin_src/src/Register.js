// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// The Register dialog box.
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { Segment, Form, Divider, Grid, Button, Modal, Image, Header, Message } from 'semantic-ui-react';

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class Register extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			open : true,
			useremail : "",
			passcode : "",
			password : "",
			password2 : "",
			surname : "",
			firstname : "",
			nickname : "",
			message : this.props.message,
			passcode_message: "Once you have entered your email address above, request a passcode to be sent to you by email using the button below, and then enter it here.",
			passwordMessage : '',
			passwordMessageColor : 'white',
			passwordMatchMessage : '',
			passwordMatchMessageColor : 'white',
			passwordQuality: 0
		};

		this.styles = {
			actionStyle : { paddingLeft: '1.5em', paddingRight: '1.5em' },
			messageStyle : {height: '38px', width: '100%', textAlign: 'center', fontSize: 'small'}
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	getPasscode = () => {
		this.setState({passcode_message:"Sending passcode."});

		this.props.imersiasdk.GET("passcodes", {"useremail":this.state.useremail})
		.then((resp) => {
			this.setState({passcode_message:"A passcode has been sent to your email address.  Please check for this and enter it below."});
		})
		.catch((err) => {
			this.setState({passcode_message:"Error sending passcode, try again with the button below, or contact the system administrator."});
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Perform a login and either close the dialog or show a message, depending on the outcome.
	// ----------------------------------------------------------------------------------------------------
	doRegister = () => {
		if (this.state.password === this.state.password2)
		{
			if (this.state.passwordQuality >= 0.5)
			{
				let headers = {
					passcode: this.state.passcode
				}

				let body = {
					useremail: this.state.useremail,
					password: this.state.password,
					details: {
						firstname : this.state.firstname,
						surname : this.state.surname,
						nickname : this.state.nickname
					}
				};

				this.props.imersiasdk.POST("user", headers, body)
				.then ((resp) => {
					this.props.doneRegistering();
				}).catch ((err) => {
					switch (err.json.error)
					{
						case "invalid":
							this.setState({message:"Invalid passcode - make sure you use the latest."});
							break;
						case "timeout":
							this.setState({message:"Passcode timed out - request a new one."});
							break;
						case "mumtries":
							this.setState({message:"Too many attempts - request a new passcode."});
							break;
						case "used":
							this.setState({message:"Passcode already used - request a new one."});
							break;
						case "database":
						case "smtp":
							this.setState({message:"MR Server error - contact your administrator."});
							break;
						default:
							this.setState({message:"Error registering - email address is probably already registered on this system."});
							break;
					}
				});
			}
			else {
				this.setState({message:"Password is not complex enough"});
			}
		}
		else {
			this.setState({message:"Passwords don't match"});
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	cancel = () => {
		this.props.doneRegistering();
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Set the local variables when the input fields are changed, ready for when the user presses Login
	// ----------------------------------------------------------------------------------------------------
	handleChange = (e, { name, value }) => {
		this.setState({ [name]: value });

		switch (name)
		{
			case "password" :
				this.checkPassword(value, this.state.password2);
				break;
			case "password2" :
				this.checkPassword(this.state.password, value);
				break;
			default:
				break;
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	checkPassword = (pwd, pwd2) => {
		let strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
		let mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
		let enoughRegex = new RegExp("(?=.{6,}).*", "g");

		if (pwd.length === 0) {
			this.setState({ passwordMessage : '', passwordMessageColor: 'white', passwordQuality: 0});
		} else if (false === enoughRegex.test(pwd)) {
			this.setState({ passwordMessage : 'too short', passwordMessageColor: 'red', passwordQuality: 0.1})
		} else if (strongRegex.test(pwd)) {
			this.setState({ passwordMessage : 'strong', passwordMessageColor: 'green', passwordQuality: 1})
		} else if (mediumRegex.test(pwd)) {
			this.setState({ passwordMessage : 'medium', passwordMessageColor: 'orange', passwordQuality: 0.5})
		} else {
			this.setState({ passwordMessage : 'weak', passwordMessageColor: 'red', passwordQuality: 0.1})
		}

		if ((pwd.length > 0) && (pwd2.length > 0))
		{
			if (pwd === pwd2)
			{
				this.setState({ passwordMatchMessage : 'matches', passwordMatchMessageColor: 'green'})
			}
			else {
				this.setState({ passwordMatchMessage : 'no match', passwordMatchMessageColor: 'red', passwordQuality: 0})
			}
		}
		else {
			if (pwd.length === 0)
			{
				this.setState({ passwordMatchMessage : '', passwordMessageColor: 'white', passwordMatchMessageColor: 'white'})
			}
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Check for the enter key
	// ----------------------------------------------------------------------------------------------------
	handleKeyPress = (e) => {
		if (e.charCode === 13) { this.doRegister(e); };
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		return (
			<Modal open={this.state.open} size='tiny' >
				<Modal.Content>
					<Image fluid size='medium' src='/images/logo.png' centered />
					<Header as='h4' textAlign='center'>
						<span>{this.state.message}</span>
					</Header>
			        <Divider />
			        <Grid columns='one'>
						<Grid.Row>
							<Grid.Column>
					            <Form>
					                <Form.Field>
					                    <Form.Input placeholder="Email Address" name="useremail" type="text" value={this.state.useremail} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
					                </Form.Field>
					                <Form.Field>
										<Grid relaxed columns='two'>
											<Grid.Row>
												<Grid.Column width={11}>
													<Form.Input placeholder="Password" name="password" type="password" value={this.state.password} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
												</Grid.Column>
												<Grid.Column width={5}>
													<Message color={this.state.passwordMessageColor} style={this.styles.messageStyle}>{this.state.passwordMessage}</Message>
												</Grid.Column>
											</Grid.Row>
										</Grid>
									</Form.Field>
									<Form.Field>
										<Grid relaxed columns='two'>
											<Grid.Row>
												<Grid.Column width={11}>
													<Form.Input placeholder="Password check" name="password2" type="password" value={this.state.password2} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
												</Grid.Column>
												<Grid.Column width={5}>
													<Message color={this.state.passwordMatchMessageColor} style={this.styles.messageStyle}>{this.state.passwordMatchMessage}</Message>
												</Grid.Column>
											</Grid.Row>
										</Grid>
					                </Form.Field>
									<Form.Field>
										<Form.Input placeholder="First name" name="firstname" type="text" value={this.state.firstname} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
									</Form.Field>
									<Form.Field>
										<Form.Input placeholder="Surname" name="surname" type="text" value={this.state.surname} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
									</Form.Field>
									<Form.Field>
										<Form.Input placeholder="Nickname" name="nickname" type="text" value={this.state.nickname} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
									</Form.Field>
									<Form.Field>
										<Segment secondary mini textAlign="center">
											{this.state.passcode_message}
										</Segment>
										<Form.Input placeholder="Passcode" name="passcode" type="text" value={this.state.passcode} onKeyPress={this.handleKeyPress} onChange={this.handleChange}/>
									</Form.Field>
					            </Form>
							</Grid.Column>
						</Grid.Row>
			        </Grid>
				</Modal.Content>
				<Modal.Actions style={this.styles.actionStyle}>
			        <Grid columns='three'>
			            <Grid.Row>
							<Grid.Column>
								<Button fluid basic color='red' onClick={this.cancel}>Cancel</Button>
							</Grid.Column>
							<Grid.Column>
								<Button fluid basic color='orange' onClick={this.getPasscode}>Get Passcode</Button>
							</Grid.Column>
			                <Grid.Column>
			                    <Button fluid basic color='green' onClick={this.doRegister}>Register</Button>
			                </Grid.Column>
			            </Grid.Row>
					</Grid>
				</Modal.Actions>
			</Modal>
		)
	}
};
// --------------------------------------------------------------------------------------------------------

export default Register;
