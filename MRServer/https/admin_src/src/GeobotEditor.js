// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draws a geobot card given the details
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import classNames from 'classnames';
import { Modal, Segment, Item, Form, Table, Input, Header, Card, Label, Image, Icon, Menu, Button } from 'semantic-ui-react';
import ImersiaSDK from './ImersiaSDK.js';
import EditJSON from './EditJSON.js';
import GeobotClasses from './patterns/GeobotClasses.json';

import Dropzone from 'react-dropzone'

import noimage from './images/noimage.png';

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class GeobotEditor extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.state = {
			editingclass: false,
			geobotdetails: JSON.parse(JSON.stringify(this.props.geobotdetails)),
			geobotclassdetails: {}
		};

		this.styles = {
			dropzone: {
				display:'flex',
				'justify-content': 'center',
				'align-items': 'center',
				height:"100px",
				'border-style': 'dashed',
				'border-color': 'grey',
				'border-width': "1px",
				'border-radius': "5px"
			}
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	changeClassDetails = (newclassdetails) =>
	{
		this.setState({geobotclassdetails:JSON.parse(JSON.stringify(newclassdetails))});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Set the local variables when the input fields are changed, ready for when the user presses Login
	// ----------------------------------------------------------------------------------------------------
	handleChange = (e, { name, value, checked}) => {
		let newGeobotDetails = this.state.geobotdetails;
		if (value)
		{
			newGeobotDetails[name] = value;
		}
		else
		{
			newGeobotDetails[name] = checked;
		}
		this.setState({ geobotdetails: newGeobotDetails });
		this.props.changeDetails(newGeobotDetails);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Upload files to the Geobot and set the imageurl
	// ----------------------------------------------------------------------------------------------------
	onDrop = (acceptedFiles, rejectedFiles) => {
		// Do something with files
		console.log(acceptedFiles);
		this.props.imersiasdk.POSTFILES({geobotid:this.state.geobotdetails.geobotid}, acceptedFiles)
		.then ((resp) => {
			let newGeobotDetails = this.state.geobotdetails;
			newGeobotDetails.imageurl = resp.json.fileurls[0];
			this.setState({ geobotdetails: newGeobotDetails });
			this.props.changeDetails(newGeobotDetails);
		})
		.catch ((err) => {
			// Do nothing
		});
    }
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// What to do to save the
	// ----------------------------------------------------------------------------------------------------
	cancelEdit = () => {
		this.props.cancelEdit();
	}

	saveEdit = () => {
		this.props.saveEdit();
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleEditClassClick = (e) =>
	{
		this.setState({editingclass:true});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	cancelEdit = () => {
		this.setState({editingclass:false});
	}

	saveEdit = () => {
		const metadatarecord = {
			key:this.state.geobotdetails.class,
			value:this.state.geobotclassdetails
		}
		this.props.imersiasdk.POST("metadata", {geobotid:this.state.geobotdetails.geobotid}, metadatarecord)
		.then ((resp) => {
			this.setState({editingclass:false});
		})
		.catch ((err) => {
			this.setState({editingclass:false});
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	editClass = () =>
	{
		var imageurl = this.state.geobotdetails.imageurl;
		return (
			<Modal open={this.state.editingclass} centered={false}>
				<Modal.Header>Edit Class
					<Button basic positive floated="right" onClick={this.saveEdit}>
						Save <Icon name='right chevron' />
					</Button>
					<Button basic negative floated="right" onClick={this.cancelEdit}>
						<Icon name='cancel' /> Cancel
					</Button>
				</Modal.Header>
				<Modal.Content>
					<EditJSON
						imersiasdk={this.props.imersiasdk}
						jsonPattern={GeobotClasses}
						parameters={{geobotid:this.state.geobotdetails.geobotid}}
						selector={this.state.geobotdetails.class}
						changeJsonDetails={this.changeClassDetails}
					/>
				</Modal.Content>
			</Modal>
		)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	createClassesDropdown = () =>
	{
		let returnArray = [];
		Object.keys(GeobotClasses).forEach((key) => {
			returnArray.push({text:key, value:key})
		})
		return (returnArray)
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		var imageurl = this.state.geobotdetails.imageurl;

		return (
			<Form>
				<Table celled fluid padded definition>
					<Table.Body>
						<Table.Row>
							<Table.Cell>Image</Table.Cell>
							<Table.Cell>
								<Item.Group divided>
									<Item>
										<Item.Image size='small' src={((imageurl==="")?noimage:imageurl)} />
										<Item.Content>
											<Dropzone onDrop={this.onDrop}>
											 {({ getRootProps, getInputProps }) => (
												<div style={this.styles.dropzone} {...getRootProps({ refKey: 'innerRef' })}>
												  <input {...getInputProps()} />
												  <p>Click or drop an image here.</p>
												</div>
											  )}
											</Dropzone>
										</Item.Content>
									</Item>
								</Item.Group>
								<Form.Input name='imageurl' value={this.state.geobotdetails.imageurl} type='text' onChange={this.handleChange}/>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell>Name</Table.Cell>
							<Table.Cell>
								<Form.Input name='name' value={this.state.geobotdetails.name} type='text' onChange={this.handleChange}/>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell>Description</Table.Cell>
							<Table.Cell>
								<Form.TextArea name='description' value={this.state.geobotdetails.description} onChange={this.handleChange}/>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell>Class</Table.Cell>
							<Table.Cell>
								<Form.Dropdown name='class' fluid value={this.state.geobotdetails.class} selection options={this.createClassesDropdown()} onChange={this.handleChange}/>
								<Button basic fluid onClick={this.handleEditClassClick}>Edit {this.state.geobotdetails.class} details</Button>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell>Hidden</Table.Cell>
							<Table.Cell>
								<Form.Checkbox name="hidden" checked={this.state.geobotdetails.hidden} onChange={this.handleChange} label='hide this geobot from the public'/>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell>GeobotID</Table.Cell>
							<Table.Cell>{this.state.geobotdetails.geobotid}</Table.Cell>
						</Table.Row>
					</Table.Body>
				</Table>
				{this.editClass()}
			</Form>
		)
	}
};
// --------------------------------------------------------------------------------------------------------

export default GeobotEditor;
