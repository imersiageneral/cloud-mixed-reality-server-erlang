// --------------------------------------------------------------------------------------------------------
// A dynamically updating channel title.
// --------------------------------------------------------------------------------------------------------
import React, { Component } from 'react';
import { } from 'semantic-ui-react'

class ChannelTitle extends Component {
    constructor (props)
    {
        super (props);
        this.state = {
            channeldetails : {}
        }
    }
    componentDidMount = () => {
        this.props.imersiasdk.GET("channels", {channelid:this.props.channelid})
        .then ((resp) => {
            this.setState({channeldetails:resp.json});
            document.addEventListener(this.props.channelid, this.processWotchaEvent);
        })
        .catch((err) => {
            // Do nothing
        });
    }
    componentWillUnmount = () => {
        document.removeEventListener(this.props.channelid, this.processWotchaEvent);
    }
    processWotchaEvent = (message) =>
    {
        if (message.detail.wotcha.hasOwnProperty("channel_set"))
        {
            this.setState({channeldetails: message.detail.wotcha.channel_set});
        }
        else if (message.detail.wotcha.hasOwnProperty("channel_rename"))
        {
            this.setState({channeldetails: message.detail.wotcha.channel_rename});
        }
    }
    render ()
    {
        return (<span>{this.state.channeldetails.name}</span>);
    }
}
// --------------------------------------------------------------------------------------------------------


export default ChannelTitle;
