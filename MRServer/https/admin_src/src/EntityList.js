// --------------------------------------------------------------------------------------------------------
// Copyright 2019 Imersia Ltd, Author Dr. Roy C. Davies (roy@imersia.com)
// --------------------------------------------------------------------------------------------------------
// Draw a lot of channel cards
// --------------------------------------------------------------------------------------------------------

import React, { Component } from 'react';
import { List, Table, Card } from 'semantic-ui-react'
import Geobot from './Geobot.js';
import GeobotCard from './GeobotCard.js';
import ChannelCard from './ChannelCard.js';
import UserCard from './UserCard.js';
import Map from './Map.js';
import Analytics from './Analytics.js';
import HeatMap from './HeatMap.js';

// --------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
class EntityList extends Component
{
	// ----------------------------------------------------------------------------------------------------
	// Set up the object
	// ----------------------------------------------------------------------------------------------------
	constructor (props)
	{
		super (props);

		this.container = React.createRef();
		let newtableheight = window.innerHeight - 85;

		this.state = {
			entities: [],

			map_style: {
					position:"absolute",
					top:"0px",
					height:"100%",
					left:"0px",
					width:"100%"
				},
				tableheight: newtableheight
		};

		this.loadEntities();
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	componentDidMount = () =>
	{
		window.addEventListener("resize", this.updateDimensions);
	}

	componentWillUnmount = () =>
	{
		window.removeEventListener("resize", this.updateDimensions);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	updateDimensions = () =>
	{
		let rect = this.container.current.parentNode.getBoundingClientRect();
		let newtableheight = window.innerHeight - 85;
		this.setState({map_style: {
				position:"absolute",
				top:rect.top,
				width:rect.width,
				left:rect.left,
				height:Math.min(newtableheight, rect.height)
			},
			tableheight: newtableheight
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// Load stuff using the admin API, and build a structure
	// ----------------------------------------------------------------------------------------------------
	loadEntities = () =>
	{
		let entities = [];
		this.props.imersiasdk.GET("admin", {command:"users", parameters:"{}"})
		.then ((resp) => {
			entities = resp.json;

			entities.forEach((user) => {
				this.props.imersiasdk.GET("admin", {command:"channels", parameters:JSON.stringify({userid:user.userid})})
				.then ((resp_ch) => {
					user.channels = resp_ch.json;

					user.channels.forEach((channel) => {
						this.props.imersiasdk.GET("admin", {command:"geobots", parameters:JSON.stringify({userid:user.userid, channelid:channel.channelid})})
						.then ((resp_gb) => {
							channel.geobots = resp_gb.json;

							channel.geobots.forEach((geobot) => {
								geobot.channel = () => channel;
								this.props.imersiasdk.GET("admin", {command:"automations", parameters:JSON.stringify({geobotid:geobot.geobotid})})
								.then((resp_auto) => {
									geobot.automations = resp_auto.json;
								})
								.catch((err) => {

								})
							});

							this.setState({entities: entities});
						})
						.catch((err) => {
							this.setState({entities: entities});
						});
						this.setState({entities: entities});
					})
				})
				.catch((err) => {
					this.setState({entities: entities});
				})
			})
		})
		.catch((err) => {
			this.setState({entities: entities});
		});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// 					<Checkbox attached="right" onChange={this.handleItemCheck}/>

	// ----------------------------------------------------------------------------------------------------
	processedEntities = () =>
	{
		let list = this.state.entities.map (user =>
			<List.Item>
				<List.Icon name='user'  color="grey"/>
				<List.Icon name={user.hidden?'delete':'check'} color="green" onClick={(e) => this.handleItemCheck(e, {itemtype:"user", item:user})}/>
				<List.Content>
					<List.Header color="teal" as="a" onClick={(e) => this.handleItem(e, {itemtype:"user", item:user})}>
						{user.details.firstname + " " + user.details.surname}
					</List.Header>
					{this.processedChannels(user.channels)}
				</List.Content>
			</List.Item>
		);

		return (<List>{list}</List>);
	}
	// ----------------------------------------------------------------------------------------------------
	processedChannels = (channels) =>
	{
		if (channels)
		{
			let list = channels.map (channel =>
				<List.Item>
					<List.Icon name='list'  color="grey"/>
					<List.Icon name={channel.hidden?'delete':'check'} color="green" onClick={(e) => this.handleItemCheck(e, {itemtype:"channel", item:channel})}/>
					<List.Icon name='question circle outline' color="olive" onClick={(e) => this.handleItem(e, {itemtype:"editchannel", item:channel})}/>
					<List.Content>
						<List.Header color="teal" as="a" onClick={(e) => this.handleItem(e, {itemtype:"channel", item:channel})}>
							{channel.name}
						</List.Header>
						{this.processedGeobots(channel.geobots)}
					</List.Content>
				</List.Item>
			);
			return (<List.List>{list}</List.List>);
		}
		else {
			return (<></>)
		}
	}
	// ----------------------------------------------------------------------------------------------------
	processedGeobots = (geobots) =>
	{
		if (geobots)
		{
			let list = geobots.map (geobot =>
				<List.Item>
					<List.Icon name='map pin' color="grey"/>
					<List.Icon name={geobot.hidden?'delete':'check'} color="green" onClick={(e) => this.handleItemCheck(e, {itemtype:"geobot", item:geobot})}/>
					<List.Icon name='question circle outline' color="olive" onClick={(e) => this.handleItem(e, {itemtype:"editgeobot", item:geobot})}/>
					<List.Icon name='line graph' color="teal" onClick={(e) => this.handleItem(e, {itemtype:"analytics", item:geobot})}/>
					<List.Icon name='map outline' color="teal" onClick={(e) => this.handleItem(e, {itemtype:"mapanalytics", item:geobot})}/>
					<List.Content>
						<List.Header as="a" onClick={(e) => this.handleItem(e, {itemtype:"geobot", item:geobot})}>
							{geobot.name}
						</List.Header>
					</List.Content>
				</List.Item>
			);
			return (<List.List>{list}</List.List>);
		}
		else {
			return (<></>)
		}
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleItem = (e, data) =>
	{
		this.setState({nextItem: data.itemtype, itemToShow:"update", item:data.item});
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	handleItemCheck = (e, data) =>
	{
		let newentities = this.state.entities;
		data.item.hidden = !data.item.hidden;
		if (data.item.geobots) this.checkgeobots (data.item, data.item.hidden);
		if (data.item.channels) this.checkchannels (data.item, data.item.hidden);
		this.setState({entities: newentities});
	}

	checkgeobots = (channel, hidden) =>
	{
		channel.geobots.forEach((geobot) =>
		{
			geobot.hidden = hidden;
		})
	}
	checkchannels = (user, hidden) =>
	{
		user.channels.forEach((channel) =>
		{
			channel.hidden = hidden;
			this.checkgeobots(channel, hidden);
		})
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	donothing = () =>
	{
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	contentBeingShown = () =>
	{
		let result = "";
		switch (this.state.itemToShow)
		{
			case "update":
				if (this.state.nextItem === "channel")
				{
					let rect = this.container.current.parentNode.getBoundingClientRect();
					let newtableheight = window.innerHeight - 85;
					this.setState({map_style: {
							position:"absolute",
							top:rect.top,
							width:rect.width,
							left:rect.left,
							height:Math.min(newtableheight, rect.height)
						},
						tableheight: newtableheight
					});
				};
				result = (<></>);
				this.setState({itemToShow:this.state.nextItem, nextItem:""});
				break;
			case "user":
				result = (
					<Card.Group centered itemsPerRow={3} stackable doubling>
						<UserCard imersiasdk={this.props.imersiasdk} userdetails={this.state.item} />
					</Card.Group>
				)
			break;
			case "channel":
				result = (
					<div style={this.state.map_style}>
						<Map
							imersiasdk={this.props.imersiasdk}
							channelid={this.state.item.channelid}
							geobots={this.state.item.geobots}
							mapcenter={this.donothing}
						/>
					</div>
				)
			break;
			case "geobot":
				result = (
					<Card.Group centered itemsPerRow={3} stackable doubling>
						<Geobot
							imersiasdk={this.props.imersiasdk}
							geobotdetails={this.state.item}
							automations={this.state.item.automations}
							viewing={true}
						/>
					</Card.Group>
				)
			break;
			case "analytics":
				result = (
					<Analytics
						imersiasdk={this.props.imersiasdk}
						entityid={{geobotid:this.state.item.geobotid}}
						startdate=""
						enddate=""
						height="80"
					/>
				)
			break;
			case "mapanalytics":
				result = (
					<HeatMap
						imersiasdk={this.props.imersiasdk}
						entityid={{geobotid:this.state.item.geobotid}}
						startdate=""
						enddate=""
						height="80"
					/>
				)
			break;
			case "editgeobot":
				result = (
					<Card.Group centered itemsPerRow={3} stackable doubling>
						<GeobotCard
							imersiasdk={this.props.imersiasdk}
							currentchannel={this.state.item.channel()}
							geobotdetails={this.state.item}
							openGeobot={this.donothing}
							closeGeobot={this.donothing}
						/>
					</Card.Group>
				)
			break;
			case "editchannel":
				result = (
					<Card.Group centered itemsPerRow={3} stackable doubling>
						<ChannelCard
							imersiasdk={this.props.imersiasdk}
							channeldetails={this.state.item}
							openChannel={this.donothing}
							closeChannel={this.donothing}
						/>
					</Card.Group>
				)
			break;
			default:
			break;
		}
		return (result);
	}
	// ----------------------------------------------------------------------------------------------------



	// ----------------------------------------------------------------------------------------------------
	// The render loop
	// ----------------------------------------------------------------------------------------------------
	render ()
	{
		return (
			<Table celled style={{height:this.state.tableheight}}>
				<Table.Body>
					<Table.Row>
						<Table.Cell width={4} verticalAlign="top">
							{this.processedEntities()}
						</Table.Cell>
						<Table.Cell width={12} verticalAlign="top">
							<div ref={this.container}>
								{this.contentBeingShown()}
							</div>
						</Table.Cell>
					</Table.Row>
				</Table.Body>
			</Table>
		)
	}
};
// --------------------------------------------------------------------------------------------------------

export default EntityList;
