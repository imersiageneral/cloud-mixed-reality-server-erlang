// --------------------------------------------------------------------------------------------------------
// A dynamically updating geobot title.
// --------------------------------------------------------------------------------------------------------
import React, { Component } from 'react';
import {  } from 'semantic-ui-react'

class GeobotTitle extends Component {
    constructor (props)
    {
        super (props);
        this.state = {
            geobotdetails : {}
        }
    }
    componentDidMount = () => {
        this.props.imersiasdk.GET("geobots", {geobotid: this.props.geobotid})
        .then ((resp) => {
            this.setState({geobotdetails:resp.json});
            document.addEventListener(this.props.geobotid, this.processWotchaEvent);
        })
        .catch ((err) => {
            //Do nothing
        })
    }
    componentWillUnmount = () => {
        document.removeEventListener(this.props.geobotid, this.processWotchaEvent);
    }
    processWotchaEvent = (message) =>
    {
        if (message.detail.wotcha.hasOwnProperty("geobot_set"))
        {
            this.setState({geobotdetails: message.detail.wotcha.geobot_set});
        }
    }
    render ()
    {
        return (<span>{this.state.geobotdetails.name}</span>);
    }
}
// --------------------------------------------------------------------------------------------------------


export default GeobotTitle;
